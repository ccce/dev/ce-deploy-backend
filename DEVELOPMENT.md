# Development Guide

## Prerequisites

- Java JDK 17
- Maven 3.8+ (or use included wrapper)
- Docker and Docker Compose
- PostgreSQL 15 (if running locally)

## Database

### Seeded Database
If you want to start a database with a binary database dump file (e.g. downloaded from production),
you can rename and move that file to `docker/db-seed/init.dump` and run:
```bash
docker compose -f compose.seeded.yml up -d
```

(So if you want to do this with hot reload, you would modify the command to only run the db service.)

If you want to get rid of the data from the database dump, you can run:
```
docker compose down --volumes
```

### Sharing Data
To share a database with someone else, you can use the following command:
```bash
pg_dump -U $CCCE_DB_USERNAME -h localhost -d $CCCE_DB_NAME > db.sql
```

This can then be used to populate an empty database by running:
```bash
pg_restore -U $CCCE_DB_USERNAME -h localhost -d $CCCE_DB_NAME db.sql
```

You can use [Nextcloud](https://nextcloud.esss.lu.se) to share the file.

### Management
```bash
docker compose logs -f db
psql -h localhost -U $CCCE_DB_USERNAME -d $CCCE_DB_NAME
docker compose down -v && docker compose up -d --build
```

## Docker Builds

```bash
docker build .
```
Builds from source using Maven with BuildKit caching.

## Local Development

### Running the Application

1. With Docker Compose (full stack):
```bash
docker compose up -d
```

2. With Spring Boot (hot-reload):
```bash
docker compose up -d db
./mvnw spring-boot:run
```

### Building and Testing
```bash
./mvnw package
./mvnw test
```

### Tools
- Spring Boot
- PostgreSQL
- Tomcat
- Pre-commit hooks
- Checkstyle
- SonarQube
