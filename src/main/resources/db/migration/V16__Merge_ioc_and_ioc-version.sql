/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

ALTER TABLE ioc ADD naming_name text NULL;
ALTER TABLE ioc ADD naming_uuid text NULL;
ALTER TABLE ioc ADD git_project_id int8 NULL;
ALTER TABLE ioc ADD CONSTRAINT ioc_project_id_un UNIQUE (git_project_id);
UPDATE ioc SET naming_name =
                          (SELECT ioc_version.naming_name FROM ioc_version where ioc_version.ioc = ioc.id);
UPDATE ioc SET naming_uuid =
                   (SELECT ioc_version.naming_uuid FROM ioc_version where ioc_version.ioc = ioc.id);
UPDATE ioc SET git_project_id =
                   (SELECT ioc_version.git_project_id FROM ioc_version where ioc_version.ioc = ioc.id);
ALTER TABLE ioc ALTER COLUMN git_project_id SET NOT NULL;
ALTER TABLE deployment DROP CONSTRAINT deployment_ioc_fk;
ALTER TABLE deployment RENAME COLUMN ioc TO ioc_version;
ALTER TABLE deployment ADD CONSTRAINT deployment_ioc_version_fk FOREIGN KEY (ioc_version) REFERENCES ioc(id);
DROP TABLE ioc_version;
ALTER TABLE ioc RENAME TO ioc_version;
