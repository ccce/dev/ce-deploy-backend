/*
 * Copyright (C) 2023 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

ALTER TABLE ioc_version RENAME TO ioc;
ALTER TABLE ioc RENAME COLUMN active_deployment TO latest_successful_deployment;

ALTER TABLE deployment RENAME TO ioc_deployment;
ALTER TABLE ioc_deployment RENAME COLUMN is_intended TO pending;
ALTER TABLE ioc_deployment RENAME COLUMN ioc_version TO ioc;

ALTER TABLE awx_job RENAME COLUMN started_at TO awx_job_started_at;
ALTER TABLE awx_job RENAME COLUMN finished_at TO awx_job_finished_at;
