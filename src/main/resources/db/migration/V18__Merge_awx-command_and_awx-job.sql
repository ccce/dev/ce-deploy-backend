/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

ALTER TABLE awx_job ADD job_type text NULL;
UPDATE awx_job SET job_type = 'DEPLOY' WHERE id IN (
                   SELECT deployment.job_id FROM deployment where deployment.is_undeploy = FALSE);
UPDATE awx_job SET job_type = 'UNDEPLOY' WHERE id IN (
    SELECT deployment.job_id FROM deployment where deployment.is_undeploy = TRUE);
DELETE FROM awx_job WHERE id NOT IN (SELECT deployment.job_id FROM deployment where deployment.job_id is not null);
ALTER TABLE awx_job ALTER COLUMN job_type SET NOT NULL;

ALTER TABLE awx_job ADD deployment int8 NULL;
ALTER TABLE awx_job ADD CONSTRAINT awx_job_deployment_fk FOREIGN KEY (deployment) REFERENCES deployment(id);
UPDATE awx_job SET deployment = (SELECT deployment.id FROM deployment where deployment.job_id = awx_job.id);
ALTER TABLE awx_job ALTER COLUMN deployment SET NOT NULL;

ALTER TABLE awx_job ADD created_at timestamp(3) NULL DEFAULT now();
UPDATE awx_job SET created_at = (SELECT deployment.created_at FROM deployment where deployment.job_id = awx_job.id);
ALTER TABLE awx_job ALTER COLUMN created_at SET NOT NULL;

ALTER TABLE awx_job ADD created_by text NULL;
UPDATE awx_job SET created_by = (SELECT deployment.created_by FROM deployment where deployment.job_id = awx_job.id);

ALTER TABLE awx_job RENAME COLUMN start_at TO started_at;

INSERT INTO awx_job (
                     job_type,
                     status,
                     created_by,
                     created_at,
                     started_at,
                     finished_at,
                     deployment,
                     awx_job_id,
                     webhook_update_at
                     )
SELECT
    command_type,
    awx_status,
    created_by,
    created_at,
    started_at,
    finished_at,
    deployment,
    awx_command_id,
    webhook_update_at
FROM awx_command;
DROP TABLE awx_command;

ALTER TABLE ioc_version ADD active_deployment int8 NULL;
ALTER TABLE ioc_version ADD CONSTRAINT ioc_version_active_deployment_fk
    FOREIGN KEY (active_deployment) REFERENCES deployment(id);
UPDATE ioc_version SET active_deployment =
    (SELECT deployment.id FROM deployment
                          where deployment.ioc_version = ioc_version.id AND deployment.is_active = TRUE);
ALTER TABLE deployment DROP COLUMN is_active;
