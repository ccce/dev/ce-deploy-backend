CREATE SCHEMA IF NOT EXISTS public AUTHORIZATION ccceuser;
SET search_path = public;

CREATE TABLE IF NOT EXISTS awx_job (
                         id bigserial NOT NULL,
                         awx_job_id int8 NOT NULL,
                         start_at timestamp NULL,
                         finished_at timestamp NULL,
                         status varchar(32) NULL,
                         awx_job_url text NOT NULL,
                         CONSTRAINT awx_job_pk PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS deployment_statistics (
                                       id bigserial NOT NULL,
                                       network varchar NOT NULL,
                                       ioc_count int8 NOT NULL,
                                       created_at date NOT NULL DEFAULT now(),
                                       CONSTRAINT deployment_statitics_pk PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS ioc (
                     id bigserial NOT NULL,
                     description varchar(128) NULL,
                     created_by varchar(32) NOT NULL,
                     created_at timestamp NULL DEFAULT now(),
                     external_name_id int8 NULL,
                     name_changed_in_ccdb bool NOT NULL DEFAULT false,
                     name_changed_in_naming bool NOT NULL DEFAULT false,
                     owner varchar(32) NULL,
                     CONSTRAINT ioc_pk PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS ioc_version (
                             id bigserial NOT NULL,
                             created_at timestamp NOT NULL DEFAULT now(),
                             created_by varchar(32) NOT NULL,
                             source_url text NOT NULL,
                             source_version varchar(64) NULL,
                             ioc int8 NOT NULL,
                             version int8 NULL,
                             naming_name varchar NULL,
                             naming_uuid varchar NULL,
                             CONSTRAINT ioc_version_pk PRIMARY KEY (id),
                             CONSTRAINT ioc_version_fk FOREIGN KEY (ioc) REFERENCES ioc(id)
);
CREATE INDEX IF NOT EXISTS ioc_version_created_at_idx ON ioc_version (created_at);

CREATE TABLE IF NOT EXISTS deployment (
                            id bigserial NOT NULL,
                            ioc_version int8 NOT NULL,
                            host_external_id int8 NOT NULL,
                            deployment_state varchar(16) NOT NULL,
                            description varchar(128) NULL,
                            created_by varchar(32) NULL,
                            created_at timestamp NOT NULL DEFAULT now(),
                            start_time timestamp NULL,
                            is_intended bool NULL,
                            is_active bool NULL,
                            finished_at timestamp(0) NULL,
                            task_id uuid NULL,
                            is_undeploy bool NULL DEFAULT false,
                            source_url text NULL,
                            source_version varchar(64) NULL,
                            naming_name varchar NULL,
                            CONSTRAINT deployment_pk PRIMARY KEY (id),
                            CONSTRAINT deployment_fk FOREIGN KEY (ioc_version) REFERENCES ioc_version(id)
);

CREATE TABLE IF NOT EXISTS deployment_job_mapping (
                                        id bigserial NOT NULL,
                                        deployment int8 NOT NULL,
                                        job int8 NOT NULL,
                                        CONSTRAINT deployment_job_mapping_pk PRIMARY KEY (id),
                                        CONSTRAINT deployment_job_mapping_fk FOREIGN KEY (job) REFERENCES awx_job(id),
                                        CONSTRAINT deployment_job_mapping_fk_1 FOREIGN KEY (deployment) REFERENCES deployment(id)
);
