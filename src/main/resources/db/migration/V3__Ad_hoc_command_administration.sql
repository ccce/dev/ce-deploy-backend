CREATE TABLE IF NOT EXISTS awx_command (
                             id bigserial NOT NULL,
                             command_type varchar(32) NOT NULL,
                             awx_status varchar(32) NOT NULL,
                             created_by varchar(32) NULL,
                             created_at timestamp NOT NULL DEFAULT now(),
                             started_at timestamp NULL,
                             finished_at timestamp(0) NULL,
                             ioc int8 NOT NULL,
                             deployment int8 NOT NULL,
                             awx_command_id int8 NOT NULL,
                             CONSTRAINT awx_command_pk PRIMARY KEY (id),
                             CONSTRAINT awx_command_deployment_fk FOREIGN KEY (deployment) REFERENCES deployment(id),
                             CONSTRAINT awx_command_ioc_fk FOREIGN KEY (ioc) REFERENCES ioc(id)
);
