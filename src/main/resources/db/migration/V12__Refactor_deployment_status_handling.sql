/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

ALTER TABLE deployment DROP COLUMN start_time;
ALTER TABLE deployment DROP COLUMN finished_at;
ALTER TABLE awx_job DROP COLUMN awx_job_url;
ALTER TABLE awx_job ADD webhook_update_at timestamp(3) NULL;
ALTER TABLE awx_command ADD webhook_update_at timestamp(3) NULL;
UPDATE deployment SET deployment_state = 'JOB' WHERE (deployment_state = 'FAILED' AND job_id IS NOT NULL)
                                                  OR deployment_state = 'SUCCESSFUL' OR deployment_state = 'RUNNING'
                                                  OR deployment_state = 'PENDING';
UPDATE deployment SET deployment_state = 'ERROR' WHERE deployment_state = 'FAILED' AND job_id IS NULL;
