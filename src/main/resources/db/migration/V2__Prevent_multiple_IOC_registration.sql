ALTER TABLE ioc ADD CONSTRAINT ioc_un UNIQUE (external_name_id);
ALTER TABLE ioc_version ALTER COLUMN source_url TYPE varchar;
ALTER TABLE ioc_version ADD CONSTRAINT ioc_version_un UNIQUE (source_url);
