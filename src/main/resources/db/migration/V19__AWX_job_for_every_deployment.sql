/*
 * Copyright (C) 2023 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

ALTER TABLE awx_job ALTER COLUMN awx_job_id DROP NOT NULL;
ALTER TABLE awx_job ADD error_details text NULL;
INSERT INTO awx_job (status, job_type, deployment, created_at, created_by)
    SELECT 'ERROR', 'DEPLOY', id, created_at, created_by FROM deployment WHERE deployment.job_id IS NULL AND deployment.is_undeploy = FALSE;
INSERT INTO awx_job (status, job_type, deployment, created_at, created_by)
    SELECT 'ERROR', 'UNDEPLOY', id, created_at, created_by FROM deployment WHERE deployment.job_id IS NULL AND deployment.is_undeploy = TRUE;
UPDATE deployment SET job_id = (SELECT awx_job.id FROM awx_job WHERE awx_job.deployment = deployment.id) WHERE deployment.job_id IS NULL;
ALTER TABLE deployment DROP COLUMN deployment_state;
