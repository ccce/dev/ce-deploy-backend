/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

ALTER TABLE deployment ADD ioc int8 NULL;
UPDATE deployment SET ioc =
                (SELECT ioc_version.ioc FROM ioc_version where ioc_version.id = deployment.ioc_version);
ALTER TABLE deployment ALTER COLUMN ioc SET NOT NULL;
ALTER TABLE deployment ADD CONSTRAINT deployment_ioc_fk FOREIGN KEY (ioc) REFERENCES ioc(id);
ALTER TABLE deployment DROP CONSTRAINT deployment_fk;
ALTER TABLE deployment DROP COLUMN ioc_version;
ALTER TABLE deployment DROP COLUMN task_id;
ALTER TABLE awx_command DROP CONSTRAINT awx_command_ioc_fk;
ALTER TABLE awx_command DROP COLUMN ioc;
