/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

ALTER TABLE ioc ALTER COLUMN description TYPE text;
ALTER TABLE ioc ALTER COLUMN created_by TYPE text;
ALTER TABLE ioc ALTER COLUMN owner TYPE text;

ALTER TABLE ioc_version ALTER COLUMN created_by TYPE text;
ALTER TABLE ioc_version ALTER COLUMN source_version TYPE text;
ALTER TABLE ioc_version ALTER COLUMN naming_name TYPE text;
ALTER TABLE ioc_version ALTER COLUMN naming_uuid TYPE text;

ALTER TABLE deployment_statistics ALTER COLUMN network TYPE text;

ALTER TABLE awx_job ALTER COLUMN status TYPE text;

ALTER TABLE deployment ALTER COLUMN deployment_state TYPE text;
ALTER TABLE deployment ALTER COLUMN description TYPE text;
ALTER TABLE deployment ALTER COLUMN created_by TYPE text;
ALTER TABLE deployment ALTER COLUMN source_version TYPE text;
ALTER TABLE deployment ALTER COLUMN naming_name TYPE text;

ALTER TABLE awx_command ALTER COLUMN command_type TYPE text;
ALTER TABLE awx_command ALTER COLUMN awx_status TYPE text;
ALTER TABLE awx_command ALTER COLUMN created_by TYPE text;
