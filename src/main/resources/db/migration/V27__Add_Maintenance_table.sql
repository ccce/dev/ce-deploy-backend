CREATE TABLE IF NOT EXISTS maintenance_mode
(
    id       boolean   NOT NULL DEFAULT true UNIQUE,
    start_at timestamp NOT NULL,
    end_at   timestamp NULL,
    message  text      NOT NULL,
    CHECK ( id ),
    CHECK ( end_at is NULL OR start_at < end_at )
);
