/*
 * Copyright (C) 2024 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

ALTER TABLE ioc_deployment ADD COLUMN deployment_type text;
ALTER TABLE awx_job ALTER COLUMN deployment DROP NOT NULL;

UPDATE ioc_deployment SET deployment_type = 'DEPLOY'
WHERE is_undeploy = false;

UPDATE ioc_deployment SET deployment_type = 'UNDEPLOY'
WHERE is_undeploy = true;

ALTER TABLE ioc_deployment
    ALTER COLUMN deployment_type SET NOT NULL;

ALTER TABLE ioc_deployment
    DROP COLUMN is_undeploy;
