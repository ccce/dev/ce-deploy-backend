/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

ALTER TABLE ioc ALTER COLUMN created_at TYPE timestamp(3);
ALTER TABLE ioc_version ALTER COLUMN created_at TYPE timestamp(3);
ALTER TABLE awx_job ALTER COLUMN start_at TYPE timestamp(3);
ALTER TABLE awx_job ALTER COLUMN finished_at TYPE timestamp(3);
ALTER TABLE deployment ALTER COLUMN created_at TYPE timestamp(3);
ALTER TABLE deployment ALTER COLUMN start_time TYPE timestamp(3);
ALTER TABLE deployment ALTER COLUMN finished_at TYPE timestamp(3);
ALTER TABLE awx_command ALTER COLUMN created_at TYPE timestamp(3);
ALTER TABLE awx_command ALTER COLUMN started_at TYPE timestamp(3);
ALTER TABLE awx_command ALTER COLUMN finished_at TYPE timestamp(3);
