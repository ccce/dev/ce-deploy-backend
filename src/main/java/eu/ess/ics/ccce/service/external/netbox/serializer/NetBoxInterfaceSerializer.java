package eu.ess.ics.ccce.service.external.netbox.serializer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import eu.ess.ics.ccce.service.external.netbox.model.*;
import java.io.IOException;

/**
 * @author <a href="mailto:domonkos.gulyas@ess.eu">Domonkos Gulyas</a>
 */
public class NetBoxInterfaceSerializer extends StdDeserializer<NetBoxInterface> {
  public NetBoxInterfaceSerializer() {
    super(NetBoxInterface.class);
  }

  @Override
  public NetBoxInterface deserialize(JsonParser jsonParser, DeserializationContext co)
      throws IOException {
    JsonNode node = jsonParser.getCodec().readTree(jsonParser);

    if (node.has("device")) {
      return jsonParser.getCodec().treeToValue(node, NetBoxDeviceInterface.class);
    } else if (node.has("virtual_machine")) {
      return jsonParser.getCodec().treeToValue(node, NetBoxVMInterface.class);
    }

    throw new IllegalArgumentException("Cannot determine subclass type");
  }
}
