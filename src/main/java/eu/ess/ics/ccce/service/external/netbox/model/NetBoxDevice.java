/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.service.external.netbox.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import java.util.List;
import java.util.Map;

/**
 * @author <a href="mailto:domonkos.gulyas@ess.eu">Domonkos Gulyas</a>
 */
public class NetBoxDevice extends NetBoxHost {
  @SerializedName("device_type")
  private final NetBoxTypePreview type;

  @SerializedName("device_role")
  private final NetBoxRolePreview deviceRole;

  @JsonCreator
  public NetBoxDevice(
      @JsonProperty("id") Long id,
      @JsonProperty("display") String display,
      @JsonProperty("name") String name,
      @JsonProperty("description") String description,
      @JsonProperty("created") String created,
      @JsonProperty("last_updated") String lastUpdated,
      @JsonProperty("tags") List<NetBoxPropertyPreview> tags,
      @JsonProperty("primary_ip") NetBoxIpAddressPreview primaryIp,
      @JsonProperty("custom_fields") Map<String, String> customFields,
      @JsonProperty("device_type") NetBoxTypePreview type,
      @JsonProperty("device_role") NetBoxRolePreview deviceRole) {
    super(id, display, name, description, created, lastUpdated, tags, primaryIp, customFields);
    this.type = type;
    this.deviceRole = deviceRole;
  }

  @Override
  public String getType() {
    return type.getSlug();
  }

  @Override
  public boolean isIoc() {
    return deviceRole.isIoc();
  }

  public NetBoxRolePreview getDeviceRole() {
    return deviceRole;
  }
}
