/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.service.internal.deployment;

import static eu.ess.ics.ccce.common.Constants.BATCH_JOB_TYPES;
import static eu.ess.ics.ccce.common.Constants.UNDEPLOYMENT_TYPES;
import static eu.ess.ics.ccce.common.Utils.normalizeDate;
import static eu.ess.ics.ccce.common.Utils.validateAndApplyTimeZone;

import com.google.common.collect.ImmutableList;
import com.google.gson.Gson;
import eu.ess.ics.ccce.common.Constants;
import eu.ess.ics.ccce.common.Utils;
import eu.ess.ics.ccce.common.alert.DeploymentAlertGenerator;
import eu.ess.ics.ccce.common.conversion.PagingUtil;
import eu.ess.ics.ccce.exceptions.*;
import eu.ess.ics.ccce.repository.api.IIocDeploymentRepository;
import eu.ess.ics.ccce.repository.api.IIocRepository;
import eu.ess.ics.ccce.repository.entity.AwxJobEntity;
import eu.ess.ics.ccce.repository.entity.IocDeploymentEntity;
import eu.ess.ics.ccce.repository.entity.IocEntity;
import eu.ess.ics.ccce.rest.model.BatchType;
import eu.ess.ics.ccce.rest.model.awx.request.AwxJobMeta;
import eu.ess.ics.ccce.rest.model.host.response.HostInfoWithId;
import eu.ess.ics.ccce.rest.model.ioc.request.IocDeploymentStatus;
import eu.ess.ics.ccce.rest.model.ioc.request.IocOrder;
import eu.ess.ics.ccce.rest.model.job.request.JobKind;
import eu.ess.ics.ccce.rest.model.job.request.JobOrder;
import eu.ess.ics.ccce.rest.model.job.request.JobStatusQuery;
import eu.ess.ics.ccce.rest.model.job.response.*;
import eu.ess.ics.ccce.service.external.awx.AwxService;
import eu.ess.ics.ccce.service.external.awx.model.Credential;
import eu.ess.ics.ccce.service.external.awx.model.JobStatus;
import eu.ess.ics.ccce.service.external.awx.model.response.Command;
import eu.ess.ics.ccce.service.external.awx.model.response.CommandDetails;
import eu.ess.ics.ccce.service.external.awx.model.response.JobTemplate;
import eu.ess.ics.ccce.service.external.gitlab.GitLabService;
import eu.ess.ics.ccce.service.external.naming.NamingService;
import eu.ess.ics.ccce.service.external.netbox.model.NetBoxHostIdentifier;
import eu.ess.ics.ccce.service.internal.PagingLimitDto;
import eu.ess.ics.ccce.service.internal.deployment.dto.AwxJobType;
import eu.ess.ics.ccce.service.internal.deployment.dto.DeploymentTask;
import eu.ess.ics.ccce.service.internal.deployment.dto.DeploymentType;
import eu.ess.ics.ccce.service.internal.host.HostService;
import eu.ess.ics.ccce.service.internal.ioc.IocUtil;
import eu.ess.ics.ccce.service.internal.security.AuthorizationService;
import eu.ess.ics.ccce.service.internal.security.dto.UserDetails;
import jakarta.persistence.NoResultException;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.gitlab.api.models.GitlabProject;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 */
@Service
public class DeploymentService {

  private static final String SET_STATE_STARTED = "state=started";
  private static final String SET_SERVICE_ENABLED = "enabled=true";
  private static final String SET_STATE_STOPPED = "state=stopped";
  private static final String SET_SERVICE_DISABLED = "enabled=false";

  @Value("${server.time-zone}")
  private String serverTimeZone;

  /**
   * Finds the active deployment for an IOC by iocId, and marks it as undeployed in DB (Sets the
   * active, and intended fields to false)
   *
   * @param iocId the Id of the ioc that has to be marked as undeployed in the DB
   */
  public void undeployInDb(Long iocId) {

    try {
      IocEntity deployedIocVersion = iocRepository.findById(iocId);
      deployedIocVersion.setLatestSuccessfulDeployment(null);
      iocRepository.updateIoc(deployedIocVersion);
    } catch (EmptyResultDataAccessException e) {
    }
  }

  @Transactional
  public void deleteDeploymentForIoc(final IocEntity ioc) {
    final List<IocDeploymentEntity> deploymentEntities =
        deploymentRepository.findAllByIocId(ioc.getId());

    for (final IocDeploymentEntity iocDeploymentEntity : deploymentEntities) {
      final List<AwxJobEntity> awxJobEntities =
          deploymentRepository.findAllAwxJobsByIocDeploymentId(iocDeploymentEntity.getId());
      for (AwxJobEntity awxJobEntity : awxJobEntities) {
        deploymentRepository.deleteAwxJob(awxJobEntity);
      }
      deploymentRepository.deleteIocDeployment(iocDeploymentEntity);
    }
  }

  static class DeploymentRequest {
    private IocEntity ioc;
    private HostInfoWithId host;

    public DeploymentRequest(IocEntity ioc, HostInfoWithId host) {
      this.ioc = ioc;
      this.host = host;
    }

    public IocEntity getIoc() {
      return ioc;
    }

    public HostInfoWithId getHost() {
      return host;
    }
  }

  private static final Logger LOGGER = LoggerFactory.getLogger(DeploymentService.class);

  private static final long WEBHOOK_JOB_START_DELAY = 30L * 1_000;
  private static final long WEBHOOK_JOB_FINISH_DELAY = 5L * 60L * 1_000;
  private static final long WEBHOOK_COMMAND_FINISH_DELAY = 15L * 1_000;
  private static final String AWX_ARG_SEPARATOR = " ";
  private final IIocDeploymentRepository deploymentRepository;
  private final IIocRepository iocRepository;
  private final PagingUtil utils;
  private final HostService hostService;
  private final AwxService awxService;
  private final AuthorizationService authorizationService;
  private final GitLabService gitLabService;
  private final NamingService namingService;
  private final DeploymentAlertGenerator deploymentAlertGenerator;
  private final ZonedDateTime playbookTransitionTime;
  private final Integer iocsOnHostLimit;

  public DeploymentService(
      IIocDeploymentRepository deploymentRepository,
      IIocRepository iocRepository,
      PagingUtil utils,
      HostService hostService,
      AwxService awxService,
      AuthorizationService authorizationService,
      GitLabService gitLabService,
      DeploymentAlertGenerator deploymentAlertGenerator,
      Integer iocsOnHostLimit,
      ZonedDateTime playbookTransitionTime,
      NamingService namingService) {
    this.deploymentRepository = deploymentRepository;
    this.iocRepository = iocRepository;
    this.utils = utils;
    this.hostService = hostService;
    this.awxService = awxService;
    this.authorizationService = authorizationService;
    this.gitLabService = gitLabService;
    this.deploymentAlertGenerator = deploymentAlertGenerator;
    this.iocsOnHostLimit = iocsOnHostLimit;
    this.playbookTransitionTime = playbookTransitionTime;
    this.namingService = namingService;
  }

  public Deployment createDeployment(DeploymentTask deploymentTask, String sourceVersion)
      throws Exception {
    authorizationService.checkDeploymentRights(
        deploymentTask.getIoc().getGitProjectId(), deploymentTask.getUserDetails());

    Deployment activeDeployment =
        findActiveDeploymentForIocDetails(deploymentTask.getIoc().getId());
    if (activeDeployment != null
        && !activeDeployment.isUndeployment()
        && !targetHostMatchesWIthActiveDeploymentHost(deploymentTask, activeDeployment)) {
      throw new OperationNotAllowedException(
          "Moving IOC from host "
              + activeDeployment.getHost().getFqdn()
              + " to host "
              + deploymentTask.getHost().getFqdn()
              + " is not allowed!");
    }

    checkConcurrency(deploymentTask.getIoc());
    if (!deploymentTask.isUndeploy()
        && !(activeDeployment != null
            && !activeDeployment.isUndeployment()
            && targetHostMatchesWIthActiveDeploymentHost(deploymentTask, activeDeployment))) {
      checkHostCapacity(deploymentTask.getHost());
    }

    Set<NetBoxHostIdentifier> concurrentHostIds =
        getConcurrentHostIds(deploymentTask.getIoc(), deploymentTask.getHost().getHostIdentifier());

    if (!concurrentHostIds.isEmpty()) {
      IocDeploymentEntity deployment =
          createDeployments(
              new DeploymentRequest(deploymentTask.getIoc(), deploymentTask.getHost()),
              deploymentTask.getUserDetails().getUserName(),
              true,
              deploymentTask.isUndeploy(),
              sourceVersion);

      String sourceVersionShort =
          gitLabService.getShortGitId(deployment.getGitProjectId(), deployment.getSourceVersion());

      GitlabProject project =
          gitLabService.getNullableGitlabProject(
              GitLabService.getGitProjectIdForDeployment(deployment));

      return DeploymentUtil.convertToDeploymentDetails(
          project,
          deployment,
          sourceVersionShort,
          true,
          deploymentTask.getHost().getFqdn(),
          deploymentTask.getHost().getName(),
          deploymentTask.getHost().getNetwork(),
          serverTimeZone);
    }

    IocDeploymentEntity deployment =
        createDeployments(
            new DeploymentRequest(deploymentTask.getIoc(), deploymentTask.getHost()),
            deploymentTask.getUserDetails().getUserName(),
            false,
            deploymentTask.isUndeploy(),
            sourceVersion);

    startDeployment(
        deployment,
        deploymentTask.getHost().getFqdn(),
        deploymentTask.isUndeploy() ? DeploymentType.UNDEPLOY : DeploymentType.DEPLOY,
        deploymentTask.getUserDetails().getUserName());
    String sourceVersionShort =
        gitLabService.getShortGitId(deployment.getGitProjectId(), deployment.getSourceVersion());

    GitlabProject project =
        gitLabService.getNullableGitlabProject(
            GitLabService.getGitProjectIdForDeployment(deployment));

    return DeploymentUtil.convertToDeploymentDetails(
        project,
        deployment,
        sourceVersionShort,
        true,
        deploymentTask.getHost().getFqdn(),
        deploymentTask.getHost().getName(),
        deploymentTask.getHost().getNetwork(),
        serverTimeZone);
  }

  private static boolean targetHostMatchesWIthActiveDeploymentHost(
      DeploymentTask deploymentTask, Deployment activeDeployment) {
    return deploymentTask.getHost().getHostId().equals(activeDeployment.getHost().getHostId());
  }

  private void checkHostCapacity(HostInfoWithId host) {
    if (iocsOnHostLimit <= iocRepository.countAssociatedDeployments(host.getId(), host.isVm())) {
      throw new LimitExceededException(
          "Max number of IOCs per host is %d.".formatted(iocsOnHostLimit));
    }
  }

  @NotNull
  public Set<NetBoxHostIdentifier> getConcurrentHostIds(
      IocEntity ioc, NetBoxHostIdentifier hostIdentifier) {
    List<IocEntity> ongoingDeployments =
        deploymentRepository.findAllPendingOrQueuedDeployments().stream()
            .map(IocDeploymentEntity::getIoc)
            .collect(Collectors.toList());

    List<NetBoxHostIdentifier> ongoingCommandHostIds =
        deploymentRepository.findAllQueuedOrPendingCommands().stream()
            .map(AwxJobEntity::getDeployment)
            .map(IocDeploymentEntity::getNetBoxHostIdentifier)
            .toList();

    Set<NetBoxHostIdentifier> ongoingAffectedHostIds =
        findDeploymentHostsForIocs(ongoingDeployments);
    ongoingAffectedHostIds.addAll(ongoingCommandHostIds);

    Set<NetBoxHostIdentifier> hostsForLimit = findDeploymentHostsForIocs(ImmutableList.of(ioc));
    List<NetBoxHostIdentifier> ongoingCommandsForIocHostIds =
        deploymentRepository.findAllQueuedOrPendingCommandsForIoc(ioc).stream()
            .map(AwxJobEntity::getDeployment)
            .map(IocDeploymentEntity::getNetBoxHostIdentifier)
            .toList();

    hostsForLimit.addAll(ongoingCommandsForIocHostIds);
    if (hostsForLimit.stream().noneMatch(id -> id.equals(hostIdentifier))) {
      hostsForLimit.add(hostIdentifier);
    }

    return hostsForLimit.stream()
        .filter(ongoingAffectedHostIds::contains)
        .collect(Collectors.toSet());
  }

  @Transactional
  public void executeNextQueuedDeployment() {
    AwxJobEntity nextQueuedJob = deploymentRepository.findFirstQueuedAwxJob();

    if (nextQueuedJob != null) {
      boolean isBatchJob = BATCH_JOB_TYPES.contains(AwxJobType.valueOf(nextQueuedJob.getJobType()));
      List<IocDeploymentEntity> deployments =
          isBatchJob
              ? deploymentRepository.findDeploymentsForBatchJob(nextQueuedJob.getId())
              : ImmutableList.of(nextQueuedJob.getDeployment());
      if (hasNoConcurrency(deployments)) {
        List<IocEntity> ongoingDeployments =
            deploymentRepository.findAllPendingAwxJobs().stream()
                .map(AwxJobEntity::getDeployment)
                .map(IocDeploymentEntity::getIoc)
                .collect(Collectors.toList());

        Set<NetBoxHostIdentifier> ongoingAffectedHosts =
            findDeploymentHostsForIocs(ongoingDeployments);

        if (isBatchJob) {

          List<NetBoxHostIdentifier> targetHostIds =
              deployments.stream().map(IocDeploymentEntity::getNetBoxHostIdentifier).toList();
          if (!CollectionUtils.intersection(ongoingAffectedHosts, targetHostIds).isEmpty()) {
            executeNextBatchDeployment(nextQueuedJob, deployments);
          }

        } else {
          IocDeploymentEntity nextQueuedJobDeployment = nextQueuedJob.getDeployment();
          if (!ongoingAffectedHosts.contains(nextQueuedJobDeployment.getNetBoxHostIdentifier())) {

            if (Constants.ALL_DEPLOYMENT_TYPES.contains(nextQueuedJob.getJobType())) {
              executeNextDeployment(nextQueuedJobDeployment);
            } else {
              executeNextCommand(nextQueuedJob);
            }
          }
        }
      }
    }
  }

  private void executeNextCommand(AwxJobEntity nextQueuedJob) {
    try {
      nextQueuedJob.setStatus(JobStatus.CREATED.name());
      deploymentRepository.updateAwxJob(nextQueuedJob);
      executeCommand(nextQueuedJob);
    } catch (Exception e) {
      LOGGER.error("Error during executing queued command task: {}", e.getMessage(), e);
    }
  }

  private void executeNextBatchDeployment(
      AwxJobEntity awxJobEntity, List<IocDeploymentEntity> deployments) {
    try {
      updateQueuedToPendingBatchDeployments(awxJobEntity, deployments);
      startBatchDeployment(
          awxJobEntity,
          deployments,
          AwxJobType.BATCH_DEPLOY.equals(AwxJobType.valueOf(awxJobEntity.getJobType()))
              ? BatchType.DEPLOY
              : BatchType.UNDEPLOY,
          awxJobEntity.getCreatedBy());
    } catch (Exception e) {
      LOGGER.error("Error during executing queued batch deployment task: {}", e.getMessage(), e);
    }
  }

  @Transactional
  public void updateQueuedToPendingBatchDeployments(
      AwxJobEntity awxJobEntity, List<IocDeploymentEntity> deployments) {
    awxJobEntity.setStatus(JobStatus.CREATED.name());
    deploymentRepository.updateAwxJob(awxJobEntity);

    for (IocDeploymentEntity deployment : deployments) {
      deployment.setPending(true);
      deploymentRepository.updateDeployment(deployment);
    }
  }

  private void executeNextDeployment(IocDeploymentEntity nextQueuedJobDeployment) {
    try {
      updateQueuedToPendingDeployments(nextQueuedJobDeployment);
      startDeployment(
          nextQueuedJobDeployment,
          hostService
              .getHostByNetBoxId(
                  nextQueuedJobDeployment.getNetBoxHostId(), nextQueuedJobDeployment.isVm())
              .getFqdn(),
          DeploymentType.valueOf(nextQueuedJobDeployment.getDeploymentType()),
          nextQueuedJobDeployment.getCreatedBy());
    } catch (Exception e) {
      LOGGER.error("Error during executing queued deployment task: {}", e.getMessage(), e);
    }
  }

  private boolean hasNoConcurrency(List<IocDeploymentEntity> deployments) {
    return deploymentRepository.findConcurrentPendingAwxJobsForDeployments(deployments).isEmpty();
  }

  /**
   * Checks if an IOC has any ongoing operation (un/deployment, start/stop command). Throws an
   * exception if there is an ongoing operation.
   *
   * @param ioc the IOC that has to be checked
   * @throws ConcurrentOperationFoundException if the IOC has any ongoing operation
   */
  public void checkConcurrency(IocEntity ioc) throws ConcurrentOperationFoundException {
    if (hasOngoingDeployment(ioc)) {
      throw new ConcurrentOperationFoundException(
          "There is an ongoing deployment for this IOC! (ID: " + ioc + ")");
    }
    if (hasOngoingCommand(ioc)) {
      throw new ConcurrentOperationFoundException(
          "There is an ongoing start/stop command for this IOC! (ID: " + ioc + ")");
    }
  }

  /**
   * Determines from an IOC entity if it is being queued (by the backend, or by AWX) for
   * (un)deployment
   *
   * @param ioc the IOC entity that has to be checked
   * @return <code>true</code> if the IOC is in queue to be (un)deployed <code>false</code> if the
   *     IOC is not in queue to be (un)deployed
   */
  public boolean hasOngoingDeployment(IocEntity ioc) {
    List<AwxJobEntity> concurrentDeploymentsByIoc =
        deploymentRepository.findOngoingDeploymentsForIoc(ioc);
    return !concurrentDeploymentsByIoc.isEmpty();
  }

  /**
   * Determines from an IOC entity if it has an ongoing(not finished) command
   *
   * @param ioc the IOC entity that has to be checked
   * @return <code>true</code> if the IOC has an ongoing command <code>false</code> if the IOC
   *     doesn't have ongoing command
   */
  public boolean hasOngoingCommand(IocEntity ioc) {
    List<AwxJobEntity> concurrentCommandsByIoc = deploymentRepository.findOngoingCommandsByIoc(ioc);
    return !concurrentCommandsByIoc.isEmpty();
  }

  /**
   * Finds the first(oldest) Deployment entity in the DB that is being queued by the backend
   *
   * @return the latest Deployment entity that is being queued, or null
   */
  private IocDeploymentEntity deploymentQueueNextSingle() {
    try {
      return deploymentRepository.findFirstQueued();
      // todo this exception may not be thrown after the modification of repository to use stream
      // and Optional
    } catch (EmptyResultDataAccessException e) {
      LOGGER.info("No queued deployment in database");
    }
    return null;
  }

  private void startDeployment(
      IocDeploymentEntity deployment,
      String targetHost,
      DeploymentType deploymentType,
      String userName)
      throws Exception {
    boolean isUndeploy = UNDEPLOYMENT_TYPES.contains(deploymentType.name());
    if (deployment == null) {
      throw new Exception();
    }

    if (!ImmutableList.of(JobStatus.QUEUED_BY_BACKEND.name(), JobStatus.CREATED.name())
        .contains(deployment.getAwxJob().getStatus())) {
      LOGGER.warn(
          "AWX job already started for deployment ({}), AWX job ID: {}",
          deployment.getId(),
          deployment.getAwxJob().getId());
      return;
    }

    eu.ess.ics.ccce.service.external.awx.model.response.Job job = null;
    Exception awxError = null;

    String iocsJson = new Gson().toJson(List.of(deployment.getIoc().getNamingName()));
    try {
      JobTemplate ccceTemplate = awxService.getJobTemplate(awxService.getJobTemplateName());
      // Launch the job
      job =
          awxService.launchJob(
              ccceTemplate.getRelated().getLaunch(), targetHost, iocsJson, deploymentType);
    } catch (Exception e) {
      awxError = e;
      LOGGER.error(e.getMessage(), e);
    }

    if (job != null) {
      updateDeploymentAwxJob(deployment.getAwxJob(), job);

      LOGGER.debug(
          "IOCs have been "
              + (isUndeploy ? "UNDEPLOYED" : "DEPLOYED")
              + " by [{}], IOCs: {} to hosts: {}",
          userName,
          iocsJson,
          targetHost);
    } else {
      updateDeploymentAwxJob(
          deployment.getAwxJob(),
          List.of(deployment),
          awxError != null ? awxError.getMessage() : "");
      executeNextQueuedDeployment();

      if (awxError != null) {
        throw awxError;
      }
      throw new ServiceException("Error during creating deployment");
    }
  }

  public void startBatchDeployment(
      AwxJobEntity awxJobEntity,
      List<IocDeploymentEntity> deployments,
      BatchType batchType,
      String userName)
      throws Exception {
    if (deployments == null || deployments.isEmpty()) {
      throw new Exception();
    }

    if (!ImmutableList.of(JobStatus.QUEUED_BY_BACKEND.name(), JobStatus.CREATED.name())
        .contains(awxJobEntity.getStatus())) {
      LOGGER.warn("AWX job already started, AWX job ID: {}", awxJobEntity.getId());
      return;
    }
    List<String> affectedIocs =
        deployments.stream()
            .map(deployment -> deployment.getIoc().getNamingName())
            .collect(Collectors.toList());

    Set<String> affectedHosts =
        deployments.stream().map(IocDeploymentEntity::getHostFqdn).collect(Collectors.toSet());

    String iocsJson = new Gson().toJson(affectedIocs);

    eu.ess.ics.ccce.service.external.awx.model.response.Job job = null;
    Exception awxError = null;
    try {
      JobTemplate ccceTemplate = awxService.getJobTemplate(awxService.getJobTemplateName());
      // Launch the job
      job =
          awxService.launchJob(
              ccceTemplate.getRelated().getLaunch(),
              affectedHosts,
              iocsJson,
              DeploymentType.UNDEPLOY);
    } catch (Exception e) {
      awxError = e;
      LOGGER.error(e.getMessage(), e);
    }

    if (job != null) {
      updateBatchDeploymentAwxJob(awxJobEntity, job);

      LOGGER.debug(
          "IOCs have been "
              + (BatchType.UNDEPLOY.equals(batchType) ? "UNDEPLOYED" : "DEPLOYED")
              + " by [{}], IOCs: {}",
          userName,
          iocsJson);
    } else {
      updateDeploymentAwxJob(
          awxJobEntity, deployments, awxError != null ? awxError.getMessage() : "");
      executeNextQueuedDeployment();

      if (awxError != null) {
        throw awxError;
      }
      throw new ServiceException("Error during creating deployment");
    }
  }

  private AwxJobEntity updateBatchDeploymentAwxJob(
      AwxJobEntity awxJobEntity, eu.ess.ics.ccce.service.external.awx.model.response.Job job) {
    awxJobEntity.setAwxJobId(job.getId());
    awxJobEntity.setAwxJobStartedAt(job.getStarted());
    awxJobEntity.setStatus(job.getStatus().name());
    deploymentRepository.updateAwxJob(awxJobEntity);

    return awxJobEntity;
  }

  @Transactional
  public Deployment createUndeployment(DeploymentTask deploymentTask, String gitVersion)
      throws Exception {
    return createDeployment(deploymentTask, gitVersion);
  }

  public AwxJobEntity storeBatchDeployment(
      List<IocDeploymentEntity> deployments,
      String createdBy,
      BatchType batchType,
      boolean queued) {
    Date createdAt = new Date();
    AwxJobEntity batchJobEntity = new AwxJobEntity();
    batchJobEntity.setCreatedAt(createdAt);
    batchJobEntity.setJobType(
        BatchType.DEPLOY.equals(batchType)
            ? AwxJobType.BATCH_DEPLOY.name()
            : AwxJobType.BATCH_UNDEPLOY.name());
    batchJobEntity.setStatus(
        queued ? JobStatus.QUEUED_BY_BACKEND.name() : JobStatus.CREATED.name());
    batchJobEntity.setCreatedBy(createdBy);
    deploymentRepository.createAwxJob(batchJobEntity);

    for (IocDeploymentEntity deployment : deployments) {
      deployment.setAwxJob(batchJobEntity);
      deployment.setCreatedAt(createdAt);
      deployment.setPending(!queued);
      deploymentRepository.updateDeployment(deployment);
    }

    return batchJobEntity;
  }

  public IocDeploymentEntity prepareDeploymentForBatch(
      IocEntity ioc,
      HostInfoWithId host,
      String sourceVersion,
      String createdBy,
      BatchType batchType) {
    String gitProjectUrl = gitLabService.projectInfo(ioc.getGitProjectId()).getHttpUrl();
    final IocDeploymentEntity iocDeploymentEntity = new IocDeploymentEntity();
    iocDeploymentEntity.setIoc(ioc);
    iocDeploymentEntity.setDeploymentType(
        BatchType.UNDEPLOY.equals(batchType)
            ? DeploymentType.BATCH_UNDEPLOY.name()
            : DeploymentType.BATCH_DEPLOY.name());
    iocDeploymentEntity.setCreatedBy(createdBy);
    iocDeploymentEntity.setHostFqdn(host.getFqdn());
    iocDeploymentEntity.setNetBoxHostId(host.getId());
    iocDeploymentEntity.setVm(host.isVm());
    iocDeploymentEntity.setHostName(host.getName());
    iocDeploymentEntity.setHostNetwork(host.getNetwork());
    iocDeploymentEntity.setNamingName(ioc.getNamingName());
    iocDeploymentEntity.setGitProjectId(ioc.getGitProjectId());
    iocDeploymentEntity.setSourceVersion(sourceVersion);
    iocDeploymentEntity.setGitProjectUrl(gitProjectUrl);
    return iocDeploymentEntity;
  }

  @Transactional
  public IocDeploymentEntity createDeployments(
      DeploymentRequest deploymentRequest,
      String createdBy,
      boolean queued,
      boolean undeploy,
      String sourceVersion) {
    String gitProjectUrl =
        gitLabService.projectInfo(deploymentRequest.getIoc().getGitProjectId()).getHttpUrl();

    IocDeploymentEntity iocDeploymentEntity = new IocDeploymentEntity();
    iocDeploymentEntity.setIoc(deploymentRequest.getIoc());
    iocDeploymentEntity.setCreatedAt(new Date());
    iocDeploymentEntity.setDeploymentType(
        undeploy ? DeploymentType.UNDEPLOY.name() : DeploymentType.DEPLOY.name());
    iocDeploymentEntity.setCreatedBy(createdBy);
    iocDeploymentEntity.setPending(!queued);
    iocDeploymentEntity.setHostFqdn(deploymentRequest.host.getFqdn());
    iocDeploymentEntity.setNetBoxHostId(deploymentRequest.host.getId());
    iocDeploymentEntity.setVm(deploymentRequest.host.isVm());
    iocDeploymentEntity.setHostName(deploymentRequest.host.getName());
    iocDeploymentEntity.setHostNetwork(deploymentRequest.host.getNetwork());
    iocDeploymentEntity.setNamingName(deploymentRequest.getIoc().getNamingName());
    iocDeploymentEntity.setGitProjectId(deploymentRequest.getIoc().getGitProjectId());
    iocDeploymentEntity.setSourceVersion(sourceVersion);
    iocDeploymentEntity.setGitProjectUrl(gitProjectUrl);
    deploymentRepository.createDeployment(iocDeploymentEntity);

    AwxJobEntity awxJob = new AwxJobEntity();
    awxJob.setStatus(queued ? JobStatus.QUEUED_BY_BACKEND.name() : JobStatus.CREATED.name());
    awxJob.setJobType(undeploy ? AwxJobType.UNDEPLOY.name() : AwxJobType.DEPLOY.name());
    awxJob.setDeployment(iocDeploymentEntity);
    awxJob.setCreatedBy(createdBy);
    deploymentRepository.createAwxJob(awxJob);

    iocDeploymentEntity.setAwxJob(awxJob);
    deploymentRepository.updateDeployment(iocDeploymentEntity);

    return iocDeploymentEntity;
  }

  @Transactional
  public void updateQueuedToPendingDeployments(IocDeploymentEntity iocDeploymentEntity) {
    AwxJobEntity jobEntity = iocDeploymentEntity.getAwxJob();
    jobEntity.setStatus(JobStatus.CREATED.name());
    deploymentRepository.updateAwxJob(jobEntity);

    iocDeploymentEntity.setPending(true);
    deploymentRepository.updateDeployment(iocDeploymentEntity);
  }

  @Transactional
  public AwxJobEntity updateDeploymentAwxJob(
      AwxJobEntity awxJob, eu.ess.ics.ccce.service.external.awx.model.response.Job job) {
    awxJob.setAwxJobId(job.getId());
    awxJob.setAwxJobStartedAt(job.getStarted());
    awxJob.setStatus(job.getStatus().name());
    deploymentRepository.updateAwxJob(awxJob);

    return awxJob;
  }

  @Transactional
  public AwxJobEntity updateDeploymentAwxJob(
      AwxJobEntity awxJob, List<IocDeploymentEntity> deployments, String errorMessage) {
    awxJob.setStatus(JobStatus.ERROR.name());
    awxJob.setErrorDetails(errorMessage);
    deploymentRepository.updateAwxJob(awxJob);

    for (IocDeploymentEntity deployment : deployments) {
      deployment.setPending(false);
      deploymentRepository.updateDeployment(deployment);
    }

    return awxJob;
  }

  @Transactional
  public AwxJobEntity updateCommandJob(AwxJobEntity awxJobEntity, String errorMessage) {
    awxJobEntity.setStatus(JobStatus.ERROR.name());
    awxJobEntity.setErrorDetails(errorMessage);

    deploymentRepository.updateAwxJob(awxJobEntity);

    return awxJobEntity;
  }

  /**
   * Returns the AwxJobDetails based on a given awxJobId.
   *
   * @param jobId the awxJobId
   * @return AwxJobDetails response
   */
  public AwxJobDetails fetchAwxJobStatus(long jobId) {
    try {
      AwxJobEntity awxJobEntity = deploymentRepository.findJobById(jobId);
      if (Constants.ALL_DEPLOYMENT_TYPES.contains(awxJobEntity.getJobType())) {
        return fetchDeploymentJobDetails(awxJobEntity);
      } else if (Constants.COMMAND_TYPES.contains(awxJobEntity.getJobType())) {
        return fetchCommandDetails(awxJobEntity);
      }
      throw new InvalidJobTypeException("Job type not valid");
    } catch (EmptyResultDataAccessException e) {
      throw new EntityNotFoundException("AWX Job Entity in CE DB", jobId);
    }
  }

  private AwxJobDetails fetchDeploymentJobDetails(AwxJobEntity awxJobEntity) {
    if (Constants.FINISHED_AWX_STATUSES.contains(awxJobEntity.getStatus())) {
      return JobUtil.convertToAwxJobDetails(awxJobEntity, serverTimeZone);
    } else {
      if (isWebHookOk(awxJobEntity)) {
        return JobUtil.convertToAwxJobDetails(awxJobEntity, serverTimeZone);
      } else {
        // TODO: alert about webhook malfunction
        LOGGER.error(
            "WEBHOOK HAS NOT FIRED for deployment job (ID: {})", awxJobEntity.getAwxJobId());
        return updateJobAndDeploymentUsingAwx(awxJobEntity);
      }
    }
  }

  private AwxJobDetails updateJobAndDeploymentUsingAwx(AwxJobEntity awxJobEntity)
      throws AwxServiceException {
    eu.ess.ics.ccce.service.external.awx.model.response.JobDetails job =
        awxService.getJobDetails(awxJobEntity.getAwxJobId());
    updateAwxJob(
        awxJobEntity,
        job.getJob().getStatus(),
        Utils.toZonedDateTime(job.getJob().getStarted(), serverTimeZone),
        Utils.toZonedDateTime(job.getJob().getFinished(), serverTimeZone),
        false);
    if (BATCH_JOB_TYPES.contains(AwxJobType.valueOf(awxJobEntity.getJobType()))) {
      updateBatchDeployment(
          deploymentRepository.findDeploymentsForBatchJob(awxJobEntity.getId()),
          job.getJob().getStatus(),
          job.isIcsRoleReached());
    } else {
      updateDeployment(
          awxJobEntity.getDeployment(), job.getJob().getStatus(), job.isIcsRoleReached());
    }
    return JobUtil.convertCommandToAwxJobDetails(job, serverTimeZone);
  }

  private boolean isWebHookOk(AwxJobEntity job) {
    boolean batchJob = BATCH_JOB_TYPES.contains(AwxJobType.valueOf(job.getJobType()));
    if (!batchJob
        && (job.getDeployment() == null
            || job.getDeployment().getCreatedAt() == null
            || job.getAwxJobId() == null)) {
      return true;
    }
    if (job.getStatus() == null
        || !ImmutableList.builder()
            .addAll(Constants.FINISHED_JOB_STATES)
            .add(JobStatus.RUNNING)
            .build()
            .contains(JobStatus.valueOf(job.getStatus()))) {
      long endTime =
          batchJob ? job.getCreatedAt().getTime() : job.getDeployment().getCreatedAt().getTime();
      long startDelay = new Date().getTime() - endTime;
      boolean ok = startDelay < WEBHOOK_JOB_START_DELAY;
      if (!ok) {
        LOGGER.info(
            "Webhook job start delay exceeds limit, delay: {}, ms (limit: {} ms)",
            startDelay,
            WEBHOOK_JOB_START_DELAY);
      }
      return ok;
    }
    if (JobStatus.RUNNING.equals(JobStatus.valueOf(job.getStatus()))) {
      if (job.getWebhookUpdateAt() == null) {
        LOGGER.info("No webhook update for running job");
        return false;
      }
      long finishDelay = new Date().getTime() - job.getWebhookUpdateAt().getTime();
      boolean ok = finishDelay < WEBHOOK_JOB_FINISH_DELAY;
      if (!ok) {
        LOGGER.info(
            "Webhook job finish delay exceeds limit, delay: {} ms (limit: {} ms)",
            finishDelay,
            WEBHOOK_JOB_FINISH_DELAY);
      }
      return ok;
    }
    boolean anyUpdate = job.getWebhookUpdateAt() != null;
    if (!anyUpdate) {
      LOGGER.info("No webhook update for the job");
    }
    return anyUpdate;
  }

  private boolean isWebHookOkCommand(AwxJobEntity command) {
    if (command.getCreatedAt() == null || command.getAwxJobId() == null) {
      return true;
    }
    if (command.getStatus() == null
        || !Constants.FINISHED_JOB_STATES.contains(JobStatus.valueOf(command.getStatus()))) {
      long finishDelay = new Date().getTime() - command.getCreatedAt().getTime();
      boolean ok = finishDelay < WEBHOOK_COMMAND_FINISH_DELAY;
      if (!ok) {
        LOGGER.info(
            "Webhook command finish delay exceeds limit, delay: {} ms (limit: {} ms)",
            finishDelay,
            WEBHOOK_COMMAND_FINISH_DELAY);
      }
      return ok;
    }
    boolean anyUpdate = command.getWebhookUpdateAt() != null;
    if (!anyUpdate) {
      LOGGER.info("No webhook update for the command");
    }
    return anyUpdate;
  }

  /**
   * Retrieves standard logs of AWX jobs and ad-hoc commands from AWX service.
   *
   * @param jobId job ID
   * @return Job log descriptor
   */
  public AwxJobLog fetchAwxJobLog(long jobId) {
    try {
      AwxJobEntity job = deploymentRepository.findJobById(jobId);
      return awxService.getJobLog(job.getAwxJobId(), AwxJobType.valueOf(job.getJobType()));
    } catch (EmptyResultDataAccessException e) {
      throw new EntityNotFoundException("Job", jobId);
    }
  }

  public void updateAwxJobOrCommand(AwxJobMeta awxEntity) {
    AwxJobEntity awxJobEntity = null;
    try {
      awxJobEntity = deploymentRepository.findByAwxJobId(awxEntity.getId());
    } catch (EmptyResultDataAccessException e) {
    }

    if (awxJobEntity != null) {
      LOGGER.debug("AWX WEBHOOK - updateAwxJobOrCommand: {}", awxEntity);
      webHookUpdateAwxJob(awxJobEntity, awxEntity);
    } else {
      LOGGER.debug("AWX WEBHOOK update for not tracked entity, ID: {}", awxEntity.getId());
    }
  }

  public void webHookUpdateAwxJob(AwxJobEntity awxJobEntity, AwxJobMeta job) {
    updateAwxJob(awxJobEntity, job.getStatus(), job.getStarted(), job.getFinished(), true);
    Boolean icsRoleReached = null;
    if (awxJobEntity.getStatus().equals(JobStatus.FAILED.name())) {
      eu.ess.ics.ccce.service.external.awx.model.response.JobDetails jobDetails;
      try {
        jobDetails = awxService.getJobDetails(awxJobEntity.getAwxJobId());
        icsRoleReached = jobDetails.isIcsRoleReached();
      } catch (AwxServiceException e) {
        LOGGER.error("Cannot get details of failed AWX job, cause: {}", e.getMessage(), e);
      }
    }
    updateDeployment(awxJobEntity.getDeployment(), job.getStatus(), icsRoleReached);
  }

  void updateAwxJob(
      AwxJobEntity awxJobEntity,
      JobStatus status,
      ZonedDateTime started,
      ZonedDateTime finished,
      boolean webhook) {
    if (status != null) {
      awxJobEntity.setStatus(status.name());
      awxJobEntity.setAwxJobStartedAt(Utils.toUtilDate(started));
      awxJobEntity.setAwxJobFinishedAt(Utils.toUtilDate(finished));
      if (webhook) {
        awxJobEntity.setWebhookUpdateAt(new Date());
      }
      deploymentRepository.updateAwxJob(awxJobEntity);
    }
  }

  @Transactional
  public void updateBatchDeployment(
      List<IocDeploymentEntity> deployments, JobStatus status, Boolean icsRoleReached) {
    if (deployments != null
        && !deployments.isEmpty()
        && status != null
        && deployments.stream().anyMatch(IocDeploymentEntity::isPending)) {

      boolean deploymentFinished = Constants.FINISHED_JOB_STATES.contains(status);

      if (deploymentFinished) {
        boolean deploymentFailed = Constants.FAILED_JOB_STATES.contains(status) && !icsRoleReached;
        for (IocDeploymentEntity deployment : deployments) {
          if (!deploymentFailed) {
            IocEntity deployedIocVersion = deployment.getIoc();
            deployedIocVersion.setLatestSuccessfulDeployment(deployment);
            deployedIocVersion.setDeployedWithOldPlaybook(false);
            iocRepository.updateIoc(deployedIocVersion);
          }
          deployment.setPending(false);
          deploymentRepository.updateDeployment(deployment);
        }
        executeNextQueuedDeployment();
      }
    }
  }

  @Transactional
  public void updateDeployment(
      IocDeploymentEntity deployment, JobStatus status, Boolean icsRoleReached) {
    if (deployment != null && status != null && deployment.isPending()) {

      boolean deploymentFinished = Constants.FINISHED_JOB_STATES.contains(status);

      if (deploymentFinished) {
        if (Constants.FAILED_JOB_STATES.contains(status) && !icsRoleReached) {
          // Deployment job wasn't successful on AWX side
          // and ICS roles weren't reached at all during playbook execution
          deployment.setPending(false);
        } else {
          deployment.setPending(false);
          IocEntity deployedIocVersion = deployment.getIoc();
          deployedIocVersion.setLatestSuccessfulDeployment(deployment);
          deployedIocVersion.setDeployedWithOldPlaybook(false);
          iocRepository.updateIoc(deployedIocVersion);
        }

        deploymentRepository.updateDeployment(deployment);
        executeNextQueuedDeployment();
      }
    }
  }

  /**
   * Finds the latest (un)deployment Entity (ordered by date) for an IOC, based on the IOC ID.
   *
   * @param iocId the ID of the IOC for which the latest (un)deployment has to be found
   * @return The latest (un)deployment Entity for the IOC, or <code>null</code> if no (un)deployment
   *     can be found
   */
  public IocDeploymentEntity findLatestDeploymentForIoc(Long iocId) {
    return deploymentRepository.findLatestDeploymentForIoc(iocId);
  }

  /**
   * Finds the actual deployment for a certain IOC
   *
   * @param iocId the ID of the IOC
   * @return Deployment object, or <code>null</code> if there is no actual deployment for the
   *     certain IOC
   * @throws NoResultException if the IOC can not be found with the ID
   */
  public Deployment findActiveDeploymentForIocInfo(Long iocId) {
    IocDeploymentEntity activeDeployment = findActiveDeploymentEntityForIocId(iocId);
    return findActiveDeploymentForIocInfo(activeDeployment);
  }

  /**
   * Converts (active) deployment entity information to Deployment response object. The host
   * information is not resolved on-the-fly, but extracted from the DB!
   *
   * @param activeDeployment the deployment entity that has to be converted
   * @return Deployment response based on the deployment entity, or <code>null</code> if there is no
   *     (active) deployment
   */
  public Deployment findActiveDeploymentForIocInfo(IocDeploymentEntity activeDeployment) {
    // todo we may refactor the function call because is used in one place, doesn't need to extract
    // the host info here
    return DeploymentUtil.convertToDeploymentInfo(
        activeDeployment,
        false,
        activeDeployment == null ? null : activeDeployment.getHostFqdn(),
        activeDeployment == null ? null : activeDeployment.getHostName(),
        activeDeployment == null ? null : activeDeployment.getHostNetwork(),
        serverTimeZone);
  }

  /**
   * Finds the (currently) active (un)deployment Entity for an IOC based on the IOC ID.
   *
   * @param iocId the ID of the IOC for which the (active) (un)deployment Entity has to be found
   * @return The active (un)deployment entity for an IOC, or <code>null</code>, if there were not
   *     (un)deployments for it
   */
  public IocDeploymentEntity findActiveDeploymentEntityForIocId(Long iocId) {
    IocEntity iocVersion = iocRepository.findById(iocId);
    // todo it may throw NoResultException if the IOC can not be found
    return iocVersion != null ? iocVersion.getLatestSuccessfulDeployment() : null;
  }

  /**
   * Finds, and converts the active (un)deployment for an IOC to Deployment response object. All the
   * host information will come from the DB stored for the (un)deployment (will not be looked up
   * on-the-fly)
   *
   * @param iocId The ID of the IOC for which the Deployment info should be determined
   * @return The converted Deployment response of the active (un)deployment for a certain IOC, or
   *     <code>null</code> if no (un)deployment can be found for the IOC
   */
  public Deployment findActiveDeploymentForIocDetails(Long iocId) {
    IocDeploymentEntity activeDeployment = findActiveDeploymentEntityForIocId(iocId);
    GitlabProject project =
        activeDeployment == null
            ? null
            : gitLabService.getNullableGitlabProject(
                GitLabService.getGitProjectIdForDeployment(activeDeployment));
    return findActiveDeploymentForIocDetails(activeDeployment, project);
  }

  /**
   * Finds all successfully deployed, active IOCs associated to a host. The response is paged!
   *
   * @param hostIdentifier the associated host's NetBox ID
   * @param iocName
   * @param orderBy
   * @param isAsc
   * @param page the requested page-number
   * @param limit the requested (maximum) response size
   * @return list of IOC entities that are successfully deployed to, and are active on a certain
   *     host
   */
  public List<IocEntity> findDeployedIocsByNetBoxHostId(
      NetBoxHostIdentifier hostIdentifier,
      String iocName,
      IocOrder orderBy,
      Boolean isAsc,
      int page,
      int limit) {
    return iocRepository.findAll(
        IocDeploymentStatus.DEPLOYED,
        null,
        ImmutableList.of(hostIdentifier),
        iocName,
        orderBy,
        isAsc,
        page,
        limit,
        false);
  }

  public Deployment findActiveDeploymentForIocDetails(
      IocDeploymentEntity activeDeployment, GitlabProject project) {
    boolean hostExternalIdValid = false;
    String fqdn = null;
    String hostName = null;
    String network = null;
    String sourceVersionShort = null;
    if (activeDeployment != null) {
      fqdn = activeDeployment.getHostFqdn();
      hostName = activeDeployment.getHostName();
      network = activeDeployment.getHostNetwork();
      try {
        HostInfoWithId host =
            hostService.getHostByNetBoxId(
                activeDeployment.getNetBoxHostId(), activeDeployment.isVm());
        hostExternalIdValid = host != null;
      } catch (Exception e) {
        LOGGER.error("Cannot retrieve host data from NetBox service.", e);
      }
      sourceVersionShort =
          gitLabService.getShortGitId(
              activeDeployment.getGitProjectId(), activeDeployment.getSourceVersion());
    }

    return DeploymentUtil.convertToDeploymentDetails(
        project,
        activeDeployment,
        sourceVersionShort,
        hostExternalIdValid,
        fqdn,
        hostName,
        network,
        serverTimeZone);
  }

  public Set<NetBoxHostIdentifier> findDeploymentHostsForIocs(List<IocEntity> iocs) {
    Set<NetBoxHostIdentifier> activeDeploymentHosts = new HashSet<>();
    Set<NetBoxHostIdentifier> intendedDeploymentHosts = new HashSet<>();

    for (IocEntity ioc : iocs) {
      try {
        IocDeploymentEntity activeDeployment = ioc.getLatestSuccessfulDeployment();
        if (activeDeployment != null) {
          activeDeploymentHosts.add(activeDeployment.getNetBoxHostIdentifier());
        }

        IocDeploymentEntity intendedDeployment =
            deploymentRepository.findPendingDeploymentForIoc(ioc.getId());
        if (intendedDeployment != null && intendedDeployment.getNetBoxHostId() != null) {
          intendedDeploymentHosts.add(intendedDeployment.getNetBoxHostIdentifier());
        }
      } catch (EmptyResultDataAccessException e) {

      }
    }

    Set<NetBoxHostIdentifier> allHosts = new HashSet<>();
    allHosts.addAll(activeDeploymentHosts);
    allHosts.addAll(intendedDeploymentHosts);

    return allHosts;
  }

  public DeploymentInfoDetails findDeploymentById(long deploymentId) {
    IocDeploymentEntity deployment = deploymentRepository.findDeploymentById(deploymentId);
    return DeploymentUtil.convertToDeploymentInfoDetails(
        deploymentAlertGenerator,
        extractDeploymentDetails(deployment),
        deployment.getIoc().getId(),
        awxService.generateAwxJobUrl(deployment.getAwxJob()));
  }

  private Deployment extractDeploymentDetails(IocDeploymentEntity iocDeploymentEntity) {
    boolean hostExternalIdValid = false;
    String fqdn = iocDeploymentEntity.getHostFqdn();
    String hostName = iocDeploymentEntity.getHostName();
    String network = iocDeploymentEntity.getHostNetwork();
    try {
      HostInfoWithId host =
          hostService.getHostByNetBoxId(
              iocDeploymentEntity.getNetBoxHostId(), iocDeploymentEntity.isVm());
      hostExternalIdValid = host != null;
    } catch (Exception e) {
      LOGGER.error("Cannot retrieve host data from NetBox service.", e);
    }

    String sourceVersionShort =
        gitLabService.getShortGitId(
            iocDeploymentEntity.getGitProjectId(), iocDeploymentEntity.getSourceVersion());

    return DeploymentUtil.convertToDeploymentDetails(
        gitLabService.getNullableGitlabProject(
            GitLabService.getGitProjectIdForDeployment(iocDeploymentEntity)),
        iocDeploymentEntity,
        sourceVersionShort,
        hostExternalIdValid,
        fqdn,
        hostName,
        network,
        serverTimeZone);
  }

  @Transactional
  public Job startIoc(
      Deployment activeDeployment,
      IocEntity iocEntity,
      Long gitlabProjectId,
      UserDetails userDetails)
      throws Exception {

    authorizationService.checkDeploymentRights(gitlabProjectId, userDetails);

    checkConcurrency(iocEntity);

    Set<NetBoxHostIdentifier> concurrentHostIds =
        getConcurrentHostIds(
            iocEntity, HostService.decodeHostIdentifier(activeDeployment.getHost().getHostId()));

    if (!concurrentHostIds.isEmpty()) {
      AwxJobEntity queuedAwxJob =
          createQueuedCommandRecord(AwxJobType.START, activeDeployment, userDetails);
      return JobUtil.convertToJob(queuedAwxJob, serverTimeZone);
    }

    AwxJobEntity commandEntity =
        createCommandRecord(AwxJobType.START, activeDeployment, userDetails);

    AwxJobEntity executedCommand = executeCommand(commandEntity);

    LOGGER.debug(
        "IOC [{}] has been STARTED on host [{}] by [{}]",
        activeDeployment.getIocName(),
        activeDeployment.getHost().getFqdn(),
        userDetails.getUserName());

    return JobUtil.convertToJob(executedCommand, serverTimeZone);
  }

  private AwxJobEntity createQueuedCommandRecord(
      AwxJobType awxJobType, Deployment activeDeployment, UserDetails userDetails) {
    AwxJobEntity queuedAwxJob = new AwxJobEntity();
    queuedAwxJob.setStatus(JobStatus.QUEUED_BY_BACKEND.name());
    queuedAwxJob.setJobType(awxJobType.name());
    queuedAwxJob.setDeployment(deploymentRepository.findDeploymentById(activeDeployment.getId()));
    queuedAwxJob.setCreatedBy(userDetails.getUserName());
    deploymentRepository.createAwxJob(queuedAwxJob);
    return queuedAwxJob;
  }

  /**
   * Stops a certain IOC using AWX command. For clarity the activeDeployment has to be
   * added/determined.
   *
   * @param activeDeployment the active deployment for the IOC for which the stop command has to be
   *     sent out
   * @param iocEntity the IOC entity that should be stopped
   * @param gitlabProjectId the git projectId (to check user permission for the IOC)
   * @param userDetails the userDetails object for checking permission
   * @return the Operation response containing info about the command
   * @throws AwxServiceException if there was an exception during the command execution
   * @throws ConcurrentOperationFoundException if there is an ongoing operation (un/deployment, or
   *     start/stop) when the user wants to stop the IOC
   */
  @Transactional
  public Job stopIoc(
      Deployment activeDeployment,
      IocEntity iocEntity,
      Long gitlabProjectId,
      UserDetails userDetails)
      throws Exception {

    authorizationService.checkDeploymentRights(gitlabProjectId, userDetails);

    checkConcurrency(iocEntity);

    Set<NetBoxHostIdentifier> concurrentHostIds =
        getConcurrentHostIds(
            iocEntity, HostService.decodeHostIdentifier(activeDeployment.getHost().getHostId()));

    if (!concurrentHostIds.isEmpty()) {
      AwxJobEntity queuedAwxJob =
          createQueuedCommandRecord(AwxJobType.STOP, activeDeployment, userDetails);
      return JobUtil.convertToJob(queuedAwxJob, serverTimeZone);
    }

    AwxJobEntity commandEntity =
        createCommandRecord(AwxJobType.STOP, activeDeployment, userDetails);
    AwxJobEntity executedCommand = executeCommand(commandEntity);

    LOGGER.debug(
        "IOC [{}] has been STOPPED on host [{}] by [{}]",
        activeDeployment.getIocName(),
        activeDeployment.getHost().getFqdn(),
        userDetails.getUserName());

    return JobUtil.convertToJob(executedCommand, serverTimeZone);
  }

  public AwxJobDetails fetchCommandDetails(AwxJobEntity awxCommandEntity) {
    if (Constants.FINISHED_AWX_STATUSES.contains(awxCommandEntity.getStatus())) {
      return JobUtil.convertToAwxJobDetails(awxCommandEntity, serverTimeZone);
    } else {
      if (isWebHookOkCommand(awxCommandEntity)) {
        return JobUtil.convertToAwxJobDetails(awxCommandEntity, serverTimeZone);
      } else {
        // TODO: alert about webhook malfunction
        LOGGER.error(
            "WEBHOOK HAS NOT FIRED for deployment command (ID: {})",
            awxCommandEntity.getAwxJobId());
        return updateCommandUsingAwx(awxCommandEntity);
      }
    }
  }

  private AwxJobDetails updateCommandUsingAwx(AwxJobEntity awxCommandEntity)
      throws AwxServiceException {
    if (awxCommandEntity.getAwxJobId() == null) {
      executeNextQueuedDeployment();
      return JobUtil.convertToAwxJobDetails(awxCommandEntity, serverTimeZone);
    } else {
      CommandDetails commandDetails = awxService.getCommandDetails(awxCommandEntity.getAwxJobId());
      JobStatus commandStatus = commandDetails.getCommand().getStatus();
      updateAwxJob(
          awxCommandEntity,
          commandStatus,
          Utils.toZonedDateTime(commandDetails.getCommand().getStarted(), serverTimeZone),
          Utils.toZonedDateTime(commandDetails.getCommand().getFinished(), serverTimeZone),
          false);
      updateCommand(commandStatus);
      return JobUtil.convertCommandToAwxJobDetails(commandDetails, serverTimeZone);
    }
  }

  private void updateCommand(JobStatus commandStatus) {
    if (Constants.FINISHED_JOB_STATES.contains(commandStatus)) {
      executeNextQueuedDeployment();
    }
  }

  @Transactional
  public AwxJobEntity createCommandRecord(
      AwxJobType type, Deployment activeDeployment, UserDetails userDetails) {
    AwxJobEntity awxCommand = new AwxJobEntity();
    awxCommand.setJobType(type.name());
    awxCommand.setStatus(JobStatus.CREATED.name());
    awxCommand.setDeployment(deploymentRepository.findDeploymentById(activeDeployment.getId()));
    awxCommand.setCreatedBy(userDetails.getUserName());
    deploymentRepository.createAwxJob(awxCommand);
    return awxCommand;
  }

  private String getIocNameParam(IocDeploymentEntity activeDeployment) {
    ZonedDateTime deploymentTime =
        Utils.toZonedDateTime(activeDeployment.getAwxJob().getAwxJobStartedAt(), serverTimeZone);
    String paramPrefix = "name=" + Utils.getIocMetricPrefix(deploymentTime, playbookTransitionTime);
    return paramPrefix + IocUtil.transformIocName(activeDeployment.getNamingName());
  }

  private AwxJobEntity executeCommand(AwxJobEntity commandEntity) throws Exception {

    IocDeploymentEntity activeDeployment = commandEntity.getDeployment();
    IocEntity ioc = activeDeployment.getIoc();
    AwxJobType awxJobType = AwxJobType.valueOf(commandEntity.getJobType());

    String args = getCommandArgs(activeDeployment, awxJobType);

    if (ioc.getGitProjectId() == null) {
      throw new UnprocessableEntityException("IOC's Gitlab project ID is invalid");
    }

    JobTemplate ccceTemplate = awxService.getJobTemplate(awxService.getJobTemplateName());
    List<Credential> credentials =
        awxService.getCredentials(ccceTemplate.getRelated().getCredentials());
    if (credentials.isEmpty()) {
      throw new EntityNotFoundException(
          "Credentials for Job template (name:" + awxService.getJobTemplateName() + ") not found");
    }

    Command command = null;
    Exception awxError = null;

    try {
      String module = "service";
      String extra = "";
      command =
          awxService.launchAdHocCommand(
              ccceTemplate.getInventory(),
              activeDeployment.getHostFqdn(),
              credentials.get(0),
              module,
              args,
              extra);
    } catch (Exception e) {
      awxError = e;
      LOGGER.error(e.getMessage(), e);
    }

    if (command != null) {
      // UPDATE command record
      commandEntity.setAwxJobId(command.getId());
      commandEntity.setStatus(command.getStatus().name());
      commandEntity.setAwxJobStartedAt(command.getStarted());
      commandEntity.setCreatedAt(command.getCreated());
      deploymentRepository.updateAwxJob(commandEntity);

      LOGGER.debug(
          "IOC [{}] has been [{}] on host [{}] by [{}]",
          ioc.getNamingName(),
          awxJobType.equals(AwxJobType.START) ? "STARTED" : "STOPPED",
          activeDeployment.getHostFqdn(),
          activeDeployment.getCreatedBy());
      return commandEntity;
    } else {
      updateCommandJob(commandEntity, awxError != null ? awxError.getMessage() : "");
      executeNextQueuedDeployment();

      if (awxError != null) {
        throw awxError;
      }
      throw new ServiceException("Error during creating command");
    }
  }

  private String getCommandArgs(IocDeploymentEntity activeDeployment, AwxJobType jobType) {
    if (Constants.COMMAND_JOB_TYPES.contains(jobType)) {
      final String state = jobType.equals(AwxJobType.START) ? SET_STATE_STARTED : SET_STATE_STOPPED;
      final String enabled =
          jobType.equals(AwxJobType.START) ? SET_SERVICE_ENABLED : SET_SERVICE_DISABLED;
      return StringUtils.join(
          ImmutableList.of(getIocNameParam(activeDeployment), state, enabled), AWX_ARG_SEPARATOR);
    }
    // To avoid development mistakes
    throw new ServiceException("Invalid command job type: " + jobType.name());
  }

  /**
   * Checks if an IOC(version) has ever been deployed.
   *
   * @param iocId the IOC-version Id that has to be checked if it was deployed
   * @return <code>true</code> if IOC(version) is/was deployed <code>false</code> if IOC(version)
   *     was never deployed
   */
  public boolean checkIfIocVersionWasDeployed(long iocId) {
    long deploymentCount = deploymentRepository.countAllDeploymentsForIoc(iocId);

    if (deploymentCount == 0) {
      return false;
    }

    return true;
  }

  /**
   * Counts the (actual) successful deployments on a host based by the NetBox ID.
   *
   * @param netBoxId the host's NetBox ID
   * @param isVm th is host a Vm or not
   * @return The number of (actual) successful deployments on a host
   */
  public long countTotalDeploymentsForPaging(Long netBoxId, Boolean isVm) {
    return iocRepository.countAssociatedDeployments(netBoxId, isVm);
  }

  @Scheduled(fixedDelayString = "${webhook.check.period.sec}", timeUnit = TimeUnit.SECONDS)
  public void updateAwxEntitiesIfNeeded() {
    List<AwxJobEntity> ongoingJobs = deploymentRepository.findAllOngoing();
    LOGGER.info("Periodic status check of ongoing jobs (count: {})", ongoingJobs.size());
    for (AwxJobEntity job : ongoingJobs) {
      if (Constants.ALL_DEPLOYMENT_TYPES.contains(job.getJobType())) {
        if (!isWebHookOk(job)) {
          LOGGER.error("WEBHOOK HAS NOT FIRED for AWX job (ID: {})", job.getAwxJobId());
          try {
            updateJobAndDeploymentUsingAwx(job);
          } catch (Exception e) {
            LOGGER.error("Scheduled update failed for AWX job: {}", job.getAwxJobId());
          }
        }
      } else if (Constants.COMMAND_TYPES.contains(job.getJobType())) {
        if (!isWebHookOkCommand(job)) {
          LOGGER.error("WEBHOOK HAS NOT FIRED for AWX command (ID: {})", job.getAwxJobId());
          try {
            updateCommandUsingAwx(job);
          } catch (Exception e) {
            LOGGER.error("Scheduled update failed for AWX command: {}", job.getAwxJobId());
          }
        }
      }
    }
  }

  public PagedJobResponse findAllJobs(
      Long iocId,
      JobKind type,
      JobStatusQuery status,
      String user,
      String query,
      LocalDateTime startDate,
      LocalDateTime endDate,
      String inputTimeZone,
      String hostId,
      JobOrder orderBy,
      Boolean isAsc,
      Integer page,
      Integer limit) {

    final String timeZone = inputTimeZone == null ? serverTimeZone : inputTimeZone;

    NetBoxHostIdentifier hostIdentifier = HostService.decodeHostIdentifier(hostId);
    Date normalizedStartDate =
        normalizeDate(validateAndApplyTimeZone(startDate, timeZone), serverTimeZone);
    Date normalizedEndDate =
        normalizeDate(validateAndApplyTimeZone(endDate, timeZone), serverTimeZone);

    PagingLimitDto pagingLimitDto = utils.pageLimitConverter(page, limit);
    String statusString = status == null ? null : status.name();
    Long netBoxHostId = hostIdentifier == null ? null : hostIdentifier.getNetBoxHostId();
    Boolean hostIsVm = hostIdentifier == null ? null : hostIdentifier.isVm();

    List<AwxJobEntity> operationEntities =
        deploymentRepository.findAllAwxJobs(
            iocId,
            type,
            statusString,
            user,
            query,
            normalizedStartDate,
            normalizedEndDate,
            netBoxHostId,
            hostIsVm,
            orderBy,
            isAsc,
            pagingLimitDto.getPage(),
            pagingLimitDto.getLimit());

    List<Job> operations =
        operationEntities.stream()
            .map(oe -> JobUtil.convertToBaseJob(oe, deploymentRepository, serverTimeZone))
            .collect(Collectors.toList());

    long totalCount =
        deploymentRepository.countJobsForPaging(
            iocId,
            type,
            statusString,
            user,
            query,
            normalizedStartDate,
            normalizedEndDate,
            netBoxHostId,
            hostIsVm);
    return new PagedJobResponse(
        totalCount,
        operations.size(),
        pagingLimitDto.getPage(),
        pagingLimitDto.getLimit(),
        operations);
  }

  /**
   * Finds, and extract JobDetails based on the jobId. The AWX job URL will only be generated if the
   * user is logged in! (The host information is extracted from the DB entity, and will not be
   * resolved on-the-fly)
   *
   * @param jobId the Job ID
   * @return the Job details based on a certain operation ID
   * @throws NoResultException if the operation cannot be found with the given ID
   */
  public JobDetails findJobById(long jobId) throws NoResultException {
    AwxJobEntity job = deploymentRepository.findJobById(jobId);
    if (BATCH_JOB_TYPES.contains(AwxJobType.valueOf(job.getJobType()))) {

      List<IocDeploymentEntity> deploymentsForBatchJob =
          deploymentRepository.findDeploymentsForBatchJob(job.getId());
      Map<Long, GitlabProject> projectsForDeployments =
          gitLabService.getProjectsForDeployments(deploymentsForBatchJob);
      Map<Long, String> shortIdsForDeployments =
          gitLabService.getShortIdsForDeployments(deploymentsForBatchJob);

      return JobUtil.convertToBatchJobDetails(
          projectsForDeployments,
          shortIdsForDeployments,
          job,
          deploymentsForBatchJob,
          awxService.generateAwxJobUrl(job),
          serverTimeZone,
          hostService);
    }
    Deployment deployment = extractDeploymentDetails(job.getDeployment());
    GitlabProject project =
        gitLabService.getNullableGitlabProject(
            deployment == null ? null : deployment.getGitProjectId());
    return JobUtil.convertToJobDetails(
        project,
        job,
        deployment,
        awxService.generateAwxJobUrl(job),
        deploymentAlertGenerator,
        serverTimeZone);
  }

  public void updateActiveDeploymentHost(
      IocEntity ioc, IocDeploymentEntity activeDeployment, String hostId, String username) {
    NetBoxHostIdentifier hostIdentifier = HostService.decodeHostIdentifier(hostId);
    HostInfoWithId host =
        hostService.getHostByNetBoxId(hostIdentifier.getNetBoxHostId(), hostIdentifier.isVm());

    IocDeploymentEntity modifiedDeployment = new IocDeploymentEntity();
    modifiedDeployment.setIoc(activeDeployment.getIoc());
    modifiedDeployment.setAwxJob(activeDeployment.getAwxJob());
    modifiedDeployment.setHostFqdn(host.getFqdn());
    modifiedDeployment.setHostName(host.getName());
    modifiedDeployment.setNetBoxHostId(host.getId());
    modifiedDeployment.setVm(host.isVm());
    modifiedDeployment.setHostNetwork(host.getNetwork());
    modifiedDeployment.setCreatedBy(username);
    modifiedDeployment.setCreatedAt(new Date());
    modifiedDeployment.setDeploymentType(activeDeployment.getDeploymentType());
    modifiedDeployment.setPending(false);
    modifiedDeployment.setSourceVersion(activeDeployment.getSourceVersion());
    modifiedDeployment.setNamingName(activeDeployment.getNamingName());
    modifiedDeployment.setGitProjectId(activeDeployment.getGitProjectId());
    modifiedDeployment.setGitProjectUrl(activeDeployment.getGitProjectUrl());

    deploymentRepository.createDeployment(modifiedDeployment);

    ioc.setLatestSuccessfulDeployment(modifiedDeployment);
    iocRepository.updateIoc(ioc);
  }

  public void setServerTimeZone(String serverTimeZone) {
    this.serverTimeZone = serverTimeZone;
  }
}
