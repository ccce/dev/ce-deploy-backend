/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.service.internal.statistics;

import eu.ess.ics.ccce.common.Utils;
import eu.ess.ics.ccce.repository.api.IDeploymentStatRepository;
import eu.ess.ics.ccce.repository.api.IIocRepository;
import eu.ess.ics.ccce.repository.entity.DeploymentStatisticsEntity;
import eu.ess.ics.ccce.repository.entity.IocDeploymentEntity;
import eu.ess.ics.ccce.service.internal.status.StatusService;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.apache.commons.lang3.BooleanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 */
@Component
public class StatisticsCronService {

  private static final Logger LOGGER = LoggerFactory.getLogger(StatisticsCronService.class);

  @Value("${server.time-zone}")
  private String serverTimeZone;

  private final StatusService statusService;
  private final IDeploymentStatRepository deploymentStatRepository;
  private final IIocRepository iocRepository;

  public StatisticsCronService(
      StatusService statusService,
      IDeploymentStatRepository deploymentStatRepository,
      IIocRepository iocRepository) {
    this.statusService = statusService;
    this.deploymentStatRepository = deploymentStatRepository;
    this.iocRepository = iocRepository;
    ;
  }

  @Scheduled(cron = "${statistics.deployments.cron}")
  public void collectDeploymentStatistics() {

    LOGGER.debug("Started collecting active IOCs from Prometheus");

    try {
      List<IocDeploymentEntity> allActualDeployments =
          iocRepository.findIocsWithActiveIocDeployments();

      Map<String, Long> networkStat =
          allActualDeployments.stream()
              .filter(
                  d ->
                      BooleanUtils.toBoolean(
                          statusService.checkDeployedIocIsActive(
                              d.getNamingName(),
                              Utils.toZonedDateTime(
                                  d.getAwxJob().getAwxJobStartedAt(), serverTimeZone),
                              d.getHostFqdn())))
              .collect(
                  Collectors.groupingBy(
                      IocDeploymentEntity::getHostNetwork, Collectors.counting()));

      if (!networkStat.isEmpty()) {
        storeStatistics(networkStat);
      }
    } catch (Exception e) {
      LOGGER.error("Error while running IOC deployment statistics scheduler", e);
    }

    LOGGER.debug("Active IOC collection finished");
  }

  private void storeStatistics(Map<String, Long> statistics) {
    if ((statistics == null) || (statistics.isEmpty())) {
      return;
    }

    statistics.forEach(
        (network, iocCount) -> {
          DeploymentStatisticsEntity dst = new DeploymentStatisticsEntity();
          dst.setNetwork(network);
          dst.setIocCount(iocCount);

          try {
            deploymentStatRepository.saveStatistics(dst);
          } catch (Exception e) {
            LOGGER.error("Error while trying to store deployment statistic", e);
          }
        });
  }
}
