package eu.ess.ics.ccce.service.external.netbox.serializer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import eu.ess.ics.ccce.service.external.netbox.model.*;
import java.io.IOException;

/**
 * @author <a href="mailto:domonkos.gulyas@ess.eu">Domonkos Gulyas</a>
 */
public class NetBoxBaseObjectSerializer extends StdDeserializer<BaseNetBoxObject> {
  public NetBoxBaseObjectSerializer() {
    super(BaseNetBoxObject.class);
  }

  @Override
  public BaseNetBoxObject deserialize(JsonParser jsonParser, DeserializationContext co)
      throws IOException {
    JsonNode node = jsonParser.getCodec().readTree(jsonParser);

    if (node.has("address")) {
      return jsonParser.getCodec().treeToValue(node, NetBoxIpAddress.class);
    } else if (node.has("primary_ip")) {
      return jsonParser.getCodec().treeToValue(node, NetBoxHost.class);
    } else if (node.has("vid")) {
      return jsonParser.getCodec().treeToValue(node, NetBoxVlan.class);
    } else if (node.has("vlan_count")) {
      return jsonParser.getCodec().treeToValue(node, NetBoxVlanGroup.class);
    } else if (node.has("untagged_vlan")) {
      return jsonParser.getCodec().treeToValue(node, NetBoxInterface.class);
    }

    throw new IllegalArgumentException("Cannot determine subclass type");
  }
}
