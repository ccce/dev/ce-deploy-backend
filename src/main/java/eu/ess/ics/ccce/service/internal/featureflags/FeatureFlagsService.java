/*
 * Copyright (C) 2024 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.service.internal.featureflags;

import eu.ess.ics.ccce.configuration.FeatureFlags;
import eu.ess.ics.ccce.exceptions.FeatureDisabledException;
import eu.ess.ics.ccce.exceptions.InputValidationException;
import eu.ess.ics.ccce.rest.model.admin.request.FeatureState;
import eu.ess.ics.ccce.rest.model.admin.request.ToggleFeatureRequest;
import eu.ess.ics.ccce.service.internal.featureflags.dto.Feature;
import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author <a href="mailto:domonkos.gulyas@ess.eu">Domonkos Gulyas</a>
 */
@Service
public class FeatureFlagsService {
  private final FeatureFlags featureFlags;
  private final Set<Feature> disabledFeatures;
  private final Set<Feature> enabledFeatures;

  public FeatureFlagsService(FeatureFlags featureFlags) {
    this.featureFlags = featureFlags;
    this.disabledFeatures = Collections.synchronizedSet(new HashSet<>());
    this.enabledFeatures = Collections.synchronizedSet(new HashSet<>());
  }

  public void checkFeature(final String tagName, final String operationName) {
    if (isDisabled(tagName, operationName)) {
      throw new FeatureDisabledException(tagName, operationName);
    }
  }

  @Transactional
  public void toggleFeature(ToggleFeatureRequest request) {
    Feature feature = new Feature(request.tagName(), request.operationName());
    if (FeatureState.ENABLE.equals(request.state())) {
      if (!isDisabled(request.tagName(), request.operationName())) {
        throw new InputValidationException("The requested feature is enabled already.");
      }
      enabledFeatures.add(feature);
      disabledFeatures.remove(feature);
    } else {
      if (isDisabled(request.tagName(), request.operationName())) {
        throw new InputValidationException("The requested feature is disabled already.");
      }
      enabledFeatures.remove(feature);
      disabledFeatures.add(feature);
    }
  }

  private boolean disabledByConfiguration(final String tagName, final String operationName) {

    if (enabledByAdmin(tagName, operationName)) {
      return false;
    }

    Map<String, Map<String, Boolean>> featureGroupMap = featureFlags.getEnabled();
    if (featureGroupMap.containsKey(tagName)) {
      Map<String, Boolean> methodMap = featureGroupMap.get(tagName);
      return methodMap.getOrDefault(operationName, false);
    }

    return false;
  }

  private boolean isDisabled(String tagName, String operationName) {
    return disabledByAdmin(tagName, operationName)
        || disabledByConfiguration(tagName, operationName);
  }

  private boolean disabledByAdmin(final String tagName, final String operationName) {
    return disabledFeatures.contains(new Feature(tagName, operationName));
  }

  private boolean enabledByAdmin(final String tagName, final String operationName) {
    return disabledFeatures.contains(new Feature(tagName, operationName));
  }
}
