/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.service.external.netbox;

import eu.ess.ics.ccce.exceptions.EntityNotFoundException;
import eu.ess.ics.ccce.exceptions.NetBoxServiceException;
import eu.ess.ics.ccce.exceptions.RemoteServiceException;
import eu.ess.ics.ccce.rest.model.host.response.HostInfo;
import eu.ess.ics.ccce.rest.model.host.response.HostInfoWithId;
import eu.ess.ics.ccce.service.external.common.HttpClientService;
import eu.ess.ics.ccce.service.external.netbox.graphql.GraphQLClientService;
import eu.ess.ics.ccce.service.external.netbox.graphql.model.*;
import eu.ess.ics.ccce.service.external.netbox.model.*;
import eu.ess.ics.ccce.service.internal.PagingLimitDto;
import eu.ess.ics.ccce.service.internal.host.HostService;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.BiFunction;
import java.util.stream.Collectors;
import okhttp3.Headers;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

/**
 * @author <a href="mailto:domonkos.gulyas@ess.eu">Domonkos Gulyas</a>
 */
@Service
public class NetBoxService {

  private static final Logger LOGGER = LoggerFactory.getLogger(NetBoxService.class);
  private static final String VIRTUAL_MACHINES_URL = "/api/virtualization/virtual-machines/";
  private static final String DEVICES_URL = "/api/dcim/devices/";
  private static final String IP_ADDRESSES_URL = "/api/ipam/ip-addresses/";
  private static final String DEVICE_INTERFACES_URL = "/api/dcim/interfaces/";
  private static final String VM_INTERFACES_URL = "/api/virtualization/interfaces/";
  private static final String VLANS_URL = "/api/ipam/vlans/";
  private static final String VLAN_GROUPS_URL = "/api/ipam/vlan-groups/";

  private static final String TYPE_VM = "VirtualMachine";

  @Value("${netbox.server.address}")
  private String netBoxServerAddress;

  @Value("${netbox.server.access.token}")
  private String netBoxToken;

  @Value("${netbox.host.groups.filter}")
  private List<String> networkScopes;

  private final HttpClientService httpClientService;
  private final GraphQLClientService graphQLClientService;

  public NetBoxService(
      HttpClientService httpClientService, GraphQLClientService graphQLClientService) {
    this.httpClientService = httpClientService;
    this.graphQLClientService = graphQLClientService;
  }

  @Cacheable(value = "host_by_netbox_id", cacheManager = "netBoxCacheManager")
  public HostInfo findHostById(Long id, Boolean isVm) {

    if (id == null || isVm == null) {
      throw new EntityNotFoundException("NetBox host not found.");
    }

    GraphQLHostResponse graphQLHostResponse =
        isVm ? graphQLClientService.fetchSingleVM(id) : graphQLClientService.fetchSingleDevice(id);

    if (graphQLHostResponse == null) {
      throw new EntityNotFoundException("NetBox host not found.");
    }

    return convertToHostInfo(id, isVm, graphQLHostResponse);
  }

  public void checkHostExists(Long id, Boolean isVm) {
    if (!(isVm
        ? graphQLClientService.isVirtualMachineExists(id)
        : graphQLClientService.isDeviceExists(id))) {
      throw new EntityNotFoundException("NetBox host not found.");
    }
  }

  public HostInfo hostWithNetworkDetails(NetBoxHost host) {
    NetBoxIpAddress ipAddress = findIpAddressById(host.getIpdAddressId());

    final NetBoxInterface primaryInterface =
        host.isVm()
            ? findPrimaryInterface(findInterfacesForVM(host.getId()), ipAddress)
            : findPrimaryInterface(findInterfacesForDevice(host.getId()), ipAddress);
    NetBoxVlan vlan = findVlanForHost(primaryInterface);

    String vlanName = vlan == null ? null : vlan.getName();
    String vlanGroupName = vlan == null ? null : vlan.getGroupName();

    return new HostInfo(
        host.getId(),
        ipAddress.getDnsName(),
        host.getName(),
        vlanGroupName,
        host.getDescription(),
        vlanName,
        host.getType(),
        host.isVm(),
        convertTags(host.getTags()));
  }

  public List<HostInfoWithId> addNetworkDetailsToHosts(List<NetBoxHost> hostList) {
    List<Long> ipIdList =
        hostList.stream().map(NetBoxHost::getIpdAddressId).collect(Collectors.toList());
    List<NetBoxIpAddress> ipAddresses =
        findIpAddressByIds(ipIdList).getResults().stream()
            .map(NetBoxIpAddress.class::cast)
            .collect(Collectors.toList());

    List<Long> deviceInterfaceIds =
        ipAddresses.stream()
            .filter(NetBoxIpAddress::isAssignedToDeviceInterface)
            .map(NetBoxIpAddress::getAssignedObjectId)
            .collect(Collectors.toList());

    List<Long> vmInterfaceIds =
        ipAddresses.stream()
            .filter(NetBoxIpAddress::isAssignedToVirtualMachineInterface)
            .map(NetBoxIpAddress::getAssignedObjectId)
            .collect(Collectors.toList());

    List<NetBoxInterface> deviceInterfaces =
        deviceInterfaceIds.isEmpty()
            ? new ArrayList<>()
            : findDeviceInterfacesByIds(deviceInterfaceIds).getResults().stream()
                .map(NetBoxDeviceInterface.class::cast)
                .collect(Collectors.toList());
    List<NetBoxInterface> vmInterfaces =
        vmInterfaceIds.isEmpty()
            ? new ArrayList<>()
            : findVMInterfacesByIds(vmInterfaceIds).getResults().stream()
                .map(NetBoxVMInterface.class::cast)
                .collect(Collectors.toList());

    List<Long> vlanIds =
        deviceInterfaces.stream()
            .map(NetBoxInterface::getVlanId)
            .filter(Objects::nonNull)
            .collect(Collectors.toList());
    vlanIds.addAll(
        vmInterfaces.stream()
            .map(NetBoxInterface::getVlanId)
            .filter(Objects::nonNull)
            .collect(Collectors.toList()));

    List<NetBoxVlan> vlans =
        findVlansByIds(vlanIds).getResults().stream()
            .map(NetBoxVlan.class::cast)
            .collect(Collectors.toList());

    return hostList.stream()
        .map(
            netBoxHost ->
                assignNetworkDetails(
                    netBoxHost, ipAddresses, vmInterfaces, deviceInterfaces, vlans))
        .map(NetBoxService::applyHostId)
        .collect(Collectors.toList());
  }

  private HostInfo assignNetworkDetails(
      NetBoxHost host,
      List<NetBoxIpAddress> ipAddresses,
      List<NetBoxInterface> vmInterfaces,
      List<NetBoxInterface> deviceInterfaces,
      List<NetBoxVlan> vlans) {

    boolean isVm = host.isVm();
    NetBoxIpAddress ipAddress =
        ipAddresses.stream()
            .filter(netBoxIpAddress -> netBoxIpAddress.getId().equals(host.getIpdAddressId()))
            .findFirst()
            .orElse(null);

    if (ipAddress == null) {
      return new HostInfo(
          host.getId(),
          null,
          host.getName(),
          null,
          host.getDescription(),
          null,
          host.getType(),
          isVm,
          convertTags(host.getTags()));
    }

    NetBoxInterface primaryInterface =
        isVm
            ? findPrimaryInterface(vmInterfaces, ipAddress)
            : findPrimaryInterface(deviceInterfaces, ipAddress);

    NetBoxVlan vlan =
        primaryInterface == null
            ? null
            : vlans.stream()
                .filter(netBoxVlan -> netBoxVlan.getId().equals(primaryInterface.getVlanId()))
                .findFirst()
                .orElse(null);

    String fqdn = ipAddress.getDnsName();
    String vlanName = vlan == null ? null : vlan.getName();
    String vlanGroupName = vlan == null ? null : vlan.getGroupName();

    return new HostInfo(
        host.getId(),
        fqdn,
        host.getName(),
        vlanGroupName,
        host.getDescription(),
        vlanName,
        host.getType(),
        isVm,
        convertTags(host.getTags()));
  }

  @Cacheable(value = "all_hosts_from_netbox", cacheManager = "netBoxCacheManager")
  public List<NetBoxHost> findAllIocHosts() {
    final List<NetBoxHost> allVirtualMachines =
        findTagsAll(this::findVirtualMachines, this.networkScopes).stream()
            .map(NetBoxHost.class::cast)
            .collect(Collectors.toList());

    final List<NetBoxHost> allDevices =
        findTagsAll(this::findDevices, this.networkScopes).stream()
            .map(NetBoxHost.class::cast)
            .collect(Collectors.toList());
    allVirtualMachines.addAll(allDevices);
    return allVirtualMachines;
  }

  @Nullable
  private NetBoxVlan findVlanForHost(NetBoxInterface assignedInterface) {
    if (assignedInterface == null) {
      return null;
    } else {
      return assignedInterface.getUntaggedVlan() == null
          ? null
          : findVlanById(assignedInterface.getUntaggedVlan().getId());
    }
  }

  @Nullable
  private NetBoxInterface findPrimaryInterface(
      List<NetBoxInterface> interfaces, NetBoxIpAddress ipAddress) {
    return ipAddress.isAssignedToInterface()
        ? interfaces.stream()
            .filter(
                netBoxInterface ->
                    Objects.equals(netBoxInterface.getId(), ipAddress.getAssignedObjectId()))
            .findFirst()
            .orElse(null)
        : null;
  }

  private NetBoxVlan findVlanById(Long vlanId) {
    try {
      UriComponentsBuilder ub =
          UriComponentsBuilder.fromUriString(getEndpointUrl(VLANS_URL + vlanId));

      HttpClientService.ServiceResponse<NetBoxVlan> response =
          httpClientService.executeGetRequestAndWrappingWithJackson(
              ub.build().toUri().toString(), createAuthHeader(), NetBoxVlan.class);
      if (HttpClientService.isSuccessHttpStatusCode(response.getStatusCode())) {
        return response.getEntity();
      } else if (HttpStatus.NOT_FOUND.value() == response.getStatusCode()) {
        throw new EntityNotFoundException("NetBox VLAN", vlanId);
      } else {
        LOGGER.error(
            "Fetch device interfaces from NetBox failed response code: "
                + response.getStatusCode());
        throw new NetBoxServiceException("Fetch device interfaces from NetBox failed");
      }
    } catch (MalformedURLException | RemoteServiceException e) {
      LOGGER.error(e.getMessage(), e);
      throw new NetBoxServiceException("Fetch device interfaces from NetBox failed");
    }
  }

  private List<NetBoxInterface> findInterfacesForDevice(Long deviceId) {
    try {
      UriComponentsBuilder ub =
          UriComponentsBuilder.fromUriString(getEndpointUrl(DEVICE_INTERFACES_URL));
      ub.queryParam("device_id", deviceId);

      HttpClientService.ServiceResponse<PagedNetBoxResponse> response =
          httpClientService.executeGetRequestAndWrappingWithJackson(
              ub.build().toUri().toString(), createAuthHeader(), PagedNetBoxResponse.class);
      if (HttpClientService.isSuccessHttpStatusCode(response.getStatusCode())) {
        return response.getEntity().getResults().stream()
            .map(NetBoxInterface.class::cast)
            .collect(Collectors.toList());
      } else {
        LOGGER.error(
            "Fetch device interfaces from NetBox failed response code: "
                + response.getStatusCode());
        throw new NetBoxServiceException("Fetch device interfaces from NetBox failed");
      }
    } catch (MalformedURLException | RemoteServiceException e) {
      LOGGER.error(e.getMessage(), e);
      throw new NetBoxServiceException("Fetch device interfaces from NetBox failed");
    }
  }

  private List<NetBoxInterface> findInterfacesForVM(Long vmId) {
    try {
      UriComponentsBuilder ub =
          UriComponentsBuilder.fromUriString(getEndpointUrl(VM_INTERFACES_URL));
      ub.queryParam("virtual_machine_id", vmId);

      HttpClientService.ServiceResponse<PagedNetBoxResponse> response =
          httpClientService.executeGetRequestAndWrappingWithJackson(
              ub.build().toUri().toString(), createAuthHeader(), PagedNetBoxResponse.class);
      if (HttpClientService.isSuccessHttpStatusCode(response.getStatusCode())) {
        return response.getEntity().getResults().stream()
            .map(NetBoxInterface.class::cast)
            .collect(Collectors.toList());
      } else {
        LOGGER.error(
            "Fetch VM interfaces from NetBox failed response code: " + response.getStatusCode());
        throw new NetBoxServiceException("Fetch VM interfaces from NetBox failed");
      }
    } catch (MalformedURLException | RemoteServiceException e) {
      LOGGER.error(e.getMessage(), e);
      throw new NetBoxServiceException("Fetch VM interfaces from NetBox failed");
    }
  }

  public NetBoxIpAddress findIpAddressById(Long ipdAddressId) {
    try {
      UriComponentsBuilder ub =
          UriComponentsBuilder.fromUriString(getEndpointUrl(IP_ADDRESSES_URL + ipdAddressId));

      HttpClientService.ServiceResponse<NetBoxIpAddress> response =
          httpClientService.executeGetRequestAndWrappingWithJackson(
              ub.build().toUri().toString(), createAuthHeader(), NetBoxIpAddress.class);

      if (HttpClientService.isSuccessHttpStatusCode(response.getStatusCode())) {
        return response.getEntity();
      } else if (HttpStatus.NOT_FOUND.value() == response.getStatusCode()) {
        throw new EntityNotFoundException("NetBox IP address", ipdAddressId);
      } else {
        LOGGER.error("Fetch IP address ID failed response code: " + response.getStatusCode());
        throw new NetBoxServiceException("Fetch IP address ID failed (id: " + ipdAddressId + ")");
      }
    } catch (MalformedURLException | RemoteServiceException e) {
      LOGGER.error(e.getMessage(), e);
      throw new NetBoxServiceException("Fetch IP address ID failed (id: " + ipdAddressId + ")");
    }
  }

  private PagedNetBoxResponse findIpAddressByIds(List<Long> ids) {
    try {
      UriComponentsBuilder ub =
          UriComponentsBuilder.fromUriString(getEndpointUrl(IP_ADDRESSES_URL));

      ub.queryParam("id", ids);
      ub.queryParam("limit", 1000);

      HttpClientService.ServiceResponse<PagedNetBoxResponse> response =
          httpClientService.executeGetRequestAndWrappingWithJackson(
              ub.build().toUri().toString(), createAuthHeader(), PagedNetBoxResponse.class);

      if (HttpClientService.isSuccessHttpStatusCode(response.getStatusCode())) {
        return response.getEntity();
      } else {
        LOGGER.error(
            "Fetch IP addresses from NetBox failed response code: " + response.getStatusCode());
        throw new NetBoxServiceException("Fetch IP addresses from NetBox failed");
      }
    } catch (MalformedURLException | RemoteServiceException e) {
      LOGGER.error(e.getMessage(), e);
      throw new NetBoxServiceException("Fetch IP addresses from NetBox failed");
    }
  }

  private PagedNetBoxResponse findVlansByIds(List<Long> ids) {
    try {
      UriComponentsBuilder ub = UriComponentsBuilder.fromUriString(getEndpointUrl(VLANS_URL));

      ub.queryParam("id", ids);
      ub.queryParam("limit", 1000);

      HttpClientService.ServiceResponse<PagedNetBoxResponse> response =
          httpClientService.executeGetRequestAndWrappingWithJackson(
              ub.build().toUri().toString(), createAuthHeader(), PagedNetBoxResponse.class);

      if (HttpClientService.isSuccessHttpStatusCode(response.getStatusCode())) {
        return response.getEntity();
      } else {
        LOGGER.error("Fetch VLANs from NetBox failed response code: " + response.getStatusCode());
        throw new NetBoxServiceException("Fetch VLANs from NetBox failed");
      }
    } catch (MalformedURLException | RemoteServiceException e) {
      LOGGER.error(e.getMessage(), e);
      throw new NetBoxServiceException("Fetch VLANs from NetBox failed");
    }
  }

  private PagedNetBoxResponse findDeviceInterfacesByIds(List<Long> ids) {
    try {
      UriComponentsBuilder ub =
          UriComponentsBuilder.fromUriString(getEndpointUrl(DEVICE_INTERFACES_URL));

      ub.queryParam("id", ids);
      ub.queryParam("limit", 1000);

      HttpClientService.ServiceResponse<PagedNetBoxResponse> response =
          httpClientService.executeGetRequestAndWrappingWithJackson(
              ub.build().toUri().toString(), createAuthHeader(), PagedNetBoxResponse.class);

      if (HttpClientService.isSuccessHttpStatusCode(response.getStatusCode())) {
        return response.getEntity();
      } else {
        LOGGER.error("Fetch device interfaces failed response code: " + response.getStatusCode());
        throw new NetBoxServiceException("Fetch device interfaces from NetBox failed");
      }
    } catch (MalformedURLException | RemoteServiceException e) {
      LOGGER.error(e.getMessage(), e);
      throw new NetBoxServiceException("Fetch device interfaces from NetBox failed");
    }
  }

  private PagedNetBoxResponse findVMInterfacesByIds(List<Long> ids) {
    try {
      UriComponentsBuilder ub =
          UriComponentsBuilder.fromUriString(getEndpointUrl(VM_INTERFACES_URL));

      ub.queryParam("id", ids);
      ub.queryParam("limit", 1000);

      HttpClientService.ServiceResponse<PagedNetBoxResponse> response =
          httpClientService.executeGetRequestAndWrappingWithJackson(
              ub.build().toUri().toString(), createAuthHeader(), PagedNetBoxResponse.class);

      if (HttpClientService.isSuccessHttpStatusCode(response.getStatusCode())) {
        return response.getEntity();
      } else {
        LOGGER.error("Fetch VM interfaces failed response code: " + response.getStatusCode());
        throw new NetBoxServiceException("Fetch VM interfaces from NetBox failed");
      }
    } catch (MalformedURLException | RemoteServiceException e) {
      LOGGER.error(e.getMessage(), e);
      throw new NetBoxServiceException("Fetch VM interfaces from NetBox failed");
    }
  }

  private List<BaseNetBoxObject> findAll(
      BiFunction<PagingLimitDto, Map<String, String>, PagedNetBoxResponse> findFunction,
      Map<String, String> keyWordArguments) {
    List<BaseNetBoxObject> results = new ArrayList<>();
    int limit = 1000;
    int page = 0;
    long totalCount;

    do {
      PagedNetBoxResponse response =
          findFunction.apply(new PagingLimitDto(page, limit), keyWordArguments);
      results.addAll(response.getResults());
      totalCount = response.getCount();
      page++;
    } while (results.size() < totalCount);

    return results;
  }

  private List<BaseNetBoxObject> findTagsAll(
      BiFunction<PagingLimitDto, Map<String, String>, PagedNetBoxResponse> findFunction,
      List<String> values) {
    List<BaseNetBoxObject> results = new ArrayList<>();

    if (values == null) {
      results.addAll(findAll(findFunction, Map.of("role", "ioc-host")));
    } else {
      values.stream()
          .map(v -> findAll(findFunction, Map.of("role", "ioc-host", "tag", v)))
          .forEach(res -> results.addAll(res));
    }
    return results;
  }

  private PagedNetBoxResponse findDevices(
      PagingLimitDto pagingLimitDto, Map<String, String> keyWordArguments) {
    return findPagedNetboxEntities(DEVICES_URL, "IOC devices", pagingLimitDto, keyWordArguments);
  }

  public PagedNetBoxResponse findVirtualMachines(
      PagingLimitDto pagingLimitDto, Map<String, String> keyWordArguments) {
    return findPagedNetboxEntities(
        VIRTUAL_MACHINES_URL, "IOC virtual machines", pagingLimitDto, keyWordArguments);
  }

  public PagedNetBoxResponse findDeviceInterfaces(PagingLimitDto pagingLimitDto) {
    return findPagedNetboxEntities(DEVICE_INTERFACES_URL, "device interfaces", pagingLimitDto);
  }

  public List<NetBoxInterface> findDeviceInterfacesByVlan(Long vlanId) {
    try {
      UriComponentsBuilder ub =
          UriComponentsBuilder.fromUriString(getEndpointUrl(DEVICE_INTERFACES_URL));

      ub.queryParam("vlan_id", vlanId);
      ub.queryParam("limit", 1000);
      HttpClientService.ServiceResponse<PagedNetBoxResponse> response =
          httpClientService.executeGetRequestAndWrappingWithJackson(
              ub.build().toUri().toString(), createAuthHeader(), PagedNetBoxResponse.class);
      if (HttpClientService.isSuccessHttpStatusCode(response.getStatusCode())) {
        return response.getEntity().getResults().stream()
            .map(NetBoxInterface.class::cast)
            .collect(Collectors.toList());
      } else {
        LOGGER.error(
            "Fetch device interfaces from NetBox failed response code: "
                + response.getStatusCode());
        throw new NetBoxServiceException("Fetch device interfaces from NetBox failed");
      }
    } catch (MalformedURLException | RemoteServiceException e) {
      LOGGER.error(e.getMessage(), e);
      throw new NetBoxServiceException("Fetch device interfaces from NetBox failed");
    }
  }

  public List<NetBoxInterface> findVMInterfacesByVlan(Long vlanId) {
    try {
      UriComponentsBuilder ub =
          UriComponentsBuilder.fromUriString(getEndpointUrl(VM_INTERFACES_URL));
      ub.queryParam("vlan_id", vlanId);
      ub.queryParam("limit", 1000);

      HttpClientService.ServiceResponse<PagedNetBoxResponse> response =
          httpClientService.executeGetRequestAndWrappingWithJackson(
              ub.build().toUri().toString(), createAuthHeader(), PagedNetBoxResponse.class);
      if (HttpClientService.isSuccessHttpStatusCode(response.getStatusCode())) {
        return response.getEntity().getResults().stream()
            .map(NetBoxInterface.class::cast)
            .collect(Collectors.toList());
      } else {
        LOGGER.error(
            "Fetch device interfaces from NetBox failed response code: "
                + response.getStatusCode());
        throw new NetBoxServiceException("Fetch device interfaces from NetBox failed");
      }
    } catch (MalformedURLException | RemoteServiceException e) {
      LOGGER.error(e.getMessage(), e);
      throw new NetBoxServiceException("Fetch device interfaces from NetBox failed");
    }
  }

  public PagedNetBoxResponse findVMInterfaces(PagingLimitDto pagingLimitDto) {
    return findPagedNetboxEntities(VM_INTERFACES_URL, "device interfaces", pagingLimitDto);
  }

  public PagedNetBoxResponse findIpAddresses(PagingLimitDto pagingLimitDto) {
    return findPagedNetboxEntities(IP_ADDRESSES_URL, "IP addresses", pagingLimitDto);
  }

  public PagedNetBoxResponse findVlans(
      PagingLimitDto pagingLimitDto, Map<String, String> keyWordArguments) {
    return findPagedNetboxEntities(VLANS_URL, "VLANs", pagingLimitDto, keyWordArguments);
  }

  public PagedNetBoxResponse findVlanGroups(PagingLimitDto pagingLimitDto) {
    return findPagedNetboxEntities(VLAN_GROUPS_URL, "VLAN groups", pagingLimitDto);
  }

  public PagedNetBoxResponse findPagedNetboxEntities(
      final String url, String entityName, PagingLimitDto pagingLimitDto) {
    return findPagedNetboxEntities(url, entityName, pagingLimitDto, Map.of());
  }

  public PagedNetBoxResponse findPagedNetboxEntities(
      final String url,
      String entityName,
      PagingLimitDto pagingLimitDto,
      Map<String, String> keyWordArguments) {
    try {
      UriComponentsBuilder ub = UriComponentsBuilder.fromUriString(getEndpointUrl(url));
      ub.queryParam("limit", pagingLimitDto.getLimit());
      ub.queryParam("offset", pagingLimitDto.getLimit() * pagingLimitDto.getPage());
      for (Map.Entry<String, String> keyValue : keyWordArguments.entrySet()) {
        ub.queryParam(keyValue.getKey(), keyValue.getValue());
      }

      HttpClientService.ServiceResponse<PagedNetBoxResponse> response =
          httpClientService.executeGetRequestAndWrappingWithJackson(
              ub.build().toUri().toString(), createAuthHeader(), PagedNetBoxResponse.class);
      if (HttpClientService.isSuccessHttpStatusCode(response.getStatusCode())) {
        return response.getEntity();
      } else {
        String msg =
            "Fetch %s from Netbox failed, response code: %d"
                .formatted(entityName, response.getStatusCode());
        LOGGER.error(msg);
        throw new NetBoxServiceException(msg);
      }
    } catch (MalformedURLException | RemoteServiceException e) {
      LOGGER.error(e.getMessage(), e);
      throw new NetBoxServiceException("Fetch %s from NetBox failed".formatted(entityName));
    }
  }

  private NetBoxDevice findDeviceById(Long id) {
    try {
      UriComponentsBuilder ub =
          UriComponentsBuilder.fromUriString(getEndpointUrl(DEVICES_URL + id));

      HttpClientService.ServiceResponse<NetBoxDevice> response =
          httpClientService.executeGetRequestAndWrappingWithJackson(
              ub.build().toUri().toString(), createAuthHeader(), NetBoxDevice.class);

      if (HttpClientService.isSuccessHttpStatusCode(response.getStatusCode())) {
        return response.getEntity();
      } else if (HttpStatus.NOT_FOUND.value() == response.getStatusCode()) {
        throw new EntityNotFoundException("NetBox device", id);
      } else {
        LOGGER.error("Find device from NetBox by ID response code: " + response.getStatusCode());
        throw new NetBoxServiceException("Fetch device by name ID (ID: " + id + ")");
      }
    } catch (MalformedURLException | RemoteServiceException e) {
      LOGGER.error(e.getMessage(), e);
      throw new NetBoxServiceException("Fetch device by ID failed (ID: " + id + ")");
    }
  }

  private NetBoxVirtualMachine findVirtualMachineById(Long id) {
    try {
      UriComponentsBuilder ub =
          UriComponentsBuilder.fromUriString(getEndpointUrl(VIRTUAL_MACHINES_URL + id));

      HttpClientService.ServiceResponse<NetBoxVirtualMachine> response =
          httpClientService.executeGetRequestAndWrappingWithJackson(
              ub.build().toUri().toString(), createAuthHeader(), NetBoxVirtualMachine.class);

      if (HttpClientService.isSuccessHttpStatusCode(response.getStatusCode())) {
        return response.getEntity();
      } else if (HttpStatus.NOT_FOUND.value() == response.getStatusCode()) {
        throw new EntityNotFoundException("NetBox virtual machine", id);
      } else {
        LOGGER.error(
            "Find virtual machine from NetBox by ID response code: " + response.getStatusCode());
        throw new NetBoxServiceException("Fetch virtual machines by ID failed (id: " + id + ")");
      }
    } catch (MalformedURLException | RemoteServiceException e) {
      LOGGER.error(e.getMessage(), e);
      throw new NetBoxServiceException("Fetch virtual machines by ID failed (name: " + id + ")");
    }
  }

  public BaseNetBoxObject findVirtualMachineByName(String name) {
    try {
      UriComponentsBuilder ub =
          UriComponentsBuilder.fromUriString(getEndpointUrl(VIRTUAL_MACHINES_URL));
      ub.queryParam("name", name);

      HttpClientService.ServiceResponse<PagedNetBoxResponse> response =
          httpClientService.executeGetRequestAndWrappingWithJackson(
              ub.build().toUri().toString(), createAuthHeader(), PagedNetBoxResponse.class);

      if (HttpClientService.isSuccessHttpStatusCode(response.getStatusCode())) {
        PagedNetBoxResponse responseBody = response.getEntity();
        if (responseBody.getResults().isEmpty()) {
          throw new EntityNotFoundException("Virtual machine", name);
        }
        return responseBody.getResults().get(0);
      } else {
        LOGGER.error(
            "Find virtual machine from NetBox by name response code: " + response.getStatusCode());
        throw new NetBoxServiceException(
            "Fetch virtual machines by name failed (name: " + name + ")");
      }
    } catch (MalformedURLException | RemoteServiceException e) {
      LOGGER.error(e.getMessage(), e);
      throw new NetBoxServiceException(
          "Fetch virtual machines by name failed (name: " + name + ")");
    }
  }

  public BaseNetBoxObject findDeviceByName(String name) {
    try {
      UriComponentsBuilder ub = UriComponentsBuilder.fromUriString(getEndpointUrl(DEVICES_URL));
      ub.queryParam("name", name);

      HttpClientService.ServiceResponse<PagedNetBoxResponse> response =
          httpClientService.executeGetRequestAndWrappingWithJackson(
              ub.build().toUri().toString(), createAuthHeader(), PagedNetBoxResponse.class);

      if (HttpClientService.isSuccessHttpStatusCode(response.getStatusCode())) {
        PagedNetBoxResponse responseBody = response.getEntity();
        if (responseBody.getResults().isEmpty()) {
          throw new EntityNotFoundException("Device", name);
        }
        return responseBody.getResults().get(0);
      } else {
        LOGGER.error("Find device from NetBox by name response code: " + response.getStatusCode());
        throw new NetBoxServiceException("Fetch device by name failed (name: " + name + ")");
      }
    } catch (MalformedURLException | RemoteServiceException e) {
      LOGGER.error(e.getMessage(), e);
      throw new NetBoxServiceException("Fetch device by name failed (name: " + name + ")");
    }
  }

  public List<NetBoxHostIdentifier> findHostIdentifiersByNetworkData(
      String network, String networkScope) {

    List<BaseNetBoxObject> matchingVlans =
        findAll(this::findVlans, Map.of("q", network)).stream()
            .filter(vlan -> filterByNetworkScope(vlan, networkScope))
            .collect(Collectors.toList());

    List<NetBoxInterface> deviceInterfaces = new ArrayList<>();
    List<NetBoxInterface> vmInterfaces = new ArrayList<>();

    for (BaseNetBoxObject vlan : matchingVlans) {
      deviceInterfaces.addAll(this.findDeviceInterfacesByVlan(vlan.getId()));
      vmInterfaces.addAll(this.findDeviceInterfacesByVlan(vlan.getId()));
    }

    List<NetBoxHostIdentifier> result =
        deviceInterfaces.stream()
            .map(
                netBoxInterface ->
                    new NetBoxHostIdentifier(netBoxInterface.getAssignedObjectId(), false))
            .collect(Collectors.toList());

    result.addAll(
        vmInterfaces.stream()
            .map(
                netBoxInterface ->
                    new NetBoxHostIdentifier(netBoxInterface.getAssignedObjectId(), true))
            .collect(Collectors.toList()));

    return result;
  }

  @NotNull
  private boolean filterByNetworkScope(BaseNetBoxObject vlan, String networkScope) {
    return networkScope == null || ((NetBoxVlan) vlan).getGroupName().contains(networkScope);
  }

  private String getEndpointUrl(String endpointPostfix) throws MalformedURLException {
    return new URL(new URL(netBoxServerAddress), endpointPostfix).toString();
  }

  private Headers createAuthHeader() {
    return Headers.of("Authorization", "Token " + netBoxToken);
  }

  private static List<String> convertTags(List<NetBoxPropertyPreview> tagsPreview) {
    return tagsPreview.stream().map(NetBoxPropertyPreview::getDisplay).collect(Collectors.toList());
  }

  private static HostInfoWithId applyHostId(HostInfo host) {
    return new HostInfoWithId(host, HostService.encodeHostIdentifier(host.getHostIdentifier()));
  }

  @NotNull
  private static HostInfo convertToHostInfo(
      Long id, Boolean isVm, GraphQLHostResponse graphQLHostResponse) {
    PrimaryIp primaryIp = graphQLHostResponse.getPrimaryIp();
    Interface assignedObject = primaryIp == null ? null : primaryIp.getAssignedObject();
    Vlan vlan = assignedObject == null ? null : assignedObject.getUntaggedVlan();
    VlanGroup vlanGroup = vlan == null ? null : vlan.getGroup();

    String fqdn = primaryIp == null ? null : primaryIp.getDnsName();
    List<String> tags = graphQLHostResponse.getTags().stream().map(Tag::getName).toList();
    String deviceType = getDeviceType(graphQLHostResponse, isVm);

    return new HostInfo(
        id,
        fqdn,
        graphQLHostResponse.getName(),
        vlanGroup == null ? null : vlanGroup.getName(),
        graphQLHostResponse.getDescription(),
        vlan == null ? null : vlan.getName(),
        deviceType,
        isVm,
        tags);
  }

  @Nullable
  private static String getDeviceType(GraphQLHostResponse graphQLHostResponse, boolean isVm) {
    return isVm
        ? TYPE_VM
        : (graphQLHostResponse.getDeviceType() == null
            ? null
            : graphQLHostResponse.getDeviceType().getSlug());
  }
}
