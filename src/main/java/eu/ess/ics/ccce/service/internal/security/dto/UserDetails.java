/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.service.internal.security.dto;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.security.core.GrantedAuthority;

/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 */
public class UserDetails {

  Collection<RoleAuthority> authorities;
  private String userName;
  private String fullName;
  private String token;

  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public String getToken() {
    return token;
  }

  public void setToken(String token) {
    this.token = token;
  }

  public Collection<? extends GrantedAuthority> getAuthorities() {
    return authorities;
  }

  public List<String> getRoles() {
    return authorities != null
        ? authorities.stream().map(RoleAuthority::getAuthority).collect(Collectors.toList())
        : Collections.emptyList();
  }

  public void setRoles(List<String> roles) {
    this.authorities =
        roles != null
            ? roles.stream().map(RoleAuthority::new).collect(Collectors.toList())
            : Collections.emptyList();
  }

  public String getFullName() {
    return fullName;
  }

  public void setFullName(String fullName) {
    this.fullName = fullName;
  }
}
