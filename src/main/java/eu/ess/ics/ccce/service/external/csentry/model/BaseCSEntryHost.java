/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.service.external.csentry.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import eu.ess.ics.ccce.common.Utils;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 */
public class BaseCSEntryHost {
  private long id;
  private String fqdn;
  private String name;
  private String scope;

  @SerializedName("is_ioc")
  @JsonProperty("is_ioc")
  private boolean ioc;

  @SerializedName("device_type")
  @JsonProperty("device_type")
  private String deviceType;

  private String description;
  private String user;

  @SerializedName("created_at")
  @JsonProperty("created_at")
  private Date createdAt;

  @SerializedName("ansible_vars")
  @JsonProperty("ansible_vars")
  private Map<String, Object> ansibleVars;

  @SerializedName("ansible_groups")
  @JsonProperty("ansible_groups")
  private List<String> ansibleGroups;

  public BaseCSEntryHost(
      long id,
      String fqdn,
      String name,
      String scope,
      boolean ioc,
      String deviceType,
      String description,
      String user,
      Date createdAt,
      Map<String, Object> ansibleVars,
      List<String> ansibleGroups) {
    this.id = id;
    this.fqdn = fqdn;
    this.name = name;
    this.scope = scope;
    this.ioc = ioc;
    this.deviceType = deviceType;
    this.description = description;
    this.user = user;
    this.createdAt = Utils.cloneDate(createdAt);
    this.ansibleVars = ansibleVars;
    this.ansibleGroups = ansibleGroups;
  }

  public long getId() {
    return id;
  }

  public String getFqdn() {
    return fqdn;
  }

  public void setFqdn(String fqdn) {
    this.fqdn = fqdn;
  }

  public String getScope() {
    return scope;
  }

  public boolean isIoc() {
    return ioc;
  }

  public String getDeviceType() {
    return deviceType;
  }

  public String getDescription() {
    return description;
  }

  public String getUser() {
    return user;
  }

  public Date getCreatedAt() {
    return Utils.cloneDate(createdAt);
  }

  public Map<String, Object> getAnsibleVars() {
    return ansibleVars;
  }

  public String getName() {
    return name;
  }

  public List<String> getAnsibleGroups() {
    return ansibleGroups;
  }
}
