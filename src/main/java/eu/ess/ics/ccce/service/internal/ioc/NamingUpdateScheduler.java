/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.service.internal.ioc;

import eu.ess.ics.ccce.repository.api.IIocRepository;
import eu.ess.ics.ccce.repository.entity.IocEntity;
import eu.ess.ics.ccce.service.external.naming.NamingService;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 */
@Component
public class NamingUpdateScheduler {

  private static final Logger LOGGER = LoggerFactory.getLogger(NamingUpdateScheduler.class);

  private final NamingService namingService;
  private final IIocRepository iocRepository;
  private final IocService iocService;

  public NamingUpdateScheduler(
      NamingService namingService, IIocRepository iocRepository, IocService iocService) {
    this.namingService = namingService;
    this.iocRepository = iocRepository;
    this.iocService = iocService;
  }

  /**
   * Regularly checks if name has been changed in Naming, and updates name in DB if it has been
   * changed, and IOC has not been deployed.
   */
  @Scheduled(cron = "${naming.update.names.cron}")
  public void checkAndUpdateNames() {
    try {
      checkNames();
    } catch (Exception e) {
      LOGGER.error("Error while running naming-update scheduler", e);
    }
  }

  private void checkNames() {
    List<IocEntity> allIoc =
        iocRepository.findAll(null, null, null, null, null, null, 0, Integer.MAX_VALUE, true);

    for (IocEntity i : allIoc) {
      try {
        iocService.checkOrUpdateIocFromNamingService(i);
      } catch (Exception e) {
        LOGGER.error("Error while trying to automatically update naming-name for IOC", e);
      }
    }
  }
}
