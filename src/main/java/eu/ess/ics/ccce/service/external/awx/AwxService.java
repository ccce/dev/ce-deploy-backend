/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.service.external.awx;

import static eu.ess.ics.ccce.common.Constants.UNDEPLOYMENT_TYPES;

import com.google.common.collect.ImmutableMap;
import eu.ess.ics.ccce.common.Constants;
import eu.ess.ics.ccce.exceptions.AwxServiceException;
import eu.ess.ics.ccce.exceptions.EntityNotFoundException;
import eu.ess.ics.ccce.exceptions.RemoteServiceException;
import eu.ess.ics.ccce.repository.entity.AwxJobEntity;
import eu.ess.ics.ccce.rest.model.job.response.AwxJobLog;
import eu.ess.ics.ccce.service.external.awx.model.Credential;
import eu.ess.ics.ccce.service.external.awx.model.Credentials;
import eu.ess.ics.ccce.service.external.awx.model.Hosts;
import eu.ess.ics.ccce.service.external.awx.model.Inventories;
import eu.ess.ics.ccce.service.external.awx.model.JobEvents;
import eu.ess.ics.ccce.service.external.awx.model.request.NewCommandBody;
import eu.ess.ics.ccce.service.external.awx.model.request.NewJobBody;
import eu.ess.ics.ccce.service.external.awx.model.response.BasePagedAwxResponse;
import eu.ess.ics.ccce.service.external.awx.model.response.Command;
import eu.ess.ics.ccce.service.external.awx.model.response.CommandDetails;
import eu.ess.ics.ccce.service.external.awx.model.response.DeployExtraVars;
import eu.ess.ics.ccce.service.external.awx.model.response.Host;
import eu.ess.ics.ccce.service.external.awx.model.response.Inventory;
import eu.ess.ics.ccce.service.external.awx.model.response.Job;
import eu.ess.ics.ccce.service.external.awx.model.response.JobDetails;
import eu.ess.ics.ccce.service.external.awx.model.response.JobHostSummaries;
import eu.ess.ics.ccce.service.external.awx.model.response.JobHostSummary;
import eu.ess.ics.ccce.service.external.awx.model.response.JobTemplate;
import eu.ess.ics.ccce.service.external.awx.model.response.JobTemplates;
import eu.ess.ics.ccce.service.external.awx.model.response.UndeployExtraVars;
import eu.ess.ics.ccce.service.external.common.HttpClientService;
import eu.ess.ics.ccce.service.internal.deployment.dto.AwxJobType;
import eu.ess.ics.ccce.service.internal.deployment.dto.DeploymentType;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import okhttp3.Headers;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 */
@Service
public class AwxService {
  private static final Logger LOGGER = LoggerFactory.getLogger(AwxService.class);

  public static final String AWX_URL = "awx.server.address";
  public static final String AWX_TOKEN = "awx.server.ccce.token";
  private static final String AWX_JOB_TEMPLATE_NAME = "awx.job.template.name";
  public static final String AWX_JOB_EVENT_ROLE_FILTER = "awx.job.event.role.filter";
  private static final String JOB_TEMPLATES_URL = "api/v2/job_templates";
  private static final String JOBS_URL = "api/v2/jobs/";
  private static final String JOBS_EVENT_ROLE_FILTER = "role__contains";
  private static final String PAGE_SIZE = "page_size";
  private static final String AD_HOC_COMMAND_URL = "api/v2/ad_hoc_commands/";
  private static final String INVENTORIES_URL = "/api/v2/inventories";
  private static final String HOSTS_URL = "/api/v2/hosts";
  private static final String CREDENTIALS_URL = "/api/v2/credentials";
  private static final String KEY_IOC_DEPLOYMENT_RESULT = "ioc_deployment_result";
  private static final String KEY_AWX = "awx";
  private static final String KEY_AWX_JOB_ID = "awx_job_id";
  private static final String KEY_IOCS_UNDEPLOY = "iocs_undeployed";
  private static final String KEY_IOCS_DEPLOY = "iocs_deployed";
  private final Environment env;
  private final String baseUrl;
  private final String token;
  private final String eventRoleFilter;
  private final HttpClientService httpClientService;

  public AwxService(Environment env, HttpClientService httpClientService) {
    this.env = env;
    this.baseUrl = env.getProperty(AWX_URL);
    this.token = env.getProperty(AWX_TOKEN);
    this.eventRoleFilter = env.getProperty(AWX_JOB_EVENT_ROLE_FILTER);
    this.httpClientService = httpClientService;
  }

  public JobTemplate getJobTemplate(String name) throws AwxServiceException {
    try {
      List<JobTemplate> templates =
          getAllPaged(JOB_TEMPLATES_URL, ImmutableMap.of("name", name), JobTemplates.class);
      if (templates.size() == 1) {
        return templates.stream().findFirst().get();
      }
      throw new EntityNotFoundException("AWX job template", name);
    } catch (SocketTimeoutException | RemoteServiceException | MalformedURLException e) {
      LOGGER.error(e.getMessage(), e);
      throw new AwxServiceException("Failed to get job template (name: " + name + ")");
    }
  }

  public Job launchJob(
      final String launchUrl,
      final Set<String> hostnames,
      final String iocs,
      DeploymentType deploymentType) {
    final String limit = Strings.join(hostnames, ':');
    return launchJob(
        launchUrl,
        UNDEPLOYMENT_TYPES.contains(deploymentType.name())
            ? createNewUndeploymentJob(limit, iocs)
            : createNewDeploymentJob(limit, iocs));
  }

  public Job launchJob(
      final String launchUrl,
      final String hostname,
      final String iocs,
      DeploymentType deploymentType)
      throws AwxServiceException {
    return launchJob(
        launchUrl,
        UNDEPLOYMENT_TYPES.contains(deploymentType.name())
            ? createNewUndeploymentJob(hostname, iocs)
            : createNewDeploymentJob(hostname, iocs));
  }

  private Job launchJob(final String launchUrl, final NewJobBody body) throws AwxServiceException {
    try {
      HttpClientService.ServiceResponse<Job> response =
          httpClientService.executePostRequest(
              createAuthHeader(), getEndpointUrl(launchUrl), body, Job.class);

      if (HttpClientService.isSuccessHttpStatusCode(response.getStatusCode())) {
        return response.getEntity();
      }
      LOGGER.error(
          "launchJob (launch URL: "
              + launchUrl
              + ") response code: "
              + response.getStatusCode()
              + ", error message: "
              + (response.getErrorMessage() != null ? response.getErrorMessage() : "-"));
      throw new AwxServiceException("Failed to launch job (URL: " + launchUrl + ")");
    } catch (RemoteServiceException | MalformedURLException e) {
      LOGGER.error(e.getMessage(), e);
      throw new AwxServiceException("Failed to launch job (URL: " + launchUrl + ")");
    }
  }

  public JobDetails getJobDetails(long jobId) throws AwxServiceException, EntityNotFoundException {
    try {
      HttpClientService.ServiceResponse<Job> jobResponse =
          httpClientService.executeGetRequest(
              createAuthHeader(), getEndpointUrl(JOBS_URL + jobId), Job.class);

      if (HttpClientService.isSuccessHttpStatusCode(jobResponse.getStatusCode())) {

        Job job = jobResponse.getEntity();
        String url = getEndpointUrl(job.getRelated().getJobEvents());
        UriComponentsBuilder ub = UriComponentsBuilder.fromUriString(url);
        ub.queryParam(JOBS_EVENT_ROLE_FILTER, eventRoleFilter);
        ub.queryParam(PAGE_SIZE, "1");
        url = ub.build().toUri().toString();
        boolean icsRoleReached = false;
        HttpClientService.ServiceResponse<JobEvents> eventsResponse =
            httpClientService.executeGetRequest(createAuthHeader(), url, JobEvents.class);
        if (HttpClientService.isSuccessHttpStatusCode(eventsResponse.getStatusCode())) {
          icsRoleReached = eventsResponse.getEntity().getCount() > 0;
        }

        List<JobHostSummary> hostSummaries =
            getAllPaged(job.getRelated().getJobHostSummaries(), JobHostSummaries.class);

        return new JobDetails(job, hostSummaries, icsRoleReached);
      } else if (jobResponse.getStatusCode() == HttpStatus.NOT_FOUND.value()) {
        throw new EntityNotFoundException("AWX job", jobId);
      }
      LOGGER.error(
          "getJobDetails for job (" + jobId + ") response code: " + jobResponse.getStatusCode());
      throw new AwxServiceException("Failed to get job details (AWX ID: " + jobId + ")");
    } catch (SocketTimeoutException | RemoteServiceException | MalformedURLException e) {
      LOGGER.error(e.getMessage(), e);
      throw new AwxServiceException("Failed to get job details (AWX ID: " + jobId + ")");
    }
  }

  public AwxJobLog getJobLog(long awxJobId, AwxJobType type)
      throws AwxServiceException, EntityNotFoundException {
    try {
      String urlPostfix = JOBS_URL;
      if (Constants.COMMAND_JOB_TYPES.contains(type)) {
        urlPostfix = AD_HOC_COMMAND_URL;
      }
      HttpClientService.ServiceResponse<Job> jobResponse =
          httpClientService.executeGetRequest(
              createAuthHeader(), getEndpointUrl(urlPostfix + awxJobId), Job.class);

      if (HttpClientService.isSuccessHttpStatusCode(jobResponse.getStatusCode())) {
        Job job = jobResponse.getEntity();
        HttpClientService.ServiceResponse<String> stdoutResponse =
            httpClientService.executePlainGetRequest(
                createAuthHeader(), getEndpointUrl(job.getRelated().getStdout() + "?format=html"));
        if (HttpClientService.isSuccessHttpStatusCode(stdoutResponse.getStatusCode())) {
          return new AwxJobLog(job.getId(), stdoutResponse.getEntity(), job.getElapsed());
        }
        LOGGER.error(
            "get stdout for job (ID: "
                + awxJobId
                + ") response code: "
                + stdoutResponse.getStatusCode());
        throw new AwxServiceException("Failed to get job log (AWX ID: " + awxJobId + ")");
      } else if (jobResponse.getStatusCode() == HttpStatus.NOT_FOUND.value()) {
        throw new EntityNotFoundException("AWX job", awxJobId);
      }
      LOGGER.error(
          "getJobDetails for job (" + awxJobId + ") response code: " + jobResponse.getStatusCode());
      throw new AwxServiceException("Failed to get job log (AWX ID: " + awxJobId + ")");
    } catch (SocketTimeoutException | RemoteServiceException | MalformedURLException e) {
      LOGGER.error(e.getMessage(), e);
      throw new AwxServiceException("Failed to get job log (AWX ID: " + awxJobId + ")");
    }
  }

  public Inventory getInventory(String name) throws AwxServiceException {
    try {
      List<Inventory> inventories =
          getAllPaged(INVENTORIES_URL, ImmutableMap.of("name", name), Inventories.class);
      if (inventories.size() == 1) {
        return inventories.stream().findFirst().get();
      }
      throw new EntityNotFoundException("AWX inventory", name);
    } catch (SocketTimeoutException | RemoteServiceException | MalformedURLException e) {
      LOGGER.error(e.getMessage(), e);
      throw new AwxServiceException("Failed to get inventory (name: " + name + ")");
    }
  }

  public Inventory getInventory(long id) throws AwxServiceException {
    try {
      List<Inventory> inventories =
          getAllPaged(INVENTORIES_URL, ImmutableMap.of("id", Long.toString(id)), Inventories.class);
      if (inventories.size() == 1) {
        return inventories.stream().findFirst().get();
      }
      throw new EntityNotFoundException("AWX inventory", id);
    } catch (SocketTimeoutException | RemoteServiceException | MalformedURLException e) {
      LOGGER.error(e.getMessage(), e);
      throw new AwxServiceException("Failed to get job template (AWX ID: " + id + ")");
    }
  }

  public Host getHost(long id) throws AwxServiceException {
    try {
      List<Host> hosts =
          getAllPaged(HOSTS_URL, ImmutableMap.of("id", Long.toString(id)), Hosts.class);
      if (hosts.size() == 1) {
        return hosts.stream().findFirst().get();
      }
      throw new EntityNotFoundException("AWX host", id);
    } catch (SocketTimeoutException | RemoteServiceException | MalformedURLException e) {
      LOGGER.error(e.getMessage(), e);
      throw new AwxServiceException("Failed to get host (AWX ID: " + id + ")");
    }
  }

  public Credential getCredential(String name) throws AwxServiceException {
    try {
      List<Credential> credentials =
          getAllPaged(CREDENTIALS_URL, ImmutableMap.of("name", name), Credentials.class);
      if (credentials.size() == 1) {
        return credentials.stream().findFirst().get();
      }
      throw new EntityNotFoundException("AWX credential", name);
    } catch (SocketTimeoutException | RemoteServiceException | MalformedURLException e) {
      LOGGER.error(e.getMessage(), e);
      throw new AwxServiceException("Failed to get credential (name: " + name + ")");
    }
  }

  public List<Credential> getCredentials(String url) throws AwxServiceException {
    try {
      return getAllPaged(getEndpointUrl(url), Credentials.class);
    } catch (SocketTimeoutException | RemoteServiceException | MalformedURLException e) {
      LOGGER.error(e.getMessage(), e);
      throw new AwxServiceException("Failed to get credentials (URL: " + url + ")");
    }
  }

  public Command launchAdHocCommand(
      long inventoryId,
      String hostname,
      Credential credential,
      String moduleName,
      String moduleArgs,
      String extraVars)
      throws AwxServiceException {
    try {
      HttpClientService.ServiceResponse<Command> response =
          httpClientService.executePostRequest(
              createAuthHeader(),
              getEndpointUrl(AD_HOC_COMMAND_URL),
              createNewCommand(
                  inventoryId, hostname, credential.getId(), moduleName, moduleArgs, extraVars),
              Command.class);
      if (HttpClientService.isSuccessHttpStatusCode(response.getStatusCode())) {
        return response.getEntity();
      }
      LOGGER.error("launchAdHocCommand response code: " + response.getStatusCode());
      throw new AwxServiceException(
          "Failed to launch ad-hoc command (inventory ID: "
              + inventoryId
              + ", module: "
              + moduleName
              + ")");
    } catch (RemoteServiceException | MalformedURLException e) {
      LOGGER.error(e.getMessage(), e);
      throw new AwxServiceException(
          "Failed to launch ad-hoc command (inventory ID: "
              + inventoryId
              + ", module: "
              + moduleName
              + ")");
    }
  }

  public CommandDetails getCommandDetails(long commandId)
      throws AwxServiceException, EntityNotFoundException {
    try {
      HttpClientService.ServiceResponse<Command> commandResponse =
          httpClientService.executeGetRequest(
              createAuthHeader(), getEndpointUrl(AD_HOC_COMMAND_URL + commandId), Command.class);
      if (HttpClientService.isSuccessHttpStatusCode(commandResponse.getStatusCode())) {
        Command command = commandResponse.getEntity();

        HttpClientService.ServiceResponse<String> stdoutResponse =
            httpClientService.executePlainGetRequest(
                createAuthHeader(),
                getEndpointUrl(command.getRelated().getStdout() + "?format=html"));
        if (HttpClientService.isSuccessHttpStatusCode(stdoutResponse.getStatusCode())) {
          String stdout = stdoutResponse.getEntity();
          return new CommandDetails(command, stdout);
        }
        LOGGER.error(
            "get stdout for command (ID: "
                + commandId
                + ") response code: "
                + stdoutResponse.getStatusCode());
        throw new AwxServiceException("Failed to get command details (AWX ID: " + commandId + ")");
      } else if (commandResponse.getStatusCode() == HttpStatus.NOT_FOUND.value()) {
        throw new EntityNotFoundException("AWX command", commandId);
      }
      LOGGER.error(
          "getCommandDetails for command ("
              + commandId
              + ") response code: "
              + commandResponse.getStatusCode());
      throw new AwxServiceException("Failed to get command details (AWX ID: " + commandId + ")");
    } catch (SocketTimeoutException | RemoteServiceException | MalformedURLException e) {
      LOGGER.error(e.getMessage(), e);
      throw new AwxServiceException("Failed to get command details (AWX ID: " + commandId + ")");
    }
  }

  private <T, A extends BasePagedAwxResponse<T>> List<T> getAllPaged(
      String urlPostfix, Class<A> responseClass)
      throws RemoteServiceException, MalformedURLException, SocketTimeoutException {

    return getAllPaged(urlPostfix, null, responseClass);
  }

  private <T, A extends BasePagedAwxResponse<T>> List<T> getAllPaged(
      String urlPostfix, Map<String, String> params, Class<A> responseClass)
      throws MalformedURLException, RemoteServiceException, SocketTimeoutException {

    List<T> all = new ArrayList<>();

    String url = getEndpointUrl(urlPostfix);
    if (params != null) {
      UriComponentsBuilder ub = UriComponentsBuilder.fromUriString(url);
      for (Map.Entry<String, String> param : params.entrySet()) {
        ub.queryParam(param.getKey(), param.getValue());
      }
      url = ub.build().toUri().toString();
    }

    while (url != null) {
      HttpClientService.ServiceResponse<A> serviceResponse =
          httpClientService.executeGetRequest(createAuthHeader(), url, responseClass);
      if (HttpClientService.isSuccessHttpStatusCode(serviceResponse.getStatusCode())) {
        BasePagedAwxResponse<T> response = serviceResponse.getEntity();
        all.addAll(response.getResults());
        url = response.getNext() != null ? getEndpointUrl(response.getNext()) : null;
      } else {
        throw new RemoteServiceException(
            "Error during getting AWX paged response (URL: "
                + url
                + "), status code: "
                + serviceResponse.getStatusCode());
      }
    }
    return all;
  }

  public String getJobTemplateName() {
    return env.getProperty(AWX_JOB_TEMPLATE_NAME);
  }

  private String getEndpointUrl(String endpointPostfix) throws MalformedURLException {
    return new URL(new URL(baseUrl), endpointPostfix).toString();
  }

  private Headers createAuthHeader() {
    return Headers.of("Authorization", "Bearer " + token);
  }

  private NewJobBody createNewDeploymentJob(final String limit, final String iocs) {
    return new NewJobBody(limit, new DeployExtraVars(iocs));
  }

  private NewJobBody createNewUndeploymentJob(final String limit, final String iocs) {
    return new NewJobBody(limit, new UndeployExtraVars(iocs));
  }

  private NewCommandBody createNewCommand(
      long inventoryId,
      String limit,
      long credentialId,
      String moduleName,
      String moduleArgs,
      String extraVars) {

    return new NewCommandBody(
        "run",
        inventoryId,
        limit,
        credentialId,
        moduleName,
        moduleArgs,
        0,
        3,
        extraVars,
        true,
        false);
  }

  /**
   * Generates the URL for the AWX job
   *
   * @param awxJobEnt the AWX job Entity from DB
   * @return the URL for the AWX job
   */
  public String generateAwxJobUrl(AwxJobEntity awxJobEnt) {
    String result;

    if ((awxJobEnt == null) || (awxJobEnt.getAwxJobId() == null)) {
      result = null;
    } else {
      String urlPostfix = null;
      if (Constants.ALL_DEPLOYMENT_TYPES.contains(awxJobEnt.getJobType())) {
        urlPostfix = "#/jobs/playbook";
      } else if (Constants.COMMAND_TYPES.contains(awxJobEnt.getJobType())) {
        urlPostfix = "#/jobs/command";
      }
      result =
          StringUtils.isEmpty(urlPostfix)
              ? null
              : UriComponentsBuilder.fromUriString(baseUrl)
                  .pathSegment(urlPostfix, String.valueOf(awxJobEnt.getAwxJobId()))
                  .build(false)
                  .toString();
    }

    return result;
  }
}
