/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.service.internal.ioc;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import eu.ess.ics.ccce.exceptions.GitlabServiceException;
import eu.ess.ics.ccce.exceptions.MetaDataFileException;
import eu.ess.ics.ccce.service.external.gitlab.GitLabService;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 */
@Service
public class MetadataService {
  private static final String METADATA_IOC_TYPE_FIELD = "ioc_type";
  private static final String METADATA_REQUIRE_VERSION_FIELD = "require_version";
  private static final String METADATA_EPICS_VERSION_FIELD = "epics_version";
  private static final String NFS_TYPE = "nfs";

  private final GitLabService gitLabService;

  @Value("${ioc.metadata.path}")
  private String metadataPath;

  public MetadataService(GitLabService gitLabService) {
    this.gitLabService = gitLabService;
  }

  /**
   * Tries to parse metadata file that is stored in GitLab on a certain branch/tag. MetaData is in
   * yml format, but converted to JSON format. During the parsing mandatory fields will be checked!
   * The parsed file is converted to IocMetaDataEntity.
   *
   * @param gitProjectId the GitLab project ID for the IOC.
   * @param tagOrBranch the tab or branch name for the IOC.
   * @return parsed MetaDataEntity
   * @throws GitlabServiceException if error occurs during GitLab file read.
   * @throws JsonProcessingException if error occurs during metadata processing.
   * @throws MetaDataFileException if the metadata file doesn't exist, or is empty, or mandatory
   *     fields are missing.
   */
  public void parseMetadata(Long gitProjectId, String tagOrBranch)
      throws JsonProcessingException, MetaDataFileException, GitlabServiceException {
    String jsonContent;
    String yamlContent = null;
    String metadataPathWithoutExtension = FilenameUtils.removeExtension(metadataPath);

    jsonContent =
        gitLabService.getFileContentExistenceCheck(
            gitProjectId, metadataPathWithoutExtension + ".json", tagOrBranch);
    if (jsonContent == null) {
      yamlContent =
          gitLabService.getFileContentExistenceCheck(
              gitProjectId, metadataPathWithoutExtension + ".yml", tagOrBranch);
    } else {
      try {
        JSONParser parser = new JSONParser();
        parser.parse(jsonContent);
      } catch (ParseException e) {
        throw new MetaDataFileException("IOC metadata-file content is not a valid JSON!");
      }
    }
    if (StringUtils.isEmpty(jsonContent) && StringUtils.isEmpty(yamlContent)) {
      throw new MetaDataFileException("IOC metadata-file not found!");
    }

    jsonContent = jsonContent == null ? convertYamlToJson(yamlContent) : jsonContent;

    // checking mandatory fields to exist
    checkMandatoryFields(jsonContent);
  }

  public String getRequireVersion(String json) {
    return extractField(json, METADATA_REQUIRE_VERSION_FIELD, String.class);
  }

  public String getEpicsVersion(String json) {
    return extractField(json, METADATA_EPICS_VERSION_FIELD, String.class);
  }

  public String getIocType(String json) {
    return extractField(json, METADATA_IOC_TYPE_FIELD, String.class);
  }

  private void checkMandatoryFields(String jsonContent) throws MetaDataFileException {
    // check IOC type, MANDATORY field!
    String iocType = getIocType(jsonContent);
    if (StringUtils.isEmpty(iocType)) {
      throw new MetaDataFileException(
          METADATA_IOC_TYPE_FIELD + " field is missing from meta-data!");
    }

    // epics_version, and require_version is only required if type is NFS
    boolean requireVersions = NFS_TYPE.equalsIgnoreCase(iocType);

    if (requireVersions) {
      // check epics_version, MANDATORY field!
      String epicsVersion = getEpicsVersion(jsonContent);
      if (StringUtils.isEmpty(epicsVersion)) {
        throw new MetaDataFileException(
            METADATA_EPICS_VERSION_FIELD + " field is missing from meta-data!");
      }

      // check require_version, MANDATORY field!
      String requireVersion = getRequireVersion(jsonContent);
      if (StringUtils.isEmpty(requireVersion)) {
        throw new MetaDataFileException(
            METADATA_REQUIRE_VERSION_FIELD + " field is missing from meta-data!");
      }
    }
  }

  /**
   * Extracts fields from JSON string.
   *
   * @param json the JSON string that has to be processed
   * @param fieldName the name of the field for which the value has to be looked up.
   * @param responseClass the class of the response object.
   * @param <T> class of the response object.
   * @return the extracted field-value.
   * @throws MetaDataFileException if exception occurs during the JSON parsing.
   */
  public <T> T extractField(String json, String fieldName, Class<T> responseClass)
      throws MetaDataFileException {
    JSONParser parser = new JSONParser();

    try {
      JSONObject jsonObject = (JSONObject) parser.parse(json);

      return (T) jsonObject.get(fieldName);
    } catch (ParseException e) {
      throw new MetaDataFileException("Error while parsing meta data file");
    }
  }

  /**
   * Converts YAML to JSON
   *
   * @param yaml YAML string
   * @return JSON string
   * @throws JsonProcessingException If YAML input processing fails
   */
  public static String convertYamlToJson(String yaml) throws JsonProcessingException {
    ObjectMapper yamlReader = new ObjectMapper(new YAMLFactory());
    Object obj = yamlReader.readValue(yaml, Object.class);

    ObjectMapper jsonWriter = new ObjectMapper();
    return jsonWriter.writeValueAsString(obj);
  }
}
