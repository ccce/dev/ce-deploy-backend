/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.service.external.naming;

import static eu.ess.ics.ccce.common.Constants.IOC;
import static eu.ess.ics.ccce.common.Constants.SC;

import eu.ess.ics.ccce.exceptions.*;
import eu.ess.ics.ccce.rest.model.naming.response.NameResponse;
import eu.ess.ics.ccce.service.external.common.HttpClientService;
import eu.ess.ics.ccce.service.external.naming.model.NamingIocResponse;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

/**
 * @author <a href="mailto:domonkos.gulyas@ess.eu">Domonkos Gulyas</a>
 */
@Service
public class NamingService {

  private static final String FIND_ALL_IOC_URL = "rest/deviceNames/devicetype/IOC";
  private static final String FIND_BY_ID_URL = "rest/deviceNames/{uuid}";
  private static final String STATUS_ACTIVE = "ACTIVE";
  private static final String STATUS_DELETED = "DELETED";

  @Value("${naming.base.url}")
  private String baseUrl;

  private static final Logger LOGGER = LoggerFactory.getLogger(NamingService.class);

  private final HttpClientService httpClientService;

  public NamingService(final HttpClientService httpClientService) {
    this.httpClientService = httpClientService;
  }

  @Cacheable(value = "naming_all_ioc")
  public List<NamingIocResponse> getAllIocs() {

    LOGGER.debug("Getting all IOC list from the Naming service");
    try {
      final String hostUrl = buildQueryAllIOCUrl();

      final HttpClientService.ServiceResponse<NamingIocResponse[]> namingResponse =
          httpClientService.executeGetRequestAndWrappingWithJackson(
              hostUrl, NamingIocResponse[].class);

      if (HttpClientService.isSuccessHttpStatusCode(namingResponse.getStatusCode())) {
        final List<NamingIocResponse> namingResponseEntity =
            Arrays.asList(namingResponse.getEntity());

        return getNecessaryNamingIocResponses(namingResponseEntity);
      }

      LOGGER.error(
          "Cannot get IOCs from the Naming service, errorCode: [{}]",
          namingResponse.getStatusCode());
      throw new NamingServiceException("Unable to get IOC names from the Naming service");
    } catch (MalformedURLException | RemoteServiceException e) {
      LOGGER.error("Error while trying to fetch all IOC list from the Naming service", e);
      throw new NamingServiceException("Unable to get IOC names from the Naming service");
    }
  }

  @Cacheable("ioc_by_id")
  public NamingIocResponse getIocByUuid(final String namingUUID) {
    LOGGER.debug("Getting a specific IOC from the Naming service, UUID:[{}]", namingUUID);

    try {
      final String hostUrl = buildQueryByUuidUrl(namingUUID);

      final HttpClientService.ServiceResponse<NamingIocResponse> namingResponse =
          httpClientService.executeGetRequestAndWrappingWithJackson(
              hostUrl, NamingIocResponse.class);

      // not found by UUID
      if ((namingResponse.getStatusCode() == HttpStatus.NOT_FOUND.value())
          || (namingResponse.getStatusCode() == HttpStatus.NO_CONTENT.value())) {
        throw new EntityNotFoundException("Naming UUID: ", namingUUID);
      }

      if (HttpClientService.isSuccessHttpStatusCode(namingResponse.getStatusCode())) {
        final NamingIocResponse namingIocResponse = namingResponse.getEntity();
        validateResponse(namingIocResponse);

        return namingIocResponse;
      }

      LOGGER.error(
          "Can not get IOC by UUID from the Naming service, errorCode: [{}], UUID: [{}]",
          namingResponse.getStatusCode(),
          namingUUID);
      throw new NamingServiceException(
          "Failed to get IOC name from the Naming service by UUID (UUID: " + namingUUID + ")");
    } catch (MalformedURLException | RemoteServiceException e) {
      LOGGER.error("Error while trying to fetch IOC from the Naming service", e);
      throw new NamingServiceException(
          "Failed to get IOC name from the Naming service by UUID (UUID: " + namingUUID + ")");
    }
  }

  private void validateResponse(final NamingIocResponse namingIocResponse) {
    if (STATUS_DELETED.equals(namingIocResponse.getStatus())) {
      throw new EntityNotFoundException("Naming UUID: ", namingIocResponse.getUuid());
    }

    if (!STATUS_ACTIVE.equals(namingIocResponse.getStatus())) {
      throw new NameInactiveException(namingIocResponse.getUuid(), namingIocResponse.getStatus());
    }

    if (!SC.equals(namingIocResponse.getDiscipline())
        || !IOC.equals(namingIocResponse.getDeviceType())) {
      throw new NameTypeInvalidException(namingIocResponse);
    }
  }

  public List<NameResponse> searchIocsByName(final String iocName) {
    if (StringUtils.isEmpty(iocName)) {
      return convertToNameResponses(getAllIocs());
    }

    return getAllIocs().stream()
        .filter(name -> name.getName().toUpperCase().contains(iocName.toUpperCase()))
        .map(NamingService::convertToNameResponse)
        .collect(Collectors.toList());
  }

  @NotNull
  private List<NamingIocResponse> getNecessaryNamingIocResponses(
      final List<NamingIocResponse> namingResponseEntity) {
    if (!namingResponseEntity.isEmpty()) {
      return namingResponseEntity.stream()
          .filter(namingIocResponse -> STATUS_ACTIVE.equals(namingIocResponse.getStatus()))
          .filter(namingIocResponse -> SC.equals(namingIocResponse.getDiscipline()))
          .collect(Collectors.toList());
    }

    return Collections.emptyList();
  }

  private String getEndpointUrl(final String endpointPostfix) throws MalformedURLException {
    return new URL(new URL(baseUrl), endpointPostfix).toString();
  }

  private String buildQueryAllIOCUrl() throws MalformedURLException {

    final UriComponentsBuilder ub =
        UriComponentsBuilder.fromUriString(getEndpointUrl(FIND_ALL_IOC_URL));

    return ub.toUriString();
  }

  private String buildQueryByUuidUrl(final String namingUUID) throws MalformedURLException {

    final UriComponentsBuilder ub =
        UriComponentsBuilder.fromUriString(getEndpointUrl(FIND_BY_ID_URL));

    return ub.buildAndExpand(namingUUID).toString();
  }

  public static List<NameResponse> convertToNameResponses(
      final List<NamingIocResponse> namingIocResponses) {
    return namingIocResponses.stream()
        .map(NamingService::convertToNameResponse)
        .collect(Collectors.toList());
  }

  private static NameResponse convertToNameResponse(final NamingIocResponse namingIocResponse) {
    return new NameResponse(
        namingIocResponse.getUuid(),
        namingIocResponse.getName(),
        namingIocResponse.getDescription());
  }
}
