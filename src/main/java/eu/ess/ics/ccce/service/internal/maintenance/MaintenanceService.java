/*
 * Copyright (C) 2024 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.service.internal.maintenance;

import static eu.ess.ics.ccce.common.Utils.*;

import eu.ess.ics.ccce.common.Utils;
import eu.ess.ics.ccce.exceptions.ConflictException;
import eu.ess.ics.ccce.exceptions.EntityNotFoundException;
import eu.ess.ics.ccce.repository.api.IMaintenanceModeRepository;
import eu.ess.ics.ccce.repository.entity.MaintenanceModeEntity;
import eu.ess.ics.ccce.rest.model.maintenance.request.SetMaintenance;
import eu.ess.ics.ccce.rest.model.maintenance.response.MaintenanceMode;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class MaintenanceService {
  private static final Logger LOGGER = LoggerFactory.getLogger(MaintenanceService.class);

  @Value("${server.time-zone}")
  private String serverTimeZone;

  private final IMaintenanceModeRepository maintenanceModeRepository;

  public MaintenanceService(final IMaintenanceModeRepository maintenanceModeRepository) {
    this.maintenanceModeRepository = maintenanceModeRepository;
  }

  private MaintenanceModeEntity getMaintenanceModeEntityIfExists() {
    try {
      return maintenanceModeRepository.find();
    } catch (EmptyResultDataAccessException e) {
      throw new EntityNotFoundException("No MaintenanceMode set");
    }
  }

  public MaintenanceMode getMaintenanceModeIfExists() {
    return extractForMaintenanceMode(getMaintenanceModeEntityIfExists());
  }

  private boolean isDateInMaintenancePeriod(
      final Date currentDate, final MaintenanceModeEntity maintenanceModeEntity) {
    return (maintenanceModeEntity.getStartAt().before(currentDate)
            || maintenanceModeEntity.getStartAt().equals(currentDate))
        && (maintenanceModeEntity.getEndAt() == null
            || maintenanceModeEntity.getEndAt().after(currentDate));
  }

  public MaintenanceMode getCurrentMaintenanceMode() {
    ZonedDateTime currentDateTime = ZonedDateTime.now();
    Date normalizedDate = toUtilDate(currentDateTime);
    MaintenanceModeEntity maintenanceModeEntity = getMaintenanceModeEntityIfExists();
    if (isDateInMaintenancePeriod(normalizedDate, maintenanceModeEntity)) {
      return extractForMaintenanceMode(maintenanceModeEntity);
    }
    throw new EntityNotFoundException("No MaintenanceMode set for date " + normalizedDate);
  }

  @Transactional
  public void endMaintenanceMode() {
    ZonedDateTime currentDateTime = Utils.now(serverTimeZone);
    MaintenanceModeEntity maintenanceModeEntity = getMaintenanceModeEntityIfExists();
    Date normalizedDate = normalizeDate(currentDateTime, serverTimeZone);
    maintenanceModeEntity.setEndAt(normalizedDate);
    maintenanceModeRepository.updateMaintenanceMode(maintenanceModeEntity);
  }

  private MaintenanceMode extractForMaintenanceMode(MaintenanceModeEntity maintenanceModeEntity) {
    return new MaintenanceMode(
        Utils.toZonedDateTime(maintenanceModeEntity.getStartAt(), serverTimeZone),
        Utils.toZonedDateTime(maintenanceModeEntity.getEndAt(), serverTimeZone),
        maintenanceModeEntity.getMessage());
  }

  private MaintenanceModeEntity transformSetToEntity(final SetMaintenance setMaintenance) {
    MaintenanceModeEntity maintenanceModeEntity = new MaintenanceModeEntity();
    maintenanceModeEntity.setStartAt(normalizeDate(setMaintenance.startAt(), serverTimeZone));
    if (setMaintenance.endAt() != null) {
      maintenanceModeEntity.setEndAt(normalizeDate(setMaintenance.endAt(), serverTimeZone));
    }
    maintenanceModeEntity.setMessage(setMaintenance.message());
    return maintenanceModeEntity;
  }

  @Transactional
  public MaintenanceMode setMaintenanceMode(SetMaintenance body) {
    LOGGER.info("Setting maintenance mode to {}", body);
    if (body.endAt() != null && body.endAt().isBefore(now(serverTimeZone))) {
      throw new ConflictException("Maintenance mode must end after the current time.");
    }
    try {
      getMaintenanceModeEntityIfExists();
      maintenanceModeRepository.updateMaintenanceMode(transformSetToEntity(body));
    } catch (EntityNotFoundException e) {
      maintenanceModeRepository.createMaintenanceMode(transformSetToEntity(body));
    }
    return getMaintenanceModeIfExists();
  }

  public String checkMaintenanceMode(String path) {
    try {
      Pattern pattern = Pattern.compile(".*(/(maintenance_mode)|(authentication)/?).*");
      Matcher matcher = pattern.matcher(path);
      boolean matches = matcher.matches();

      if (!matches) {
        return getCurrentMaintenanceMode().message();

      } else {
        return null;
      }
    } catch (EntityNotFoundException e) {
      return null;
    }
  }

  public void setServerTimeZone(String serverTimeZone) {
    this.serverTimeZone = serverTimeZone;
  }
}
