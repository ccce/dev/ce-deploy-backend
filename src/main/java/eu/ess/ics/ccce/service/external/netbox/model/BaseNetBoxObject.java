/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.service.external.netbox.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

/**
 * @author <a href="mailto:domonkos.gulyas@ess.eu">Domonkos Gulyas</a>
 */
public class BaseNetBoxObject {
  private final Long id;
  private final String display;
  private final String name;
  private final String description;
  private final String created;
  private final String lastUpdated;
  private final List<NetBoxPropertyPreview> tags;

  @JsonCreator
  public BaseNetBoxObject(
      @JsonProperty("id") Long id,
      @JsonProperty("display") String display,
      @JsonProperty("name") String name,
      @JsonProperty("description") String description,
      @JsonProperty("created") String created,
      @JsonProperty("last_updated") String lastUpdated,
      @JsonProperty("tags") List<NetBoxPropertyPreview> tags) {
    this.id = id;
    this.display = display;
    this.name = name;
    this.description = description;
    this.created = created;
    this.lastUpdated = lastUpdated;
    this.tags = tags;
  }

  public Long getId() {
    return id;
  }

  public String getDisplay() {
    return display;
  }

  public String getName() {
    return name;
  }

  public String getDescription() {
    return description;
  }

  public String getCreated() {
    return created;
  }

  public String getLastUpdated() {
    return lastUpdated;
  }

  public List<NetBoxPropertyPreview> getTags() {
    return tags;
  }
}
