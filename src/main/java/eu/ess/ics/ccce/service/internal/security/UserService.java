/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.service.internal.security;

import eu.ess.ics.ccce.configuration.WebSecurityConfig;
import eu.ess.ics.ccce.exceptions.*;
import eu.ess.ics.ccce.exceptions.AuthenticationException;
import eu.ess.ics.ccce.exceptions.ServiceException;
import eu.ess.ics.ccce.service.external.rbac.RBACService;
import eu.ess.ics.ccce.service.external.rbac.model.RBACToken;
import eu.ess.ics.ccce.service.external.rbac.model.RBACUserInfo;
import eu.ess.ics.ccce.service.internal.security.dto.LoginTokenDto;
import eu.ess.ics.ccce.service.internal.security.dto.UserDetails;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 */
@Service
public class UserService {

  private static final Logger LOGGER = LoggerFactory.getLogger(UserService.class);

  private final RBACService rbacRepo;

  public UserService(RBACService rbacRepo) {
    this.rbacRepo = rbacRepo;
  }

  /**
   * Logs in user with username/password in RBAC, and if successful, generates JWT token
   *
   * @param userName The user's login name
   * @param password The user's password
   * @return LoginTokenDto with roles that can be user for generating JWT token
   * @throws AuthenticationException When user tries to log in with bad username/password
   * @throws ServiceException If _other_ error occurs during the authentication process
   */
  public LoginTokenDto loginUser(String userName, String password)
      throws ServiceException, AuthenticationException {

    try {
      RBACToken rbacToken = rbacRepo.loginUser(userName, password);

      LocalDateTime createTime =
          LocalDateTime.ofInstant(rbacToken.getCreationTime().toInstant(), ZoneId.systemDefault());

      LocalDateTime expirationTime =
          LocalDateTime.ofInstant(
              rbacToken.getExpirationTime().toInstant(), ZoneId.systemDefault());

      Duration duration = Duration.between(createTime, expirationTime);
      long expireInMinutes = Math.abs(duration.toMinutes());

      return new LoginTokenDto(rbacToken.getId(), expireInMinutes, rbacToken.getRoles());
    } catch (ServiceException | AuthenticationException e) {
      LOGGER.error("Error while trying to log in user to RBAC", e);
      throw e;
    }
  }

  /**
   * Extracting user-info from RBAC using the token they have
   *
   * @param token The user's login token
   * @return The user-information stored in RBAC
   */
  public UserDetails getUserInfoFromToken(String token) {
    return convertToUserDetails(rbacRepo.userInfoFromToken(token));
  }

  /**
   * Trying to renew user's token in RBAC.
   *
   * @param token The user's login token.
   * @return Information about the renewed token (including the new expiration date)
   * @throws ServiceException When unexpected error occurs during the renewal process
   * @throws UnauthorizedException When RBAC gives error during the token renewal process
   */
  public LoginTokenDto renewToken(String token) throws ServiceException, UnauthorizedException {

    RBACToken rbacToken = rbacRepo.renewToken(token);

    LocalDateTime createTime =
        LocalDateTime.ofInstant(rbacToken.getCreationTime().toInstant(), ZoneId.systemDefault());

    LocalDateTime expirationTime =
        LocalDateTime.ofInstant(rbacToken.getExpirationTime().toInstant(), ZoneId.systemDefault());

    Duration duration = Duration.between(createTime, expirationTime);
    long expireInMinutes = Math.abs(duration.toMinutes());

    return new LoginTokenDto(rbacToken.getId(), expireInMinutes, rbacToken.getRoles());
  }

  /**
   * Trying to log out user from RBAC. This will result in deleting token from the system.
   *
   * @param token The user's token that has to be deleted from RBAC
   * @throws ServiceException When error occurs during the token logout process
   */
  public void logoutUser(String token) throws ServiceException {
    rbacRepo.deleteToken(token);
  }

  /**
   * Trying to fetch usernames/part of usernames that have ADMIN, or INTEGRATOR roles. If query is
   * empty -> it will give back all usernames that have ADMIN/INTEGRATOR roles.
   *
   * @param query The name-, or part of the username that has to be checked if they have
   *     ADMIN/INTEGRATOR roles
   * @return List of usernames that have ADMIN/INTEGRATOR roles.
   */
  public List<String> fetchDeploymentToolUsers(String query) {
    RBACUserInfo admins = rbacRepo.fetchUsersByRole(WebSecurityConfig.ROLE_DEPLOYMENT_TOOL_ADMIN);
    RBACUserInfo integrators =
        rbacRepo.fetchUsersByRole(WebSecurityConfig.ROLE_DEPLOYMENT_TOOL_INTEGRATOR);
    return Stream.concat(admins.getUsers().stream(), integrators.getUsers().stream())
        .distinct()
        .filter(name -> StringUtils.isEmpty(query) || name.contains(query))
        .sorted()
        .collect(Collectors.toList());
  }

  /**
   * Fetching roles from RBAC based on username.
   *
   * @param username The login name to fetch the user roles.
   * @throws RemoteException When error occurs when trying to fetch roles from RBAC
   * @return List of roles that is associated to the user in RBAC
   */
  public List<String> fetchRolesByUsername(String username)
      throws AuthenticationException, RemoteException {
    return rbacRepo.fetchRolesByUsername(username).getRoles();
  }

  /**
   * Converts RBAC token to user details DTO
   *
   * @param rbacInfo RBAC token descriptor
   * @return User details DTO
   */
  public static UserDetails convertToUserDetails(RBACToken rbacInfo) {
    UserDetails result = null;

    if (rbacInfo != null) {
      result = new UserDetails();

      result.setUserName(rbacInfo.getUserName());
      result.setFullName(rbacInfo.getFirstName() + " " + rbacInfo.getLastName());
      result.setToken(rbacInfo.getId());
      result.setRoles(rbacInfo.getRoles());
    }

    return result;
  }
}
