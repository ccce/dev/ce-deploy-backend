/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.service.internal.security;

import eu.ess.ics.ccce.configuration.WebSecurityConfig;
import eu.ess.ics.ccce.exceptions.GitlabServiceException;
import eu.ess.ics.ccce.exceptions.ServiceException;
import eu.ess.ics.ccce.exceptions.UnauthorizedException;
import eu.ess.ics.ccce.service.external.gitlab.GitLabService;
import eu.ess.ics.ccce.service.external.gitlab.model.response.GitLabGroupResp;
import eu.ess.ics.ccce.service.internal.security.dto.UserDetails;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.commons.lang3.BooleanUtils;
import org.gitlab.api.models.GitlabProject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 */
@Service
public class AuthorizationService {
  private static final Logger LOGGER = LoggerFactory.getLogger(AuthorizationService.class);
  private final GitLabService gitLabService;

  private static final String GITLAB_ROOT_GROUP = "gitlab.allowed.group.id";

  private final Environment env;

  public AuthorizationService(GitLabService gitLabService, Environment env) {
    this.gitLabService = gitLabService;
    this.env = env;
  }

  private boolean hasAdminRight(UserDetails userDetails) {
    if (userDetails.getAuthorities() != null) {
      return userDetails.getAuthorities().stream()
          .anyMatch(
              a -> a.getAuthority().equalsIgnoreCase(WebSecurityConfig.ROLE_DEPLOYMENT_TOOL_ADMIN));
    }
    return false;
  }

  /**
   * Checks if the Git repo is archived, and projects GitLab group is allowed to be deployed. If
   * project is archived, or projects group is not allowed to be deployed an exception will be
   * thrown.
   *
   * @param projectId the GitLab projects ID.
   * @throws GitlabServiceException if error occurs during GitLab communication, or when allowed
   *     GitLab group is empty
   * @throws UnauthorizedException if project is archived, or group is not allowed to be deployed.
   */
  public void checkDeploymentRights(long projectId, UserDetails userDetails)
      throws GitlabServiceException, UnauthorizedException {
    if (hasAdminRight(userDetails)) {
      LOGGER.debug("User has admin rights, Deployment authorization step is omitted");
      return;
    }

    checkRepoIsArchived(projectId);
    checkRepoGroup(projectId);
  }

  /**
   * Checks if a certain GitLab project could be used to create an Operation on it. The rights are
   * checked against GitLab, and the stored allowed GitLab group IDs
   *
   * @param projectId the GitLab project ID that has to be checked
   * @throws GitlabServiceException if the project's gitlab groups cannot be fetched/determined
   * @throws UnauthorizedException if the allowed gitlab group list (stored in the
   *     application.properties) is not set, or the project's groupID is not in the allowed GitLab
   *     group ID list
   */
  private void checkRepoGroup(long projectId) throws GitlabServiceException, UnauthorizedException {

    // for the allowed GitLab groups
    List<Long> allowedGroupIds;
    // TMP variable to read allowed GitLab groups from property file
    List<String> tmpGroupList;
    // no allowed GitLab group found in the file
    try {
      tmpGroupList = env.getProperty(GITLAB_ROOT_GROUP, List.class);
    } catch (IllegalArgumentException e) {
      LOGGER.error("Allowed GitLab group is empty", e);
      throw new UnauthorizedException("There are no allowed GitLab groups to be deployed");
    }

    if ((tmpGroupList == null) || (tmpGroupList.isEmpty())) {
      throw new UnauthorizedException("There are no allowed GitLab groups to be deployed");
    }

    // convert String entries to Long values
    allowedGroupIds = tmpGroupList.stream().map(Long::valueOf).collect(Collectors.toList());

    List<GitLabGroupResp> gitlabGroups = gitLabService.getGitlabGroups(projectId);

    if (gitlabGroups == null) {
      throw new GitlabServiceException("Cannot get GitLab projects group information");
    }

    // check allowed group-list with the project parent list
    for (GitLabGroupResp g : gitlabGroups) {
      if (allowedGroupIds.contains(g.getId())) {
        return;
      }
    }

    // no match found in the allowed list
    throw new UnauthorizedException(
        "IOC is not allowed to be deployed because the GitLab group is restricted");
  }

  /**
   * Checks if a GitLab project is archived or not before deploying it.
   *
   * @param projectId the ID of the GitLab project that has to be checked.
   * @throws GitlabServiceException if there was an exception during the communication with GitLab
   * @throws ServiceException if there was any (generic) exception during the check
   * @throws UnauthorizedException if the GitLab project cannot be found, or it is archived
   */
  private void checkRepoIsArchived(long projectId)
      throws GitlabServiceException, ServiceException, UnauthorizedException {
    try {
      GitlabProject gitlabProject = gitLabService.projectInfo(projectId);

      if ((gitlabProject == null) || (BooleanUtils.toBoolean(gitlabProject.isArchived()))) {
        throw new UnauthorizedException(
            "IOC can not be deployed because it is archived, "
                + "or not being able gather information from GitLab");
      }
    } catch (GitlabServiceException | UnauthorizedException e) {
      LOGGER.error("Error while trying to get projectInfo from GitLab", e);
      throw e;
    } catch (Exception e) {
      LOGGER.error("Error while trying to get project info from GitLab", e);
      throw new ServiceException("Can not get project details from GitLab");
    }
  }

  private boolean memberListContains(List<String> members, String userName) {
    return members.stream().anyMatch(memberName -> memberName.contains(userName));
  }
}
