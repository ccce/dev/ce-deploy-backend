/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.service.external.channelfinder.model;

import java.util.List;
import org.apache.commons.lang3.StringUtils;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 */
public class Channel {
  private final String name;
  private final String owner;
  private final List<Property> properties;
  private final List<Tag> tags;

  public Channel(String name, String owner, List<Property> properties, List<Tag> tags) {
    this.name = name;
    this.owner = owner;
    this.properties = properties;
    this.tags = tags;
  }

  public String getName() {
    return name;
  }

  public String getOwner() {
    return owner;
  }

  public List<Property> getProperties() {
    return properties;
  }

  public List<Tag> getTags() {
    return tags;
  }

  public String getPropertyOrNull(String propertyName) {
    return this.properties.stream()
        .filter(p -> StringUtils.equals(p.getName(), propertyName))
        .findFirst()
        .map(Property::getValue)
        .orElse(null);
  }
}
