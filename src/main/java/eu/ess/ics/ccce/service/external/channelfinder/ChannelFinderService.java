/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.service.external.channelfinder;

import com.google.gson.reflect.TypeToken;
import eu.ess.ics.ccce.exceptions.ChannelFinderServiceException;
import eu.ess.ics.ccce.exceptions.EntityNotFoundException;
import eu.ess.ics.ccce.exceptions.RemoteServiceException;
import eu.ess.ics.ccce.rest.model.record.PVStatus;
import eu.ess.ics.ccce.service.external.channelfinder.model.Channel;
import eu.ess.ics.ccce.service.external.common.HttpClientService;
import java.lang.reflect.Type;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import okhttp3.Headers;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 */
@Service
public class ChannelFinderService {
  private static final Logger LOGGER = LoggerFactory.getLogger(ChannelFinderService.class);

  public static final String CHANNEL_FINDER_URL = "channelfinder.server.address";

  private static final String CHANNELS_URL = "ChannelFinder/resources/channels";

  private static final String CHANNELS_COUNT_URL = "ChannelFinder/resources/channels/count";

  private final String baseUrl;
  private final HttpClientService httpClientService;

  public ChannelFinderService(Environment env, HttpClientService httpClientService) {
    this.baseUrl = env.getProperty(CHANNEL_FINDER_URL);
    this.httpClientService = httpClientService;
  }

  public List<Channel> findAllChannels(
      String recordName,
      String description,
      String iocName,
      PVStatus pvStatus,
      String alias,
      Integer page,
      Integer limit) {
    try {
      Type listType = new TypeToken<ArrayList<Channel>>() {}.getType();
      UriComponentsBuilder ub = UriComponentsBuilder.fromUri(getEndpointUrI(CHANNELS_URL));
      if (!StringUtils.isEmpty(recordName)) {
        ub.queryParam("~name", "*" + recordName + "*");
      }

      if (!StringUtils.isEmpty(alias)) {
        ub.queryParam("alias", alias);
      }

      if (!StringUtils.isEmpty(iocName)) {
        ub.queryParam("iocName", "*" + iocName + "*");
      }

      if (pvStatus != null) {
        ub.queryParam("pvStatus", pvStatus.value);
      }

      if (description != null) {
        ub.queryParam("recordDesc", "*" + description + "*");
      }

      if (page != null && limit != null) {
        ub.queryParam("~from", page * limit);
        ub.queryParam("~size", limit);
      }
      HttpClientService.ServiceResponse<List<Channel>> response =
          httpClientService.executeGetRequest(
              createAuthHeader(), ub.build().toUri().toString(), listType);
      if (HttpClientService.isSuccessHttpStatusCode(response.getStatusCode())) {
        return response.getEntity();
      } else {
        LOGGER.error("findAllRecords response code: {}", response.getStatusCode());
        throw new ChannelFinderServiceException(
            "Search CF records request failed (IOC name: '"
                + (StringUtils.isNotEmpty(iocName) ? iocName : "")
                + "')");
      }
    } catch (MalformedURLException | RemoteServiceException | URISyntaxException e) {
      LOGGER.error(e.getMessage(), e);
      throw new ChannelFinderServiceException(
          "Search CF records request failed (IOC name: '"
              + (StringUtils.isNotEmpty(iocName) ? iocName : "")
              + "')");
    }
  }

  public Channel getChannelByRecordName(String recordName) {
    try {
      Type channelType = new TypeToken<Channel>() {}.getType();
      UriComponentsBuilder ub =
          UriComponentsBuilder.fromUriString(getEndpointUrI(CHANNELS_URL) + "/{recordName}");
      String url = ub.encode().buildAndExpand(recordName).toUri().toString();
      LOGGER.debug("channel url: {}", url);
      HttpClientService.ServiceResponse<Channel> response =
          httpClientService.executeGetRequest(createAuthHeader(), url, channelType);

      if (HttpClientService.isSuccessHttpStatusCode(response.getStatusCode())) {
        return response.getEntity();
      } else if (response.getStatusCode() == HttpStatus.NOT_FOUND.value()) {
        throw new EntityNotFoundException("Record", recordName);
      } else {
        LOGGER.error("channel response code: {}", response.getStatusCode());
        throw new ChannelFinderServiceException(
            "Fetch CF record request failed (Record name: '"
                + (StringUtils.isNotEmpty(recordName) ? recordName : "")
                + "')");
      }
    } catch (MalformedURLException | RemoteServiceException | URISyntaxException e) {
      LOGGER.error(e.getMessage(), e);
      throw new ChannelFinderServiceException(
          "Fetch CF record request failed (Record name: '"
              + (StringUtils.isNotEmpty(recordName) ? recordName : "")
              + "')");
    }
  }

  public long countForPaging(
      final String recordName,
      final String description,
      final String iocName,
      final PVStatus pvStatus) {
    try {
      final UriComponentsBuilder ub =
          UriComponentsBuilder.fromUri(getEndpointUrI(CHANNELS_COUNT_URL));
      if (!StringUtils.isEmpty(recordName)) {
        ub.queryParam("~name", "*" + recordName + "*");
      }

      if (!StringUtils.isEmpty(description)) {
        ub.queryParam("recordDesc", "*" + description + "*");
      }

      if (!StringUtils.isEmpty(iocName)) {
        ub.queryParam("iocName", "*" + iocName + "*");
      }

      if (pvStatus != null) {
        ub.queryParam("pvStatus", pvStatus.value);
      }

      HttpClientService.ServiceResponse<Long> response =
          httpClientService.executeGetRequest(
              createAuthHeader(),
              ub.build().toUri().toString(),
              new TypeToken<Long>() {}.getType());
      if (HttpClientService.isSuccessHttpStatusCode(response.getStatusCode())) {
        return response.getEntity();
      } else {
        LOGGER.error("Count Records response code: {}", response.getStatusCode());
        throw new ChannelFinderServiceException(
            "Search CF records request failed (IOC name: '"
                + (StringUtils.isNotEmpty(iocName) ? iocName : "")
                + "')");
      }
    } catch (MalformedURLException | RemoteServiceException | URISyntaxException e) {
      LOGGER.error(e.getMessage(), e);
      throw new ChannelFinderServiceException(
          "Search CF records request failed (IOC name: '"
              + (StringUtils.isNotEmpty(iocName) ? iocName : "")
              + "')");
    }
  }

  private URI getEndpointUrI(String endpointPostfix)
      throws MalformedURLException, URISyntaxException {
    return new URL(new URL(baseUrl), endpointPostfix).toURI();
  }

  private Headers createAuthHeader() {
    return Headers.of();
  }
}
