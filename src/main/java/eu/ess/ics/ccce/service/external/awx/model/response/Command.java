/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.service.external.awx.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import eu.ess.ics.ccce.common.Utils;
import eu.ess.ics.ccce.service.external.awx.model.JobStatus;
import java.util.Date;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 */
public class Command {
  public static class Related {
    private String stdout;
    private String inventory;
    private String events;
    private String cancel;
    private String relaunch;

    public String getStdout() {
      return stdout;
    }

    public String getInventory() {
      return inventory;
    }

    public String getEvents() {
      return events;
    }

    public String getCancel() {
      return cancel;
    }

    public String getRelaunch() {
      return relaunch;
    }
  }

  private long id;
  private String type;
  private String url;
  private Related related;
  private Date created;
  private Date modified;
  private String name;

  @JsonProperty("launch_type")
  @SerializedName("launch_type")
  private String launchType;

  private JobStatus status;
  private boolean failed;
  private Date started;
  private Date finished;

  public long getId() {
    return id;
  }

  public String getType() {
    return type;
  }

  public String getUrl() {
    return url;
  }

  public Related getRelated() {
    return related;
  }

  public Date getCreated() {
    return Utils.cloneDate(created);
  }

  public Date getModified() {
    return Utils.cloneDate(modified);
  }

  public String getName() {
    return name;
  }

  public String getLaunchType() {
    return launchType;
  }

  public JobStatus getStatus() {
    return status;
  }

  public boolean isFailed() {
    return failed;
  }

  public Date getStarted() {
    return Utils.cloneDate(started);
  }

  public Date getFinished() {
    return Utils.cloneDate(finished);
  }
}
