/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.service.internal.ioc.dto;

import java.util.Objects;

/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 */
public class NamingResponseDTO {
  private final String description;
  private final boolean nameChanged;
  private final String currentIocName;
  private final boolean nameDeleted;
  private final boolean nameInactive;
  private final boolean typeInvalid;

  private NamingResponseDTO(NamingResponseBuilder builder) {
    this.description = builder.description;
    this.nameChanged = builder.nameChanged;
    this.currentIocName = builder.currentIocName;
    this.nameDeleted = builder.nameDeleted;
    this.nameInactive = builder.nameInactive;
    this.typeInvalid = builder.typeInvalid;
  }

  public String getDescription() {
    return description;
  }

  public boolean isNameChanged() {
    return nameChanged;
  }

  public boolean isNameDeleted() {
    return nameDeleted;
  }

  public String getCurrentIocName() {
    return currentIocName;
  }

  public boolean isNameInactive() {
    return nameInactive;
  }

  public boolean isTypeInvalid() {
    return typeInvalid;
  }

  public static class NamingResponseBuilder {
    private String description;
    private boolean nameChanged;
    private String currentIocName;
    private boolean nameDeleted;
    private boolean nameInactive;
    private boolean typeInvalid;

    public NamingResponseBuilder() {}

    public NamingResponseBuilder description(String description) {
      this.description = description;
      return this;
    }

    public NamingResponseBuilder nameChanged() {
      this.nameChanged = true;
      return this;
    }

    public NamingResponseBuilder currentIocName(String currentIocName) {
      this.currentIocName = currentIocName;
      return this;
    }

    public NamingResponseBuilder nameDeleted() {
      this.nameDeleted = true;
      return this;
    }

    public NamingResponseBuilder nameInactive() {
      this.nameInactive = true;
      return this;
    }

    public NamingResponseBuilder typeInvalid() {
      this.typeInvalid = true;
      return this;
    }

    public NamingResponseDTO build() {
      return new NamingResponseDTO(this);
    }
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    NamingResponseDTO that = (NamingResponseDTO) o;
    return nameChanged == that.nameChanged
        && nameDeleted == that.nameDeleted
        && nameInactive == that.nameInactive
        && typeInvalid == that.typeInvalid
        && Objects.equals(description, that.description)
        && Objects.equals(currentIocName, that.currentIocName);
  }

  @Override
  public int hashCode() {
    return Objects.hash(
        description, nameChanged, currentIocName, nameDeleted, nameInactive, typeInvalid);
  }
}
