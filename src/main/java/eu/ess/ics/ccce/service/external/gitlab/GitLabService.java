/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.service.external.gitlab;

import com.google.gson.reflect.TypeToken;
import eu.ess.ics.ccce.common.Utils;
import eu.ess.ics.ccce.exceptions.*;
import eu.ess.ics.ccce.repository.entity.IocDeploymentEntity;
import eu.ess.ics.ccce.rest.model.git.request.GitSearchMethodEnum;
import eu.ess.ics.ccce.rest.model.git.response.GitProject;
import eu.ess.ics.ccce.rest.model.git.response.GitReference;
import eu.ess.ics.ccce.rest.model.git.response.ReferenceType;
import eu.ess.ics.ccce.rest.model.git.response.ReferenceTypeResponse;
import eu.ess.ics.ccce.rest.model.git.response.UserInfoResponse;
import eu.ess.ics.ccce.service.external.common.HttpClientService;
import eu.ess.ics.ccce.service.external.gitlab.model.dto.CommitsDto;
import eu.ess.ics.ccce.service.external.gitlab.model.dto.GitLabCommitDto;
import eu.ess.ics.ccce.service.external.gitlab.model.dto.GitProjectDto;
import eu.ess.ics.ccce.service.external.gitlab.model.response.*;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.lang.reflect.Type;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import okhttp3.Headers;
import org.apache.commons.lang3.StringUtils;
import org.gitlab.api.AuthMethod;
import org.gitlab.api.GitlabAPI;
import org.gitlab.api.TokenType;
import org.gitlab.api.models.*;
import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.models.CommitAction;
import org.gitlab4j.api.models.CommitPayload;
import org.gitlab4j.api.models.GroupProjectsFilter;
import org.gitlab4j.api.models.Project;
import org.gitlab4j.api.models.ProjectApprovalsConfig;
import org.gitlab4j.api.models.RepositoryFile;
import org.gitlab4j.api.models.TreeItem;
import org.gitlab4j.api.models.Visibility;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 */
@Service
public class GitLabService {

  private static final Logger LOGGER = LoggerFactory.getLogger(GitLabService.class);

  private static final String GIT_POSTFIX = ".git";
  private static final String GIT_TOKEN_ENDPOINT = "oauth/token";
  private static final String API_VERSION = "api/v4/";
  private static final String GROUPS_PREFIX = "groups";
  private static final String PROJECTS_FOR_GROUP_SUFFIX =
      "projects?simple=true&include_subgroups=true&visibility=public";
  private static final String ROOT_PATH = "";
  private static final String GIT_PROJECT_EXISTS = "Gitlab project exists";
  private static final String FAILED_TO_CREATE_PROJECT = "Failed to create Gitlab project";

  private final HttpClientService httpClientService;

  public GitLabService(HttpClientService httpClientService) {
    this.httpClientService = httpClientService;
  }

  @Value("${gitlab.server.address}")
  private String gitServerAddress;

  @Value("${gitlab.client.app.id}")
  private String clientId;

  @Value("${gitlab.client.app.secret}")
  private String clientSecret;

  @Value("${gitlab.tech.user.token}")
  private String token;

  @Value("${gitlab.allowed.group.id}")
  private Long allowedGroup;

  @Value("${gitlab.master.project.id}")
  private Long masterTemplateId;

  @Value("${server.time-zone}")
  private String serverTimeZone;

  @Value("${gitlab.subgroup.id.to.generate}")
  private Long subgroupToGenerate;

  public record GitFile(String content, String commitId) {}

  /**
   * Lists all Tags for a project given in the projectUrl.
   *
   * @param projectId The project ID.
   * @return List of tags committed for the project. Result will be empty list if no tags could be
   *     found.
   */
  public List<GitlabTag> tagList(long projectId) {

    GitlabAPI gitlabApi =
        GitlabAPI.connect(gitServerAddress, token, TokenType.ACCESS_TOKEN, AuthMethod.HEADER);

    return gitlabApi.getTags(projectId);
  }

  /**
   * Retrieves the reference type for a given project and reference.
   *
   * @param projectId The project ID.
   * @param reference the git reference(tag/commitId) - optional parameter
   * @return A ReferenceTypeResponse object indicating the type of reference. (COMMIT or TAG) If the
   *     project does not exist or an error occurs during retrieval, ReferenceType.UNKNOWN is
   *     returned.
   */
  public ReferenceTypeResponse getReferenceType(long projectId, String reference) {
    GitlabProject gitlabProject;
    try {
      gitlabProject = projectInfo(projectId);
    } catch (EntityNotFoundException e) {
      return new ReferenceTypeResponse(ReferenceType.UNKNOWN);
    }

    try {
      getTag(gitlabProject, reference);
      return new ReferenceTypeResponse(ReferenceType.TAG);
    } catch (EntityNotFoundException e) {
      return new ReferenceTypeResponse(ReferenceType.COMMIT);
    }
  }

  /**
   * Retrieves information about a specific GitLab tag within a project.
   *
   * @param gitlabProject The GitLab project to search within.
   * @param tagName The name of the tag to retrieve.
   * @return A GitlabTag object representing the requested tag.
   * @throws EntityNotFoundException If the specified tag is not found within the project.
   * @throws GitlabServiceException If an error occurs while attempting to fetch the tag from
   *     GitLab.
   */
  public GitlabTag getTag(GitlabProject gitlabProject, String tagName) {
    GitlabAPI gitlabApi =
        GitlabAPI.connect(gitServerAddress, token, TokenType.ACCESS_TOKEN, AuthMethod.HEADER);

    try {
      return gitlabApi.getTag(gitlabProject, tagName);
    } catch (FileNotFoundException e) {
      throw new EntityNotFoundException("Gitlab tag", tagName);
    } catch (IOException e) {
      LOGGER.error("Error while trying to fetch tag from GitLab", e);
      throw new GitlabServiceException(
          "Failed to fetch project tag from GitLab (Project ID: "
              + gitlabProject.getId()
              + ", Tag name: "
              + tagName
              + ")");
    }
  }

  /**
   * Lists commit information for a project (will contain information from all commits).
   *
   * @param projectId The project ID.
   * @return Commit list for given project. Returns empty list if no commit can be found.
   * @throws GitlabServiceException If an exception occurs while trying to communicate with GitLab
   */
  public List<CommitsDto> getCommits(long projectId) throws GitlabServiceException {
    List<CommitsDto> result;

    try {
      List<GitLabCommitDto> commits = fetchCommitsFromDefaultBranch(projectId);

      result =
          commits.stream()
              .sorted(Comparator.comparing(GitLabCommitDto::getCommitDate).reversed())
              .map(
                  c ->
                      new CommitsDto(
                          c.getAuthor(),
                          c.getCommitDate(),
                          c.getMessage(),
                          c.getId(),
                          c.getShortId()))
              .collect(Collectors.toList());

      return result;
    } catch (UncheckedIOException e) {
      LOGGER.error(e.getMessage(), e);
      throw new GitlabServiceException("Failed to get commits (Project ID: " + projectId + ")");
    }
  }

  @NotNull
  private String extractProject(String projectUrl) {
    // removing the leading gitlab server address because of the API
    String projectToSearch = extractProjectUrl(projectUrl);
    // remove .GIT ending from repo URL if present
    if (StringUtils.endsWithIgnoreCase(projectToSearch, GIT_POSTFIX)) {
      projectToSearch = StringUtils.removeEndIgnoreCase(projectToSearch, GIT_POSTFIX);
    }

    // remove leading slash character
    if (StringUtils.endsWithIgnoreCase(projectToSearch, "/")) {
      projectToSearch = StringUtils.removeEndIgnoreCase(projectToSearch, "/");
    }

    return projectToSearch;
  }

  /**
   * Gets all member names for the project.
   *
   * @param projectId The project ID.
   * @return Member names for the project. Returns empty list if no member can be found.
   * @throws GitlabServiceException If an exception occurs while trying to communicate with GitLab
   */
  public List<String> getAllProjectMembers(long projectId)
      throws GitlabServiceException, GitRepoNotFoundException {

    try {
      String query = "projects/" + projectId + "/members/all";
      List<GitLabMemberResp> allProjectMembers =
          getAllPaged(query, new TypeToken<ArrayList<GitLabMemberResp>>() {}.getType());
      return allProjectMembers.stream()
          .map(GitLabMemberResp::getUserName)
          .collect(Collectors.toList());
    } catch (RemoteServiceException e) {
      LOGGER.error(e.getMessage(), e);
      throw new GitlabServiceException(
          "Failed to get project members (Project ID: " + projectId + ")");
    }
  }

  /**
   * Gets the content of a specific file from GitLab.
   *
   * @param projectId The project ID.
   * @param filePath the file path without the GitLab-, and without the project URL
   * @param tagOrBranchName Name of the branch, or tag that the file should be gathered from
   * @return The content of the specific file
   * @throws GitlabServiceException If an exception occurs while trying to communicate with GitLab
   */
  public String getFileContent(long projectId, String filePath, String tagOrBranchName)
      throws GitlabServiceException, IOException {
    GitlabAPI gitlabApi =
        GitlabAPI.connect(gitServerAddress, token, TokenType.ACCESS_TOKEN, AuthMethod.HEADER);
    byte[] fileContent = gitlabApi.getRawFileContent(projectId, tagOrBranchName, filePath);
    return new String(fileContent, StandardCharsets.UTF_8);
  }

  public String getFileContentExistenceCheck(
      long projectId, String filePath, String tagOrBranchName) throws GitlabServiceException {
    try {
      return getFileContent(projectId, filePath, tagOrBranchName);
    } catch (IOException e) {
      return null;
    }
  }

  /**
   * Removes the leading server URL from the projectURL, and gives back relative URL
   *
   * @param projectUrl The project's URL
   * @return The relative URL to the project
   */
  private String extractProjectUrl(String projectUrl) {

    String projectToSearch;
    if (projectUrl.startsWith(gitServerAddress)) {
      projectToSearch = projectUrl.replace(gitServerAddress, "");
    } else {
      projectToSearch = projectUrl;
    }

    return projectToSearch;
  }

  /**
   * Tries to extract user information from GitLab using the login token.
   *
   * @param userName login-name of the current user
   * @return User information extracted from GitLab. If no info can be extracted the fields of the
   *     object will be empty.
   * @throws GitlabServiceException : If an exception occurs while trying to communicate with GitLab
   */
  public UserInfoResponse currentUserInfo(String userName) {
    List<UserInfoResponse> userInfoList = infoFromUserName(userName);

    if ((userInfoList != null) && (userInfoList.size() == 1)) {
      UserInfoResponse currentUser = userInfoList.get(0);

      UserInfoResponse result = new UserInfoResponse();
      result.setAvatar(currentUser.getAvatar());
      result.setEmail(currentUser.getEmail());
      result.setFullName(currentUser.getFullName());
      result.setLoginName(currentUser.getLoginName());
      return result;
    }

    throw new GitlabServiceException(
        "No or multiple Gitlab users found for current user (" + userName + ")");
  }

  /**
   * Gathers information for a specific user from GitLab
   *
   * @param userName the specific userName
   * @return Information gathered from the user stored in GitLab
   */
  public List<UserInfoResponse> infoFromUserName(String userName) {
    GitlabAPI gitlabApi =
        GitlabAPI.connect(gitServerAddress, token, TokenType.ACCESS_TOKEN, AuthMethod.HEADER);

    try {
      List<GitlabUser> users = gitlabApi.findUsers(userName);

      if (users != null && !users.isEmpty()) {
        return users.stream()
            .filter(u -> "active".equalsIgnoreCase(u.getState()))
            .map(
                u -> {
                  UserInfoResponse tmpUsr = new UserInfoResponse();
                  tmpUsr.setAvatar(u.getAvatarUrl());
                  tmpUsr.setEmail(u.getEmail());
                  tmpUsr.setFullName(u.getName());
                  /*
                  "normal" LDAP user is usually disabled by IT, and the other username
                  (with index postfix) is active, which will cause unwanted behaviour on the UI
                  */
                  tmpUsr.setLoginName(userName);
                  tmpUsr.setGitlabUserName(u.getUsername());

                  return tmpUsr;
                })
            .collect(Collectors.toList());
      } else {
        throw new EntityNotFoundException("Gitlab user", userName);
      }
    } catch (IOException e) {
      LOGGER.error(e.getMessage(), e);
      throw new GitlabServiceException("Failed to find user by name (name: " + userName + ")");
    }
  }

  /**
   * Gets all Tags, and CommitIds for a specific GitLab repo.
   *
   * @param projectId the specific GitLab project ID
   * @param reference the git reference(tag/commitId) - optional parameter
   * @param searchMethodEnum search method for git reference (EQUALS/CONTAINS) - only used when
   *     reference is filled
   * @return List of Tags, and CommitIds for a GitLab repo
   * @throws GitlabServiceException when exception occurs during GitLab API communication
   */
  @Cacheable(value = "tags_and_commit_ids")
  public List<GitReference> tagsAndCommitIds(
      long projectId, String reference, GitSearchMethodEnum searchMethodEnum)
      throws GitlabServiceException {
    List<GitReference> result = new ArrayList<>();
    try {

      List<CommitsDto> commits = getCommits(projectId);
      Stream<GitReference> commitList =
          commits.stream()
              .filter(
                  ref ->
                      StringUtils.isEmpty(reference)
                          || checkRefFilter(ref.getCommitShortId(), reference, searchMethodEnum)
                          || checkRefFilter(ref.getCommitId(), reference, searchMethodEnum))
              .map(
                  c ->
                      new GitReference(
                          c.getCommitId(),
                          c.getCommitShortId(),
                          c.getMessage(),
                          Utils.toZonedDateTime(c.getCommitDate(), serverTimeZone),
                          ReferenceType.COMMIT));

      Stream<GitReference> tagList =
          tagList(projectId).stream()
              .filter(ref -> presentInCommits(ref, commits))
              .filter(
                  ref ->
                      StringUtils.isEmpty(reference)
                          || checkRefFilter(ref.getName(), reference, searchMethodEnum))
              .map(
                  tag ->
                      new GitReference(
                          tag.getName(),
                          tag.getCommit().getShortId(),
                          tag.getMessage(),
                          Utils.toZonedDateTime(tag.getCommit().getCommittedDate(), serverTimeZone),
                          ReferenceType.TAG));

      result.addAll(tagList.collect(Collectors.toList()));
      result.addAll(commitList.collect(Collectors.toList()));
      result.sort((o1, o2) -> o2.getCommitDate().compareTo(o1.getCommitDate()));

    } catch (Exception e) {
      LOGGER.error(e.getMessage(), e);
      throw new GitlabServiceException(
          "Cannot get tags, and/or commits (Project ID: " + projectId + ")");
    }
    return result;
  }

  private boolean presentInCommits(final GitlabTag gitlabTag, final List<CommitsDto> commits) {
    return commits.stream()
        .anyMatch(commit -> commit.getCommitId().equals(gitlabTag.getCommit().getId()));
  }

  private boolean checkRefFilter(
      String refName, String filterValue, GitSearchMethodEnum searchMethod) {
    if ((searchMethod == null) || (GitSearchMethodEnum.EQUALS.equals(searchMethod))) {
      return refName.equalsIgnoreCase(filterValue);
    }

    if (GitSearchMethodEnum.CONTAINS.equals(searchMethod)) {
      return StringUtils.containsIgnoreCase(refName, filterValue);
    }

    return false;
  }

  /**
   * Gets all Tags, and CommitIds for a specific GitLab repo.
   *
   * @param projectId the specific GitLab project ID
   * @return List of Tags, and CommitIds for a GitLab repo
   * @throws GitlabServiceException when exception occurs during GitLab API communication
   */
  public List<GitReference> tagsAndCommitIds(long projectId) throws GitlabServiceException {
    return tagsAndCommitIds(projectId, null, null);
  }

  /**
   * Fetches the GitLab groups for a specific project. It retrieves all groups, not just the
   * immediate-parent.
   *
   * @param projectId the project ID.
   * @return List of the group-info in which the project is (including all ancestor groups).
   * @throws GitlabServiceException if error occurs during GirLab request
   */
  public List<GitLabGroupResp> getGitlabGroups(long projectId) throws GitlabServiceException {
    Headers headers = Headers.of("Authorization", "Bearer " + token);

    Type listType = new TypeToken<ArrayList<GitLabGroupResp>>() {}.getType();

    try {
      HttpClientService.ServiceResponse<List<GitLabGroupResp>> projectGroups =
          httpClientService.executeGetRequest(
              headers,
              gitServerAddress + API_VERSION + "projects/" + projectId + "/groups",
              listType,
              "yyyy-MM-dd HH:mm.sssZ");

      if (HttpClientService.isSuccessHttpStatusCode(projectGroups.getStatusCode())) {

        return projectGroups.getEntity();
      }

      if (projectGroups.getStatusCode() == HttpStatus.NOT_FOUND.value()) {
        throw new GitRepoNotFoundException(projectId);
      }

      LOGGER.error(
          "Error while trying to fetch Groups for GitLab project, status code: "
              + projectGroups.getStatusCode());
      throw new GitlabServiceException(
          "Error while trying to get groups (Project ID: " + projectId + ")");
    } catch (RemoteServiceException e) {
      LOGGER.error(e.getMessage(), e);
      throw new GitlabServiceException(
          "Error while trying to get groups (Project ID: " + projectId + ")");
    }
  }

  public GitlabProject projectInfo(long projectId) throws GitlabServiceException {
    GitlabAPI gitlabApi =
        GitlabAPI.connect(gitServerAddress, token, TokenType.ACCESS_TOKEN, AuthMethod.HEADER);

    try {
      return gitlabApi.getProject(projectId);
    } catch (FileNotFoundException e) {
      LOGGER.error(e.getMessage());
      throw new EntityNotFoundException("Gitlab project", projectId);
    } catch (IOException e) {
      LOGGER.error("Error while trying to fetch project info from GitLab", e);
      throw new GitlabServiceException(
          "Failed to fetch project info from GitLab (Project ID: " + projectId + ")");
    }
  }

  /**
   * Needed by DB migration (V7)
   *
   * @param projectUrl
   * @return
   * @throws GitlabServiceException
   */
  public GitlabProject projectInfo(String projectUrl) throws GitlabServiceException {
    String projectToSearch = extractProject(projectUrl);

    GitlabAPI gitlabApi =
        GitlabAPI.connect(gitServerAddress, token, TokenType.ACCESS_TOKEN, AuthMethod.HEADER);

    try {
      return gitlabApi.getProject(projectToSearch);
    } catch (IOException e) {
      LOGGER.error("Error while trying to fetch project info from GitLab", e);

      throw new GitlabServiceException(
          "Failed to fetch project info from GitLab (Project URL: " + projectUrl + ")");
    }
  }

  public List<GitProject> getProjectByUrl(String url) {
    if (!url.startsWith(gitServerAddress)) {
      throw new InputValidationException("Invalid organisation domain name");
    }
    String relativeUrl = extractProject(url);
    GitlabAPI gitlabApi =
        GitlabAPI.connect(gitServerAddress, token, TokenType.ACCESS_TOKEN, AuthMethod.HEADER);
    try {
      GitlabProject project = gitlabApi.getProject(relativeUrl);
      return List.of(new GitProject(project.getId(), project.getHttpUrl()));
    } catch (IOException e) {
      LOGGER.error("Error while trying to fetch project info from GitLab", e);

      throw new GitlabServiceException(
          "Failed to fetch project info from GitLab (Project URL: " + relativeUrl + ")");
    }
  }

  public List<GitProject> projectsFromAllowedGroup(final String query) {
    try {
      if (query != null && Utils.isUrl(query)) {
        return getProjectByUrl(query);
      }

      try (GitLabApi gitLabApi = new GitLabApi(gitServerAddress, token)) {

        GroupProjectsFilter filter = new GroupProjectsFilter();
        filter =
            filter
                .withSimple(true)
                .withIncludeSubGroups(true)
                .withVisibility(Visibility.PUBLIC)
                .withSearch(query);
        List<Project> groupProjects = gitLabApi.getGroupApi().getProjects(allowedGroup, filter);
        return groupProjects.stream().map(p -> new GitProject(p.getId(), p.getWebUrl())).toList();
      }
    } catch (GitLabApiException e) {
      LOGGER.error("Error while trying to get Projects for the allowed GitLab Group", e);
      throw new GitlabServiceException(
          "Error while trying to get Projects for the allowed GitLab group");
    }
  }

  private <T> List<T> getAllPaged(String urlPostfix, Type type) throws RemoteServiceException {

    List<T> resultsOfAllPages = new ArrayList<>();
    Integer pageNumber = null;

    do {
      HttpClientService.ServiceResponse<List<T>> projResponse =
          getPage(urlPostfix, pageNumber, type);

      if (projResponse.getStatusCode() == HttpStatus.NOT_FOUND.value()) {
        throw new GitRepoNotFoundException(urlPostfix);
      }

      if (HttpClientService.isSuccessHttpStatusCode(projResponse.getStatusCode())) {
        resultsOfAllPages.addAll(projResponse.getEntity());

        String nextPage = projResponse.getHeaders().get("X-Next-Page");
        pageNumber = StringUtils.isEmpty(nextPage) ? null : Integer.valueOf(nextPage);
      } else {
        LOGGER.error(
            "Error while trying to fetch paged data for "
                + urlPostfix
                + ", status code: "
                + projResponse.getStatusCode());
        throw new RemoteServiceException(
            "Error while trying to fetch paged data for " + urlPostfix);
      }

    } while (pageNumber != null);

    return resultsOfAllPages;
  }

  private <T> HttpClientService.ServiceResponse<List<T>> getPage(
      String urlPostfix, Integer pageNumber, Type type) throws RemoteServiceException {
    String urlEnd = urlPostfix;
    if (pageNumber != null) {
      UriComponentsBuilder ub = UriComponentsBuilder.fromUriString(urlEnd);
      ub.queryParam("page", pageNumber);
      urlEnd = ub.build().toUri().toString();
    }

    Headers headers = Headers.of("Authorization", "Bearer " + token);

    return httpClientService.executeGetRequest(
        headers, gitServerAddress + API_VERSION + urlEnd, type, "yyyy-MM-dd HH:mm.sssZ");
  }

  /**
   * Fetches commits from the default branch of a project.
   *
   * @param projectId the GitLab ID of the project
   * @return Information about the commits on the default branch that are made within the project
   */
  private List<GitLabCommitDto> fetchCommitsFromDefaultBranch(long projectId) {
    try {
      String searchUrl = "projects/" + projectId + "/repository/commits";
      return getAllPaged(searchUrl, new TypeToken<ArrayList<GitLabCommitDto>>() {}.getType());
    } catch (RemoteServiceException e) {
      LOGGER.error(e.getMessage(), e);
      throw new GitlabServiceException(
          "Error while trying to fetch the commits from the default branch (Project ID: "
              + projectId
              + ")");
    }
  }

  /**
   * Fetching git project details by ID
   *
   * @param projectId the ID for the Git project
   * @return project detail information
   * @throws FileNotFoundException when the project with ID can not be found
   */
  public GitProjectDto projectDetails(long projectId) throws FileNotFoundException {

    GitlabAPI gitlabApi =
        GitlabAPI.connect(gitServerAddress, token, TokenType.ACCESS_TOKEN, AuthMethod.HEADER);

    try {
      GitProjectDto result = null;
      GitlabProject project = gitlabApi.getProject(projectId);

      if (project != null) {
        result = new GitProjectDto();
        result.setDescription(project.getDescription());
        result.setId(Long.valueOf(project.getId()));
        result.setName(project.getName());
        result.setProjectUrl(project.getHttpUrl());
      }

      return result;
    } catch (FileNotFoundException e) {
      throw e;
    } catch (IOException e) {
      LOGGER.error("Error while trying to fetch project details from GitLab", e);

      throw new GitlabServiceException(
          "Failed to fetch project details from GitLab (Project ID: " + projectId + ")");
    }
  }

  private Project checkRepoExists(String project, Long groupId) throws GitLabApiException {

    try (GitLabApi gitLabApi = new GitLabApi(gitServerAddress, token)) {

      // this will list only the "direct" projects, the ones that are in a subgroup will not be
      // listed!
      return gitLabApi.getGroupApi().getProjects(groupId).stream()
          .filter(p -> p.getName().equals(project))
          .findFirst()
          .orElse(null);
    }
  }

  public Project createTemplatedGitProject(String repoName) throws GitLabApiException {
    try (GitLabApi gitLabApi = new GitLabApi(gitServerAddress, token)) {
      CommitPayload payload = createPayloadForInitialCommit(gitLabApi, ROOT_PATH, masterTemplateId);

      return createProjectWithInitialCommit(
          gitLabApi,
          repoName,
          subgroupToGenerate == null ? allowedGroup : subgroupToGenerate,
          payload);
    }
  }

  private CommitPayload createPayloadForInitialCommit(
      final GitLabApi gitLabApi, final String filePath, final long sourceProjectId) {
    try {
      final Project sourceProject = gitLabApi.getProjectApi().getProject(sourceProjectId);

      final List<TreeItem> tree =
          gitLabApi
              .getRepositoryApi()
              .getTree(sourceProject.getId(), filePath, sourceProject.getDefaultBranch());
      final List<CommitAction> commitActions = new ArrayList<>();
      for (final TreeItem item : tree) {
        final CommitAction action = new CommitAction();
        action.setAction(CommitAction.Action.CREATE);
        action.setContent(
            getFile(sourceProject.getId(), item.getPath(), sourceProject.getDefaultBranch())
                .content());
        action.setFilePath(calculateDestinationPath(filePath, item));
        commitActions.add(action);
      }
      return new CommitPayload().withCommitMessage("Init").withActions(commitActions);
    } catch (final Exception e) {
      LOGGER.error(
          "Unable to collect initial commit information during new template/config creation", e);
      throw new UnprocessableRequestException(
          "Error during new template/config project creation",
          "Unable to collect initial commit information");
    }
  }

  private Project createProjectWithInitialCommit(
      GitLabApi gitLabApi, String repoName, Long groupId, CommitPayload payload)
      throws GitLabApiException {
    Project gitProject = checkRepoExists(repoName, groupId);
    if (gitProject != null) {
      throw new UnprocessableRequestException(
          GIT_PROJECT_EXISTS,
          "Project \"" + repoName + "\" already exists in group (ID: " + groupId + ")");
    }
    if (payload == null) {
      throw new UnprocessableRequestException(
          FAILED_TO_CREATE_PROJECT, "Initial commit creation failed");
    }

    Project projectSpec =
        new Project()
            .withPublic(true)
            .withNamespaceId(groupId)
            // Minimum of one approval
            .withApprovalsBeforeMerge(1)
            // Merge checks: All threads resolved
            .withOnlyAllowMergeIfAllDiscussionsAreResolved(true)
            .withName(repoName);

    Project created = gitLabApi.getProjectApi().createProject(projectSpec);

    ProjectApprovalsConfig approvalsConfig = new ProjectApprovalsConfig();
    // Approval settings: Prevent approvals by users who add commits
    approvalsConfig.withMergeRequestsDisableCommittersApproval(true);
    // Prevent editing approval rules in merge requests
    approvalsConfig.withDisableOverridingApproversPerMergeRequest(true);
    // Approval settings: Prevent approval by author
    approvalsConfig.setMergeRequestsAuthorApproval(false);

    gitLabApi.getProjectApi().setApprovalsConfiguration(created.getId(), approvalsConfig);

    payload.setBranch(created.getDefaultBranch());
    gitLabApi.getCommitsApi().createCommit(created.getId(), payload);
    return created;
  }

  public GitFile getFile(Long projectId, String fileName, String ref)
      throws GitLabApiException, GitRepoNotFoundException {

    if (StringUtils.isEmpty(fileName)) {
      throw new GitRepoNotFoundException("Requested file-name cannot be empty!");
    }

    RepositoryFile file;

    try (GitLabApi gitLabApi = new GitLabApi(gitServerAddress, token)) {

      file = gitLabApi.getRepositoryFileApi().getFile(projectId, fileName, ref);
    }

    return new GitFile(file.getDecodedContentAsString(), file.getCommitId());
  }

  private String calculateDestinationPath(final String filePath, final TreeItem item) {
    return filePath.isEmpty() ? item.getPath() : item.getName();
  }

  public Optional<Project> getOptionalProjectByRepoId(Long projectId) {

    try (GitLabApi gitLabApi = new GitLabApi(gitServerAddress, token)) {

      return gitLabApi.getProjectApi().getOptionalProject(projectId);
    }
  }

  public void deleteProjectById(Long projectId) throws GitLabApiException {
    try (GitLabApi gitLabApi = new GitLabApi(gitServerAddress, token)) {

      gitLabApi.getProjectApi().deleteProject(projectId);
    }
  }

  /**
   * Extracts short Git reference hash
   *
   * @param projectId Project identifier in Gitlab
   * @param reference Git reference (commit or tag identifier)
   * @return Short Git reference hash
   */
  public String getShortGitId(long projectId, String reference) {
    try {
      List<GitReference> references =
          tagsAndCommitIds(projectId, reference, GitSearchMethodEnum.EQUALS);
      return references.isEmpty() ? null : references.get(0).getShortReference();
    } catch (Exception e) {
      LOGGER.error("Unable to get short Git reference", e);
      return null;
    }
  }

  @Nullable
  public GitlabProject getNullableGitlabProject(Long gitProjectId) {
    GitlabProject project = null;
    if (gitProjectId != null) {
      try {
        project = projectInfo(gitProjectId);
      } catch (EntityNotFoundException e) {
        LOGGER.error("Unable to get Gitlab project info", e);
      }
    }
    return project;
  }

  public Map<Long, GitlabProject> getProjectsForDeployments(List<IocDeploymentEntity> deployments) {
    return deployments.stream()
        .collect(
            Collectors.toMap(
                IocDeploymentEntity::getId, d -> getNullableGitlabProject(d.getGitProjectId())));
  }

  public Map<Long, String> getShortIdsForDeployments(List<IocDeploymentEntity> deployments) {
    return deployments.stream()
        .collect(
            Collectors.toMap(
                IocDeploymentEntity::getId,
                d -> getShortGitId(d.getGitProjectId(), d.getSourceVersion())));
  }

  public static Long getGitProjectIdForDeployment(IocDeploymentEntity deploymentEntity) {
    return deploymentEntity.getGitProjectId() == null
        ? deploymentEntity.getIoc().getGitProjectId()
        : deploymentEntity.getGitProjectId();
  }
}
