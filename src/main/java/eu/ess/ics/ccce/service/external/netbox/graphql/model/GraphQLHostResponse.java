/*
 * Copyright (C) 2024 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.service.external.netbox.graphql.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

/**
 * @author <a href="mailto:domonkos.gulyas@ess.eu">Domonkos Gulyas</a>
 */
public class GraphQLHostResponse {
  private final String id;
  private final String name;
  private final String description;
  private final DeviceType deviceType;
  private final Role role;
  private final List<Tag> tags;
  private final PrimaryIp primaryIp;

  @JsonCreator
  public GraphQLHostResponse(
      @JsonProperty("id") String id,
      @JsonProperty("name") String name,
      @JsonProperty("description") String description,
      @JsonProperty("role") Role role,
      @JsonProperty("device_type") DeviceType deviceType,
      @JsonProperty("tags") List<Tag> tags,
      @JsonProperty("primary_ip4") PrimaryIp primaryIp) {
    this.id = id;
    this.name = name;
    this.description = description;
    this.deviceType = deviceType;
    this.role = role;
    this.tags = tags;
    this.primaryIp = primaryIp;
  }

  public String getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public String getDescription() {
    return description;
  }

  public Role getRole() {
    return role;
  }

  public List<Tag> getTags() {
    return tags;
  }

  public PrimaryIp getPrimaryIp() {
    return primaryIp;
  }

  public DeviceType getDeviceType() {
    return deviceType;
  }
}
