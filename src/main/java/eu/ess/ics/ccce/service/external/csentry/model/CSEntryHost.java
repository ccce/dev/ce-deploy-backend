/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.service.external.csentry.model;

import java.util.List;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 */
public class CSEntryHost extends BaseCSEntryHost {
  private final List<CSEntryInterface> interfaces;

  public List<CSEntryInterface> getInterfaces() {
    return interfaces;
  }

  public CSEntryHost(CSEntryHostById csEntryHostById, List<CSEntryInterface> interfaces) {
    super(
        csEntryHostById.getId(),
        csEntryHostById.getFqdn(),
        csEntryHostById.getName(),
        csEntryHostById.getScope(),
        csEntryHostById.isIoc(),
        csEntryHostById.getDeviceType(),
        csEntryHostById.getDescription(),
        csEntryHostById.getUser(),
        csEntryHostById.getCreatedAt(),
        csEntryHostById.getAnsibleVars(),
        csEntryHostById.getAnsibleGroups());
    this.interfaces = interfaces;
  }
}
