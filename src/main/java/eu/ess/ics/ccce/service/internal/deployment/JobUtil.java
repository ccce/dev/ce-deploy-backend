package eu.ess.ics.ccce.service.internal.deployment;

import static eu.ess.ics.ccce.common.Constants.BATCH_JOB_TYPES;

import eu.ess.ics.ccce.common.Constants;
import eu.ess.ics.ccce.common.Utils;
import eu.ess.ics.ccce.common.alert.DeploymentAlertGenerator;
import eu.ess.ics.ccce.repository.api.IIocDeploymentRepository;
import eu.ess.ics.ccce.repository.entity.AwxJobEntity;
import eu.ess.ics.ccce.repository.entity.IocDeploymentEntity;
import eu.ess.ics.ccce.rest.model.job.JobAction;
import eu.ess.ics.ccce.rest.model.job.response.*;
import eu.ess.ics.ccce.service.external.awx.model.response.CommandDetails;
import eu.ess.ics.ccce.service.internal.deployment.dto.AwxJobType;
import eu.ess.ics.ccce.service.internal.host.HostService;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.gitlab.api.models.GitlabProject;

public class JobUtil {

  private JobUtil() {
    throw new IllegalStateException("Utility class");
  }

  public static JobDetails convertToBatchJobDetails(
      Map<Long, GitlabProject> projectsForDeployments,
      Map<Long, String> shortIdsForDeployments,
      AwxJobEntity job,
      List<IocDeploymentEntity> deployments,
      String awxJobUrl,
      String serverTimeZone,
      HostService hostService) {
    return JobDetails.createBatchJobDetails(
        job.getId(),
        job.getCreatedBy(),
        Utils.toZonedDateTime(job.getAwxJobStartedAt(), serverTimeZone),
        Utils.toZonedDateTime(job.getCreatedAt(), serverTimeZone),
        Utils.toZonedDateTime(job.getAwxJobFinishedAt(), serverTimeZone),
        JobAction.valueOf(job.getJobType()),
        calculateJobStatus(
            eu.ess.ics.ccce.service.external.awx.model.JobStatus.valueOf(job.getStatus())),
        awxJobUrl,
        job.getAwxJobId(),
        convertToJobDetailsEntries(
            deployments, projectsForDeployments, shortIdsForDeployments, hostService));
  }

  public static List<JobDetailsEntry> convertToJobDetailsEntries(
      List<IocDeploymentEntity> deployments,
      Map<Long, GitlabProject> projectsForDeployments,
      Map<Long, String> shortIdsForDeployments,
      HostService hostService) {
    return deployments.stream()
        .map(
            deployment ->
                convertToJobDetailsEntry(
                    deployment, projectsForDeployments, shortIdsForDeployments, hostService))
        .sorted(Comparator.comparing(JobDetailsEntry::iocName))
        .toList();
  }

  public static JobDetailsEntry convertToJobDetailsEntry(
      IocDeploymentEntity deployment,
      Map<Long, GitlabProject> projectsForDeployments,
      Map<Long, String> shortIdsForDeployments,
      HostService hostService) {
    boolean hostExternalIdValid = hostService.isHostExternalIdValid(deployment);
    GitlabProject project = projectsForDeployments.get(deployment.getId());
    return new JobDetailsEntry(
        deployment.getNamingName(),
        deployment.getIoc().getId(),
        deployment.getId(),
        deployment.getGitProjectId(),
        deployment.getSourceVersion(),
        project != null ? project.getHttpUrl() : null,
        shortIdsForDeployments.get(deployment.getId()),
        HostService.convertToHostWithFqdn(
            HostService.convertToHost(
                deployment.getNetBoxHostIdentifier(),
                hostExternalIdValid,
                deployment.getHostFqdn(),
                deployment.getHostName(),
                deployment.getHostNetwork())));
  }

  /**
   * Extracts information from AWXJob into JobDetails response
   *
   * @param gitlabProject GitLab project
   * @param job the AWXJob entity from the DB
   * @param deployment the deployment info
   * @param awxJobUrl URL for the AWXJob
   * @param deploymentAlertGenerator alert generator to create alerts (if needed) for the response
   * @param serverTimeZone
   * @return JobDetails response object
   */
  public static JobDetails convertToJobDetails(
      GitlabProject gitlabProject,
      AwxJobEntity job,
      Deployment deployment,
      String awxJobUrl,
      DeploymentAlertGenerator deploymentAlertGenerator,
      String serverTimeZone) {

    return JobDetails.createSingleJobDetails(
        job.getId(),
        job.getCreatedBy(),
        Utils.toZonedDateTime(job.getAwxJobStartedAt(), serverTimeZone),
        Utils.toZonedDateTime(job.getCreatedAt(), serverTimeZone),
        Utils.toZonedDateTime(job.getAwxJobFinishedAt(), serverTimeZone),
        JobAction.valueOf(job.getJobType()),
        job.getDeployment().getIoc().getNamingName(),
        job.getDeployment().getIoc().getId(),
        job.getDeployment().getId(),
        job.getDeployment().getGitProjectId(),
        job.getDeployment().getSourceVersion(),
        calculateJobStatus(
            eu.ess.ics.ccce.service.external.awx.model.JobStatus.valueOf(job.getStatus())),
        gitlabProject != null ? gitlabProject.getHttpUrl() : deployment.getSourceUrl(),
        deployment.getSourceVersionShort(),
        HostService.convertToHostWithFqdn(deployment.getHost()),
        job.getAwxJobId(),
        awxJobUrl,
        Constants.ALL_DEPLOYMENT_TYPES.contains(job.getJobType())
            ? deploymentAlertGenerator.generateDeploymentDetailsAlert(deployment)
            : null);
  }

  /**
   * Converting JobStatus (received from AWX) into OperationStatus enum.
   *
   * @param jobStatus the JobStatus received from AWX
   * @return Corresponding OperationStatus, or Unknown if JobStatus can not be converted
   */
  public static eu.ess.ics.ccce.rest.model.job.response.JobStatus calculateJobStatus(
      eu.ess.ics.ccce.service.external.awx.model.JobStatus jobStatus) {
    return switch (jobStatus) {
      case NEW, PENDING, WAITING, QUEUED_BY_BACKEND, CREATED -> JobStatus.QUEUED;
      case RUNNING -> JobStatus.RUNNING;
      case ERROR, CANCELED, FAILED -> JobStatus.FAILED;
      case SUCCESSFUL -> JobStatus.SUCCESSFUL;
      default -> JobStatus.UNKNOWN;
    };
  }

  /**
   * Converts AWX REST API response to CE backend REST API response
   *
   * @param jobDetails Job details AWX REST API response
   * @return Job details CE backend REST API response
   */
  public static AwxJobDetails convertCommandToAwxJobDetails(
      eu.ess.ics.ccce.service.external.awx.model.response.JobDetails jobDetails,
      String serverTimeZone) {
    AwxJobDetails awxJobDetails = new AwxJobDetails();

    awxJobDetails.setId(jobDetails.getJob().getId());
    awxJobDetails.setStarted(
        Utils.toZonedDateTime(jobDetails.getJob().getStarted(), serverTimeZone));
    awxJobDetails.setFinished(
        Utils.toZonedDateTime(jobDetails.getJob().getFinished(), serverTimeZone));
    awxJobDetails.setStatus(
        jobDetails.getJob().getStatus() != null
            ? calculateJobStatus(jobDetails.getJob().getStatus())
            : JobStatus.UNKNOWN);

    return awxJobDetails;
  }

  /**
   * Converts AWX REST API response to CE backend REST API response
   *
   * @param commandDetails Command details AWX REST API response
   * @return Command details CE backend REST API response
   */
  public static AwxJobDetails convertCommandToAwxJobDetails(
      CommandDetails commandDetails, String serverTimeZone) {
    AwxJobDetails awxCommandDetails = new AwxJobDetails();

    awxCommandDetails.setId(commandDetails.getCommand().getId());
    awxCommandDetails.setStatus(calculateJobStatus(commandDetails.getCommand().getStatus()));
    awxCommandDetails.setStarted(
        Utils.toZonedDateTime(commandDetails.getCommand().getStarted(), serverTimeZone));
    awxCommandDetails.setFinished(
        Utils.toZonedDateTime(commandDetails.getCommand().getFinished(), serverTimeZone));

    return awxCommandDetails;
  }

  /**
   * Converts AWX job DB entity to CE backend REST API response
   *
   * @param jobEntity The AWX job entity from the DB
   * @return Job details CE backend REST API response
   */
  public static AwxJobDetails convertToAwxJobDetails(
      AwxJobEntity jobEntity, String serverTimeZone) {
    AwxJobDetails awxJobDetails = new AwxJobDetails();

    awxJobDetails.setId(jobEntity.getAwxJobId());
    awxJobDetails.setStarted(Utils.toZonedDateTime(jobEntity.getAwxJobStartedAt(), serverTimeZone));
    awxJobDetails.setFinished(
        Utils.toZonedDateTime(jobEntity.getAwxJobFinishedAt(), serverTimeZone));
    awxJobDetails.setStatus(
        JobUtil.calculateJobStatus(
            eu.ess.ics.ccce.service.external.awx.model.JobStatus.valueOf(jobEntity.getStatus())));

    return awxJobDetails;
  }

  /**
   * Converts AWX ad-hoc command DB entity to AWX job DTO
   *
   * @param commandEntity The command entity from the DB
   * @param serverTimeZone
   * @return AWX job DTO that could be consumed by clients
   */
  public static Job convertToJob(AwxJobEntity commandEntity, String serverTimeZone) {
    return Job.createSingleJob(
        commandEntity.getId(),
        commandEntity.getCreatedBy(),
        Utils.toZonedDateTime(commandEntity.getAwxJobStartedAt(), serverTimeZone),
        Utils.toZonedDateTime(commandEntity.getCreatedAt(), serverTimeZone),
        Utils.toZonedDateTime(commandEntity.getAwxJobFinishedAt(), serverTimeZone),
        JobAction.valueOf(commandEntity.getJobType()),
        commandEntity.getDeployment().getIoc().getNamingName(),
        commandEntity.getDeployment().getIoc().getId(),
        commandEntity.getDeployment().getId(),
        commandEntity.getDeployment().getGitProjectId(),
        commandEntity.getDeployment().getSourceVersion(),
        new DeploymentHostInfo(
            HostService.encodeHostIdentifier(
                commandEntity.getDeployment().getNetBoxHostIdentifier()),
            commandEntity.getDeployment().getHostName(),
            commandEntity.getDeployment().getHostNetwork(),
            commandEntity.getDeployment().getHostFqdn()),
        calculateJobStatus(
            eu.ess.ics.ccce.service.external.awx.model.JobStatus.valueOf(
                commandEntity.getStatus())));
  }

  public static Job convertToBaseJob(
      AwxJobEntity awxJobEntity,
      IIocDeploymentRepository deploymentRepository,
      String serverTimeZone) {
    return BATCH_JOB_TYPES.contains(AwxJobType.valueOf(awxJobEntity.getJobType()))
        ? convertToBatchJob(
            awxJobEntity,
            deploymentRepository.findDeploymentsForBatchJob(awxJobEntity.getId()),
            serverTimeZone)
        : convertToJob(awxJobEntity, serverTimeZone);
  }

  public static Job convertToBatchJob(
      AwxJobEntity jobEntity, List<IocDeploymentEntity> deployments, String serverTimeZone) {
    return Job.createBatchJob(
        jobEntity.getId(),
        jobEntity.getCreatedBy(),
        Utils.toZonedDateTime(jobEntity.getAwxJobStartedAt(), serverTimeZone),
        Utils.toZonedDateTime(jobEntity.getCreatedAt(), serverTimeZone),
        Utils.toZonedDateTime(jobEntity.getAwxJobFinishedAt(), serverTimeZone),
        JobAction.valueOf(jobEntity.getJobType()),
        calculateJobStatus(
            eu.ess.ics.ccce.service.external.awx.model.JobStatus.valueOf(jobEntity.getStatus())),
        convertJobEntries(deployments));
  }

  private static List<JobEntry> convertJobEntries(List<IocDeploymentEntity> deployments) {
    return deployments.stream().map(JobUtil::convertToJobEntry).collect(Collectors.toList());
  }

  private static JobEntry convertToJobEntry(IocDeploymentEntity deployment) {
    return new JobEntry(
        deployment.getIoc().getNamingName(),
        deployment.getIoc().getId(),
        deployment.getId(),
        deployment.getGitProjectId(),
        deployment.getSourceVersion(),
        new DeploymentHostInfo(
            HostService.encodeHostIdentifier(deployment.getNetBoxHostIdentifier()),
            deployment.getHostName(),
            deployment.getHostNetwork(),
            deployment.getHostFqdn()));
  }
}
