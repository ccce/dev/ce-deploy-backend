/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.service.external.netbox.model;

import java.util.Objects;

/**
 * @author <a href="mailto:domonkos.gulyas@ess.eu">Domonkos Gulyas</a>
 */
public class NetBoxHostIdentifier {
  private final Long netBoxHostId;
  private final Boolean isVm;

  public NetBoxHostIdentifier(Long netBoxHostId, Boolean isVm) {
    this.netBoxHostId = netBoxHostId;
    this.isVm = isVm;
  }

  public Long getNetBoxHostId() {
    return netBoxHostId;
  }

  public Boolean isVm() {
    return isVm;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    NetBoxHostIdentifier that = (NetBoxHostIdentifier) o;
    return Objects.equals(netBoxHostId, that.netBoxHostId) && Objects.equals(isVm, that.isVm);
  }

  @Override
  public int hashCode() {
    return Objects.hash(netBoxHostId, isVm);
  }
}
