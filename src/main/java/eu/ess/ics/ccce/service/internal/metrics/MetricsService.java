package eu.ess.ics.ccce.service.internal.metrics;

import eu.ess.ics.ccce.repository.api.IIocDeploymentRepository;
import eu.ess.ics.ccce.repository.api.IIocRepository;
import eu.ess.ics.ccce.rest.model.job.response.JobStatus;
import eu.ess.ics.ccce.rest.model.statistics.request.StatisticType;
import eu.ess.ics.ccce.service.internal.deployment.dto.AwxJobType;
import eu.ess.ics.ccce.service.internal.statistics.StatisticsService;
import io.micrometer.core.instrument.Gauge;
import io.micrometer.core.instrument.MeterRegistry;
import jakarta.annotation.PostConstruct;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

/**
 * @author <a href="mailto:domonkos.gulyas@ess.eu">Domonkos Gulyas</a>
 */
@Service
public class MetricsService {

  // new metric names
  private static final String METRIC_NAME_IOC_COUNT = "ce.ioc.count";
  private static final String METRIC_NAME_JOB_COUNT = "ce.job.count";
  private static final String METRIC_NAME_HOST_COUNT = "ce.host.count";

  // old metric names
  private static final String METRIC_NAME_REGISTERED_IOCS = "ce.deploy.registered";
  private static final String METRIC_NAME_DEPLOYED_IOCS = "ce.deploy.deployed";
  private static final String METRIC_NAME_DEPLOYMENT_JOBS = "ce.deploy.deployments";
  private static final String METRIC_NAME_UNDEPLOYMENT_JOBS = "ce.deploy.undeployments";
  private static final String METRIC_NAME_START_COMMAND_COUNT = "ce.deploy.start.command.count";
  private static final String METRIC_NAME_STOP_COMMAND_COUNT = "ce.deploy.stop.command.count";
  private static final String METRIC_NAME_ACTIVE_IOC_COUNT = "ce.monitor.active.count";

  private static final String METRIC_DESCRIPTION_IOCS = "Count of IOCs.";
  private static final String METRIC_DESCRIPTION_REGISTERED_IOCS = "Count of registered IOCs.";
  private static final String METRIC_DESCRIPTION_DEPLOYED_IOCS = "Count of deployed IOCs.";
  private static final String METRIC_DESCRIPTION_ACTIVE_IOCS = "Count of active IOCs.";
  private static final String METRIC_DESCRIPTION_JOBS = "Count of jobs per status.";
  private static final String METRIC_DESCRIPTION_DEPLOYMENT_JOBS = "Count of deployment jobs.";
  private static final String METRIC_DESCRIPTION_UNDEPLOYMENT_JOBS = "Count of undeployment jobs.";
  private static final String METRIC_DESCRIPTION_START_COMMAND_COUNT = "Count of start commands.";
  private static final String METRIC_DESCRIPTION_STOP_COMMAND_COUNT = "Count of stop commands.";
  private static final String METRIC_DESCRIPTION_ACTIVE_HOST_WITH_ACTIVE_IOC =
      "Count of active hosts with active IOCs";
  private static final String METRIC_DESCRIPTION_ACTIVE_HOST_WITH_INACTIVE_IOC =
      "Count of active hosts with inactive IOCs";
  private static final String METRIC_DESCRIPTION_ACTIVE_HOST_WITHOUT_IOC =
      "Count of active hosts without IOCs";
  private static final String METRIC_DESCRIPTION_INACTIVE_HOST_WITH_IOC =
      "Count of inactive hosts with IOCs";
  private static final String METRIC_DESCRIPTION_INACTIVE_HOST_WITHOUT_IOC =
      "Count of inactive hosts without IOCs";
  private static final String KEY_STATUS = "status";
  private static final String KEY_JOB_TYPE = "job_type";
  private static final String KEY_NETWORK = "network";
  private static final String ACTIVE = "active";
  private static final String INACTIVE = "inactive";
  private static final String DEPLOYMENTS = "deployments";
  private static final String UNDEPLOYMENTS = "undeployments";
  private static final String START_COMMAND = "start_command";
  private static final String STOP_COMMAND = "stop_command";
  private static final Map<String, AwxJobType> JOB_TYPES_MAP =
      Map.of(
          DEPLOYMENTS,
          AwxJobType.DEPLOY,
          UNDEPLOYMENTS,
          AwxJobType.UNDEPLOY,
          START_COMMAND,
          AwxJobType.START,
          STOP_COMMAND,
          AwxJobType.STOP);
  private static final Map<String, List<String>> JOB_STATUS_MAP =
      Map.of(
          "QUEUED", List.of(JobStatus.QUEUED.name()),
          "RUNNING", List.of(JobStatus.RUNNING.name()),
          "SUCCESSFUL", List.of(JobStatus.SUCCESSFUL.name()),
          "FAILED", List.of(JobStatus.ERROR.name(), JobStatus.FAILED.name()));
  private static final String NONE = "-";
  private static final String KEY_HOSTS_IOC = "hosts_ioc";
  private static final String KEY_ACTIVE_IOC = "active_ioc";

  private final IIocRepository iocRepository;

  private final IIocDeploymentRepository iocDeploymentRepository;

  private final StatisticsService statisticsService;

  private final MeterRegistry meterRegistry;

  // metrics that are time-consuming to calculate
  private final AtomicLong activeIocCount = new AtomicLong();
  private final AtomicLong inactiveIocCount = new AtomicLong();
  private final AtomicLong iocsShouldNotBeRunning = new AtomicLong();
  private final AtomicLong activeHostWithoutIoc = new AtomicLong();
  private final AtomicLong activeHostWithInactiveIoc = new AtomicLong();
  private final AtomicLong activeHostWithActiveIoc = new AtomicLong();
  private final AtomicLong inactiveHostWithoutIoc = new AtomicLong();
  private final AtomicLong inactiveHostWithIoc = new AtomicLong();
  private final Map<String, AtomicLong> activeIocsOnNetwork = new HashMap<>();
  private final Map<String, AtomicLong> inactiveIocsOnNetwork = new HashMap<>();

  public MetricsService(
      final IIocRepository iocRepository,
      final IIocDeploymentRepository iocDeploymentRepository,
      final StatisticsService statisticsService,
      final MeterRegistry meterRegistry) {
    this.iocRepository = iocRepository;
    this.iocDeploymentRepository = iocDeploymentRepository;
    this.statisticsService = statisticsService;
    this.meterRegistry = meterRegistry;
  }

  @PostConstruct
  private void registerGaugeMetrics() {

    // old metrics
    registerOldMetrics();

    // new metrics

    registerIocMetrics();

    registerJobMetrics();

    registerHostMetrics();
  }

  private void registerIocMetrics() {
    Gauge.builder(METRIC_NAME_IOC_COUNT, inactiveIocCount::get)
        .description(METRIC_DESCRIPTION_IOCS)
        .tag(KEY_STATUS, INACTIVE)
        .tag(KEY_NETWORK, NONE)
        .register(meterRegistry);

    Gauge.builder(METRIC_NAME_IOC_COUNT, iocsShouldNotBeRunning::get)
        .description(METRIC_DESCRIPTION_IOCS)
        .tag(KEY_STATUS, ACTIVE)
        .tag(KEY_NETWORK, NONE)
        .register(meterRegistry);
  }

  private void registerOldMetrics() {
    Gauge.builder(METRIC_NAME_REGISTERED_IOCS, iocRepository::countAll)
        .description(METRIC_DESCRIPTION_REGISTERED_IOCS)
        .register(meterRegistry);

    Gauge.builder(METRIC_NAME_DEPLOYED_IOCS, iocRepository::countActualDeployments)
        .description(METRIC_DESCRIPTION_DEPLOYED_IOCS)
        .register(meterRegistry);

    Gauge.builder(
            METRIC_NAME_DEPLOYMENT_JOBS,
            () -> iocDeploymentRepository.countAwxJobsByType(AwxJobType.DEPLOY.name()))
        .description(METRIC_DESCRIPTION_DEPLOYMENT_JOBS)
        .register(meterRegistry);

    Gauge.builder(
            METRIC_NAME_UNDEPLOYMENT_JOBS,
            () -> iocDeploymentRepository.countAwxJobsByType(AwxJobType.UNDEPLOY.name()))
        .description(METRIC_DESCRIPTION_UNDEPLOYMENT_JOBS)
        .register(meterRegistry);

    Gauge.builder(
            METRIC_NAME_START_COMMAND_COUNT,
            () -> iocDeploymentRepository.countAwxJobsByType(AwxJobType.START.name()))
        .description(METRIC_DESCRIPTION_START_COMMAND_COUNT)
        .register(meterRegistry);

    Gauge.builder(
            METRIC_NAME_STOP_COMMAND_COUNT,
            () -> iocDeploymentRepository.countAwxJobsByType(AwxJobType.STOP.name()))
        .description(METRIC_DESCRIPTION_STOP_COMMAND_COUNT)
        .register(meterRegistry);

    Gauge.builder(METRIC_NAME_ACTIVE_IOC_COUNT, () -> activeIocCount)
        .description(METRIC_DESCRIPTION_ACTIVE_IOCS)
        .register(meterRegistry);
  }

  private void registerHostMetrics() {
    Gauge.builder(METRIC_NAME_HOST_COUNT, () -> activeHostWithInactiveIoc)
        .description(METRIC_DESCRIPTION_ACTIVE_HOST_WITH_INACTIVE_IOC)
        .tag(KEY_STATUS, ACTIVE)
        .tag(KEY_HOSTS_IOC, Boolean.TRUE.toString())
        .tag(KEY_ACTIVE_IOC, Boolean.FALSE.toString())
        .register(meterRegistry);

    Gauge.builder(METRIC_NAME_HOST_COUNT, () -> activeHostWithActiveIoc)
        .description(METRIC_DESCRIPTION_ACTIVE_HOST_WITH_ACTIVE_IOC)
        .tag(KEY_STATUS, ACTIVE)
        .tag(KEY_HOSTS_IOC, Boolean.TRUE.toString())
        .tag(KEY_ACTIVE_IOC, Boolean.TRUE.toString())
        .register(meterRegistry);

    Gauge.builder(METRIC_NAME_HOST_COUNT, () -> activeHostWithoutIoc)
        .description(METRIC_DESCRIPTION_ACTIVE_HOST_WITHOUT_IOC)
        .tag(KEY_STATUS, ACTIVE)
        .tag(KEY_HOSTS_IOC, Boolean.FALSE.toString())
        .tag(KEY_ACTIVE_IOC, Boolean.FALSE.toString())
        .register(meterRegistry);

    Gauge.builder(METRIC_NAME_HOST_COUNT, () -> inactiveHostWithIoc)
        .description(METRIC_DESCRIPTION_INACTIVE_HOST_WITH_IOC)
        .tag(KEY_STATUS, INACTIVE)
        .tag(KEY_HOSTS_IOC, Boolean.TRUE.toString())
        .tag(KEY_ACTIVE_IOC, Boolean.FALSE.toString())
        .register(meterRegistry);

    Gauge.builder(METRIC_NAME_HOST_COUNT, () -> inactiveHostWithoutIoc)
        .description(METRIC_DESCRIPTION_INACTIVE_HOST_WITHOUT_IOC)
        .tag(KEY_STATUS, INACTIVE)
        .tag(KEY_HOSTS_IOC, Boolean.FALSE.toString())
        .tag(KEY_ACTIVE_IOC, Boolean.FALSE.toString())
        .register(meterRegistry);
  }

  private void registerJobMetrics() {
    for (Map.Entry<String, AwxJobType> jobTypeEntry : JOB_TYPES_MAP.entrySet()) {
      for (Map.Entry<String, List<String>> jobStatus : JOB_STATUS_MAP.entrySet()) {
        Gauge.builder(
                METRIC_NAME_JOB_COUNT,
                () ->
                    iocDeploymentRepository.countAwxJobsByTypeAndStatus(
                        jobTypeEntry.getValue().name(), jobStatus.getValue()))
            .description(METRIC_DESCRIPTION_JOBS)
            .tag(KEY_JOB_TYPE, jobTypeEntry.getKey())
            .tag(KEY_STATUS, jobStatus.getKey().toLowerCase())
            .register(meterRegistry);
      }
    }
  }

  @Scheduled(fixedDelayString = "${gauge.update.interval.in.ms}")
  private void updateGauges() {
    updateIOCsPerNetworkMetrics();

    activeIocCount.set(statisticsService.calculateRunningIocs());
    inactiveIocCount.set(iocRepository.countAll() - activeIocCount.get());
    iocsShouldNotBeRunning.set(
        activeIocCount.get()
            - activeIocsOnNetwork.values().stream().mapToLong(AtomicLong::get).sum());

    activeHostWithActiveIoc.set(
        statisticsService.calculateStatistics(StatisticType.HOSTS_WITH_ACTIVE_IOCS).getValue());
    activeHostWithInactiveIoc.set(
        statisticsService.calculateStatistics(StatisticType.HOSTS_REACHABLE).getValue()
            - activeHostWithActiveIoc.get());
    long activeHostsWithIocs = activeHostWithActiveIoc.get() + activeHostWithInactiveIoc.get();
    activeHostWithoutIoc.set(statisticsService.calculateActiveIocHosts() - activeHostsWithIocs);

    inactiveHostWithIoc.set(
        statisticsService.calculateStatistics(StatisticType.HOSTS_WITH_IOCS).getValue()
            - activeHostsWithIocs);
    inactiveHostWithoutIoc.set(
        statisticsService.calculateIocHosts()
            - (activeHostsWithIocs + activeHostWithoutIoc.get() + inactiveHostWithIoc.get()));
  }

  private void updateIOCsPerNetworkMetrics() {
    List<String> networkHasIocsDeployedOn = iocRepository.findNetworkContainingDeployedIocs();
    for (String network : networkHasIocsDeployedOn) {
      long deployedIocCount = iocRepository.countIocsWithActiveIocDeploymentsOnNetwork(network);
      long activeIocCount = statisticsService.calculateCurrentlyActiveIocsOnNetwork(network);
      registerUpdateActiveIocCountOnNetwork(network, activeIocCount);
      registerUpdateInactiveIocCountOnNetwork(network, deployedIocCount - activeIocCount);
    }
  }

  private void registerUpdateInactiveIocCountOnNetwork(String network, long inactiveIocCount) {
    if (inactiveIocsOnNetwork.containsKey(network)) {
      inactiveIocsOnNetwork.get(network).set(inactiveIocCount);
    } else {
      inactiveIocsOnNetwork.put(network, new AtomicLong(inactiveIocCount));
      Gauge.builder(METRIC_NAME_IOC_COUNT, () -> inactiveIocsOnNetwork.get(network))
          .description(METRIC_DESCRIPTION_IOCS)
          .tag(KEY_STATUS, INACTIVE)
          .tag(KEY_NETWORK, network)
          .register(meterRegistry);
    }
  }

  private void registerUpdateActiveIocCountOnNetwork(String network, long activeIocCount) {
    if (activeIocsOnNetwork.containsKey(network)) {
      activeIocsOnNetwork.get(network).set(activeIocCount);
    } else {
      activeIocsOnNetwork.put(network, new AtomicLong(activeIocCount));
      Gauge.builder(METRIC_NAME_IOC_COUNT, () -> activeIocsOnNetwork.get(network))
          .description(METRIC_DESCRIPTION_IOCS)
          .tag(KEY_STATUS, ACTIVE)
          .tag(KEY_NETWORK, network)
          .register(meterRegistry);
    }
  }
}
