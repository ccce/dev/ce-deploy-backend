/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.service.internal;

/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 */
public class PagingLimitDto {
  private int page;
  private Integer limit;
  private boolean listAll = false;

  public PagingLimitDto(int page, Integer limit) {
    this.page = page;
    this.limit = limit;
  }

  public PagingLimitDto(int page, Integer limit, boolean listAll) {
    this(page, limit);

    if (listAll) {
      this.listAll = true;
      this.page = 0;
      this.limit = null;
    }
  }

  public int getPage() {
    return page;
  }

  public Integer getLimit() {
    return limit;
  }

  public boolean isListAll() {
    return listAll;
  }
}
