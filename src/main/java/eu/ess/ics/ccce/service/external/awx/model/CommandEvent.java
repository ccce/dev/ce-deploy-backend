/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.service.external.awx.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import eu.ess.ics.ccce.common.Utils;
import java.util.Date;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 */
public class CommandEvent {
  private long id;
  private String type;
  private Date created;
  private Date modified;

  @JsonProperty("event_display")
  @SerializedName("event_display")
  private String eventDisplay;

  private String stdout;

  @JsonProperty("host_name")
  @SerializedName("host_name")
  private String hostName;

  public long getId() {
    return id;
  }

  public String getType() {
    return type;
  }

  public Date getCreated() {
    return Utils.cloneDate(created);
  }

  public Date getModified() {
    return Utils.cloneDate(modified);
  }

  public String getEventDisplay() {
    return eventDisplay;
  }

  public String getStdout() {
    return stdout;
  }

  public String getHostName() {
    return hostName;
  }
}
