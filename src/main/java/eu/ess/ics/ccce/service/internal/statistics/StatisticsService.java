/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.service.internal.statistics;

import eu.ess.ics.ccce.common.Utils;
import eu.ess.ics.ccce.exceptions.UnsupportedTypeException;
import eu.ess.ics.ccce.repository.api.IDeploymentStatRepository;
import eu.ess.ics.ccce.repository.entity.DeploymentStatisticsEntity;
import eu.ess.ics.ccce.repository.entity.IocDeploymentEntity;
import eu.ess.ics.ccce.repository.impl.IocDeploymentRepository;
import eu.ess.ics.ccce.repository.impl.IocRepository;
import eu.ess.ics.ccce.rest.model.statistics.request.StatisticType;
import eu.ess.ics.ccce.rest.model.statistics.response.*;
import eu.ess.ics.ccce.service.internal.host.HostService;
import eu.ess.ics.ccce.service.internal.status.StatusService;
import java.time.*;
import java.util.*;
import java.util.stream.Collectors;
import org.apache.commons.lang3.BooleanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 */
@Service
public class StatisticsService {
  private static final Logger LOGGER = LoggerFactory.getLogger(StatisticsService.class);

  @Value("${server.time-zone}")
  private String serverTimeZone;

  private final StatusService statusService;
  private final IocRepository iocRepository;
  private final IocDeploymentRepository deploymentRepository;
  private final IDeploymentStatRepository deploymentStatRepository;
  private final HostService hostService;

  public StatisticsService(
      StatusService statusService,
      IocRepository iocRepository,
      IocDeploymentRepository deploymentRepository,
      IDeploymentStatRepository deploymentStatRepository,
      HostService hostService) {
    this.statusService = statusService;
    this.iocRepository = iocRepository;
    this.deploymentRepository = deploymentRepository;
    this.deploymentStatRepository = deploymentStatRepository;
    this.hostService = hostService;
  }

  /**
   * Calculates from DB how many total, and successful deployments were grouped by day.
   *
   * @param numberOfDays how may days has to ba calculated/included in the stat.
   * @return list of total-, and successful deployments group by date.
   */
  public List<DeploymentStatisticsResponse> calculateDeployments(int numberOfDays) {

    LOGGER.debug("Deployment-statistics calculation started");

    List<DeploymentStatisticsResponse> result = new ArrayList<>();

    LocalDate tmRng = LocalDate.now();

    for (int i = 0; i < numberOfDays; i++) {
      LocalDate actualDate = tmRng.minusDays(i);

      Date specificDate =
          Date.from(actualDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());

      long allDeployments = deploymentRepository.countAllDeployments(actualDate);

      long successfulDeployments = deploymentRepository.countSuccessfulDeployments(actualDate);

      DeploymentStatisticsResponse d =
          new DeploymentStatisticsResponse(specificDate, allDeployments, successfulDeployments);

      result.add(d);
    }

    Collections.reverse(result);

    LOGGER.debug("Deployment-statistics calculation finished");

    return result;
  }

  public List<DeploymentOnHost> calculateIocStatistics(int numberOfDays) {

    List<DeploymentOnHost> result = new ArrayList<>();

    LocalDate tmRng = LocalDate.now();

    LocalDate actualDate = tmRng.minusDays(numberOfDays);

    Date specificDate =
        Date.from(actualDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());

    List<DeploymentStatisticsEntity> deploymentStats =
        deploymentStatRepository.collectDeploymentStat(specificDate);
    if (deploymentStats != null) {

      Map<String, List<DeploymentStatisticsEntity>> groupByNetwork =
          deploymentStats.stream()
              .collect(Collectors.groupingBy(DeploymentStatisticsEntity::getNetwork));

      groupByNetwork.forEach(
          (network, list) -> {
            List<DeploymentCount> collect =
                list.stream()
                    .map(i -> new DeploymentCount(i.getIocCount(), i.getCreatedAt()))
                    .sorted((i1, i2) -> i1.getCountingDate().compareTo(i2.getCountingDate()))
                    .collect(Collectors.toList());

            result.add(new DeploymentOnHost(network, collect));
          });
    }

    return result;
  }

  public long calculateCurrentlyActiveIocsOnNetwork(String network) {
    return iocRepository.findIocsWithActiveIocDeploymentsOnNetwork(network).stream()
        .filter(
            d ->
                BooleanUtils.toBoolean(
                    statusService.checkDeployedIocIsActive(
                        d.getNamingName(),
                        Utils.toZonedDateTime(d.getAwxJob().getAwxJobStartedAt(), serverTimeZone),
                        d.getHostFqdn())))
        .count();
  }

  public List<ActiveIOCSForHistoryResponse> calculateActiveIocHistory(int numberOfDays) {
    List<ActiveIOCSForHistoryResponse> result = new ArrayList<>();

    LocalDate tmRng = LocalDate.now();

    for (int i = numberOfDays; i > 0; i--) {
      LocalDate actualDate = tmRng.minusDays(i);

      Date specificDate =
          Date.from(actualDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());

      Long deploymentCount = deploymentStatRepository.countActiveIocs(specificDate);
      result.add(new ActiveIOCSForHistoryResponse(deploymentCount, specificDate));
    }

    return result;
  }

  public List<ActiveIOCSForHistoryResponse> calculateIocDeploymentHistory(int numberOfDays) {
    List<ActiveIOCSForHistoryResponse> result = new ArrayList<>();

    LocalDate tmRng = LocalDate.now();

    for (int i = numberOfDays; i > 0; i--) {
      LocalDate actualDate = tmRng.minusDays(i);

      Date specificDate =
          Date.from(actualDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());

      Long deploymentCount = deploymentStatRepository.countSuccessfulIocDeployments(actualDate);
      result.add(new ActiveIOCSForHistoryResponse(deploymentCount, specificDate));
    }

    return result;
  }

  /**
   * Calculating IOC/Host statistics based on the "request-type". Response is a number, and the
   * "request-type"
   *
   * @param statisticType determines which statistics should be created
   * @return The calculated statistic (number), and the request type
   */
  public StatisticResponse calculateStatistics(StatisticType statisticType) {
    switch (statisticType) {
      case IOCS_REGISTERED:
        return new StatisticResponse(calculateAllCreatedIocs(), statisticType);

      case IOCS_DEPLOYED:
        return new StatisticResponse(calculateCurrentlyDeployedIocs(), statisticType);

      case IOCS_RUNNING:
        return new StatisticResponse(calculateRunningIocs(), statisticType);

      case HOSTS_REGISTERED:
        return new StatisticResponse(calculateIocHosts(), statisticType);

      case HOSTS_WITH_IOCS:
        return new StatisticResponse(calculateHostsWithDeployments(), statisticType);

      case HOSTS_WITH_ACTIVE_IOCS:
        return new StatisticResponse(calculateHostWithActiveIOCs(), statisticType);

      case HOSTS_REACHABLE:
        return new StatisticResponse(calculateReachableHosts(), statisticType);

      default:
        {
          throw new UnsupportedTypeException("Unsupported statistic type");
        }
    }
  }

  /**
   * Calculates the number of Hosts that have actual deployments on, and Host is reachable (active)
   *
   * @return Number of active hosts with actual deployments
   */
  private long calculateReachableHosts() {
    List<String> hostsWithDeployments = deploymentRepository.findAllHostsWithActualDeployments();

    return hostsWithDeployments.stream()
        .filter(h -> BooleanUtils.toBoolean(statusService.checkHostIsActive(h)))
        .count();
  }

  /**
   * Calculates the number of Hosts with active IOCs deployed on. This function uses the
   * statusService.checkIocIsActive function to determine which IOC is active, and since it can
   * return a null value (indicating that there are no such metrics based on the criteria), the
   * BooleanUtils.toBoolean function is used to map null values to false.
   *
   * @return Number of hosts with active IOCs
   */
  private long calculateHostWithActiveIOCs() {
    return iocRepository.findIocsWithActiveIocDeployments().stream()
        .filter(
            deploymentEntity ->
                BooleanUtils.toBoolean(
                    statusService.checkDeployedIocIsActive(
                        deploymentEntity.getIoc().getNamingName(),
                        Utils.toZonedDateTime(
                            deploymentEntity.getAwxJob().getAwxJobStartedAt(), serverTimeZone),
                        deploymentEntity.getHostFqdn())))
        .map(IocDeploymentEntity::getHostFqdn)
        .distinct()
        .count();
  }

  /**
   * Calculates number of hosts that have actual deployments on
   *
   * @return Number of hosts with actual deployments
   */
  private int calculateHostsWithDeployments() {
    return deploymentRepository.findAllHostsWithActualDeployments().size();
  }

  /**
   * Calculates number of IOC hosts
   *
   * @return Number of IOC hosts
   */
  public long calculateIocHosts() {
    Long countIocsHost = hostService.countIocsHost();
    return countIocsHost == null ? 0 : countIocsHost;
  }

  public long calculateActiveIocHosts() {
    return statusService.listActiveIocHosts().size();
  }

  /**
   * Calculates how many (currently) deployed IOCs are running (according to Prometheus).
   *
   * @return Number of (currently) deployed IOCs that are running
   */
  public long calculateRunningIocs() {
    List<IocDeploymentEntity> allActualDeployments =
        iocRepository.findIocsWithActiveIocDeployments();

    return allActualDeployments.stream()
        .filter(
            d ->
                BooleanUtils.toBoolean(
                    statusService.checkDeployedIocIsActive(
                        d.getNamingName(),
                        Utils.toZonedDateTime(d.getAwxJob().getAwxJobStartedAt(), serverTimeZone),
                        d.getHostFqdn())))
        .count();
  }

  /**
   * Calculates number of (currently) deployed IOCs
   *
   * @return Number of (currently) deployed IOCs
   */
  private int calculateCurrentlyDeployedIocs() {
    return iocRepository.findIocsWithActiveIocDeployments().size();
  }

  /**
   * Calculates number of IOCs that has been created/registered in the tool
   *
   * @return Number of IOCs created/registered
   */
  private Long calculateAllCreatedIocs() {
    return iocRepository.countAll();
  }
}
