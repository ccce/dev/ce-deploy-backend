/*
 * Copyright (C) 2024 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.service.external.netbox.graphql;

import eu.ess.ics.ccce.service.external.netbox.graphql.model.GraphQLHostResponse;
import graphql.kickstart.spring.webclient.boot.GraphQLRequest;
import graphql.kickstart.spring.webclient.boot.GraphQLResponse;
import graphql.kickstart.spring.webclient.boot.GraphQLWebClient;
import java.util.HashMap;
import java.util.Map;
import org.springframework.stereotype.Service;

/**
 * @author <a href="mailto:domonkos.gulyas@ess.eu">Domonkos Gulyas</a>
 */
@Service
public class GraphQLClientService {

  private static final String DEVICE_QUERY =
      """
          query($id: [String]) {
            device_list(role: "ioc-host", id: $id) {
              id
              name
              description
              role {
                slug
              }
              tags {
                name
              }
              device_type {
                slug
              }
              primary_ip4 {
                dns_name
                assigned_object {
                  ... on InterfaceType {
                    untagged_vlan {
                      name
                      group{
                        name
                      }
                    }
                  }
                }
              }
            }
          }
          """;

  private static final String VM_QUERY =
      """
          query($id: [String]) {
            virtual_machine_list(role: "ioc-host", id: $id) {
              id
              name
              description
              role {
                slug
              }
              tags {
                name
              }
              primary_ip4 {
                dns_name
                assigned_object {
                  ... on VMInterfaceType{
                    untagged_vlan {
                      name
                      group{
                        name
                      }
                    }
                  }
                }
              }
            }
          }
          """;

  public static final String VM_EXIST_QUERY =
      """
          query($id: [String]) {
            virtual_machine_list(role: "ioc-host", id: $id) {
              id
            }
          }

          """;

  public static final String DEVICE_EXIST_QUERY =
      """
          query($id: [String]) {
            device_list(role: "ioc-host", id: $id) {
              id
            }
          }

          """;

  private static final String FIELD_NAME_DEVICE_LIST = "device_list";
  private static final String FIELD_NAME_VM_LIST = "virtual_machine_list";

  private final GraphQLWebClient graphQLWebClient;

  public GraphQLClientService(GraphQLWebClient graphQLWebClient) {
    this.graphQLWebClient = graphQLWebClient;
  }

  public GraphQLHostResponse fetchSingleDevice(long id) {
    return getSingleResponse(Long.toString(id), DEVICE_QUERY, FIELD_NAME_DEVICE_LIST);
  }

  public GraphQLHostResponse fetchSingleVM(long id) {
    return getSingleResponse(Long.toString(id), VM_QUERY, FIELD_NAME_VM_LIST);
  }

  public boolean isVirtualMachineExists(Long id) {
    return getSingleResponse(Long.toString(id), VM_EXIST_QUERY, FIELD_NAME_VM_LIST) != null;
  }

  public boolean isDeviceExists(Long id) {
    return getSingleResponse(Long.toString(id), DEVICE_EXIST_QUERY, FIELD_NAME_DEVICE_LIST) != null;
  }

  private GraphQLHostResponse getSingleResponse(
      String identifier, String queryString, String listFieldName) {
    Map<String, Object> variables = new HashMap<>();
    variables.put("id", identifier);
    GraphQLRequest request =
        GraphQLRequest.builder().query(queryString).variables(variables).build();

    GraphQLResponse response = graphQLWebClient.post(request).block();
    return response != null
        ? response.getList(listFieldName, GraphQLHostResponse.class).stream()
            .findFirst()
            .orElse(null)
        : null;
  }
}
