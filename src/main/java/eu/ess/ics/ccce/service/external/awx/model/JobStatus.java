/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.service.external.awx.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 */
public enum JobStatus {
  UNKNOWN,
  CREATED,
  QUEUED_BY_BACKEND,
  @SerializedName("new")
  @JsonProperty("new")
  NEW,
  @SerializedName("pending")
  @JsonProperty("pending")
  PENDING,
  @SerializedName("waiting")
  @JsonProperty("waiting")
  WAITING,
  @SerializedName("running")
  @JsonProperty("running")
  RUNNING,
  @SerializedName("failed")
  @JsonProperty("failed")
  FAILED,
  @SerializedName("canceled")
  @JsonProperty("canceled")
  CANCELED,
  @SerializedName("error")
  @JsonProperty("error")
  ERROR,
  @SerializedName("successful")
  @JsonProperty("successful")
  SUCCESSFUL
}
