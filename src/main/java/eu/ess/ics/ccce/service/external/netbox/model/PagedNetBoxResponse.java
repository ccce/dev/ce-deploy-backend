/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.service.external.netbox.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

/**
 * @author <a href="mailto:domonkos.gulyas@ess.eu">Domonkos Gulyas</a>
 */
public class PagedNetBoxResponse {
  private final Long count;
  private final String next;
  private final String previous;
  private final List<BaseNetBoxObject> results;

  @JsonCreator
  public PagedNetBoxResponse(
      @JsonProperty("count") Long count,
      @JsonProperty("next") String next,
      @JsonProperty("previous") String previous,
      @JsonProperty("results") List<BaseNetBoxObject> results) {
    this.count = count;
    this.next = next;
    this.previous = previous;
    this.results = results;
  }

  public Long getCount() {
    return count;
  }

  public String getNext() {
    return next;
  }

  public String getPrevious() {
    return previous;
  }

  public List<BaseNetBoxObject> getResults() {
    return results;
  }
}
