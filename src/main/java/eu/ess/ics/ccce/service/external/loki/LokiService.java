/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.service.external.loki;

import eu.ess.ics.ccce.common.Utils;
import eu.ess.ics.ccce.exceptions.LokiServiceException;
import eu.ess.ics.ccce.exceptions.RemoteServiceException;
import eu.ess.ics.ccce.rest.model.loki.response.LokiMessage;
import eu.ess.ics.ccce.rest.model.loki.response.LokiResponse;
import eu.ess.ics.ccce.service.external.common.HttpClientService;
import eu.ess.ics.ccce.service.external.loki.model.Result;
import eu.ess.ics.ccce.service.external.loki.model.Root;
import eu.ess.ics.ccce.service.internal.ioc.IocUtil;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import okhttp3.Headers;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 */
@Service
public class LokiService {

  private static final String QUERY_ENDPOINT = "loki/api/v1/query_range";
  private static final String QUERY_PARAM = "query";
  private static final String HOST_PARAM = "host";
  private static final String STEP_PARAM = "step";
  private static final String TIME_RANGE_START = "start";
  private static final String DIRECTION = "direction";
  private static final String BACKWARD = "BACKWARD";
  private static final Integer DEFAULT_TIME_RANGE_MINUTES = 60;
  private static final String STEP_VALUE = "5m";
  private static final String LIMIT = "limit";
  private static final Integer LIMIT_SIZE = 5000;
  private static final String IOC_NAME_FILTER = "|=";

  private static final Logger LOGGER = LoggerFactory.getLogger(LokiService.class);

  private final String baseUrl;

  private final String timeZone;

  private final HttpClientService httpClientService;

  public LokiService(
      HttpClientService httpClientService,
      @Value("${loki.server.address}") String baseUrl,
      @Value("${server.time-zone}") String timezone) {
    this.httpClientService = httpClientService;
    this.baseUrl = baseUrl;
    this.timeZone = timezone;
  }

  /**
   * Gets the logs for a specific host. timeRange is optional, value is in minutes. If hostname
   * contains network -> syslog will be queried. If hostname is without network -> procServ logs
   * will be queried. If line-count limit exceeds the internal limit a warning message will be shown
   * to the user (shown as a toast message)
   *
   * @param hostName the name of the host.
   * @param iocName name of the ioc for querying procServ logs (optional, can be null)
   * @param timeRange optional field for timeRange. Value is in minutes. Default value (if not set)
   *     is 120.
   * @return List of strings containing the logs (syslog, or procServ depending on the hostname).
   */
  public LokiResponse getLogLines(String hostName, String iocName, Integer timeRange) {

    try {
      HttpClientService.ServiceResponse<Root> lokiResponse =
          httpClientService.executeGetRequest(
              createAuthHeader(), buildUrl(hostName, iocName, timeRange), Root.class);

      if (HttpClientService.isSuccessHttpStatusCode(lokiResponse.getStatusCode())) {
        LokiResponse response = convertToLokiResponse(lokiResponse.getEntity(), timeZone);
        // Filter out error messages about not being able to access procserv logs
        response = filterProcservlogAccessErrors(response);

        if ((response.getLines() != null) && (response.getLines().size() >= LIMIT_SIZE)) {
          response.setWarning(
              "Log line limit (" + LIMIT_SIZE + ") exceeded, contact CSI if all lines are needed");
        }

        return response;
      }

      LOGGER.error(
          "Error while getting log lines (host: {}, ioc: {}) response code: {}, error message: {}",
          hostName,
          iocName,
          lokiResponse.getStatusCode(),
          lokiResponse.getErrorMessage());
      throw new LokiServiceException(
          "Failed to get log lines for host: "
              + hostName
              + (StringUtils.isNotEmpty(iocName) ? ", IOC: " + iocName : ""));
    } catch (SocketTimeoutException e) {
      LOGGER.error("Socket timeout while trying to fetch logs from Loki", e);
      throw new LokiServiceException("Failed to get log lines - connection timeout");
    } catch (MalformedURLException | RemoteServiceException e) {
      LOGGER.error("Exception while trying to get logs from Loki", e);
      throw new LokiServiceException(
          "Failed to get log lines for host: "
              + hostName
              + (StringUtils.isNotEmpty(iocName) ? ", IOC: " + iocName : ""));
    }
  }

  private LokiResponse filterProcservlogAccessErrors(LokiResponse response) {
    LokiResponse lokiResponse = new LokiResponse();
    lokiResponse.setLines(
        response.getLines().stream()
            .filter(
                line ->
                    !StringUtils.containsIgnoreCase(line.getLogMessage(), "Can not open directory"))
            .collect(Collectors.toList()));
    return lokiResponse;
  }

  private String getEndpointUrl(String endpointPostfix) throws MalformedURLException {
    return new URL(new URL(baseUrl), endpointPostfix).toString();
  }

  private String buildUrl(String hostname, String iocName, Integer timeRange)
      throws MalformedURLException {

    Integer queryTimeRange = DEFAULT_TIME_RANGE_MINUTES;

    UriComponentsBuilder ub = UriComponentsBuilder.fromUriString(getEndpointUrl(QUERY_ENDPOINT));

    // setting up the hostName as query param
    String hostInfo = createQueryString(hostname, iocName);

    // additional query parameters
    ub.queryParam(DIRECTION, BACKWARD);
    ub.queryParam(QUERY_PARAM, hostInfo);
    ub.queryParam(STEP_PARAM, STEP_VALUE);
    ub.queryParam(LIMIT, LIMIT_SIZE);

    if (timeRange != null) {
      queryTimeRange = timeRange;
    }

    LocalDateTime tmRng = LocalDateTime.ofInstant(new Date().toInstant(), ZoneId.systemDefault());

    LocalDateTime startTime = tmRng.minusMinutes(queryTimeRange);

    ub.queryParam(TIME_RANGE_START, startTime.atZone(ZoneId.systemDefault()).toEpochSecond());

    return ub.build(false).toUriString();
  }

  /**
   * Creates authentication header for Loki queries.
   *
   * @return headers containing the authentication info
   */
  private Headers createAuthHeader() {
    return Headers.of();
  }

  private String createQueryString(String hostName, String iocName) {
    boolean isSysLog = StringUtils.isEmpty(iocName);

    String result = "{host=~\"" + hostName + ".*\"}";

    if (isSysLog) {
      result += "!~\"procServ .+\"";
    } else {
      result +=
          "| pattern \"<service> <filename> <interesting_part>\" "
              + "| service=\"procServ\" "
              + "|=\""
              + IocUtil.transformIocName(iocName)
              + "\""
              + "| line_format \"{{.interesting_part}}\"";
    }

    try {
      return URLEncoder.encode(result, StandardCharsets.UTF_8.displayName());
    } catch (UnsupportedEncodingException e) {
      LOGGER.error("Error while trying to URL encode Loki parameters", e);
      return null;
    }
  }

  /**
   * Converts the response of the Loki to List of string response.
   *
   * @param lokiResponse the response from the Loki.
   * @return List of strings containing the dates, and logs. Empty list will be responded if no logs
   *     found.
   */
  public static LokiResponse convertToLokiResponse(Root lokiResponse, String timeZone) {
    LokiResponse result = new LokiResponse();
    List<LokiMessage> lokiLogList = new ArrayList<>();

    if (lokiResponse != null
        && lokiResponse.getData() != null
        && lokiResponse.getData().getResult() != null) {
      List<LokiMessage> logList = new ArrayList<>();
      result.setLines(logList);
      for (Result r : lokiResponse.getData().getResult()) {
        for (List<String> subVal : r.getValues()) {
          lokiLogList.add(lokiLogExtraction(subVal, timeZone));
        }
      }
    }

    result.setLines(convertLokiLogsToResponse(lokiLogList));

    // always return an empty list instead of NULL value
    if (result.getLines() == null) {
      result.setLines(Collections.emptyList());
    }

    return result;
  }

  /**
   * Sorts the extracted Loki message by time.
   *
   * @param lokiLogList message-list extracted from Loki response
   * @return sorted Loki messages by time, or empty list if Loki response was null/empty
   */
  private static List<LokiMessage> convertLokiLogsToResponse(List<LokiMessage> lokiLogList) {
    List<LokiMessage> result = new ArrayList<>(Collections.emptyList());

    if (lokiLogList != null) {
      // order list by date
      lokiLogList.sort(Comparator.comparing(LokiMessage::getLogDate));
      // add log-lines
      for (LokiMessage lokiLog : lokiLogList) {
        result.add(new LokiMessage(lokiLog.getLogDate(), lokiLog.getLogMessage()));
      }
    }

    return result;
  }

  /**
   * Converting extracted Loki message into DTO object.
   *
   * @param params List of string from the Loki response that has to be converted
   * @return Converted Loki message
   */
  private static LokiMessage lokiLogExtraction(List<String> params, String timeZone) {
    LokiMessage result = null;

    if (params != null) {
      result = new LokiMessage();

      for (String p : params) {
        if (StringUtils.isNumeric(p)) {
          long millis = TimeUnit.MILLISECONDS.convert(Long.parseLong(p), TimeUnit.NANOSECONDS);

          result.setLogDate(Utils.toZonedDateTime(new Date(millis), timeZone));
        } else {
          result.setLogMessage(result.getLogMessage() + p.replace("\n", ""));
        }
      }
    }

    return result;
  }
}
