/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.service.external.prometheus;

import eu.ess.ics.ccce.exceptions.PrometheusException;
import eu.ess.ics.ccce.exceptions.RemoteServiceException;
import eu.ess.ics.ccce.service.external.common.HttpClientService;
import eu.ess.ics.ccce.service.external.prometheus.model.PrometheusResponse;
import eu.ess.ics.ccce.service.external.prometheus.model.Result;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import okhttp3.Headers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 */
@Service
public class PrometheusService {

  private static final String PROMETHEUS_HOST = "prometheus.server.address";
  private static final String PROMETHEUS_URL_POSTFIX = "api/v1/";
  private static final String PROMETHEUS_IOC_METRICS_QUERY_PREFIX =
      "query?query={ioc=~\"%s\",instance=~\"%s\"}";
  private static final String PROMETHEUS_ACTIVE_IOC_QUERY =
      "query?query=node_systemd_unit_state{name=~\"ioc[@-].*\\\\.service*\", state=~\"active\"}";
  private static final String PROMETHEUS_HOSTS_QUERY =
      "query?query=up{job=\"node\",csentry_is_ioc=\"1\"}";
  private static final String PROMETHEUS_IOC_EXPORTER = "query?query=up{job=\"ioc_exporter\"}";

  private static final Logger LOGGER = LoggerFactory.getLogger(PrometheusService.class);

  private final Environment env;
  private final HttpClientService httpClientService;

  public PrometheusService(Environment env, HttpClientService httpClientService) {
    this.env = env;
    this.httpClientService = httpClientService;
  }

  private String buildUrl(String endpointPostfix) throws MalformedURLException {
    return new URL(
            new URL(env.getProperty(PROMETHEUS_HOST) + PROMETHEUS_URL_POSTFIX), endpointPostfix)
        .toString();
  }

  /**
   * Fetches all IOCs from Prometheus. Response will be cached for a short period of time.
   *
   * @return Prometheus's response for fetching all IOCs. If error occurs during fetching an empty
   *     list will be the response.
   */
  @Cacheable(value = "prometheus_active_ioc_cache")
  public List<Result> listAllIocs() {
    LOGGER.debug("Trying to fetch all IOCs from Prometheus");

    try {

      return fetchPrometheusResult(PROMETHEUS_ACTIVE_IOC_QUERY);

    } catch (MalformedURLException | PrometheusException | RemoteServiceException e) {
      LOGGER.error("Prometheus service or active IOC status information can't be reached", e);
    } catch (Exception e) {
      LOGGER.error("Unexpected error during active IOCs listing from Prometheus", e);
    }
    return Collections.emptyList();
  }

  public List<Result> listAllMetricsForAnIoc(final String iocName, final String hostName) {
    LOGGER.debug("Trying to fetch the IOC metrics from Prometheus");

    try {
      return fetchPrometheusResult(
          PROMETHEUS_IOC_METRICS_QUERY_PREFIX.formatted(iocName, hostName));
    } catch (MalformedURLException | PrometheusException | RemoteServiceException e) {
      LOGGER.error("Prometheus service or IOC metrics can't be reached", e);
    } catch (Exception e) {
      LOGGER.error("Unexpected error during IOCs metrics from Prometheus", e);
    }
    return Collections.emptyList();
  }

  /**
   * Lists all active IOC hosts from Prometheus. Response is cached for a short period of time on
   * the backend.
   *
   * @return List of active IOC hosts. If no hosts have been found empty list will be responded.
   */
  @Cacheable(value = "prometheus_active_ioc_host_cache")
  public List<Result> listActiveIocHosts() {
    try {

      return fetchPrometheusResult(PROMETHEUS_HOSTS_QUERY);

    } catch (MalformedURLException | PrometheusException | RemoteServiceException e) {
      LOGGER.error("Prometheus service or ioc host status information cannot be reached", e);
    } catch (Exception e) {
      LOGGER.error("Unexpected error during active ioc hosts listing from Prometheus", e);
    }
    return Collections.emptyList();
  }

  /**
   * Fetches all IOC hosts from Prometheus. The results are cached for a short period of time.
   *
   * @return fetched response from Prometheus for all IOC hosts. If error occurs an empty list will
   *     be the response.
   */
  @Cacheable(value = "prometheus_all_ioc_host")
  public List<Result> listAllHost() {
    LOGGER.debug("Trying to fetch all IOC host from Prometheus");

    try {

      return fetchPrometheusResult(PROMETHEUS_HOSTS_QUERY);

    } catch (PrometheusException | RemoteServiceException | MalformedURLException e) {
      LOGGER.error("Error while trying to fetch all IOC hosts", e);
    } catch (Exception e) {
      LOGGER.error("Unexpected error while fetching all hosts from Prometheus", e);
    }

    return Collections.emptyList();
  }

  /**
   * Fetches all IOC hosts from Prometheus. Response will be cached for a short amount of time.
   *
   * @return response from Prometheus for fetching all IOC hosts. If error occurs during request an
   *     empty list will be the response.
   */
  @Cacheable(value = "prometheus_host_cache")
  public List<String> listIocHosts() {
    try {
      PrometheusResponse response = callPrometheusForResult(buildUrl(PROMETHEUS_HOSTS_QUERY));
      return response.getData().getResult().stream()
          .map(r -> r.getMetric().getInstance())
          .collect(Collectors.toList());
    } catch (MalformedURLException | PrometheusException | RemoteServiceException e) {
      LOGGER.error("Prometheus service or host status information cannot be reached", e);
    } catch (Exception e) {
      LOGGER.error("Unexpected error during active hosts listing from Prometheus", e);
    }
    return Collections.emptyList();
  }

  /**
   * Fetches IOC exporter status from Prometheus Response will be cached for a short amount of time.
   *
   * @return response from Prometheus for checking all IOC exporter. If error occurs during request
   *     an empty list will be the response.
   */
  @Cacheable(value = "ioc_exporters")
  public List<Result> allIocExporters() {
    LOGGER.debug("Trying to fetch all IOC exporters from Prometheus");

    try {

      return fetchPrometheusResult(PROMETHEUS_IOC_EXPORTER);

    } catch (PrometheusException | RemoteServiceException | MalformedURLException e) {
      LOGGER.error("Cannot get IOC exporter list from Prometheus", e);
    } catch (Exception e) {
      LOGGER.error("Unexpected error while fetching all IOC exporters from Prometheus", e);
    }

    return Collections.emptyList();
  }

  private PrometheusResponse callPrometheusForResult(String url)
      throws PrometheusException, RemoteServiceException, SocketTimeoutException {
    LOGGER.debug("Call prometheus: {}", url);

    HttpClientService.ServiceResponse<PrometheusResponse> prometheusResponse =
        httpClientService.executeGetRequest(Headers.of(), url, PrometheusResponse.class);

    if (HttpClientService.isSuccessHttpStatusCode(prometheusResponse.getStatusCode())) {
      return prometheusResponse.getEntity();
    }

    throw new PrometheusException(
        "Error during getting Prometheus response (URL: "
            + url
            + "), status code: "
            + prometheusResponse.getStatusCode());
  }

  private List<Result> fetchPrometheusResult(String url)
      throws MalformedURLException,
          RemoteServiceException,
          PrometheusException,
          SocketTimeoutException {

    PrometheusResponse response = callPrometheusForResult(buildUrl(url));

    return response.getData().getResult();
  }
}
