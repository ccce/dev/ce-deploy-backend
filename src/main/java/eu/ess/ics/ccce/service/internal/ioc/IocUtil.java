/*
 * Copyright (C) 2025 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.service.internal.ioc;

import eu.ess.ics.ccce.repository.entity.IocEntity;
import eu.ess.ics.ccce.rest.model.ioc.response.ActiveDeployment;
import eu.ess.ics.ccce.rest.model.ioc.response.Ioc;
import eu.ess.ics.ccce.rest.model.ioc.response.IocInfo;
import eu.ess.ics.ccce.rest.model.job.response.Deployment;
import eu.ess.ics.ccce.rest.model.job.response.DeploymentHostInfo;
import eu.ess.ics.ccce.rest.model.job.response.JobStatus;
import eu.ess.ics.ccce.service.internal.ioc.dto.NamingResponseDTO;
import org.apache.commons.lang3.StringUtils;
import org.gitlab.api.models.GitlabProject;

/**
 * @author <a href="mailto:domonkos.gulyas@ess.eu">Domonkos Gulyas</a>
 */
public class IocUtil {

  private static final String IOC_NAME_UNDERSCORE = "_";

  private IocUtil() {
    throw new IllegalStateException("Utility class");
  }

  /**
   * Converts IOC entity and its parameters to IOC response object that will be consumed by a
   * client.
   *
   * @param project GitLab project
   * @param namingResponse IOC description from the Naming service and alerts if name has been
   *     changed
   * @param iocEntity the IOC entity from the DB
   * @param actualDeployment information about the current IOC deployment
   * @return IOC response that could be consumed by clients.
   */
  public static Ioc convertToIocDetails(
      GitlabProject project,
      NamingResponseDTO namingResponse,
      IocEntity iocEntity,
      Deployment actualDeployment) {
    Ioc result = null;

    if (iocEntity != null) {
      result =
          new Ioc(
              namingResponse.getDescription(),
              iocEntity.getCreatedBy(),
              iocEntity.getId(),
              iocEntity.getNamingName(),
              iocEntity.getNamingUuid(),
              project != null ? iocEntity.getGitProjectId() : null,
              project != null ? project.getHttpUrl() : null,
              actualDeployment,
              iocEntity.isDeployedWithOldPlaybook());
    }

    return result;
  }

  /**
   * Conversion for IOC info object (used by IOC listings)
   *
   * @param iocEntity the IOC entity from the DB
   * @param activeDeployment information about the active IOC deployment
   * @return IOC response DTO that could be consumed by clients
   */
  public static Ioc convertToIocInfo(IocEntity iocEntity, Deployment activeDeployment) {
    Ioc result = null;

    if (iocEntity != null) {
      result =
          new Ioc(
              iocEntity.getCreatedBy(),
              iocEntity.getId(),
              iocEntity.getNamingName(),
              iocEntity.getNamingUuid(),
              iocEntity.getGitProjectId(),
              activeDeployment,
              iocEntity.isDeployedWithOldPlaybook());
    }

    return result;
  }

  /**
   * Converts IOC information to response format. Used for list responses where not every detail is
   * needed
   *
   * @param ioc information about the IOC that has to be converted
   * @return basic IOC info for client response
   */
  public static IocInfo convertToIocInfo(Ioc ioc) {

    return new IocInfo(
        ioc.getId(),
        ioc.getNamingName(),
        ioc.getCreatedBy(),
        ioc.getGitProjectId(),
        ioc.isDeployedWithOldPlaybook(),
        convertToActiveDeployment(ioc.getActiveDeployment()));
  }

  /**
   * Converts deployment information to ActiveDeployment response.
   *
   * @param deployment information about an IOC deployment that has to be converted
   * @return ActiveDeployment info for client response
   */
  private static ActiveDeployment convertToActiveDeployment(Deployment deployment) {
    if (deployment == null) {
      return null;
    }

    return new ActiveDeployment(
        new DeploymentHostInfo(
            deployment.getHost().getHostId(),
            deployment.getHost().getHostName(),
            deployment.getHost().getNetwork(),
            deployment.getHost().getFqdn()),
        deployment.isUndeployment() ? null : deployment.getSourceVersion(),
        deployment.isUndeployment() ? null : deployment.getSourceVersionShort());
  }

  /**
   * Convert active deployment to actual deployment. An active deployment is the actual deployment
   * if it is not undeployment and belonging Ansible job was successful.
   *
   * @param activeFromDatabase Deployment DTO representing the active deployment of the IOC
   * @return Deployment DTO representing the actual deployment
   */
  public static Deployment activeDeploymentToActualDeployment(Deployment activeFromDatabase) {
    // Successfully undeployed IOC must be treated as not deployed
    return activeFromDatabase != null
            && activeFromDatabase.isUndeployment()
            && JobStatus.SUCCESSFUL.equals(activeFromDatabase.getOperationStatus())
        ? null
        : activeFromDatabase;
  }

  /**
   * Transforms iocName all non-alphanumeric, and non '-' characters to "_" instead.
   *
   * @param iocName the name of the ioc. If empty -> empty string will be responded.
   * @return replaces all non-alphanumeric, and '-' in iocName (":" will with replaced by "_")
   */
  public static String transformIocName(String iocName) {
    if (StringUtils.isNotEmpty(iocName)) {
      return iocName.replaceAll("[^a-zA-Z0-9-]", IOC_NAME_UNDERSCORE);
    }

    return iocName;
  }
}
