/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.service.external.csentry.model;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 */
public class CSEntryHostById extends BaseCSEntryHost {
  private final List<String> interfaces;

  public CSEntryHostById(
      long id,
      String fqdn,
      String name,
      String scope,
      boolean ioc,
      String deviceType,
      String description,
      String user,
      Date createdAt,
      Map<String, Object> ansibleVars,
      List<String> ansibleGroups,
      List<String> interfaces) {
    super(
        id,
        fqdn,
        name,
        scope,
        ioc,
        deviceType,
        description,
        user,
        createdAt,
        ansibleVars,
        ansibleGroups);
    this.interfaces = interfaces;
  }

  public List<String> getInterfaces() {
    return interfaces;
  }
}
