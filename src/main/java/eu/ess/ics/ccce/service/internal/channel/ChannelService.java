/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.service.internal.channel;

import com.google.common.collect.ImmutableList;
import eu.ess.ics.ccce.common.Utils;
import eu.ess.ics.ccce.common.alert.RecordAlertGenerator;
import eu.ess.ics.ccce.common.conversion.ConversionUtil;
import eu.ess.ics.ccce.common.conversion.PagingUtil;
import eu.ess.ics.ccce.exceptions.EntityNotFoundException;
import eu.ess.ics.ccce.repository.entity.IocEntity;
import eu.ess.ics.ccce.rest.model.Alert;
import eu.ess.ics.ccce.rest.model.record.PVStatus;
import eu.ess.ics.ccce.rest.model.record.response.PagedRecordResponse;
import eu.ess.ics.ccce.rest.model.record.response.Record;
import eu.ess.ics.ccce.rest.model.record.response.RecordAlertResponse;
import eu.ess.ics.ccce.rest.model.record.response.RecordDetails;
import eu.ess.ics.ccce.service.external.channelfinder.ChannelFinderService;
import eu.ess.ics.ccce.service.external.channelfinder.model.Channel;
import eu.ess.ics.ccce.service.external.channelfinder.model.Property;
import eu.ess.ics.ccce.service.internal.PagingLimitDto;
import eu.ess.ics.ccce.service.internal.host.HostService;
import eu.ess.ics.ccce.service.internal.ioc.IocService;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 */
@Service
public class ChannelService {

  private static final String CHANNEL_IOC_NAME_KEY = "iocName";
  private static final String CHANNEL_HOST_NAME_KEY = "hostName";
  private static final String CHANNEL_PV_STATUS_KEY = "pvStatus";
  private static final String CHANNEL_RECORD_DESC_KEY = "recordDesc";
  private static final String CHANNEL_RECORD_TYPE_KEY = "recordType";
  private static final String CHANNEL_ALIAS_KEY = "alias";
  private static final String CHANNEL_IOC_VERSION_KEY = "iocVersion";

  private static final Logger LOGGER = LoggerFactory.getLogger(ChannelService.class);
  private final PagingUtil utils;
  private final ChannelFinderService channelFinderService;

  private final IocService iocVersionService;
  private final HostService hostService;
  private final RecordAlertGenerator recordAlertGenerator;

  public ChannelService(
      PagingUtil utils,
      ChannelFinderService channelFinderService,
      IocService iocVersionService,
      HostService hostService,
      RecordAlertGenerator recordAlertGenerator) {
    this.utils = utils;
    this.channelFinderService = channelFinderService;
    this.iocVersionService = iocVersionService;
    this.hostService = hostService;
    this.recordAlertGenerator = recordAlertGenerator;
  }

  public PagedRecordResponse findAllRecords(
      String text, String iocName, PVStatus pvStatus, Integer page, Integer limit) {
    PagingLimitDto pagingLimitDto = utils.pageLimitConverter(page, limit);
    long totalCount = channelFinderService.countForPaging(text, null, iocName, pvStatus);
    List<Channel> channels;

    if (totalCount == 0) {
      totalCount = channelFinderService.countForPaging(null, text, iocName, pvStatus);
      channels =
          channelFinderService.findAllChannels(
              null,
              text,
              iocName,
              pvStatus,
              null,
              pagingLimitDto.getPage(),
              pagingLimitDto.getLimit());
    } else {
      channels =
          channelFinderService.findAllChannels(
              text,
              null,
              iocName,
              pvStatus,
              null,
              pagingLimitDto.getPage(),
              pagingLimitDto.getLimit());
    }

    List<Record> listResult = convertToRecordList(channels, iocVersionService);
    return new PagedRecordResponse(
        listResult,
        totalCount,
        listResult.size(),
        pagingLimitDto.getPage(),
        pagingLimitDto.getLimit());
  }

  public RecordDetails findRecord(String recordName) {
    Channel channel = channelFinderService.getChannelByRecordName(recordName);
    Record channelRecord = convertToRecord(channel, iocVersionService);
    IocEntity iocVersion = null;
    try {
      iocVersion = iocVersionService.findIocByNamingName(channelRecord.getIocName());
    } catch (EntityNotFoundException e) {
      LOGGER.info("No IOC in database for ChannelFinder record ({})", recordName, e);
    }

    String hostId = null;
    try {
      hostId =
          HostService.encodeHostIdentifier(
              hostService.findHostByFQDN(channelRecord.getHostName()).getNetBoxHostIdentifier());
    } catch (EntityNotFoundException e) {
      LOGGER.info(
          "No host found by NetBox service for ChannelFinder record (" + recordName + ")", e);
    }

    // time is in UTC, and in a special format, have to convert it
    for (int i = 0; i < channel.getProperties().size(); i++) {
      Property prop = channel.getProperties().get(i);
      if (prop.getName().equalsIgnoreCase("time")) {

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSSSSS");
        LocalDateTime dateTime = LocalDateTime.parse(prop.getValue(), formatter);
        ZonedDateTime utc = Utils.validateAndApplyTimeZone(dateTime, "UTC");

        Property newProp =
            new Property(
                prop.getName(),
                prop.getOwner(),
                utc.format(DateTimeFormatter.ISO_INSTANT),
                prop.getChannels());
        channel.getProperties().set(i, newProp);
      }
    }

    List<Channel> aliases =
        channelFinderService.findAllChannels(null, null, null, null, channel.getName(), 0, null);
    return convertToRecordDetails(
        recordName, channel, iocVersion != null ? iocVersion.getId() : null, hostId, aliases);
  }

  public RecordAlertResponse getAlerts(String recordName) {
    try {
      Record channelRecord = this.findRecord(recordName);
      List<Alert> alerts = recordAlertGenerator.generateAlerts(channelRecord);
      return new RecordAlertResponse(
          recordName, ConversionUtil.determineAlertSeverity(alerts), alerts);
    } catch (EmptyResultDataAccessException e) {
      throw new EntityNotFoundException("Record", recordName);
    }
  }

  /**
   * Converting PV status text fetched from ChannelFinder into PVStatus
   *
   * @param pvStatus the PV status received from ChannelFinder
   * @return the converted PVStatus, or <code>null</code>, if the string parameter is not
   *     convertable
   */
  private static PVStatus convertPvStatus(String pvStatus) {
    if (StringUtils.isNotEmpty(pvStatus)) {
      return StringUtils.equalsIgnoreCase(PVStatus.ACTIVE.value, pvStatus)
          ? PVStatus.ACTIVE
          : StringUtils.equalsIgnoreCase(PVStatus.INACTIVE.value, pvStatus)
              ? PVStatus.INACTIVE
              : null;
    }

    return null;
  }

  private static List<Record> convertToRecordList(List<Channel> channels, IocService iocService) {
    List<String> iocNames =
        channels.stream()
            .map(c -> c.getPropertyOrNull(CHANNEL_IOC_NAME_KEY))
            .filter(Objects::nonNull)
            .toList();

    Map<String, IocEntity> iocsByName = iocService.findIocsByName(iocNames);

    List<Record> result = new ArrayList<>();
    for (Channel c : channels) {
      String iocName = c.getPropertyOrNull(CHANNEL_IOC_NAME_KEY);

      IocEntity iocEnt = iocsByName.get(iocName);
      Long iocId = iocEnt == null ? null : iocEnt.getId();
      result.add(
          new Record(
              c.getName(),
              iocName,
              iocId,
              c.getPropertyOrNull(CHANNEL_HOST_NAME_KEY),
              convertPvStatus(c.getPropertyOrNull(CHANNEL_PV_STATUS_KEY)),
              c.getPropertyOrNull(CHANNEL_IOC_VERSION_KEY),
              c.getPropertyOrNull(CHANNEL_RECORD_DESC_KEY),
              c.getPropertyOrNull(CHANNEL_RECORD_TYPE_KEY),
              getAlias(c)));
    }

    return result;
  }

  private static Record convertToRecord(Channel channel, IocService iocService) {
    String iocName = channel.getPropertyOrNull(CHANNEL_IOC_NAME_KEY);

    Optional<IocEntity> iocEnt = iocService.findOptionalIocByNamingName(iocName);
    Long iocId = iocEnt.map(IocEntity::getId).orElse(null);

    return new Record(
        channel.getName(),
        iocName,
        iocId,
        channel.getPropertyOrNull(CHANNEL_HOST_NAME_KEY),
        convertPvStatus(channel.getPropertyOrNull(CHANNEL_PV_STATUS_KEY)),
        channel.getPropertyOrNull(CHANNEL_IOC_VERSION_KEY),
        channel.getPropertyOrNull(CHANNEL_RECORD_DESC_KEY),
        channel.getPropertyOrNull(CHANNEL_RECORD_TYPE_KEY),
        getAlias(channel));
  }

  private static RecordDetails convertToRecordDetails(
      String recordName, Channel channel, Long iocId, String hostId, List<Channel> aliases) {
    List<String> specialKeys =
        ImmutableList.of(
            CHANNEL_IOC_NAME_KEY,
            CHANNEL_HOST_NAME_KEY,
            CHANNEL_PV_STATUS_KEY,
            CHANNEL_IOC_VERSION_KEY);
    return new RecordDetails(
        channel.getName(),
        channel.getPropertyOrNull(CHANNEL_IOC_NAME_KEY),
        channel.getPropertyOrNull(CHANNEL_HOST_NAME_KEY),
        convertPvStatus(channel.getPropertyOrNull(CHANNEL_PV_STATUS_KEY)),
        channel.getPropertyOrNull(CHANNEL_IOC_VERSION_KEY),
        channel.getPropertyOrNull(CHANNEL_RECORD_DESC_KEY),
        channel.getPropertyOrNull(CHANNEL_RECORD_TYPE_KEY),
        getAlias(channel, recordName),
        iocId,
        hostId,
        channel.getProperties().stream()
            .filter(p -> !specialKeys.contains(p.getName()))
            .collect(Collectors.toMap(Property::getName, Property::getValue)),
        getAliasesToList(recordName, channel, aliases));
  }

  private static List<String> getAliasesToList(
      String recordName, Channel channel, List<Channel> aliases) {
    return aliases.stream()
        .map(Channel::getName)
        .filter(aliasName -> !recordName.equals(aliasName) && !channel.getName().equals(aliasName))
        .toList();
  }

  private static String getAlias(Channel channel, String recordName) {
    String aliasName = channel.getPropertyOrNull(CHANNEL_ALIAS_KEY);
    return (recordName.equals(aliasName) || channel.getName().equals(aliasName)) ? null : aliasName;
  }

  private static String getAlias(Channel channel) {
    String aliasName = channel.getPropertyOrNull(CHANNEL_ALIAS_KEY);
    return channel.getName().equals(aliasName) ? null : aliasName;
  }
}
