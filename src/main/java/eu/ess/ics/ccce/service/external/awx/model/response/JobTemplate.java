/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.service.external.awx.model.response;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 */
public class JobTemplate {
  public static class Related {
    private String launch;
    private String credentials;

    public String getLaunch() {
      return launch;
    }

    public void setLaunch(String launch) {
      this.launch = launch;
    }

    public String getCredentials() {
      return credentials;
    }
  }

  private long id;
  private String type;
  private String url;
  private String name;
  private Long inventory;
  private Related related;

  public long getId() {
    return id;
  }

  public String getType() {
    return type;
  }

  public String getUrl() {
    return url;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Related getRelated() {
    return related;
  }

  public void setRelated(Related related) {
    this.related = related;
  }

  public Long getInventory() {
    return inventory;
  }
}
