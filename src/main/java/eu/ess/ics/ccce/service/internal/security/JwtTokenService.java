/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.service.internal.security;

import eu.ess.ics.ccce.service.internal.security.dto.UserDetails;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 */
@Service
public class JwtTokenService {

  public static final String TOKEN = "token";
  public static final String ROLES = "roles";

  @Value("${jwt.secret}")
  private String secret;

  /**
   * Retrieves userName from JWT token.
   *
   * @param token the JWT token.
   * @return the userName from the JWT token.
   */
  public String getUsernameFromToken(String token) {
    return getClaimFromToken(token, Claims::getSubject);
  }

  /**
   * Retrieves expiration date from the JWT token.
   *
   * @param token the JWT token.
   * @return the token expiration date.
   */
  public Date getExpirationDateFromToken(String token) {
    return getClaimFromToken(token, Claims::getExpiration);
  }

  public <T> T getClaimFromToken(String token, Function<Claims, T> claimsResolver) {
    final Claims claims = getAllClaimsFromToken(token);
    return claimsResolver.apply(claims);
  }

  // for retrieving any information from token we will need the secret key
  private Claims getAllClaimsFromToken(String token) {
    return Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody();
  }

  // check if the token has expired
  private Boolean isTokenExpired(String token) {
    final Date expiration = getExpirationDateFromToken(token);
    return expiration.before(new Date());
  }

  /**
   * Retrieving a field value from the JWT token.
   *
   * @param token The JWT token.
   * @param key The field name(key) for which the value has to be looked up.
   * @param tClass Name of the return class.
   * @param <T> Type of the return class.
   * @return The value for the specific key from the JWT token.
   */
  public <T> T getClaim(String token, String key, Class<T> tClass) {
    final Claims claims = getAllClaimsFromToken(token);
    return claims.get(key, tClass);
  }

  /**
   * Generates JWT token from userInformation, and expiration interval.
   *
   * @param userDetails Information about the user.
   * @param tokenExpiration JWT token expiration interval.
   * @return The generated JWT token.
   */
  public String generateToken(UserDetails userDetails, long tokenExpiration) {
    Map<String, Object> claims = new HashMap<>();
    claims.put(TOKEN, userDetails.getToken());
    claims.put(ROLES, userDetails.getRoles());
    return doGenerateToken(claims, userDetails.getUserName(), tokenExpiration);
  }

  // while creating the token -
  // 1. Define  claims of the token, like Issuer, Expiration, Subject, and the ID
  // 2. Sign the JWT using the HS512 algorithm and secret key.
  // 3. According to JWS Compact
  // Serialization(https://tools.ietf.org/html/draft-ietf-jose-json-web-signature-41#section-3.1)
  //   compaction of the JWT to a URL-safe string
  private String doGenerateToken(Map<String, Object> claims, String subject, long tokenExpiration) {

    long jwtTokenValidity = tokenExpiration * 60;

    return Jwts.builder()
        .setClaims(claims)
        .setSubject(subject)
        .setIssuedAt(new Date(System.currentTimeMillis()))
        .setExpiration(new Date(System.currentTimeMillis() + jwtTokenValidity * 1000))
        .signWith(SignatureAlgorithm.HS512, secret)
        .compact();
  }

  // validate token

  /**
   * Checks if the JWT token is valid.
   *
   * @param token The JWT token that has to be checked.
   * @param userDetails The users details.
   * @return <code>true</code> if the JWT token is valid <code>false</code> if the JWT token is not
   *     valid
   */
  public Boolean validateToken(String token, UserDetails userDetails) {
    final String username = getUsernameFromToken(token);
    return (username.equals(userDetails.getUserName()) && !isTokenExpired(token));
  }
}
