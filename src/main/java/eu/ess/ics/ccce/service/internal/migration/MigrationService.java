package eu.ess.ics.ccce.service.internal.migration;

import eu.ess.ics.ccce.exceptions.EntityNotFoundException;
import eu.ess.ics.ccce.repository.api.IIocDeploymentRepository;
import eu.ess.ics.ccce.repository.entity.IocDeploymentEntity;
import eu.ess.ics.ccce.rest.model.host.response.HostInfoWithId;
import eu.ess.ics.ccce.rest.model.migration.response.MigrationDeviance;
import eu.ess.ics.ccce.service.internal.host.HostService;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 */
@Service
public class MigrationService {

  private static final Logger LOGGER = LoggerFactory.getLogger(MigrationService.class);

  private final IIocDeploymentRepository deploymentRepository;
  private final HostService hostService;

  @Autowired
  public MigrationService(IIocDeploymentRepository deploymentRepository, HostService hostService) {
    this.deploymentRepository = deploymentRepository;
    this.hostService = hostService;
  }

  public MigrationDeviance deviance() {
    Integer notFound = 0, networkNameChanged = 0, nameChanged = 0, bothChanged = 0, allJobs = 0;
    int limit = 30, page = 0;
    Set<Long> checkThisIocs = new HashSet<>();
    Set<String> networkScopes = new HashSet<>();

    long entCount = deploymentRepository.countAllEntities();
    while (allJobs < entCount) {
      List<IocDeploymentEntity> deplList = deploymentRepository.listEntities(page, limit);

      for (IocDeploymentEntity deployment : deplList) {
        try {
          HostInfoWithId host =
              hostService.getHostByNetBoxId(deployment.getNetBoxHostId(), deployment.isVm());

          if (host == null) {
            notFound++;
          } else {
            networkScopes.add(host.getScope());

            if (!host.getName().equalsIgnoreCase(deployment.getHostName())) {
              nameChanged++;
            }

            if (!host.getNetwork().equalsIgnoreCase(deployment.getHostNetwork())) {
              networkNameChanged++;
            }

            if ((!host.getNetwork().equalsIgnoreCase(deployment.getHostNetwork()))
                && (!host.getName().equalsIgnoreCase(deployment.getHostName()))) {
              bothChanged++;
            }
          }
        } catch (EntityNotFoundException e) {
          notFound++;
          checkThisIocs.add(deployment.getIoc().getId());
        }
      }

      allJobs += deplList.size();
      page++;
    }

    List<String> unresolvableIocs = checkThisIocs.stream().map(String::valueOf).toList();

    return new MigrationDeviance(
        notFound,
        networkNameChanged,
        nameChanged,
        bothChanged,
        allJobs,
        unresolvableIocs,
        networkScopes.stream().toList());
  }

  public void removeIocs(String networkScope, String iocPrefix) {
    if (StringUtils.isNotEmpty(networkScope)) {
      deleteIocsWithNetworkScope(networkScope);
    }

    if (StringUtils.isNotEmpty(iocPrefix)) {
      deleteNeverDeployedIocs(iocPrefix);
    }
  }

  private void deleteNeverDeployedIocs(String iocPrefix) {
    LOGGER.info("Deleting never deployed IOCs with {} prefix", iocPrefix);
    deploymentRepository.deleteNeverDeployedWithPrefix(iocPrefix);
    LOGGER.info("Never deployed IOCs with {} prefix deleted", iocPrefix);
  }

  private void deleteIocsWithNetworkScope(String networkScope) {
    LOGGER.info("Deleting IOCs deployed to {} network", networkScope);

    List<Long> iocIds = collectIOCIdOnNetwork(networkScope);

    deploymentRepository.deleteIocsFromANetwork(iocIds);
    LOGGER.info("IOCs deployed to {} network deleted", networkScope);
  }

  private List<Long> collectIOCIdOnNetwork(String networkScope) {
    int page = 0, limit = 100, entCount = 0;
    long all = deploymentRepository.countAllEntities();
    List<Long> result = new ArrayList<>();
    Set<String> scopes = new HashSet<>();

    while (entCount < all) {
      List<IocDeploymentEntity> deplList = deploymentRepository.listEntities(page, limit);

      for (IocDeploymentEntity deployment : deplList) {
        try {
          HostInfoWithId host =
              hostService.getHostByNetBoxId(deployment.getNetBoxHostId(), deployment.isVm());

          if (!scopes.contains(host.getScope())) {
            scopes.add(host.getScope());
          }

          if (host.getScope().equalsIgnoreCase(networkScope)) {
            result.add(deployment.getIoc().getId());
          }
        } catch (EntityNotFoundException e) {
          continue;
        }
      }

      page++;
      entCount += deplList.size();
    }

    return result.stream().distinct().toList();
  }
}
