/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.service.external.prometheus.model;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import java.util.HashMap;
import java.util.Map;

/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Metric {

  @JsonProperty("__name__")
  @SerializedName("__name__")
  private String serviceName;

  @JsonProperty("csentry_device_type")
  @SerializedName("csentry_device_type")
  private String csentryDeviceType;

  @JsonProperty("csentry_domain")
  @SerializedName("csentry_domain")
  private String csentryDomain;

  @JsonProperty("csentry_group_cslab_generallab")
  @SerializedName("csentry_group_cslab_generallab")
  private String csentryGroupCslabGenerallab;

  @JsonProperty("csentry_group_iocs")
  @SerializedName("csentry_group_iocs")
  private String csentryGroupIocs;

  @JsonProperty("csentry_group_labnetworks")
  @SerializedName("csentry_group_labnetworks")
  private String csentryGroupLabnetworks;

  @JsonProperty("csentry_group_technicalnetwork")
  @SerializedName("csentry_group_technicalnetwork")
  private String csentryGroupTechnicalnetwork;

  @JsonProperty("csentry_group_servers")
  @SerializedName("csentry_group_servers")
  private String csentryGroupServers;

  @JsonProperty("csentry_group_virtualmachine")
  @SerializedName("csentry_group_virtualmachine")
  private String csentryGroupVirtualmachine;

  @JsonProperty("csentry_is_ioc")
  @SerializedName("csentry_is_ioc")
  private String csentryIsIoc;

  @JsonProperty("csentry_vlan")
  @SerializedName("csentry_vlan")
  private String csentryVlan;

  @JsonProperty("csentry_vlan_id")
  @SerializedName("csentry_vlan_id")
  private String csentryVlanId;

  @JsonProperty("instance")
  private String instance;

  @JsonProperty("job")
  private String job;

  @JsonProperty("name")
  private String name;

  @JsonProperty("state")
  private String state;

  @JsonProperty("type")
  private String type;

  @JsonProperty("ioc")
  private String ioc;

  @JsonIgnore private Map<String, Object> additionalProperties = new HashMap<String, Object>();

  public String getServiceName() {
    return serviceName;
  }

  public void setServiceName(String serviceName) {
    this.serviceName = serviceName;
  }

  public String getCsentryDeviceType() {
    return csentryDeviceType;
  }

  public void setCsentryDeviceType(String csentryDeviceType) {
    this.csentryDeviceType = csentryDeviceType;
  }

  public String getCsentryDomain() {
    return csentryDomain;
  }

  public void setCsentryDomain(String csentryDomain) {
    this.csentryDomain = csentryDomain;
  }

  public String getCsentryGroupCslabGenerallab() {
    return csentryGroupCslabGenerallab;
  }

  public void setCsentryGroupCslabGenerallab(String csentryGroupCslabGenerallab) {
    this.csentryGroupCslabGenerallab = csentryGroupCslabGenerallab;
  }

  public String getCsentryGroupIocs() {
    return csentryGroupIocs;
  }

  public void setCsentryGroupIocs(String csentryGroupIocs) {
    this.csentryGroupIocs = csentryGroupIocs;
  }

  public String getCsentryGroupLabnetworks() {
    return csentryGroupLabnetworks;
  }

  public void setCsentryGroupLabnetworks(String csentryGroupLabnetworks) {
    this.csentryGroupLabnetworks = csentryGroupLabnetworks;
  }

  public String getCsentryGroupServers() {
    return csentryGroupServers;
  }

  public void setCsentryGroupServers(String csentryGroupServers) {
    this.csentryGroupServers = csentryGroupServers;
  }

  public String getCsentryGroupVirtualmachine() {
    return csentryGroupVirtualmachine;
  }

  public void setCsentryGroupVirtualmachine(String csentryGroupVirtualmachine) {
    this.csentryGroupVirtualmachine = csentryGroupVirtualmachine;
  }

  public String getCsentryIsIoc() {
    return csentryIsIoc;
  }

  public void setCsentryIsIoc(String csentryIsIoc) {
    this.csentryIsIoc = csentryIsIoc;
  }

  public String getCsentryVlan() {
    return csentryVlan;
  }

  public void setCsentryVlan(String csentryVlan) {
    this.csentryVlan = csentryVlan;
  }

  public String getCsentryVlanId() {
    return csentryVlanId;
  }

  public void setCsentryVlanId(String csentryVlanId) {
    this.csentryVlanId = csentryVlanId;
  }

  public void setAdditionalProperties(Map<String, Object> additionalProperties) {
    this.additionalProperties = additionalProperties;
  }

  public String getCsentryGroupTechnicalnetwork() {
    return csentryGroupTechnicalnetwork;
  }

  public void setCsentryGroupTechnicalnetwork(String csentryGroupTechnicalnetwork) {
    this.csentryGroupTechnicalnetwork = csentryGroupTechnicalnetwork;
  }

  @JsonProperty("instance")
  public String getInstance() {
    return instance;
  }

  @JsonProperty("instance")
  public void setInstance(String instance) {
    this.instance = instance;
  }

  @JsonProperty("job")
  public String getJob() {
    return job;
  }

  @JsonProperty("job")
  public void setJob(String job) {
    this.job = job;
  }

  @JsonProperty("name")
  public String getName() {
    return name;
  }

  @JsonProperty("name")
  public void setName(String name) {
    this.name = name;
  }

  @JsonProperty("state")
  public String getState() {
    return state;
  }

  @JsonProperty("state")
  public void setState(String state) {
    this.state = state;
  }

  @JsonProperty("type")
  public String getType() {
    return type;
  }

  @JsonProperty("type")
  public void setType(String type) {
    this.type = type;
  }

  @JsonProperty("ioc")
  public String getIoc() {
    return ioc;
  }

  @JsonProperty("ioc")
  public void setIoc(String ioc) {
    this.ioc = ioc;
  }

  @JsonAnyGetter
  public Map<String, Object> getAdditionalProperties() {
    return this.additionalProperties;
  }

  @JsonAnySetter
  public void setAdditionalProperty(String name, Object value) {
    this.additionalProperties.put(name, value);
  }
}
