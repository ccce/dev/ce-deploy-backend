/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.service.internal.status;

import eu.ess.ics.ccce.common.Constants;
import eu.ess.ics.ccce.common.Utils;
import eu.ess.ics.ccce.service.external.prometheus.PrometheusService;
import eu.ess.ics.ccce.service.external.prometheus.model.Metric;
import eu.ess.ics.ccce.service.external.prometheus.model.Result;
import eu.ess.ics.ccce.service.internal.ioc.IocUtil;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.commons.lang3.BooleanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 */
@Service
public class StatusService {

  private static final Double IS_OK = 1.0;

  Logger logger = LoggerFactory.getLogger(StatusService.class);

  private final PrometheusService prometheusService;

  private final ZonedDateTime playbookTransitionTime;

  public StatusService(PrometheusService prometheusService, ZonedDateTime playbookTransitionTime) {
    this.prometheusService = prometheusService;
    this.playbookTransitionTime = playbookTransitionTime;
  }

  /**
   * Gets status information about specific IOC running on a specific host from Prometheus. Requests
   * are cached for a small amount of time (for specific host) in order to reduce network traffic!
   * (Checks for .service along with state=active flag in Prometheus)
   *
   * @param iocName the naming-name of the ioc for which the status has to be determined
   * @param deploymentTime deployment time of the IOC - if null, IOC is not deployed
   * @param hostName the host fqdn on which the ioc may run, for which the status has to be
   *     determined
   * @return <code>true</code>, if ioc is available <code>false</code>, if ioc is not active <code>
   *     null</code> if IOC can not be found in Prometheus response, or IOC is not deployed
   */
  public Boolean checkDeployedIocIsActive(
      String iocName, ZonedDateTime deploymentTime, String hostName) {

    if (!StringUtils.hasText(hostName) || (deploymentTime == null)) {
      return null;
    }

    List<Result> iocList = prometheusService.listAllIocs();

    String metricNamePrefix = Utils.getIocMetricPrefix(deploymentTime, playbookTransitionTime);

    boolean found = false;

    for (Result r : iocList) {
      Metric metric = r.getMetric();

      if ((metric.getInstance().equals(hostName))
          && (metric
              .getName()
              .equals(metricNamePrefix + IocUtil.transformIocName(iocName) + ".service"))) {
        found = true;
        if (isValid(r.getValue())) {
          return true;
        }
      }
    }

    if (found) {
      return false;
    }

    // IOC entry not found in Prometheus -> could be stopped, or the host is "down"
    if (BooleanUtils.isTrue(checkHostIsActive(hostName))) {
      return false;
    }

    return null;
  }

  public Boolean checkNonDeployedIocIsActive(String iocName) {

    List<Result> iocList = prometheusService.listAllIocs();

    boolean found = false;

    for (Result r : iocList) {
      Metric metric = r.getMetric();

      if ((metric
              .getName()
              .equals(
                  Constants.OLD_IOC_METRICS_PREFIX
                      + IocUtil.transformIocName(iocName)
                      + ".service"))
          || (metric
              .getName()
              .equals(
                  Constants.NEW_IOC_METRICS_PREFIX
                      + IocUtil.transformIocName(iocName)
                      + ".service"))) {
        found = true;
        if (isValid(r.getValue())) {
          return true;
        }
      }
    }

    if (found) {
      return false;
    }

    return null;
  }

  public List<String> fetchWhereIOCIsRunning(String iocName) {

    List<String> result = new ArrayList<>();

    List<Result> iocList = prometheusService.listAllIocs();

    for (Result r : iocList) {
      Metric metric = r.getMetric();

      if ((metric
              .getName()
              .equals(
                  Constants.OLD_IOC_METRICS_PREFIX
                      + IocUtil.transformIocName(iocName)
                      + ".service"))
          || (metric
              .getName()
              .equals(
                  Constants.NEW_IOC_METRICS_PREFIX
                      + IocUtil.transformIocName(iocName)
                      + ".service"))) {
        if (isValid(r.getValue())) {
          result.add(metric.getInstance());
        }
      }
    }

    return result;
  }

  private boolean isValid(List<Double> value) {
    for (Double v : value) {
      if (IS_OK.equals(v)) {
        return true;
      }
    }

    return false;
  }

  public List<String> checkIOCMetrics(final String iocName, final String hostName) {
    if (!StringUtils.hasText(iocName) || !StringUtils.hasText(hostName)) {
      return Collections.emptyList();
    }
    return prometheusService.listAllMetricsForAnIoc(iocName, hostName).stream()
        .filter(result -> isValid(result.getValue()))
        .map(Result::getMetric)
        .map(Metric::getServiceName)
        .collect(Collectors.toList());
  }

  /**
   * Checks if IOC exporter service is up and running on a specific host
   *
   * @param hostName the host where the exporter should be checked
   * @return
   *     <p><b>true</b> if the PV exporter service is up and running on the host
   *     <p><b>false</b> if the PV exporter is down on the host
   *     <p><b>null</b> if PV exporter can not be found in Prometheus response
   */
  public Boolean checkIOCExporterForHost(String hostName) {

    List<Result> exporterResponse = prometheusService.allIocExporters();

    return checkForHostInResponse(hostName, exporterResponse);
  }

  /**
   * Gets status information about specific host from Prometheus. Requests are cached for a small
   * amount of time (for specific host) in order to reduce network traffic! (Checks for job=node in
   * Prometheus)
   *
   * @param hostName the name of the host for which the status has to be determined
   * @return
   *     <p><b>true</b>, if host is available
   *     <p><b>false</b>, if host is not available
   *     <p><b>null</b> if there was a problem during the Prometheus query
   */
  public Boolean checkHostIsActive(String hostName) {

    List<Result> hostList = prometheusService.listAllHost();

    return checkForHostInResponse(hostName, hostList);
  }

  private Boolean checkForHostInResponse(String hostName, List<Result> prometheusResponse) {
    if (org.apache.commons.lang3.StringUtils.isEmpty(hostName)) {
      return null;
    }

    if ((prometheusResponse == null) || (prometheusResponse.isEmpty())) {
      return null;
    }

    boolean found = false;

    for (Result r : prometheusResponse) {
      Metric metric = r.getMetric();

      if (metric.getInstance().equals(hostName)) {

        found = true;
        if (isValid(r.getValue())) {
          return true;
        }
      }
    }

    if (found) {
      return false;
    }

    return null;
  }

  public List<Result> listActiveIocHosts() {
    logger.debug("Trying to fetch all active IOC hosts from Prometheus");

    List<Result> activeHosts = prometheusService.listActiveIocHosts();

    return activeHosts.stream().filter(h -> isValid(h.getValue())).collect(Collectors.toList());
  }

  public List<String> listIocHosts() {
    logger.debug("Trying to fetch all IOC hosts from Prometheus");

    return prometheusService.listIocHosts();
  }
}
