/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.service.external.awx.model.request;

import com.google.gson.annotations.SerializedName;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 */
public class NewCommandBody {
  @SerializedName("job_type")
  private String jobType;

  private long inventory;
  private String limit;
  private long credential;

  @SerializedName("module_name")
  private String moduleName;

  @SerializedName("module_args")
  private String moduleArgs;

  private int forks;
  private int verbosity;

  @SerializedName("extra_vars")
  private String extraVars;

  @SerializedName("become_enabled")
  private boolean becomeEnabled;

  @SerializedName("diff_mode")
  private boolean diffMode;

  public NewCommandBody(
      String jobType,
      long inventory,
      String limit,
      long credential,
      String moduleName,
      String moduleArgs,
      int forks,
      int verbosity,
      String extraVars,
      boolean becomeEnabled,
      boolean diffMode) {
    this.jobType = jobType;
    this.inventory = inventory;
    this.limit = limit;
    this.credential = credential;
    this.moduleName = moduleName;
    this.moduleArgs = moduleArgs;
    this.forks = forks;
    this.verbosity = verbosity;
    this.extraVars = extraVars;
    this.becomeEnabled = becomeEnabled;
    this.diffMode = diffMode;
  }

  public String getJobType() {
    return jobType;
  }

  public long getInventory() {
    return inventory;
  }

  public String getLimit() {
    return limit;
  }

  public long getCredential() {
    return credential;
  }

  public String getModuleName() {
    return moduleName;
  }

  public String getModuleArgs() {
    return moduleArgs;
  }

  public int getForks() {
    return forks;
  }

  public int getVerbosity() {
    return verbosity;
  }

  public String getExtraVars() {
    return extraVars;
  }

  public boolean isBecomeEnabled() {
    return becomeEnabled;
  }

  public boolean isDiffMode() {
    return diffMode;
  }
}
