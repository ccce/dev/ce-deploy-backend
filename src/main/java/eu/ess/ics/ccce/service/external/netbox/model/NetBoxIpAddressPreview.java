/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.service.external.netbox.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author <a href="mailto:domonkos.gulyas@ess.eu">Domonkos Gulyas</a>
 */
public class NetBoxIpAddressPreview extends NetBoxPropertyPreview {
  private final Long family;
  private final String address;

  @JsonCreator
  public NetBoxIpAddressPreview(
      @JsonProperty("id") Long id,
      @JsonProperty("url") String url,
      @JsonProperty("display") String display,
      @JsonProperty("family") Long family,
      @JsonProperty("address") String address) {
    super(id, url, display, null, null);
    this.family = family;
    this.address = address;
  }

  public Long getFamily() {
    return family;
  }

  public String getAddress() {
    return address;
  }
}
