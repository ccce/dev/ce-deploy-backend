/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.service.internal.ioc;

import static eu.ess.ics.ccce.common.Constants.*;

import com.fasterxml.jackson.core.JsonProcessingException;
import eu.ess.ics.ccce.common.Utils;
import eu.ess.ics.ccce.common.alert.IOCAlertGenerator;
import eu.ess.ics.ccce.common.conversion.ConversionUtil;
import eu.ess.ics.ccce.common.conversion.PagingUtil;
import eu.ess.ics.ccce.configuration.WebSecurityConfig;
import eu.ess.ics.ccce.exceptions.*;
import eu.ess.ics.ccce.repository.api.IIocDeploymentRepository;
import eu.ess.ics.ccce.repository.api.IIocRepository;
import eu.ess.ics.ccce.repository.entity.AwxJobEntity;
import eu.ess.ics.ccce.repository.entity.IocDeploymentEntity;
import eu.ess.ics.ccce.repository.entity.IocEntity;
import eu.ess.ics.ccce.rest.model.Alert;
import eu.ess.ics.ccce.rest.model.BatchType;
import eu.ess.ics.ccce.rest.model.git.response.GitReference;
import eu.ess.ics.ccce.rest.model.host.response.HostInfoWithId;
import eu.ess.ics.ccce.rest.model.host.response.PagedAssociatedIocs;
import eu.ess.ics.ccce.rest.model.ioc.request.CreateIoc;
import eu.ess.ics.ccce.rest.model.ioc.request.IOCUpdateRequest;
import eu.ess.ics.ccce.rest.model.ioc.request.IocDeploymentStatus;
import eu.ess.ics.ccce.rest.model.ioc.request.IocOrder;
import eu.ess.ics.ccce.rest.model.ioc.response.*;
import eu.ess.ics.ccce.rest.model.job.request.DeploymentRequest;
import eu.ess.ics.ccce.rest.model.job.response.Deployment;
import eu.ess.ics.ccce.rest.model.job.response.Job;
import eu.ess.ics.ccce.service.external.gitlab.GitLabService;
import eu.ess.ics.ccce.service.external.naming.NamingService;
import eu.ess.ics.ccce.service.external.naming.model.NamingIocResponse;
import eu.ess.ics.ccce.service.external.netbox.model.NetBoxHost;
import eu.ess.ics.ccce.service.external.netbox.model.NetBoxHostIdentifier;
import eu.ess.ics.ccce.service.internal.PagingLimitDto;
import eu.ess.ics.ccce.service.internal.deployment.DeploymentService;
import eu.ess.ics.ccce.service.internal.deployment.JobUtil;
import eu.ess.ics.ccce.service.internal.deployment.dto.DeploymentTask;
import eu.ess.ics.ccce.service.internal.host.HostService;
import eu.ess.ics.ccce.service.internal.ioc.dto.NamingResponseDTO;
import eu.ess.ics.ccce.service.internal.security.AuthorizationService;
import eu.ess.ics.ccce.service.internal.security.dto.UserDetails;
import eu.ess.ics.ccce.service.internal.status.StatusService;
import jakarta.annotation.Nonnull;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.apache.commons.lang3.StringUtils;
import org.gitlab.api.models.GitlabProject;
import org.gitlab4j.api.GitLabApiException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 */
@Service
public class IocService {

  private static final Logger LOGGER = LoggerFactory.getLogger(IocService.class);

  private final IIocRepository iocRepository;
  private final PagingUtil utils;
  private final DeploymentService deploymentService;
  private final StatusService statusService;
  private final AuthorizationService authorizationService;
  private final MetadataService metadataService;
  private final NamingService namingService;
  private final GitLabService gitLabService;
  private final IOCAlertGenerator IOCAlertGenerator;
  private final HostService hostService;
  private final IIocDeploymentRepository deploymentRepository;
  private final Integer iocsOnHostLimit;

  public IocService(
      IIocRepository iocRepository,
      PagingUtil utils,
      DeploymentService deploymentService,
      StatusService statusService,
      AuthorizationService authorizationService,
      MetadataService metadataService,
      NamingService namingService,
      GitLabService gitLabService,
      IOCAlertGenerator IOCAlertGenerator,
      HostService hostService,
      IIocDeploymentRepository deploymentRepository,
      Integer iocsOnHostLimit) {
    this.iocRepository = iocRepository;
    this.utils = utils;
    this.deploymentService = deploymentService;
    this.statusService = statusService;
    this.authorizationService = authorizationService;
    this.metadataService = metadataService;
    this.namingService = namingService;
    this.gitLabService = gitLabService;
    this.IOCAlertGenerator = IOCAlertGenerator;
    this.hostService = hostService;
    this.deploymentRepository = deploymentRepository;
    this.iocsOnHostLimit = iocsOnHostLimit;
  }

  @Value("${server.time-zone}")
  private String serverTimeZone;

  /**
   * Fetches the IOC from DB by ID. Also checking if the naming-name has been changed in Naming. If
   * name has been changed the "name-change" flag updated in the DB.
   *
   * @param id the IOC ID.
   * @return the IOC detail response
   */
  public Ioc searchIocById(long id) {
    IocEntity iocEnt = searchAndCheckById(id);

    return extractForIocDetails(iocEnt);
  }

  public IocEntity searchAndCheckById(long id) {
    try {
      return iocRepository.findById(id);
    } catch (EmptyResultDataAccessException e) {
      throw new EntityNotFoundException("IOC", id);
    }
  }

  public IocDetails getIoc(long id) {
    IocEntity iocEntity = searchAndCheckById(id);
    IocDetails iocDetails = new IocDetails(extractForIocDetails(iocEntity));
    iocDetails.setOperationInProgress(
        deploymentService.hasOngoingDeployment(iocEntity)
            || deploymentService.hasOngoingCommand(iocEntity));
    return iocDetails;
  }

  public IocEntity findIocById(long id) {
    try {
      return iocRepository.findById(id);
    } catch (EmptyResultDataAccessException e) {
      throw new EntityNotFoundException("IOC", id);
    }
  }

  /**
   * Checks IOC name and description in the Naming service. If name has been changed, and IOC is not
   * deployed -> it updates data in DB (no alert in this case). If name has been changed and IOC is
   * deployed -> creates alert.
   *
   * @param ioc the IOC entity from the DB
   * @return A DTO object containing: description of the IOC boolean of slot if it has been deleted
   *     boolean of name if it has been changed
   */
  public NamingResponseDTO checkOrUpdateIocFromNamingService(IocEntity ioc) {

    if (ioc != null) {
      NamingIocResponse namingIocResponse = null;
      try {
        namingIocResponse = namingService.getIocByUuid(ioc.getNamingUuid());
      } catch (EntityNotFoundException e) {
        LOGGER.error("Name cannot be found in the Naming service", e);
        return new NamingResponseDTO.NamingResponseBuilder().nameDeleted().build();
      } catch (NameInactiveException e) {
        LOGGER.error("Name has an inactive status in the Naming service", e);
        return new NamingResponseDTO.NamingResponseBuilder().nameInactive().build();
      } catch (NameTypeInvalidException e) {
        LOGGER.error("Name is a non SC-IOC", e);
        return new NamingResponseDTO.NamingResponseBuilder().typeInvalid().build();
      } catch (Exception e) {
        LOGGER.error("Name cannot be fetched from the Naming service", e);
      }

      if (namingIocResponse == null) {
        return new NamingResponseDTO.NamingResponseBuilder().build();
      }

      String iocDescription = namingIocResponse.getDescription();

      // Naming entry exists but Name has been deleted
      if ((namingIocResponse.getUuid() == null) && (ioc.getNamingUuid() != null)) {
        return new NamingResponseDTO.NamingResponseBuilder()
            .description(iocDescription)
            .nameDeleted()
            .build();
      }

      // name has been changed
      if (!StringUtils.equals(namingIocResponse.getName(), ioc.getNamingName())) {

        Deployment actualDeployment =
            IocUtil.activeDeploymentToActualDeployment(
                deploymentService.findActiveDeploymentForIocInfo(ioc.getId()));

        // has actual deployment, create ALERT
        if (actualDeployment != null) {
          return new NamingResponseDTO.NamingResponseBuilder()
              .description(iocDescription)
              .nameChanged()
              .currentIocName(namingIocResponse.getName())
              .build();
          // IOC is not deployed, change in DB
        } else {
          ioc.setNamingName(namingIocResponse.getName());

          iocRepository.updateIoc(ioc);
        }
      }

      return new NamingResponseDTO.NamingResponseBuilder().description(iocDescription).build();
    }
    // IOC was NULL
    return null;
  }

  /**
   * TRANSACTIONAL method! Method used to create a new IOC.
   *
   * @param iocToCreate parameters for IOC to create entities in the DB
   * @param userDetails information about the user who would like to create the IOC
   * @return detailed information about the created IOC
   * @throws MetaDataFileException if there is a problem when trying to find/parse metadata on the
   *     git repo
   * @throws UnauthorizedException if user doesn't have right to create IOC with the given
   *     parameters
   * @throws GitlabServiceException if there was a problem when trying to access git
   * @throws ServiceException in any other case if a problem occurs during IOC creation
   */
  @Transactional
  public Ioc createNewIoc(CreateIoc iocToCreate, UserDetails userDetails)
      throws MetaDataFileException,
          UnauthorizedException,
          GitlabServiceException,
          ServiceException,
          GitLabApiException {

    final NamingIocResponse fromNamingService =
        getIocFromNamingServiceByUuid(iocToCreate.getNamingUuid());
    checkDuplicationForName(fromNamingService.getName());

    Long gitProjectId = createOrGetGitProject(iocToCreate);
    checkDuplicationForGitRepository(gitProjectId);

    String userName = userDetails.getUserName();
    authorizationService.checkDeploymentRights(gitProjectId, userDetails);

    IocEntity ioc = createNewIOCEntity(userName, fromNamingService, gitProjectId);

    LOGGER.debug("IOC CREATED by [{}] with parameters: {}", userName, iocToCreate);

    return IocUtil.convertToIocDetails(
        gitLabService.getNullableGitlabProject(gitProjectId),
        checkOrUpdateIocFromNamingService(ioc),
        ioc,
        null);
  }

  private Long createOrGetGitProject(CreateIoc iocToCreate) throws GitLabApiException {

    String repoName = iocToCreate.getRepoName();

    if (StringUtils.isNoneEmpty(repoName)) {
      if (!repoName.startsWith("e3-ioc-")) {
        repoName = "e3-ioc-" + repoName;
      }

      return gitLabService.createTemplatedGitProject(repoName).getId();
    }

    return iocToCreate.getGitProjectId();
  }

  private void checkDuplicationForGitRepository(final Long gitProjectId) {
    if (iocRepository.findIocByRepoId(gitProjectId) != null) {
      throw new ConflictException("Git repository is already in use!");
    }
  }

  private void checkDuplicationForName(final String namingName) {
    if (iocRepository.countIocByNamingName(namingName) > 0) {
      throw new ConflictException("IOC name is already in use!");
    }
  }

  private NamingIocResponse getIocFromNamingServiceByUuid(final String uuid) {
    try {
      return namingService.getIocByUuid(uuid);
    } catch (EntityNotFoundException e) {
      LOGGER.error(e.getMessage());
      throw e;
    } catch (NamingServiceException e) {
      LOGGER.error("Name identifier not found in the Naming service while trying to create IOC");
      throw new IncompleteRequestException("Name identifier not found in the Naming service");
    } catch (Exception e) {
      throw new UnprocessableEntityException(e.getMessage());
    }
  }

  /**
   * Creates an IOC-, and IOCVersion entity in the DB with the specific parameters. Used to create a
   * new IOC from UI with minimal parameters.
   *
   * @param userName the user who created the IOC
   * @param namingIocResponse detailed information about the naming-name that will be used by the
   *     IOC
   * @param gitProjectId the git project ID where the IOC code resides
   * @return the created IOC entity with the set parameters
   */
  @Transactional
  public IocEntity createNewIOCEntity(
      String userName, NamingIocResponse namingIocResponse, Long gitProjectId) {
    IocEntity ioc = new IocEntity();
    ioc.setCreatedBy(userName);
    ioc.setGitProjectId(gitProjectId);
    ioc.setNamingUuid(namingIocResponse.getUuid());
    ioc.setNamingName(namingIocResponse.getName());
    ioc.setDeployedWithOldPlaybook(true);
    iocRepository.createIoc(ioc);

    return ioc;
  }

  public PagedIocResponse findAll(
      IocDeploymentStatus deploymentStatus,
      String createdBy,
      String network,
      String networkScope,
      String hostType,
      String hostName,
      String searchText,
      IocOrder orderBy,
      Boolean isAsc,
      Integer page,
      Integer limit,
      boolean listAll) {
    List<IocInfo> listResult = new ArrayList<>();

    PagingLimitDto pagingLimitDto = utils.pageLimitConverter(page, limit, listAll);

    String createdByString = StringUtils.isEmpty(createdBy) ? null : createdBy;
    String searchTextString = StringUtils.isEmpty(searchText) ? null : searchText;
    String networkString = StringUtils.isEmpty(network) ? null : network;
    String networkScopeString = StringUtils.isEmpty(networkScope) ? null : networkScope;
    String hostTypeString = StringUtils.isEmpty(hostType) ? null : hostType;
    String hostNameString = StringUtils.isEmpty(hostName) ? null : hostName;

    List<NetBoxHostIdentifier> netBoxIds =
        getNetBoxIdsByHostInformation(
            networkString, networkScopeString, hostTypeString, hostNameString);

    if (netBoxIds != null) {
      if (netBoxIds.isEmpty() || IocDeploymentStatus.NOT_DEPLOYED.equals(deploymentStatus)) {
        return new PagedIocResponse(
            listResult, 0, 0, pagingLimitDto.getPage(), pagingLimitDto.getLimit());
      }

      if (IocDeploymentStatus.ALL.equals(deploymentStatus)) {
        // The filtering based on host details is only makes sense if the IOC is deployed
        deploymentStatus = IocDeploymentStatus.DEPLOYED;
      }
    }

    List<IocEntity> iocs =
        iocRepository.findAll(
            deploymentStatus,
            createdByString,
            netBoxIds,
            searchTextString,
            orderBy,
            isAsc,
            pagingLimitDto.getPage(),
            pagingLimitDto.getLimit(),
            listAll);

    if (iocs != null) {

      for (IocEntity i : iocs) {
        listResult.add(IocUtil.convertToIocInfo(extractForIocInfo(i)));
      }
    }

    long totalCount =
        iocRepository.countForPaging(deploymentStatus, createdByString, netBoxIds, searchText);

    return new PagedIocResponse(
        listResult,
        totalCount,
        listResult.size(),
        pagingLimitDto.getPage(),
        pagingLimitDto.getLimit());
  }

  private List<NetBoxHostIdentifier> getNetBoxIdsByHostInformation(
      String networkString,
      String networkScopeString,
      String hostTypeString,
      String hostNameString) {
    if (needFilteringFromNetBox(
        networkString, networkScopeString, hostTypeString, hostNameString)) {
      return hostService
          .findAllHosts(hostNameString, networkString, networkScopeString, hostTypeString)
          .stream()
          .map(NetBoxHost::getNetBoxHostIdentifier)
          .collect(Collectors.toList());
    }
    return null;
  }

  private boolean needFilteringFromNetBox(
      final String networkString,
      final String networkScopeString,
      final String hostTypeString,
      String hostNameString) {
    return networkString != null
        || networkScopeString != null
        || hostTypeString != null
        || hostNameString != null;
  }

  private Ioc extractForIocInfo(IocEntity iocEntity) {
    IocDeploymentEntity activeEntity =
        deploymentService.findActiveDeploymentEntityForIocId(iocEntity.getId());
    Deployment activeDeployment =
        IocUtil.activeDeploymentToActualDeployment(
            deploymentService.findActiveDeploymentForIocInfo(activeEntity));
    return IocUtil.convertToIocInfo(iocEntity, activeDeployment);
  }

  public Ioc extractForIocDetails(@Nonnull IocEntity iocEntity) {
    IocDeploymentEntity activeEntity = iocEntity.getLatestSuccessfulDeployment();

    GitlabProject iocProject = gitLabService.getNullableGitlabProject(iocEntity.getGitProjectId());

    Deployment actualDeployment =
        IocUtil.activeDeploymentToActualDeployment(
            deploymentService.findActiveDeploymentForIocDetails(
                activeEntity, getGitLabProjectForDeployment(iocProject, activeEntity)));

    return IocUtil.convertToIocDetails(
        iocProject, checkOrUpdateIocFromNamingService(iocEntity), iocEntity, actualDeployment);
  }

  /**
   * Checks if Metadata provided in repository is parseable before it will be deployed (metadata has
   * correct format) Metadata file is required to be in the repo!
   *
   * @param iocEntity IOC entity
   * @param sourceVersion The sourceVersion about the IOC that has to be deployed
   * @param userDetails The requester user details
   * @return The DB entity of the IOC
   * @throws JsonProcessingException When error occurs when processing the metadata from the
   *     repository
   * @throws EntityNotFoundException If IOC is not found with the specific Id
   */
  public void checkMetadata(IocEntity iocEntity, String sourceVersion, UserDetails userDetails)
      throws JsonProcessingException, EntityNotFoundException {
    String userName = userDetails.getUserName();

    Long projectId = iocEntity.getGitProjectId();
    metadataService.parseMetadata(projectId, sourceVersion);

    LOGGER.debug(
        "IOC id[{}] Metadata checked by [{}], parameters {}",
        iocEntity.getId(),
        userName,
        sourceVersion);
  }

  /**
   * Update an IOC naming-attributes and/or Git project by ID. If a field is left empty the old
   * value will not be overwritten.
   *
   * @param ioc the IOC entity
   * @param updateBody the update values (Git project ID and/or naming ID)
   * @param user the user who requested the update
   * @return the update IOC details
   * @throws EmptyResultDataAccessException if IOC can not be found with the given ID
   */
  @Transactional
  @PreAuthorize(WebSecurityConfig.IS_ADMIN)
  public Ioc checkMetadata(IocEntity ioc, IOCUpdateRequest updateBody, UserDetails user)
      throws EmptyResultDataAccessException, UnprocessableEntityException {
    LOGGER.debug("Trying to update IOC");

    deploymentService.checkConcurrency(ioc);

    boolean isAdmin = user.getRoles().contains(WebSecurityConfig.ROLE_DEPLOYMENT_TOOL_ADMIN);
    boolean wasDeployed = deploymentService.checkIfIocVersionWasDeployed(ioc.getId());

    IocDeploymentEntity activeEntity =
        deploymentService.findActiveDeploymentEntityForIocId(ioc.getId());
    Deployment actualDeployment =
        IocUtil.activeDeploymentToActualDeployment(
            deploymentService.findActiveDeploymentForIocInfo(activeEntity));

    if (actualDeployment != null) {
      // Update Git project ID is not allowed for currently deployed IOCs
      if (updateBody.getGitProjectId() != null
          && !updateBody.getGitProjectId().equals(ioc.getGitProjectId())) {
        LOGGER.error("Git project of a deployed IOC cannot be changed");
        throw new OperationNotAllowedException(
            "Git repository of currently deployed IOC cannot be modified");
      }
    }

    boolean iocChanged = false;

    if (updateBody.getGitProjectId() != null
        && !updateBody.getGitProjectId().equals(ioc.getGitProjectId())) {
      validateGitProjectUpdate(updateBody, user, isAdmin, wasDeployed);

      ioc.setGitProjectId(updateBody.getGitProjectId());
      iocChanged = true;
    }

    if (nameUpdated(ioc, updateBody)) {
      NamingIocResponse namingIocResponse =
          getIocFromNamingServiceByUuid(updateBody.getNamingUuid());
      validateNameUpdate(namingIocResponse.getName(), isAdmin);

      ioc.setNamingName(namingIocResponse.getName());
      ioc.setNamingUuid(namingIocResponse.getUuid());
      iocChanged = true;
    }

    if (iocChanged) {
      iocRepository.updateIoc(ioc);
    }

    return searchIocById(ioc.getId());
  }

  private void validateGitProjectUpdate(
      final IOCUpdateRequest updateBody,
      final UserDetails user,
      final boolean isAdmin,
      final boolean wasDeployed) {
    if (wasDeployed) {
      if (!isAdmin) {
        throw new UnauthorizedException(
            "Only administrator users are permitted to modify IOC's Git repository which is/was deployed!");
      }
    } else {
      if (!isAdmin) {
        throw new UnauthorizedException(
            "Only administrator users are permitted to modify IOC's Git repository!");
      }
    }

    checkDuplicationForGitRepository(updateBody.getGitProjectId());

    try {
      gitLabService.projectInfo(updateBody.getGitProjectId());
      authorizationService.checkDeploymentRights(updateBody.getGitProjectId(), user);
    } catch (EntityNotFoundException e) {
      throw new UnprocessableEntityException(e.getMessage());
    }
  }

  private void validateNameUpdate(final String namingName, final boolean isAdmin) {
    if (!isAdmin) {
      throw new UnauthorizedException("Only administrator users are permitted to modify IOC name!");
    }

    checkDuplicationForName(namingName);
  }

  private boolean nameUpdated(final IocEntity ioc, final IOCUpdateRequest updateBody) {
    return (updateBody.getNamingUuid() != null
        && !updateBody.getNamingUuid().equals(ioc.getNamingUuid()));
  }

  private List<Ioc> getDeployedIocsForHost(
      NetBoxHostIdentifier netBoxHostIdentifier,
      String iocName,
      IocOrder orderBy,
      Boolean isAsc,
      int page,
      int limit) {
    return deploymentService
        .findDeployedIocsByNetBoxHostId(netBoxHostIdentifier, iocName, orderBy, isAsc, page, limit)
        .stream()
        .map(this::extractForIocInfo)
        .collect(Collectors.toList());
  }

  /**
   * Tries to delete IOC from the DB. IOC mustn't be deployed! It is a TRANSACTIONAL method! It
   * deletes IOC from IOCVersion AND IOC tables.
   *
   * @param user the user who wants to delete the IOC
   * @param iocId the ID of the IOC that has to be deleted
   * @throws EntityNotFoundException if IOC can not be found with the ID
   * @throws UnauthorizedException if IOC can not be deleted because lack of rights
   * @throws AuthenticationException if IOC can not be deleted because is/was deployed
   */
  @Transactional
  @PreAuthorize(WebSecurityConfig.IS_ADMIN)
  public void deleteIoc(UserDetails user, long iocId) throws GitLabApiException {

    IocEntity ioc = findIocById(iocId);

    // do not allow deleting IOC if it was deployed
    if (ioc.getLatestSuccessfulDeployment() != null
        && !UNDEPLOYMENT_TYPES.contains(ioc.getLatestSuccessfulDeployment().getDeploymentType())) {
      throw new OperationNotAllowedException("IOC can't be deleted because it is deployed!");
    }

    deploymentService.checkConcurrency(ioc);

    if (ioc.getGitProjectId() != null) {

      if (gitLabService.getOptionalProjectByRepoId(ioc.getGitProjectId()).isPresent()) {
        gitLabService.deleteProjectById(ioc.getGitProjectId());
      }
    }

    // delete dependencies
    ioc.setLatestSuccessfulDeployment(null);
    deploymentService.deleteDeploymentForIoc(ioc);
    // delete IOC
    iocRepository.deleteIoc(ioc);

    LOGGER.debug(
        "IOC was successfully deleted with Id: [{}] and name: [{}]",
        ioc.getId(),
        ioc.getNamingName());
  }

  /**
   * Fetches all IOCs that are deployed on a certain host, and are currently active. The response is
   * paged!
   *
   * @param hostId encoded ID of the requested host
   * @param iocName the name of the IOC (filter param)
   * @param orderBy the result should be ordered by which field
   * @param isAsc the order should be and ASC or in DESC
   * @param page the requested page-number
   * @param limit the (maximum) limit of the response size
   * @return list of IOCs that are deployed to the requested host, and are active
   */
  public PagedAssociatedIocs getAssociatedIocs(
      String hostId, String iocName, IocOrder orderBy, Boolean isAsc, Integer page, Integer limit) {
    // determining parameters for searching
    NetBoxHostIdentifier hostIdentifier = HostService.decodeHostIdentifier(hostId);
    PagingLimitDto pagingLimitDto = utils.pageLimitConverter(page, limit, false);

    long totalCount =
        deploymentService.countTotalDeploymentsForPaging(
            hostIdentifier.getNetBoxHostId(), hostIdentifier.isVm());
    List<Ioc> deployedIocsForHost =
        getDeployedIocsForHost(
            hostIdentifier,
            iocName,
            orderBy,
            isAsc,
            pagingLimitDto.getPage(),
            pagingLimitDto.getLimit());

    return new PagedAssociatedIocs(
        totalCount,
        deployedIocsForHost.size(),
        pagingLimitDto.getPage(),
        pagingLimitDto.getLimit(),
        deployedIocsForHost.stream().map(IocUtil::convertToIocInfo).collect(Collectors.toList()));
  }

  public IocStatusResponse getStatus(long iocId) {
    try {
      IocEntity iocEntity = iocRepository.findById(iocId);
      IocDeploymentEntity activeEntity = iocEntity.getLatestSuccessfulDeployment();
      Deployment activeDeployment =
          IocUtil.activeDeploymentToActualDeployment(
              deploymentService.findActiveDeploymentForIocInfo(activeEntity));

      String iocName = iocEntity.getNamingName();
      ZonedDateTime deploymentTime =
          ((activeDeployment == null) || (activeEntity == null))
              ? null
              : activeEntity.getAwxJob() != null
                  ? Utils.toZonedDateTime(
                      activeEntity.getAwxJob().getAwxJobStartedAt(), serverTimeZone)
                  : null;

      Boolean isIocActive =
          activeDeployment == null
              ? statusService.checkNonDeployedIocIsActive(iocName)
              : statusService.checkDeployedIocIsActive(
                  iocName, deploymentTime, activeDeployment.getHost().getFqdn());
      return new IocStatusResponse(iocId, isIocActive);
    } catch (EmptyResultDataAccessException e) {
      throw new EntityNotFoundException("IOC", iocId);
    }
  }

  public IocAlertResponse getAlerts(long iocId) {
    try {
      IocEntity iocEntity = iocRepository.findById(iocId);
      IocDeploymentEntity activeEntity = iocEntity.getLatestSuccessfulDeployment();

      GitlabProject iocProject =
          gitLabService.getNullableGitlabProject(iocEntity.getGitProjectId());

      Deployment actualDeployment =
          IocUtil.activeDeploymentToActualDeployment(
              deploymentService.findActiveDeploymentForIocDetails(
                  activeEntity, getGitLabProjectForDeployment(iocProject, activeEntity)));

      String sourceVersionShort =
          actualDeployment == null ? null : actualDeployment.getSourceVersionShort();

      String iocName = iocEntity.getNamingName();
      IocDeploymentEntity latestDeployment =
          deploymentService.findLatestDeploymentForIoc(iocEntity.getId());

      String hostNameOfActiveDeployment =
          actualDeployment == null ? null : actualDeployment.getHost().getFqdn();

      ZonedDateTime deploymentTime =
          actualDeployment == null ? null : actualDeployment.getStartDate();

      String hostName = hostNameOfActiveDeployment;
      Boolean isIocActive =
          actualDeployment == null
              ? statusService.checkNonDeployedIocIsActive(iocName)
              : statusService.checkDeployedIocIsActive(iocName, deploymentTime, hostName);

      List<String> hostList = statusService.fetchWhereIOCIsRunning(iocName);

      Boolean isRunningOnCorrectHost = null;

      if (!hostList.isEmpty()) {
        isRunningOnCorrectHost = hostList.contains(hostName);
      }

      final List<String> iocMetrics =
          statusService.checkIOCMetrics(iocName, hostNameOfActiveDeployment);

      List<Alert> alerts =
          IOCAlertGenerator.generateAlerts(
              iocEntity.getNamingName(),
              actualDeployment,
              latestDeployment,
              activeEntity == null
                  ? null
                  : hostService.hasTargetHostRenamed(
                      activeEntity.getNetBoxHostId(),
                      activeEntity.isVm(),
                      activeEntity.getHostFqdn(),
                      activeEntity.getHostName(),
                      activeEntity.getHostNetwork()),
              iocMetrics.contains("ioc_repository_dirty"),
              iocMetrics.contains("ioc_coredump_exists"),
              iocMetrics.contains("ioc_repository_local_commits"),
              iocMetrics.contains("ioc_cellmode_used"),
              iocMetrics.contains("ioc_essioc_missing"),
              checkOrUpdateIocFromNamingService(iocEntity),
              iocProject,
              sourceVersionShort != null,
              statusService.checkIOCExporterForHost(hostNameOfActiveDeployment) != null,
              isIocActive,
              isRunningOnCorrectHost);
      return new IocAlertResponse(iocId, ConversionUtil.determineAlertSeverity(alerts), alerts);
    } catch (EmptyResultDataAccessException e) {
      throw new EntityNotFoundException("IOC", iocId);
    }
  }

  public IocEntity findIocByRepoId(long repoId) {
    return iocRepository.findIocByRepoId(repoId);
  }

  public List<IocEntity> findIocByNamingUUId(String namingUUId) {
    return iocRepository.findIocByNamingUUID(namingUUId);
  }

  public IocEntity findIocByNamingName(String namingName) {
    try {
      return iocRepository.findIocByNamingName(namingName);
    } catch (EmptyResultDataAccessException e) {
      throw new EntityNotFoundException("IOC", namingName);
    }
  }

  @Transactional
  public Optional<IocEntity> findOptionalIocByNamingName(String namingName) {
    return iocRepository.findOptionalIocByNamingName(namingName);
  }

  public IocDescriptionResponse getIocDescription(long iocId) {
    IocEntity iocEntity = searchAndCheckById(iocId);
    NamingResponseDTO namingResponseDTO = checkOrUpdateIocFromNamingService(iocEntity);
    return new IocDescriptionResponse(namingResponseDTO.getDescription());
  }

  public Map<String, IocEntity> findIocsByName(List<String> iocNames) {
    List<IocEntity> iocs = iocRepository.findIocsByName(iocNames);

    return iocs.stream().collect(Collectors.toMap(IocEntity::getNamingName, Function.identity()));
  }

  public Job stopIoc(UserDetails user, long iocId) {
    try {
      Ioc ioc = searchIocById(iocId);
      Deployment activeDeployment = ioc.getActiveDeployment();
      if (activeDeployment != null && !activeDeployment.isUndeployment()) {
        return deploymentService.stopIoc(
            activeDeployment, findIocById(iocId), ioc.getGitProjectId(), user);
      } else {
        throw new IocNotDeployedException(iocId);
      }
    } catch (EntityNotFoundException | UnprocessableEntityException | IocNotDeployedException e) {
      LOGGER.error(e.getMessage(), e);
      throw e;
    } catch (Exception e) {
      LOGGER.error("Error while trying to stop IOC with ID: {}", iocId, e);
      throw new ServiceException("Error while trying to stop IOC with ID: " + iocId, e);
    }
  }

  public Job startIoc(UserDetails user, long iocId) {
    try {
      Ioc ioc = searchIocById(iocId);
      Deployment activeDeployment = ioc.getActiveDeployment();
      if (activeDeployment != null && !activeDeployment.isUndeployment()) {
        return deploymentService.startIoc(
            activeDeployment, findIocById(iocId), ioc.getGitProjectId(), user);
      } else {
        throw new IocNotDeployedException(iocId);
      }
    } catch (EntityNotFoundException | UnprocessableEntityException | IocNotDeployedException e) {
      LOGGER.error(e.getMessage(), e);
      throw e;
    } catch (RemoteException e) {
      LOGGER.error("Remote exception while trying to start IOC", e);
      throw e;
    } catch (Exception e) {
      LOGGER.error("Error while trying to start IOC with ID: {}", iocId, e);
      throw new ServiceException("Error while trying to start IOC with ID: " + iocId, e);
    }
  }

  private void checkGitTagOrCommitId(Long projectId, String gitRefVersion)
      throws GitTagOrCommitIdNotFoundException {

    if (projectId != null && (StringUtils.isNotEmpty(gitRefVersion))) {
      List<String> availableGitRefs =
          gitLabService.tagsAndCommitIds(projectId).stream()
              .map(GitReference::getReference)
              .collect(Collectors.toList());
      if (!availableGitRefs.contains(gitRefVersion)) {
        throw new GitTagOrCommitIdNotFoundException(
            "Git tag or commit ID ("
                + gitRefVersion
                + ") not found for Git project ("
                + projectId
                + ")");
      }
    }
  }

  /**
   * Checks if all mandatory fields are present in request when updating an IOC. Repository URL, and
   * tag/branch-name are optional, but if one is present -> other should be also present.
   *
   * @param hostId the hostID to deploy the IOC co
   * @param sourceVersion the Git version that has to be deployed
   * @param projectId the Git repository project ID for the IOC
   * @throws IncompleteRequestException when mandatory/necessary fields are missing from the request
   */
  private void checkIocUpdateAndDeployBody(Long projectId, String hostId, String sourceVersion) {
    if (hostId == null) {
      throw new IncompleteRequestException("Target host ID is required!");
    }

    checkGitTagOrCommitId(projectId, sourceVersion);
  }

  public Job updateAndDeployIoc(UserDetails user, long iocId, String hostId, String sourceVersion) {
    LOGGER.debug("Trying to update and deploy Ioc with ID: {}", iocId);
    IocEntity iocEntity = findIocById(iocId);
    checkIocUpdateAndDeployBody(iocEntity.getGitProjectId(), hostId, sourceVersion);

    try {
      LOGGER.debug("Trying to update Ioc with ID: {}", iocId);
      HostInfoWithId host = null;
      if (StringUtils.isNotEmpty(hostId)) {
        host = hostService.getHostByNetBoxId(hostId);
      }
      IocEntity ioc = searchAndCheckById(iocId);
      checkMetadata(ioc, sourceVersion, user);

      LOGGER.debug("Trying to deploy Ioc with ID: {}", iocId);
      Deployment deployment =
          deploymentService.createDeployment(
              DeploymentTask.createDeploymentTask(ioc, host, user), sourceVersion);

      return JobUtil.convertToJob(
          deploymentRepository.findJobById(deployment.getJobId()), serverTimeZone);
    } catch (MetaDataFileException | JsonProcessingException e) {
      LOGGER.error("Error while trying to process metadata file content " + e.getMessage(), e);
      throw new MetaDataFileException(e.getMessage());
    } catch (EntityNotFoundException e) {
      LOGGER.error(e.getMessage(), e);
      throw new UnprocessableEntityException(e.getMessage());
    } catch (EmptyResultDataAccessException e) {
      LOGGER.error("Entity not found for update with ID: {}", iocId, e);
      throw new EntityNotFoundException("IOC", iocId);
    } catch (NetBoxServiceException
        | LimitExceededException
        | UnauthorizedException
        | AwxServiceException
        | OperationNotAllowedException
        | GitlabServiceException e) {
      LOGGER.error(e.getMessage(), e);
      throw e;
    } catch (Exception e) {
      LOGGER.error("Error while trying to update and deploy IOC with ID: {}", iocId, e);
      throw new ServiceException(
          "Error while trying to update and deploy IOC with ID: " + iocId, e);
    }
  }

  public Job createUndeployment(UserDetails user, long iocId) {
    LOGGER.debug("Trying to undeploy Ioc with ID: {}", iocId);
    try {
      IocEntity iocEntity = findIocById(iocId);
      Ioc ioc = searchIocById(iocId);
      if (ioc.getActiveDeployment() == null) {
        throw new IocNotDeployedException(iocId);
      }

      String hostId;
      if (ioc.getActiveDeployment().getHost() != null
          && ioc.getActiveDeployment().getHost().getHostId() != null) {
        hostId = ioc.getActiveDeployment().getHost().getHostId();
      } else {
        throw new UnprocessableEntityException("Deployment host's NetBox ID is invalid");
      }
      HostInfoWithId host = hostService.getHostByNetBoxId(hostId);

      Deployment undeployment =
          deploymentService.createUndeployment(
              DeploymentTask.createUndeploymentTask(iocEntity, host, user),
              ioc.getActiveDeployment().getSourceVersion());

      return JobUtil.convertToJob(
          deploymentRepository.findJobById(undeployment.getJobId()), serverTimeZone);
    } catch (EntityNotFoundException
        | AwxServiceException
        | ConcurrentOperationFoundException
        | UnauthorizedException
        | IocNotDeployedException
        | NetBoxServiceException
        | UnprocessableEntityException e) {
      LOGGER.error(e.getMessage(), e);
      throw e;
    } catch (Exception e) {
      LOGGER.error("Error while trying to undeploy IOC", e);
      throw new ServiceException(e);
    }
  }

  private GitlabProject getGitLabProjectForDeployment(
      GitlabProject iocProject, IocDeploymentEntity activeEntity) {
    if (iocProject == null
        || (activeEntity != null
            && activeEntity.getGitProjectId() != null
            && !Objects.equals(activeEntity.getGitProjectId(), iocProject.getId().longValue()))) {
      return gitLabService.getNullableGitlabProject(activeEntity.getGitProjectId());
    }
    return iocProject;
  }

  public Job createBatchDeployment(
      UserDetails user, List<DeploymentRequest> deployments, BatchType batchType) throws Exception {
    checkIocDuplication(deployments);
    if (batchType.equals(BatchType.DEPLOY)) {
      checkCapacityForMultipleHosts(deployments);
    }

    boolean queued = false;
    List<IocDeploymentEntity> deploymentEntities = new ArrayList<>();

    for (DeploymentRequest deployment : deployments) {
      IocEntity iocEntity = searchAndCheckById(deployment.iocId());
      authorizationService.checkDeploymentRights(iocEntity.getGitProjectId(), user);

      String sourceVersion = getSourceVersion(iocEntity, deployment, batchType);
      checkIocUpdateAndDeployBody(
          iocEntity.getGitProjectId(), deployment.hostId(), deployment.sourceVersion());
      HostInfoWithId host = hostService.getHostByNetBoxId(deployment.hostId());

      if (BatchType.DEPLOY.equals(batchType)) {
        checkMetadata(iocEntity, sourceVersion, user);
        checkIfIocAlreadyDeployed(iocEntity);
      }

      deploymentService.checkConcurrency(iocEntity);
      Set<NetBoxHostIdentifier> concurrentHostIds =
          deploymentService.getConcurrentHostIds(iocEntity, host.getHostIdentifier());

      deploymentEntities.add(
          deploymentService.prepareDeploymentForBatch(
              iocEntity, host, sourceVersion, user.getUserName(), batchType));
      if (!concurrentHostIds.isEmpty()) {
        queued = true;
      }
    }

    AwxJobEntity awxJobEntity =
        deploymentService.storeBatchDeployment(
            deploymentEntities, user.getUserName(), batchType, queued);

    if (!queued) {
      deploymentService.startBatchDeployment(
          awxJobEntity, deploymentEntities, batchType, user.getUserName());
    }

    return JobUtil.convertToBatchJob(awxJobEntity, deploymentEntities, serverTimeZone);
  }

  private void checkIfIocAlreadyDeployed(IocEntity iocEntity) {
    Deployment activeDeployment =
        deploymentService.findActiveDeploymentForIocDetails(iocEntity.getId());
    if (activeDeployment != null) {
      throw new OperationNotAllowedException(
          "IOC (ID: " + iocEntity.getId() + ") is already deployed");
    }
  }

  private String getSourceVersion(
      IocEntity iocEntity, DeploymentRequest deploymentRequest, BatchType batchType) {
    switch (batchType) {
      case UNDEPLOY -> {
        IocDeploymentEntity latestSuccessfulDeployment = iocEntity.getLatestSuccessfulDeployment();
        if (latestSuccessfulDeployment == null) {
          throw new IocNotDeployedException(iocEntity.getId());
        }
        return latestSuccessfulDeployment.getSourceVersion();
      }
      case DEPLOY -> {
        String sourceVersion = deploymentRequest.sourceVersion();
        if (sourceVersion == null || sourceVersion.isBlank()) {
          throw new IncompleteRequestException("Source version is required!");
        }

        return sourceVersion;
      }
      default -> throw new IllegalArgumentException("Unsupported batch type: " + batchType);
    }
  }

  public void checkCapacityForMultipleHosts(List<DeploymentRequest> deployments) {
    deployments.stream()
        .collect(Collectors.groupingBy(DeploymentRequest::hostId, Collectors.counting()))
        .forEach(
            (hostId, jobCount) -> {
              NetBoxHostIdentifier hostIdentifier = HostService.decodeHostIdentifier(hostId);
              if (iocsOnHostLimit
                  < iocRepository.countAssociatedDeployments(
                          hostIdentifier.getNetBoxHostId(), hostIdentifier.isVm())
                      + jobCount) {
                throw new LimitExceededException(
                    "Max number of IOCs per host is %d.".formatted(iocsOnHostLimit));
              }
            });
  }

  private void checkIocDuplication(List<DeploymentRequest> deployments) {

    if (deployments.stream().map(DeploymentRequest::iocId).distinct().count()
        != deployments.size()) {
      throw new InputValidationException("The request contains duplicated IOCs.");
    }
  }
}
