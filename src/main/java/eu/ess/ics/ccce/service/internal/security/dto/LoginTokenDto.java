/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.service.internal.security.dto;

import java.util.List;

/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 */
public class LoginTokenDto {
  private String token;
  private long expirationDuration;
  private List<String> roles;

  public LoginTokenDto(String token, long expirationDuration, List<String> roles) {
    this.token = token;
    this.expirationDuration = expirationDuration;
    this.roles = roles;
  }

  public String getToken() {
    return token;
  }

  public long getExpirationDuration() {
    return expirationDuration;
  }

  public List<String> getRoles() {
    return roles;
  }
}
