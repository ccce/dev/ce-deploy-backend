/*
 * Copyright (C) 2025 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.service.internal.deployment;

import static eu.ess.ics.ccce.common.Constants.UNDEPLOYMENT_TYPES;

import eu.ess.ics.ccce.common.Utils;
import eu.ess.ics.ccce.common.alert.DeploymentAlertGenerator;
import eu.ess.ics.ccce.repository.entity.IocDeploymentEntity;
import eu.ess.ics.ccce.rest.model.job.response.*;
import eu.ess.ics.ccce.service.internal.host.HostService;
import org.apache.commons.lang3.StringUtils;
import org.gitlab.api.models.GitlabProject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author <a href="mailto:domonkos.gulyas@ess.eu">Domonkos Gulyas</a>
 */
public class DeploymentUtil {

  private static final Logger LOGGER = LoggerFactory.getLogger(DeploymentUtil.class);

  private DeploymentUtil() {
    throw new IllegalStateException("Utility class");
  }

  /**
   * Conversion for deployment info object (used by deployment listings)
   *
   * @param iocDeploymentEntity the deployment entity from the DB
   * @param serverTimeZone
   * @return Deployment response DTO that could be consumed by clients
   */
  public static Deployment convertToDeploymentInfo(
      IocDeploymentEntity iocDeploymentEntity,
      boolean externalIdValid,
      String fqdn,
      String hostName,
      String network,
      String serverTimeZone) {
    Deployment result = null;

    if (iocDeploymentEntity != null) {
      result =
          new Deployment(
              iocDeploymentEntity.getId(),
              iocDeploymentEntity.getCreatedBy(),
              Utils.toZonedDateTime(iocDeploymentEntity.getCreatedAt(), serverTimeZone),
              iocDeploymentEntity.getAwxJob() != null
                  ? Utils.toZonedDateTime(
                      iocDeploymentEntity.getAwxJob().getAwxJobStartedAt(), serverTimeZone)
                  : null,
              iocDeploymentEntity.getAwxJob() != null
                  ? Utils.toZonedDateTime(
                      iocDeploymentEntity.getAwxJob().getAwxJobFinishedAt(), serverTimeZone)
                  : null,
              iocDeploymentEntity.getIoc().getNamingName(),
              StringUtils.isEmpty(iocDeploymentEntity.getNamingName())
                  ? iocDeploymentEntity.getIoc().getNamingName()
                  : iocDeploymentEntity.getNamingName(),
              iocDeploymentEntity.getGitProjectId(),
              iocDeploymentEntity.getSourceVersion(),
              HostService.convertToHost(
                  iocDeploymentEntity.getNetBoxHostIdentifier(),
                  externalIdValid,
                  fqdn,
                  hostName,
                  network),
              iocDeploymentEntity.getAwxJob() == null
                  ? null
                  : JobUtil.convertToJob(iocDeploymentEntity.getAwxJob(), serverTimeZone)
                      .getStatus(),
              iocDeploymentEntity.getAwxJob() != null
                  ? iocDeploymentEntity.getAwxJob().getAwxJobId()
                  : null,
              UNDEPLOYMENT_TYPES.contains(iocDeploymentEntity.getDeploymentType()),
              iocDeploymentEntity.getAwxJob().getId());
    }

    return result;
  }

  /**
   * Conversion for deployment details object (used by deployment details query)
   *
   * @param project Gitlab project
   * @param iocDeploymentEntity The deployment entity from the DB
   * @param sourceVersionShort Short Git reference hash
   * @param hostExternalIdValid Indicates if external host ID still exists according to external
   *     host service
   * @param fqdn Fully qualified domain name (from external service)
   * @param hostName Host name (from external service)
   * @param hostNetwork Network identifier (from external service)
   * @return Deployment response DTO that could be consumed by clients
   */
  public static Deployment convertToDeploymentDetails(
      GitlabProject project,
      IocDeploymentEntity iocDeploymentEntity,
      String sourceVersionShort,
      boolean hostExternalIdValid,
      String fqdn,
      String hostName,
      String hostNetwork,
      String serverTimeZone) {
    Deployment result = null;

    if (iocDeploymentEntity != null) {
      result =
          new Deployment(
              iocDeploymentEntity.getId(),
              iocDeploymentEntity.getCreatedBy(),
              Utils.toZonedDateTime(iocDeploymentEntity.getCreatedAt(), serverTimeZone),
              iocDeploymentEntity.getAwxJob() != null
                  ? Utils.toZonedDateTime(
                      iocDeploymentEntity.getAwxJob().getAwxJobStartedAt(), serverTimeZone)
                  : null,
              iocDeploymentEntity.getAwxJob() != null
                  ? Utils.toZonedDateTime(
                      iocDeploymentEntity.getAwxJob().getAwxJobFinishedAt(), serverTimeZone)
                  : null,
              iocDeploymentEntity.getIoc().getNamingName(),
              StringUtils.isEmpty(iocDeploymentEntity.getNamingName())
                  ? iocDeploymentEntity.getIoc().getNamingName()
                  : iocDeploymentEntity.getNamingName(),
              project != null ? iocDeploymentEntity.getGitProjectId() : null,
              project != null ? project.getHttpUrl() : null,
              project != null ? iocDeploymentEntity.getSourceVersion() : null,
              sourceVersionShort,
              HostService.convertToHost(
                  iocDeploymentEntity.getNetBoxHostIdentifier(),
                  hostExternalIdValid,
                  fqdn,
                  hostName,
                  hostNetwork),
              iocDeploymentEntity.getAwxJob() == null
                  ? null
                  : JobUtil.convertToJob(iocDeploymentEntity.getAwxJob(), serverTimeZone)
                      .getStatus(),
              iocDeploymentEntity.getAwxJob() != null
                  ? iocDeploymentEntity.getAwxJob().getAwxJobId()
                  : null,
              UNDEPLOYMENT_TYPES.contains(iocDeploymentEntity.getDeploymentType()),
              iocDeploymentEntity.getAwxJob().getId());
    }

    return result;
  }

  /**
   * Converts deployment detail information to response format
   *
   * @param deploymentAlertGenerator The alert-generator instance
   * @param deployment the deployment with details that has to be converted
   * @param iocId the ID of the IOC that has been deployed
   * @param awxJobUrl the URL for the AWX job
   * @return detailed deployment information for client response
   */
  public static DeploymentInfoDetails convertToDeploymentInfoDetails(
      DeploymentAlertGenerator deploymentAlertGenerator,
      Deployment deployment,
      Long iocId,
      String awxJobUrl) {

    return new DeploymentInfoDetails(
        deployment.getId(),
        deployment.getCreatedBy(),
        deployment.getCreatedAt(),
        deployment.getStartDate(),
        deployment.getEndDate(),
        deployment.getAwxJobId(),
        HostService.convertToHostWithFqdn(deployment.getHost()),
        deployment.isUndeployment(),
        deployment.getNamingName(),
        deployment.getGitProjectId(),
        deployment.getSourceUrl(),
        deployment.getSourceVersion(),
        deployment.getSourceVersionShort(),
        iocId,
        awxJobUrl,
        deploymentAlertGenerator.generateDeploymentDetailsAlert(deployment),
        deployment.getJobId());
  }
}
