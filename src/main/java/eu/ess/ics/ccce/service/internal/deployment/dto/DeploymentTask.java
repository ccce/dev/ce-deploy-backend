/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.service.internal.deployment.dto;

import eu.ess.ics.ccce.repository.entity.IocEntity;
import eu.ess.ics.ccce.rest.model.host.response.HostInfoWithId;
import eu.ess.ics.ccce.service.internal.security.dto.UserDetails;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 */
public class DeploymentTask {
  private final IocEntity ioc;
  private final HostInfoWithId host;
  private final boolean undeploy;
  private final UserDetails userDetails;

  private DeploymentTask(
      IocEntity ioc, HostInfoWithId host, boolean undeploy, UserDetails userDetails) {
    this.ioc = ioc;
    this.host = host;
    this.undeploy = undeploy;
    this.userDetails = userDetails;
  }

  public static DeploymentTask createDeploymentTask(
      IocEntity ioc, HostInfoWithId host, UserDetails userDetails) {
    return new DeploymentTask(ioc, host, false, userDetails);
  }

  public static DeploymentTask createUndeploymentTask(
      IocEntity ioc, HostInfoWithId host, UserDetails userDetails) {
    return new DeploymentTask(ioc, host, true, userDetails);
  }

  public IocEntity getIoc() {
    return ioc;
  }

  public HostInfoWithId getHost() {
    return host;
  }

  public boolean isUndeploy() {
    return undeploy;
  }

  public UserDetails getUserDetails() {
    return userDetails;
  }
}
