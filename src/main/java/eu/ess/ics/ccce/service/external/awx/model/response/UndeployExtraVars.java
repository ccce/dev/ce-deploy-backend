/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.service.external.awx.model.response;

import com.google.gson.annotations.SerializedName;
import eu.ess.ics.ccce.service.external.awx.model.ExtraVars;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 */
public class UndeployExtraVars implements ExtraVars {
  @SerializedName("iocs_to_undeploy_json")
  private final String iocsToUndeploy;

  @SerializedName("iocs_to_deploy_json")
  private final String iocsToDeploy;

  public UndeployExtraVars(String iocsToUndeploy) {
    this.iocsToUndeploy = iocsToUndeploy;
    this.iocsToDeploy = "[]";
  }

  @Override
  public String getIocsToUndeploy() {
    return iocsToUndeploy;
  }

  @Override
  public String getIocsToDeploy() {
    return iocsToDeploy;
  }
}
