package eu.ess.ics.ccce.service.internal.inventory;

import eu.ess.ics.ccce.repository.entity.IocDeploymentEntity;
import eu.ess.ics.ccce.repository.impl.IocRepository;
import eu.ess.ics.ccce.rest.model.host.response.HostInfoWithId;
import eu.ess.ics.ccce.rest.model.host.response.PagedNetBoxHostResponse;
import eu.ess.ics.ccce.rest.model.inventory.response.HostInventoryList;
import eu.ess.ics.ccce.rest.model.inventory.response.IOCInventory;
import eu.ess.ics.ccce.rest.model.inventory.response.IOCInventoryList;
import eu.ess.ics.ccce.rest.model.inventory.response.InventoryResponse;
import eu.ess.ics.ccce.rest.model.inventory.response.MetaStructure;
import eu.ess.ics.ccce.service.internal.host.HostService;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.TreeMap;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;

/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 */
@Service
public class InventoryService {

  private final HostService hostService;
  private final IocRepository iocRepository;

  public InventoryService(HostService hostService, IocRepository iocRepository) {
    this.hostService = hostService;
    this.iocRepository = iocRepository;
  }

  public InventoryResponse getInventory() {

    List<IocDeploymentEntity> iocsForInventory = iocRepository.findIOCsForInventory();

    Map<String, List<IOCInventory>> iocInv =
        iocsForInventory.stream()
            .map(
                ioc ->
                    new IOCInventory(
                        ioc.getNamingName(),
                        ioc.getGitProjectUrl(),
                        ioc.getSourceVersion(),
                        ioc.getHostFqdn()))
            .collect(Collectors.groupingBy(IOCInventory::fqdn));

    MetaStructure meta =
        new MetaStructure(
            iocInv.entrySet().stream()
                .collect(Collectors.toMap(Entry::getKey, e -> new IOCInventoryList(e.getValue()))));

    meta.setHostVars(sortInventory(meta.getHostVars()));

    return new InventoryResponse(meta, new HostInventoryList(getAllHostFqdn()));
  }

  private Map<String, IOCInventoryList> sortInventory(Map<String, IOCInventoryList> iocInventory) {

    Map<String, IOCInventoryList> result = new TreeMap<>();

    ArrayList<String> sortedKeys = new ArrayList<>(iocInventory.keySet());

    Collections.sort(sortedKeys);

    for (String key : sortedKeys) {
      IOCInventoryList iocList = iocInventory.get(key);
      iocList.iocs().sort(Comparator.comparing(IOCInventory::namingName));

      result.put(key, new IOCInventoryList(iocList.iocs()));
    }

    return result;
  }

  private List<String> getAllHostFqdn() {

    int page = 0;
    long totalHostCount;
    List<HostInfoWithId> netBoxHosts = new ArrayList<>();
    do {
      PagedNetBoxHostResponse pagedHosts =
          hostService.findAllFromNetBoxPaged(null, null, page, null, null);
      netBoxHosts.addAll(pagedHosts.getNetBoxHosts());
      totalHostCount = pagedHosts.getTotalCount();
      page++;
    } while (netBoxHosts.size() < totalHostCount);

    return netBoxHosts.stream()
        .map(HostInfoWithId::getFqdn)
        .filter(Objects::nonNull)
        .sorted()
        .toList();
  }
}
