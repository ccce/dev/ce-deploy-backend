/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.service.external.netbox.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

/**
 * @author <a href="mailto:domonkos.gulyas@ess.eu">Domonkos Gulyas</a>
 */
public class NetBoxIpAddress extends BaseNetBoxObject {

  private final String address;
  private final String dnsName;
  private final String assignedObjectType;
  private final Long assignedObjectId;

  @JsonCreator
  public NetBoxIpAddress(
      @JsonProperty("id") Long id,
      @JsonProperty("display") String display,
      @JsonProperty("name") String name,
      @JsonProperty("description") String description,
      @JsonProperty("created") String created,
      @JsonProperty("last_updated") String lastUpdated,
      @JsonProperty("tags") List<NetBoxPropertyPreview> tags,
      @JsonProperty("address") String address,
      @JsonProperty("dns_name") String dnsName,
      @JsonProperty("assigned_object_type") String assignedObjectType,
      @JsonProperty("assigned_object_id") Long assignedObjectId) {
    super(id, display, name, description, created, lastUpdated, tags);
    this.address = address;
    this.dnsName = dnsName;
    this.assignedObjectType = assignedObjectType;
    this.assignedObjectId = assignedObjectId;
  }

  public String getAddress() {
    return address;
  }

  public String getDnsName() {
    return dnsName;
  }

  public String getAssignedObjectType() {
    return assignedObjectType;
  }

  public Long getAssignedObjectId() {
    return assignedObjectId;
  }

  public boolean isAssignedToInterface() {
    return isAssignedToDeviceInterface() || isAssignedToVirtualMachineInterface();
  }

  public boolean isAssignedToDeviceInterface() {
    return "dcim.interface".equals(assignedObjectType);
  }

  public boolean isAssignedToVirtualMachineInterface() {
    return "virtualization.vminterface".equals(assignedObjectType);
  }
}
