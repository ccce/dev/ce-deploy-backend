/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.service.external.awx.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import eu.ess.ics.ccce.common.Utils;
import eu.ess.ics.ccce.service.external.awx.model.JobStatus;
import java.util.Date;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 */
public class Job {
  public static class Related {
    private String stdout;

    @JsonProperty("job_events")
    @SerializedName("job_events")
    private String jobEvents;

    @JsonProperty("job_host_summaries")
    @SerializedName("job_host_summaries")
    private String jobHostSummaries;

    private String cancel;
    private String relaunch;

    public String getCancel() {
      return cancel;
    }

    public String getRelaunch() {
      return relaunch;
    }

    public String getStdout() {
      return stdout;
    }

    public String getJobEvents() {
      return jobEvents;
    }

    public String getJobHostSummaries() {
      return jobHostSummaries;
    }
  }

  private long id;
  private long job;
  private String type;
  private long inventory;
  private Related related;

  @SerializedName("job_type")
  private String jobType;

  private String url;
  private JobStatus status;
  private Date started;
  private Date finished;
  private double elapsed;

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public long getJob() {
    return job;
  }

  public String getType() {
    return type;
  }

  public long getInventory() {
    return inventory;
  }

  public Related getRelated() {
    return related;
  }

  public String getJobType() {
    return jobType;
  }

  public JobStatus getStatus() {
    return status;
  }

  public void setStatus(JobStatus status) {
    this.status = status;
  }

  public String getUrl() {
    return url;
  }

  public Date getStarted() {
    return Utils.cloneDate(started);
  }

  public Date getFinished() {
    return Utils.cloneDate(finished);
  }

  public double getElapsed() {
    return elapsed;
  }
}
