/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.service.internal.security;

import eu.ess.ics.ccce.common.security.EncryptUtil;
import eu.ess.ics.ccce.configuration.WebSecurityConfig;
import eu.ess.ics.ccce.exceptions.AuthenticationException;
import eu.ess.ics.ccce.exceptions.ServiceException;
import eu.ess.ics.ccce.exceptions.UnauthorizedException;
import eu.ess.ics.ccce.rest.model.authentication.response.LoginResponse;
import eu.ess.ics.ccce.service.internal.security.dto.LoginTokenDto;
import eu.ess.ics.ccce.service.internal.security.dto.UserDetails;
import java.util.List;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 */
@Service
public class AuthenticationService {

  private static final Logger LOGGER = LoggerFactory.getLogger(AuthenticationService.class);

  @Value("${jwt.expire.in.minutes}")
  private Integer tokenExpiration;

  private final UserService userService;
  private final JwtTokenService jwtTokenService;
  private final EncryptUtil encryptUtil;

  public AuthenticationService(
      UserService userService, JwtTokenService jwtTokenService, EncryptUtil encryptUtil) {
    this.userService = userService;
    this.jwtTokenService = jwtTokenService;
    this.encryptUtil = encryptUtil;
  }

  /**
   * Checks if user has permission in RBAC, and if has, logs in. Successful login will result in
   * creating a JWT for the REST API communication
   *
   * @param userName The login name for the user
   * @param password The password for the user
   * @return After successful login the backend will generate a JWT that can be user for the REST
   *     API communication
   * @throws AuthenticationException If user has bad username/password, or doesn't have permission
   *     to log in
   */
  public LoginResponse login(String userName, String password) throws AuthenticationException {

    LoginTokenDto tokenDto = userService.loginUser(userName, password);
    // checking user roles in RBAC
    checkUserRights(tokenDto.getRoles(), userName);

    UserDetails userDetails = new UserDetails();
    userDetails.setUserName(userName);
    userDetails.setToken(encryptUtil.encrypt(tokenDto.getToken()));
    userDetails.setRoles(tokenDto.getRoles());

    long tokenExpiresIn = Math.min(tokenExpiration, tokenDto.getExpirationDuration());

    return new LoginResponse(jwtTokenService.generateToken(userDetails, tokenExpiresIn));
  }

  /**
   * Checks if user has roles in RBAC at the beginning of the login process
   *
   * @param userRoles The roles of the user according to RBAC
   * @param userName The name of the user
   * @throws UnauthorizedException If user doesn't have permissions in RBAC
   */
  private void checkUserRights(List<String> userRoles, String userName)
      throws UnauthorizedException {
    List<String> rolesUserHas =
        userRoles.stream()
            .filter(WebSecurityConfig.ALLOWED_ROLES_TO_LOGIN::contains)
            .collect(Collectors.toList());

    if (rolesUserHas.isEmpty()) {
      LOGGER.error("User ({}) doesn't have permission to log in", userName);
      throw new UnauthorizedException("You don't have permission to log in");
    }
  }

  public LoginResponse renewToken(UserDetails user) {

    LoginTokenDto token = userService.renewToken(user.getToken());
    user.setToken(encryptUtil.encrypt(user.getToken()));

    long tokenExpiresIn = Math.min(tokenExpiration, token.getExpirationDuration());

    return new LoginResponse(jwtTokenService.generateToken(user, tokenExpiresIn));
  }

  public void logout(UserDetails user) throws ServiceException {
    userService.logoutUser(user.getToken());
  }

  public List<String> getUserRoles(UserDetails user) {
    UserDetails userInfo = userService.getUserInfoFromToken(user.getToken());
    return userInfo.getRoles();
  }

  public List<String> getUsers(UserDetails user, String query) {
    return userService.fetchDeploymentToolUsers(query);
  }
}
