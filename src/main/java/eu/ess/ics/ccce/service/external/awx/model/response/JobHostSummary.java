/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.service.external.awx.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import eu.ess.ics.ccce.common.Utils;
import java.util.Date;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 */
public class JobHostSummary {
  public static class Host {
    private int id;
    private String name;
    private String description;

    public int getId() {
      return id;
    }

    public String getName() {
      return name;
    }

    public String getDescription() {
      return description;
    }
  }

  public static class Job {
    private long id;
    private String name;
    private String description;
    private String status;
    private boolean failed;
    private double elapsed;
    private String type;

    @SerializedName("job_template_id")
    @JsonProperty("job_template_id")
    private int jobTemplateId;

    @SerializedName("job_template_name")
    @JsonProperty("job_template_name")
    private String jobTemplateName;

    public long getId() {
      return id;
    }

    public String getName() {
      return name;
    }

    public String getDescription() {
      return description;
    }

    public String getStatus() {
      return status;
    }

    public boolean isFailed() {
      return failed;
    }

    public double getElapsed() {
      return elapsed;
    }

    public String getType() {
      return type;
    }

    public int getJobTemplateId() {
      return jobTemplateId;
    }

    public String getJobTemplateName() {
      return jobTemplateName;
    }
  }

  public static class SummaryFields {
    private Host host;
    private Job job;

    public Host getHost() {
      return host;
    }

    public Job getJob() {
      return job;
    }
  }

  private long id;
  private String type;

  @SerializedName("summary_fields")
  @JsonProperty("summary_fields")
  private SummaryFields summaryFields;

  private Date created;
  private Date modified;
  private long job;
  private Long host;

  @SerializedName("host_name")
  @JsonProperty("host_name")
  private String hostName;

  private int changed;
  private int dark;
  private int failures;
  private int ok;
  private int processed;
  private int skipped;
  private boolean failed;
  private int ignored;
  private int rescued;

  public long getId() {
    return id;
  }

  public String getType() {
    return type;
  }

  public SummaryFields getSummaryFields() {
    return summaryFields;
  }

  public Date getCreated() {
    return Utils.cloneDate(created);
  }

  public Date getModified() {
    return Utils.cloneDate(modified);
  }

  public long getJob() {
    return job;
  }

  public Long getHost() {
    return host;
  }

  public String getHostName() {
    return hostName;
  }

  public int getChanged() {
    return changed;
  }

  public int getDark() {
    return dark;
  }

  public int getFailures() {
    return failures;
  }

  public int getOk() {
    return ok;
  }

  public int getProcessed() {
    return processed;
  }

  public int getSkipped() {
    return skipped;
  }

  public boolean isFailed() {
    return failed;
  }

  public int getIgnored() {
    return ignored;
  }

  public int getRescued() {
    return rescued;
  }
}
