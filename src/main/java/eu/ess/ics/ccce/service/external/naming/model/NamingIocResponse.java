/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.service.external.naming.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author <a href="mailto:domonkos.gulyas@ess.eu">Domonkos Gulyas</a>
 */
public class NamingIocResponse {

  private String uuid;
  private String name;
  private String deviceType;
  private String discipline;
  private String description;
  private String status;

  @JsonCreator
  public NamingIocResponse(
      @JsonProperty("uuid") final String uuid,
      @JsonProperty("name") final String name,
      @JsonProperty("deviceType") final String deviceType,
      @JsonProperty("discipline") final String discipline,
      @JsonProperty("description") final String description,
      @JsonProperty("status") final String status) {
    this.uuid = uuid;
    this.name = name;
    this.deviceType = deviceType;
    this.discipline = discipline;
    this.description = description;
    this.status = status;
  }

  public String getUuid() {
    return uuid;
  }

  public void setUuid(final String uuid) {
    this.uuid = uuid;
  }

  public String getName() {
    return name;
  }

  public void setName(final String name) {
    this.name = name;
  }

  public String getDeviceType() {
    return deviceType;
  }

  public void setDeviceType(final String deviceType) {
    this.deviceType = deviceType;
  }

  public String getDiscipline() {
    return discipline;
  }

  public void setDiscipline(final String discipline) {
    this.discipline = discipline;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(final String description) {
    this.description = description;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(final String status) {
    this.status = status;
  }
}
