/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.service.external.netbox.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author <a href="mailto:domonkos.gulyas@ess.eu">Domonkos Gulyas</a>
 */
public class NetBoxPropertyPreview {
  private final Long id;
  private final String url;
  private final String display;
  private final String name;
  private final String slug;

  @JsonCreator
  public NetBoxPropertyPreview(
      @JsonProperty("id") Long id,
      @JsonProperty("url") String url,
      @JsonProperty("display") String display,
      @JsonProperty("name") String name,
      @JsonProperty("slug") String slug) {
    this.id = id;
    this.url = url;
    this.display = display;
    this.name = name;
    this.slug = slug;
  }

  public Long getId() {
    return id;
  }

  public String getUrl() {
    return url;
  }

  public String getDisplay() {
    return display;
  }

  public String getName() {
    return name;
  }

  public String getSlug() {
    return slug;
  }
}
