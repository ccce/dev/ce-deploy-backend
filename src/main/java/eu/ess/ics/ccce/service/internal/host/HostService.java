/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.service.internal.host;

import eu.ess.ics.ccce.common.Utils;
import eu.ess.ics.ccce.common.alert.HostAlertGenerator;
import eu.ess.ics.ccce.common.conversion.ConversionUtil;
import eu.ess.ics.ccce.common.conversion.PagingUtil;
import eu.ess.ics.ccce.exceptions.EntityNotFoundException;
import eu.ess.ics.ccce.exceptions.InputValidationException;
import eu.ess.ics.ccce.repository.api.IIocRepository;
import eu.ess.ics.ccce.repository.entity.IocDeploymentEntity;
import eu.ess.ics.ccce.rest.model.Alert;
import eu.ess.ics.ccce.rest.model.host.request.HostIocFilter;
import eu.ess.ics.ccce.rest.model.host.response.*;
import eu.ess.ics.ccce.rest.model.job.response.Host;
import eu.ess.ics.ccce.rest.model.job.response.HostWithFqdn;
import eu.ess.ics.ccce.service.external.netbox.NetBoxService;
import eu.ess.ics.ccce.service.external.netbox.model.*;
import eu.ess.ics.ccce.service.internal.PagingLimitDto;
import eu.ess.ics.ccce.service.internal.security.dto.UserDetails;
import eu.ess.ics.ccce.service.internal.status.StatusService;
import java.util.*;
import java.util.stream.Collectors;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 */
@Service
public class HostService {

  private static final Logger LOGGER = LoggerFactory.getLogger(HostService.class);

  private final PagingUtil utils;
  private final StatusService statusService;
  private final NetBoxService netBoxService;
  private final IIocRepository iocRepository;
  private final HostAlertGenerator hostAlertGenerator;

  public HostService(
      PagingUtil utils,
      StatusService statusService,
      NetBoxService netBoxService,
      IIocRepository iocRepository,
      HostAlertGenerator hostAlertGenerator) {
    this.utils = utils;
    this.statusService = statusService;
    this.netBoxService = netBoxService;
    this.iocRepository = iocRepository;
    this.hostAlertGenerator = hostAlertGenerator;
  }

  public PagedNetBoxHostResponse findAllFromNetBoxPaged(
      String text, HostIocFilter filter, Integer page, Integer limit, UserDetails user) {

    PagingLimitDto pagingLimitDto = utils.pageLimitConverter(page, limit);
    String searchTextString = StringUtils.isEmpty(text) ? null : text;
    String userName = user == null ? null : user.getUserName();

    List<NetBoxHost> allHosts = netBoxService.findAllIocHosts();

    List<NetBoxHost> filteredHosts = getFilteredHosts(allHosts, searchTextString, filter, userName);

    List<HostInfoWithId> pagedHosts =
        netBoxService.addNetworkDetailsToHosts(
            filteredHosts.stream()
                .skip((long) pagingLimitDto.getPage() * pagingLimitDto.getLimit())
                .limit(pagingLimitDto.getLimit())
                .collect(Collectors.toList()));

    return new PagedNetBoxHostResponse(
        filteredHosts.size(),
        pagedHosts.size(),
        pagingLimitDto.getPage(),
        pagingLimitDto.getLimit(),
        pagedHosts);
  }

  @NotNull
  private List<NetBoxHost> getFilteredHosts(
      List<NetBoxHost> allHosts, String searchCriteria, HostIocFilter filter, String userName) {
    List<NetBoxHost> filteredHosts = filterByFreetext(allHosts, searchCriteria);

    return filteredHosts.stream()
        .filter(host -> matchesBasedOnFilter(host, filter, userName))
        .toList();
  }

  @NotNull
  private List<NetBoxHost> filterByFreetext(List<NetBoxHost> allHosts, String searchTextString) {

    String hostNameFilterString =
        searchTextString != null && searchTextString.contains(".")
            ? fqdnToHostname(searchTextString)
            : searchTextString;

    List<NetBoxHost> filteredHosts =
        allHosts.stream()
            .filter(host -> Utils.checkTextContains(host.getName(), hostNameFilterString))
            .toList();

    if (filteredHosts.isEmpty()) {
      filteredHosts =
          allHosts.stream()
              .filter(host -> Utils.checkTextContains(host.getDescription(), searchTextString))
              .toList();
    }

    return filteredHosts;
  }

  public boolean matchesBasedOnFilter(NetBoxHost host, HostIocFilter filter, String user) {

    if (HostIocFilter.NO_IOCS.equals(filter)) {
      List<NetBoxHostIdentifier> hostDeployments =
          iocRepository.findAllHostIdWithLatestSuccessfulDeployments();
      return !hasMatchInHosts(host, hostDeployments);
    }

    if (HostIocFilter.IOCS_DEPLOYED.equals(filter)) {
      List<NetBoxHostIdentifier> hostDeployments =
          iocRepository.findAllHostIdWithLatestSuccessfulDeployments();
      return hasMatchInHosts(host, hostDeployments);
    }

    if (HostIocFilter.OWN.equals(filter)) {
      List<NetBoxHostIdentifier> ownDeployments =
          iocRepository.findAllHostIdLatestSuccessfulDeploymentForUser(user);
      hasMatchInHosts(host, ownDeployments);
    }

    return true;
  }

  private boolean hasMatchInHosts(NetBoxHost host, List<NetBoxHostIdentifier> hostIdsInDb) {
    return hostIdsInDb.contains(host.getNetBoxHostIdentifier());
  }

  public List<NetBoxHost> findAllHosts(
      String hostName, String network, String networkScope, String hostType) {

    List<NetBoxHostIdentifier> hostFilterByNetwork =
        createHostFilterByNetwork(network, networkScope);

    return netBoxService.findAllIocHosts().stream()
        .filter(host -> Utils.checkTextContains(host.getName(), hostName))
        .filter(host -> Utils.checkTextContains(host.getType(), hostType))
        .filter(host -> checkHostIdContains(host.getNetBoxHostIdentifier(), hostFilterByNetwork))
        .collect(Collectors.toList());
  }

  private List<NetBoxHostIdentifier> createHostFilterByNetwork(
      String network, String networkScope) {
    if (network != null || networkScope != null) {
      return netBoxService.findHostIdentifiersByNetworkData(network, networkScope);
    }
    return null;
  }

  private boolean checkHostIdContains(
      NetBoxHostIdentifier netBoxHostIdentifier, List<NetBoxHostIdentifier> idFilter) {
    if (idFilter != null && !idFilter.isEmpty()) {
      return idFilter.contains(netBoxHostIdentifier);
    }
    return true;
  }

  public HostStatusResponse findHostStatus(String hostId) {
    NetBoxHostIdentifier hostIdentifier = decodeHostIdentifier(hostId);
    HostInfo netBoxHost =
        netBoxService.findHostById(hostIdentifier.getNetBoxHostId(), hostIdentifier.isVm());

    return new HostStatusResponse(
        hostId,
        assigned(netBoxHost.getId(), netBoxHost.isVm()),
        getHostStatus(netBoxHost.getFqdn()));
  }

  public HostAlertResponse findHostAlerts(String hostId) {
    NetBoxHostIdentifier hostIdentifier = decodeHostIdentifier(hostId);
    HostInfo netBoxHost =
        netBoxService.findHostById(hostIdentifier.getNetBoxHostId(), hostIdentifier.isVm());
    List<Alert> hostAlerts =
        hostAlertGenerator.generateHostDetailsAlert(
            netBoxHost.getFqdn(),
            iocRepository.countAssociatedDeployments(netBoxHost.getId(), netBoxHost.isVm()));

    return new HostAlertResponse(
        hostId, ConversionUtil.determineAlertSeverity(hostAlerts), hostAlerts);
  }

  public HostInfoWithId getHostByNetBoxId(String netBoxId) {
    NetBoxHostIdentifier hostIdentifier = decodeHostIdentifier(netBoxId);
    return getHostByNetBoxId(hostIdentifier.getNetBoxHostId(), hostIdentifier.isVm());
  }

  public HostInfoWithId getHostByNetBoxId(Long hostNetBoxId, Boolean isVm) {
    HostInfo netBoxHost = netBoxService.findHostById(hostNetBoxId, isVm);
    return convertToTargetNetBoxHost(netBoxHost);
  }

  public HostStatus getHostStatus(String host) {
    if (StringUtils.isNotEmpty(host)) {
      return BooleanUtils.toBoolean(statusService.checkHostIsActive(host))
          ? HostStatus.AVAILABLE
          : HostStatus.UNREACHABLE;
    }

    return HostStatus.UNREACHABLE;
  }

  public long countIocHostOnTechnicalNetwork() {
    return findAllHosts(null, null, "TechnicalNetwork", null).size();
  }

  public Map<String, Integer> countIocHostScopes(List<IocDeploymentEntity> deployments) {
    List<NetBoxHostIdentifier> hostIds =
        deployments.stream()
            .map(IocDeploymentEntity::getNetBoxHostIdentifier)
            .distinct()
            .collect(Collectors.toList());
    Map<NetBoxHostIdentifier, String> idScopeMap =
        hostIds.stream()
            .map(
                hostIdentifier -> {
                  try {
                    return netBoxService.findHostById(
                        hostIdentifier.getNetBoxHostId(), hostIdentifier.isVm());
                  } catch (EntityNotFoundException e) {
                    return null;
                  }
                })
            .filter(Objects::nonNull)
            .collect(
                Collectors.toMap(
                    hostInfo -> new NetBoxHostIdentifier(hostInfo.getId(), hostInfo.isVm()),
                    HostInfo::getScope));

    Map<String, Integer> scopeCountMap = new HashMap<>();
    for (IocDeploymentEntity deployment : deployments) {
      String scope = idScopeMap.get(deployment.getNetBoxHostIdentifier());

      // for hosts that have been deleted from NetBox
      if (StringUtils.isEmpty(scope)) {
        scope = "Unknown";
      }

      if (scopeCountMap.containsKey(scope)) {
        scopeCountMap.put(scope, scopeCountMap.get(scope) + 1);
      } else {
        scopeCountMap.put(scope, 1);
      }
    }

    // for sorting map by key in alphabetic order
    return new TreeMap<>(scopeCountMap);
  }

  public HostInfoWithId findAndConvertHostByFQDN(String fqdn) {
    NetBoxHost host = findHostByFQDN(fqdn);
    HostInfo hostWithNetworkDetails = netBoxService.hostWithNetworkDetails(host);
    return new HostInfoWithId(
        hostWithNetworkDetails, encodeHostIdentifier(host.getNetBoxHostIdentifier()));
  }

  public NetBoxHost findHostByFQDN(String fqdn) {
    String hostname = fqdnToHostname(fqdn);
    return findHostByName(hostname);
  }

  public void checkHostExists(String hostId) {
    NetBoxHostIdentifier hostIdentifier = decodeHostIdentifier(hostId);
    netBoxService.checkHostExists(hostIdentifier.getNetBoxHostId(), hostIdentifier.isVm());
  }

  private NetBoxHost findHostByName(String hostname) {
    try {
      return (NetBoxVirtualMachine) netBoxService.findVirtualMachineByName(hostname);
    } catch (EntityNotFoundException e) {
      return (NetBoxDevice) netBoxService.findDeviceByName(hostname);
    }
  }

  private boolean assigned(long netBoxHostId, boolean isVm) {
    return !iocRepository.findAllActualIocDeploymentsByHostId(netBoxHostId, isVm).isEmpty();
  }

  /**
   * Finds all IOC hosts from NetBox
   *
   * @return All IOC hosts that is listed in NetBox, or empty list when response is empty
   */
  public List<NetBoxHost> findAllHosts() {
    return netBoxService.findAllIocHosts();
  }

  /**
   * Counts IOC hosts according to NetBox
   *
   * @return number of IOC hosts, or NULL if error occurs during NetBox fetch
   */
  public Long countIocsHost() {
    try {
      return (long) netBoxService.findAllIocHosts().size();
    } catch (Exception e) {
      return null;
    }
  }

  /**
   * Lists all IOC hosts from NetBox that have some issue(s) according to Prometheus
   *
   * @return All IOC hosts that have issues, or empty list
   */
  public List<NetBoxHost> findAllHostsWithIssues() {

    List<NetBoxHost> hosts = findAllHosts();
    List<NetBoxHost> hostsWithIssues = new ArrayList<>();

    // check for issues
    for (NetBoxHost host : hosts) {
      String fqdn = netBoxService.findIpAddressById(host.getIpdAddressId()).getDnsName();
      if (!BooleanUtils.toBoolean(statusService.checkIOCExporterForHost(fqdn))) {
        hostsWithIssues.add(host);
      }
    }

    return hostsWithIssues;
  }

  /**
   * Checks if a host has been renamed in NetBox compared to the stored deployed values.
   *
   * @param hostNetBoxId the target host's NetBox ID
   * @param isVm the target host is a VM or not
   * @param fqdn the stored fqdn name
   * @param hostName the stored hostName
   * @param network the stored network
   * @return <code>true</code> if fqdn, or hostName, or network differs from the stored ones <code>
   *     false</code> if all parameters equals to the stored ones <code>null</code> if netBoxId can
   *     not be resolved
   */
  public Boolean hasTargetHostRenamed(
      Long hostNetBoxId, Boolean isVm, String fqdn, String hostName, String network) {
    try {
      HostInfoWithId targetHost = getHostByNetBoxId(hostNetBoxId, isVm);

      return !targetHost.getFqdn().equals(fqdn)
          || !targetHost.getName().equals(hostName)
          || !targetHost.getNetwork().equals(network);
    } catch (Exception e) {
      // host can not be resolved
      return null;
    }
  }

  private static HostInfoWithId convertToTargetNetBoxHost(HostInfo netBoxHost) {
    return new HostInfoWithId(netBoxHost, encodeHostIdentifier(netBoxHost.getHostIdentifier()));
  }

  @NotNull
  public static String fqdnToHostname(String fqdn) {
    return fqdn.indexOf(".") > 0 ? fqdn.substring(0, fqdn.indexOf(".")) : fqdn;
  }

  public static String encodeHostIdentifier(NetBoxHostIdentifier hostIdentifier) {
    String hostIdRepresentation =
        "%s_%s".formatted(hostIdentifier.getNetBoxHostId(), hostIdentifier.isVm());
    byte[] encodedBytes = Base64.getEncoder().encode(hostIdRepresentation.getBytes());
    return new String(encodedBytes);
  }

  public static NetBoxHostIdentifier decodeHostIdentifier(String encodedHostId) {
    if (encodedHostId == null || encodedHostId.isEmpty()) {
      return null;
    }

    String decodedString = new String(Base64.getDecoder().decode(encodedHostId));
    String[] hostIdParts = decodedString.split("_");

    if (hostIdParts.length == 2) {
      try {
        Long hostId = Long.parseLong(hostIdParts[0]);
        Boolean isVm = Boolean.parseBoolean(hostIdParts[1]);
        return new NetBoxHostIdentifier(hostId, isVm);
      } catch (Exception e) {
        throw new InputValidationException("Unable decode hostId");
      }
    }
    throw new InputValidationException("Unable decode hostId");
  }

  /**
   * Host DTO from raw data
   *
   * @param hostIdentifier NetBox indentifier of the host
   * @param hostExternalIdValid Indicates if external host ID still exists according to external
   *     host service
   * @param fqdn Fully qualified domain name (from external service)
   * @param hostName Host name (from external service)
   * @param network Network identifier (from external service)
   * @return Host DTO that could be consumed by clients
   */
  public static Host convertToHost(
      NetBoxHostIdentifier hostIdentifier,
      boolean hostExternalIdValid,
      String fqdn,
      String hostName,
      String network) {
    return new Host(
        encodeHostIdentifier(hostIdentifier), hostExternalIdValid, fqdn, hostName, network);
  }

  public boolean isHostExternalIdValid(IocDeploymentEntity deployment) {
    try {
      HostInfoWithId host = getHostByNetBoxId(deployment.getNetBoxHostId(), deployment.isVm());
      return host != null;
    } catch (Exception e) {
      LOGGER.error("Cannot retrieve host data from NetBox service.", e);
    }
    return false;
  }

  /**
   * Converts NetBox's Host response to HostWithFqdn. (Extracting necessary fields into DTO)
   *
   * @param host NetBox's Host response
   * @return Converted HostWithFqdn DTO
   */
  public static HostWithFqdn convertToHostWithFqdn(Host host) {
    return new HostWithFqdn(host.getHostId(), host.isExternalIdValid(), host.getFqdn());
  }
}
