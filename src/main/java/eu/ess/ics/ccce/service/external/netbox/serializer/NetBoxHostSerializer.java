package eu.ess.ics.ccce.service.external.netbox.serializer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import eu.ess.ics.ccce.service.external.netbox.model.NetBoxDevice;
import eu.ess.ics.ccce.service.external.netbox.model.NetBoxHost;
import eu.ess.ics.ccce.service.external.netbox.model.NetBoxVirtualMachine;
import java.io.IOException;

/**
 * @author <a href="mailto:domonkos.gulyas@ess.eu">Domonkos Gulyas</a>
 */
public class NetBoxHostSerializer extends StdDeserializer<NetBoxHost> {
  public NetBoxHostSerializer() {
    super(NetBoxHost.class);
  }

  @Override
  public NetBoxHost deserialize(JsonParser jsonParser, DeserializationContext co)
      throws IOException {
    JsonNode node = jsonParser.getCodec().readTree(jsonParser);

    if (node.has("device_type")) {
      return jsonParser.getCodec().treeToValue(node, NetBoxDevice.class);
    } else if (node.has("memory")) {
      return jsonParser.getCodec().treeToValue(node, NetBoxVirtualMachine.class);
    }

    throw new IllegalArgumentException("Cannot determine subclass type");
  }
}
