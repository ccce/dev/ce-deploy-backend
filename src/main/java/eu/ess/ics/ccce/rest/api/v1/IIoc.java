/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.rest.api.v1;

import eu.ess.ics.ccce.configuration.WebSecurityConfig;
import eu.ess.ics.ccce.configuration.exception.GeneralException;
import eu.ess.ics.ccce.exceptions.EntityNotFoundException;
import eu.ess.ics.ccce.exceptions.GitlabServiceException;
import eu.ess.ics.ccce.rest.model.ioc.request.CreateIoc;
import eu.ess.ics.ccce.rest.model.ioc.request.IOCUpdateRequest;
import eu.ess.ics.ccce.rest.model.ioc.request.IocDeploymentStatus;
import eu.ess.ics.ccce.rest.model.ioc.request.IocOrder;
import eu.ess.ics.ccce.rest.model.ioc.request.UpdateHostRequest;
import eu.ess.ics.ccce.rest.model.ioc.response.Ioc;
import eu.ess.ics.ccce.rest.model.ioc.response.IocAlertResponse;
import eu.ess.ics.ccce.rest.model.ioc.response.IocDescriptionResponse;
import eu.ess.ics.ccce.rest.model.ioc.response.IocDetails;
import eu.ess.ics.ccce.rest.model.ioc.response.IocStatusResponse;
import eu.ess.ics.ccce.rest.model.ioc.response.PagedIocResponse;
import eu.ess.ics.ccce.rest.model.job.response.Deployment;
import eu.ess.ics.ccce.rest.model.job.response.IdUsedByIoc;
import eu.ess.ics.ccce.rest.model.loki.response.LokiResponse;
import eu.ess.ics.ccce.service.internal.security.dto.UserDetails;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 */
@RequestMapping("/api/v1/iocs")
@Tag(name = "IOCs")
public interface IIoc {
  @Operation(summary = "List IOCs")
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "401",
            description = "Unauthorized",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "500",
            description = "Service exception",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "200",
            description = "A paged array of IOCs",
            content = @Content(schema = @Schema(implementation = PagedIocResponse.class)))
      })
  @GetMapping(produces = {"application/json"})
  PagedIocResponse listIocs(
      @AuthenticationPrincipal UserDetails user,
      @Parameter(description = "IOC's current deployment status")
          @RequestParam(required = false, name = "deployment_status")
          IocDeploymentStatus deploymentStatus,
      @Parameter(description = "Search by user-name who created the ioc")
          @RequestParam(required = false, name = "created_by")
          String createdBy,
      @Parameter(description = "Network") @RequestParam(required = false) String network,
      @Parameter(description = "Network scope")
          @RequestParam(required = false, name = "network_scope")
          String networkScope,
      @Parameter(description = "Host type") @RequestParam(required = false, name = "host_type")
          String hostType,
      @Parameter(description = "Host name") @RequestParam(required = false, name = "host_name")
          String hostName,
      @Parameter(description = "Search text (Search for Naming name)")
          @RequestParam(required = false)
          String query,
      @Parameter(description = "Order by")
          @RequestParam(required = false, name = "order_by", defaultValue = "IOC_NAME")
          IocOrder orderBy,
      @Parameter(description = "Order Ascending")
          @RequestParam(required = false, name = "order_asc", defaultValue = "true")
          Boolean isAsc,
      @Parameter(description = "Page offset") @RequestParam(required = false) Integer page,
      @Parameter(description = "Page size") @RequestParam(required = false) Integer limit,
      @Parameter(
              description =
                  "Use this parameter to list all IOCs in the response! Setting this parameter to true will override the Page and Limit parameters!")
          @RequestParam(required = false, name = "list_all", defaultValue = "false")
          boolean listAll);

  @Operation(summary = "Find IOC details by ID")
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "401",
            description = "Unauthorized",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "404",
            description = "Ioc not found",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "500",
            description = "Service exception",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "200",
            description = "Found IOC",
            content = @Content(schema = @Schema(implementation = IocDetails.class)))
      })
  @GetMapping(
      value = "/{ioc_id}",
      produces = {"application/json"})
  IocDetails getIoc(
      @AuthenticationPrincipal UserDetails user,
      @Parameter(in = ParameterIn.PATH, description = "Unique ID of IOC", required = true)
          @PathVariable("ioc_id")
          long iocId)
      throws EntityNotFoundException;

  @Operation(summary = "Find IOC description by ID")
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "404",
            description = "Ioc not found",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "500",
            description = "Service exception",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "200",
            description = "Found IOC description",
            content = @Content(schema = @Schema(implementation = IocDescriptionResponse.class)))
      })
  @GetMapping(
      value = "/{ioc_id}/description",
      produces = {"application/json"})
  IocDescriptionResponse getIocDescription(
      @Parameter(in = ParameterIn.PATH, description = "Unique ID of IOC", required = true)
          @PathVariable("ioc_id")
          long iocId);

  @Operation(
      summary = "Create a new IOC",
      security = {@SecurityRequirement(name = "bearerAuth")})
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "201",
            description = "IOC created",
            content = @Content(schema = @Schema(implementation = Ioc.class))),
        @ApiResponse(
            responseCode = "400",
            description = "Incomplete, or bad request",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "401",
            description = "Unauthorized",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "403",
            description = "Forbidden: User doesn't have the necessary permissions in Gitlab",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "409",
            description = "IOC already created",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "422",
            description = "Unprocessable request",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "424",
            description = "Metadata file not found, or not processable",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "500",
            description = "Service exception",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "503",
            description = "Remote service exception",
            content = @Content(schema = @Schema(implementation = GeneralException.class)))
      })
  @PostMapping(
      produces = {"application/json"},
      consumes = {"application/json"})
  @ResponseStatus(HttpStatus.CREATED)
  @PreAuthorize(WebSecurityConfig.IS_ADMIN_OR_INTEGRATOR)
  Ioc createIoc(
      @AuthenticationPrincipal UserDetails user,
      @Parameter(
              in = ParameterIn.DEFAULT,
              description = "IOC to create",
              required = true,
              schema = @Schema(implementation = CreateIoc.class))
          @RequestBody
          CreateIoc body);

  @Operation(
      summary = "Deletes a single IOC based on the ID supplied",
      security = {@SecurityRequirement(name = "bearerAuth")})
  @ApiResponses(
      value = {
        @ApiResponse(responseCode = "204", description = "IOC deleted"),
        @ApiResponse(
            responseCode = "401",
            description = "Unauthorized",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "409",
            description = "Concurrent deployment, undeployment, start or stop for IOC is ongoing",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "500",
            description = "Service exception",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "403",
            description = "Forbidden: User doesn't have the necessary permissions",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "503",
            description = "Git repository can not be deleted",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "404",
            description = "IOC not found",
            content = @Content(schema = @Schema(implementation = GeneralException.class)))
      })
  @DeleteMapping(
      value = "/{ioc_id}",
      produces = {"application/json"})
  @ResponseStatus(HttpStatus.NO_CONTENT)
  ResponseEntity deleteIoc(
      @AuthenticationPrincipal UserDetails user,
      @Parameter(in = ParameterIn.PATH, description = "Id of IOC to delete", required = true)
          @PathVariable("ioc_id")
          long iocId);

  @Operation(
      summary = "Updating an IOC by ID",
      security = {@SecurityRequirement(name = "bearerAuth")})
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "401",
            description = "Unauthorized",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "403",
            description = "Forbidden: User doesn't have the necessary permissions",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "404",
            description = "Ioc not found",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "409",
            description =
                "IOC already created or concurrent deployment, undeployment, start or stop for IOC is ongoing",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "422",
            description = "Unprocessable request",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "500",
            description = "Service exception",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "503",
            description = "Remote service exception",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "200",
            description = "IOC updated",
            content = @Content(schema = @Schema(implementation = Ioc.class)))
      })
  @PatchMapping(
      value = "/{ioc_id}",
      produces = {"application/json"})
  Ioc updateIoc(
      @AuthenticationPrincipal UserDetails user,
      @Parameter(in = ParameterIn.PATH, description = "Unique ID of IOC", required = true)
          @PathVariable("ioc_id")
          long iocId,
      @RequestBody IOCUpdateRequest updateBody)
      throws EntityNotFoundException;

  @Operation(
      summary = "Undeploy in database",
      description = "Marks an IOC in database as undeployed - will not call AWX",
      security = {@SecurityRequirement(name = "bearerAuth")})
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "200",
            description = "Undeployment created",
            content =
                @Content(
                    array = @ArraySchema(schema = @Schema(implementation = Deployment.class)))),
        @ApiResponse(
            responseCode = "401",
            description = "Unauthorized",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "403",
            description = "Forbidden: user doesn't have the necessary permissions to undeploy",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "404",
            description = "Not found",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "409",
            description = "Ongoing deployment, or undeployment for IOC",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "422",
            description = "IOC is not deployed",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "500",
            description = "Service exception",
            content = @Content(schema = @Schema(implementation = GeneralException.class)))
      })
  @PatchMapping(
      value = "/{ioc_id}/undeploy_in_db",
      produces = {"application/json"})
  @ResponseStatus(HttpStatus.NO_CONTENT)
  @PreAuthorize(WebSecurityConfig.IS_ADMIN)
  void unDeployInDb(
      @AuthenticationPrincipal UserDetails user,
      @Parameter(
              in = ParameterIn.PATH,
              description = "The id of the IOC to undeploy",
              required = true)
          @PathVariable("ioc_id")
          long iocId);

  @Operation(
      summary = "Modify deployment host in database",
      description = "Modifies host of IOC's active deployment in database - will not call AWX",
      security = {@SecurityRequirement(name = "bearerAuth")})
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "200",
            description = "Successful host update",
            content = @Content(schema = @Schema(implementation = Ioc.class))),
        @ApiResponse(
            responseCode = "401",
            description = "Unauthorized",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "403",
            description = "Forbidden: user doesn't have the necessary permissions to modify",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "404",
            description = "Not found",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "409",
            description = "Ongoing operation for IOC",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "422",
            description = "IOC is not deployed",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "500",
            description = "Service exception",
            content = @Content(schema = @Schema(implementation = GeneralException.class)))
      })
  @PatchMapping(
      value = "/{ioc_id}/active_deployment/host",
      produces = {"application/json"})
  @PreAuthorize(WebSecurityConfig.IS_ADMIN)
  Ioc updateActiveDeploymentHost(
      @AuthenticationPrincipal UserDetails user,
      @Parameter(in = ParameterIn.PATH, description = "The ID of the IOC", required = true)
          @PathVariable("ioc_id")
          long iocId,
      @Parameter(
              in = ParameterIn.DEFAULT,
              description = "IOC to update",
              required = true,
              schema = @Schema(implementation = UpdateHostRequest.class))
          @Valid
          @RequestBody
          UpdateHostRequest body);

  @Operation(summary = "Fetches the Prometheus status of the IOC")
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "200",
            description = "Log lines",
            content = @Content(schema = @Schema(implementation = IocStatusResponse.class))),
        @ApiResponse(
            responseCode = "401",
            description = "Unauthorized",
            content =
                @Content(
                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "404",
            description = "Ioc not found",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "500",
            description = "Service exception",
            content =
                @Content(
                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = @Schema(implementation = GeneralException.class)))
      })
  @GetMapping(
      value = "/{ioc_id}/status",
      produces = {"application/json"})
  IocStatusResponse fetchIocStatus(
      @Parameter(in = ParameterIn.PATH, description = "Unique ID of the IOC", required = true)
          @PathVariable("ioc_id")
          long iocId);

  @Operation(summary = "Fetches alerts of the IOC")
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "200",
            description = "Alerts of the IOC",
            content = @Content(schema = @Schema(implementation = IocAlertResponse.class))),
        @ApiResponse(
            responseCode = "404",
            description = "Ioc not found",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "500",
            description = "Service exception",
            content =
                @Content(
                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = @Schema(implementation = GeneralException.class)))
      })
  @GetMapping(
      value = "/{ioc_id}/alerts",
      produces = {"application/json"})
  IocAlertResponse fetchIocAlerts(
      @Parameter(in = ParameterIn.PATH, description = "Unique ID of the IOC", required = true)
          @PathVariable("ioc_id")
          long iocId);

  @Operation(
      summary = "Fetches procServ Log lines for a specific host",
      security = {@SecurityRequirement(name = "bearerAuth")})
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "200",
            description = "Log lines",
            content = @Content(schema = @Schema(implementation = LokiResponse.class))),
        @ApiResponse(
            responseCode = "401",
            description = "Unauthorized",
            content =
                @Content(
                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "503",
            description = "Logging server unavailable",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "500",
            description = "Service exception",
            content =
                @Content(
                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = @Schema(implementation = GeneralException.class)))
      })
  @GetMapping(
      value = "/{host_name}/{ioc_name}/log",
      produces = {"application/json"})
  @PreAuthorize(WebSecurityConfig.IS_ADMIN_OR_INTEGRATOR)
  LokiResponse fetchProcServLogLines(
      @Parameter(
              in = ParameterIn.PATH,
              description = "Host name (without network part)",
              required = true)
          @PathVariable("host_name")
          String hostName,
      @Parameter(
              in = ParameterIn.PATH,
              description = "Name of the IOC to get procServ logs",
              required = true)
          @PathVariable("ioc_name")
          String iocName,
      @Parameter(description = "Time range (in minutes)")
          @RequestParam(required = false, name = "time_range")
          Integer timeRange);

  @ApiResponses({
    @ApiResponse(
        responseCode = "200",
        description = "Ok",
        content = @Content(schema = @Schema(implementation = IdUsedByIoc.class))),
    @ApiResponse(
        responseCode = "500",
        description = "Service exception",
        content = @Content(schema = @Schema(implementation = GeneralException.class)))
  })
  @Operation(
      summary = "Checks if Git Repository Id is being used by any of the IOCs",
      description =
          """
          Checks Git Repository Id if it is in use by IOC.\s

          Function is not checking the Git repo ID in the deployment history!\s

          If no IOC uses the Git repo Id then the iocId will be NULL!""")
  @GetMapping(
      value = "/check_git_repo_id/{repo_id}",
      produces = {"application/json"})
  IdUsedByIoc checkGitRepoId(
      @Parameter(in = ParameterIn.PATH, description = "Git repository Id", required = true)
          @PathVariable("repo_id")
          long repoId)
      throws GitlabServiceException;
}
