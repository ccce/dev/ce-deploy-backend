/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.rest.api.v1;

import eu.ess.ics.ccce.configuration.WebSecurityConfig;
import eu.ess.ics.ccce.configuration.exception.GeneralException;
import eu.ess.ics.ccce.exceptions.EntityNotFoundException;
import eu.ess.ics.ccce.exceptions.NetBoxServiceException;
import eu.ess.ics.ccce.rest.model.host.request.HostIocFilter;
import eu.ess.ics.ccce.rest.model.host.response.*;
import eu.ess.ics.ccce.rest.model.ioc.request.IocOrder;
import eu.ess.ics.ccce.rest.model.loki.response.LokiResponse;
import eu.ess.ics.ccce.service.internal.security.dto.UserDetails;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 */
@RequestMapping("/api/v1/hosts")
@Tag(name = "Hosts")
public interface IHost {

  @Operation(summary = "Check host exists")
  @ApiResponses(
      value = {
        @ApiResponse(responseCode = "200", description = "Hosts exists in NetBox"),
        @ApiResponse(responseCode = "404", description = "Not found"),
        @ApiResponse(
            responseCode = "503",
            description = "Remote server exception",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "500",
            description = "Service exception",
            content = @Content(schema = @Schema(implementation = GeneralException.class)))
      })
  @RequestMapping(value = "/{host_id}/exists", method = RequestMethod.GET)
  @ResponseStatus(HttpStatus.OK)
  void checkHostExists(
      @Parameter(in = ParameterIn.PATH, description = "Host identifier", required = true)
          @PathVariable("host_id")
          String hostId);

  @Operation(summary = "List hosts")
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "401",
            description = "Unauthorized",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "200",
            description = "A paged array of hosts",
            content = @Content(schema = @Schema(implementation = PagedNetBoxHostResponse.class))),
        @ApiResponse(
            responseCode = "503",
            description = "Remote server exception",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "500",
            description = "Service exception",
            content = @Content(schema = @Schema(implementation = GeneralException.class)))
      })
  @GetMapping(produces = {"application/json"})
  PagedNetBoxHostResponse listHosts(
      @AuthenticationPrincipal UserDetails user,
      @Parameter(description = "Search text (Search for host name or description)")
          @RequestParam(required = false)
          String text,
      @Parameter(description = "Filtering depending on deployments") @RequestParam(required = false)
          HostIocFilter filter,
      @Parameter(description = "Page offset") @RequestParam(required = false) Integer page,
      @Parameter(description = "Page size", schema = @Schema(maximum = "100"))
          @RequestParam(required = false)
          Integer limit,
      @Parameter(
              description =
                  "Use this parameter to list all Hosts in the response! Setting this parameter to true will override the Page and Limit parameters!")
          @RequestParam(required = false, name = "list_all", defaultValue = "false")
          boolean listAll);

  @Operation(summary = "Find host status by external ID")
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "404",
            description = "Not found",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "503",
            description = "Remote server exception",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "500",
            description = "Service exception",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "200",
            description = "Host status",
            content = @Content(schema = @Schema(implementation = HostStatusResponse.class)))
      })
  @GetMapping(
      value = "/{host_id}/status",
      produces = {"application/json"})
  HostStatusResponse findHostStatus(
      @Parameter(in = ParameterIn.PATH, description = "Host identifier", required = true)
          @PathVariable("host_id")
          String hostId);

  @Operation(summary = "Find host alerts by external ID")
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "404",
            description = "Not found",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "503",
            description = "Remote server exception",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "500",
            description = "Service exception",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "200",
            description = "Host alerts",
            content = @Content(schema = @Schema(implementation = HostAlertResponse.class)))
      })
  @GetMapping(
      value = "/{host_id}/alerts",
      produces = {"application/json"})
  HostAlertResponse findHostAlerts(
      @Parameter(in = ParameterIn.PATH, description = "Host identifier", required = true)
          @PathVariable("host_id")
          String hostId);

  @Operation(summary = "Find associated IOCs for the host")
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "401",
            description = "Unauthorized",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "500",
            description = "Service exception",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "200",
            description = "Found Host",
            content = @Content(schema = @Schema(implementation = PagedAssociatedIocs.class)))
      })
  @GetMapping(
      value = "/{host_id}/iocs",
      produces = {"application/json"})
  PagedAssociatedIocs findAssociatedIocsByHostId(
      @AuthenticationPrincipal UserDetails user,
      @Parameter(in = ParameterIn.PATH, description = "Host identifier", required = true)
          @PathVariable("host_id")
          String hostId,
      @Parameter(description = "IOC Name") @RequestParam(required = false, name = "ioc_name")
          String iocName,
      @Parameter(description = "Order by")
          @RequestParam(required = false, name = "order_by", defaultValue = "IOC_NAME")
          IocOrder orderBy,
      @Parameter(description = "Order Ascending")
          @RequestParam(required = false, name = "order_asc", defaultValue = "true")
          Boolean isAsc,
      @Parameter(description = "Page offset") @RequestParam(required = false) Integer page,
      @Parameter(description = "Page size", schema = @Schema(maximum = "100"))
          @RequestParam(required = false)
          Integer limit);

  @Operation(summary = "Find host by FQDN")
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "401",
            description = "Unauthorized",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "404",
            description = "Not found",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "500",
            description = "Service exception",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "200",
            description = "Found Host",
            content = @Content(schema = @Schema(implementation = HostInfoWithId.class)))
      })
  @GetMapping(
      value = "/{fqdn}/info_by_fqdn",
      produces = {"application/json"})
  HostInfoWithId findNetBoxHostByFqdn(
      @AuthenticationPrincipal UserDetails user,
      @Parameter(in = ParameterIn.PATH, description = "The host's FQDN", required = true)
          @PathVariable
          String fqdn)
      throws EntityNotFoundException, NetBoxServiceException;

  @Operation(
      summary = "Fetches syslog lines for a specific host",
      security = {@SecurityRequirement(name = "bearerAuth")})
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "200",
            description = "Log lines",
            content = @Content(schema = @Schema(implementation = LokiResponse.class))),
        @ApiResponse(
            responseCode = "401",
            description = "Unauthorized",
            content =
                @Content(
                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "503",
            description = "Logging server unavailable",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "500",
            description = "Service exception",
            content =
                @Content(
                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = @Schema(implementation = GeneralException.class)))
      })
  @GetMapping(
      value = "/{host_name}/log",
      produces = {"application/json"})
  @PreAuthorize(WebSecurityConfig.IS_ADMIN_OR_INTEGRATOR)
  LokiResponse fetchSyslogLines(
      @Parameter(
              in = ParameterIn.PATH,
              description = "Host name (without network part)",
              required = true)
          @PathVariable(name = "host_name")
          String hostName,
      @Parameter(description = "Time range (in minutes)")
          @RequestParam(required = false, name = "time_range")
          Integer timeRange);

  @Operation(summary = "Find host by Host ID")
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "401",
            description = "Unauthorized",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "404",
            description = "Not found",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "500",
            description = "Service exception",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "200",
            description = "Found Host",
            content = @Content(schema = @Schema(implementation = HostInfoWithId.class)))
      })
  @GetMapping(
      value = "/{host_id}/info_by_host_id",
      produces = {"application/json"})
  HostInfoWithId findNetBoxHostByHostId(
      @AuthenticationPrincipal UserDetails user,
      @Parameter(in = ParameterIn.PATH, description = "The host's Host ID", required = true)
          @PathVariable("host_id")
          String hostId)
      throws EntityNotFoundException, NetBoxServiceException;
}
