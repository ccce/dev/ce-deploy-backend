/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.rest.model.awx.request;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import eu.ess.ics.ccce.common.Utils;
import eu.ess.ics.ccce.common.conversion.ZonedDateTimeSerializer;
import eu.ess.ics.ccce.service.external.awx.model.JobStatus;
import java.time.ZonedDateTime;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 */
public class AwxJobMeta {
  private long id;
  private @JsonSerialize(using = ZonedDateTimeSerializer.class) ZonedDateTime started;
  private @JsonSerialize(using = ZonedDateTimeSerializer.class) ZonedDateTime finished;
  private JobStatus status;

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public ZonedDateTime getStarted() {
    return Utils.cloneDate(started);
  }

  public void setStarted(ZonedDateTime started) {
    this.started = Utils.cloneDate(started);
  }

  public ZonedDateTime getFinished() {
    return Utils.cloneDate(finished);
  }

  public void setFinished(ZonedDateTime finished) {
    this.finished = Utils.cloneDate(finished);
  }

  public JobStatus getStatus() {
    return status;
  }

  public void setStatus(JobStatus status) {
    this.status = status;
  }

  @Override
  public String toString() {
    return "AwxJobMeta{"
        + "id="
        + id
        + ", started="
        + started
        + ", finished="
        + finished
        + ", status="
        + status
        + '}';
  }
}
