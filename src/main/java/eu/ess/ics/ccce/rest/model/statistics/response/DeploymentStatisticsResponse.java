/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.rest.model.statistics.response;

import java.util.Date;
import java.util.Objects;

/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 */
public class DeploymentStatisticsResponse {
  private Date deploymentDate;
  private long numberOfDeployments;
  private long successfulDeployments;

  public DeploymentStatisticsResponse(
      Date deploymentDate, long numberOfDeployments, long successfulDeployments) {
    this.deploymentDate = deploymentDate;
    this.numberOfDeployments = numberOfDeployments;
    this.successfulDeployments = successfulDeployments;
  }

  public Date getDeploymentDate() {
    return deploymentDate;
  }

  public long getSuccessfulDeployments() {
    return successfulDeployments;
  }

  public long getNumberOfDeployments() {
    return numberOfDeployments;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    DeploymentStatisticsResponse that = (DeploymentStatisticsResponse) o;
    return numberOfDeployments == that.numberOfDeployments
        && successfulDeployments == that.successfulDeployments
        && Objects.equals(deploymentDate, that.deploymentDate);
  }

  @Override
  public int hashCode() {
    return Objects.hash(deploymentDate, numberOfDeployments, successfulDeployments);
  }

  @Override
  public String toString() {
    return "DeploymentStatisticsResponse{"
        + "deploymentDate="
        + deploymentDate
        + ", numberOfDeployments="
        + numberOfDeployments
        + ", successfulDeployments="
        + successfulDeployments
        + '}';
  }
}
