/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.rest.controller;

import eu.ess.ics.ccce.exceptions.GitlabServiceException;
import eu.ess.ics.ccce.exceptions.RemoteException;
import eu.ess.ics.ccce.exceptions.ServiceException;
import eu.ess.ics.ccce.exceptions.TooManyEntitiesException;
import eu.ess.ics.ccce.repository.entity.IocEntity;
import eu.ess.ics.ccce.rest.api.v1.INames;
import eu.ess.ics.ccce.rest.model.FeatureTag;
import eu.ess.ics.ccce.rest.model.job.response.IdUsedByIoc;
import eu.ess.ics.ccce.rest.model.naming.response.NameResponse;
import eu.ess.ics.ccce.service.external.naming.NamingService;
import eu.ess.ics.ccce.service.internal.ioc.IocService;
import java.util.List;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 */
@RestController
@FeatureTag("names")
public class NamesController implements INames {

  private static final Logger LOGGER = LoggerFactory.getLogger(NamesController.class);

  private final NamingService namingService;
  private final IocService iocService;

  public NamesController(NamingService namingService, IocService iocService) {
    this.namingService = namingService;
    this.iocService = iocService;
  }

  @Override
  public List<NameResponse> fetchIOCByName(String iocName) {
    try {
      return namingService.searchIocsByName(iocName);
    } catch (ServiceException | RemoteException e) {
      LOGGER.error(e.getMessage(), e);
      throw e;
    } catch (Exception e) {
      LOGGER.error("Error while trying to fetch IOC names by name", e);
      throw new ServiceException("Error while trying to fetch IOC names by name");
    }
  }

  @Override
  public IdUsedByIoc checkNamingUUId(String namingUUId) throws GitlabServiceException {
    try {
      List<IocEntity> iocEnts = iocService.findIocByNamingUUId(namingUUId);

      // more IOCs use the same Naming UUID
      if (iocEnts.size() > 1) {
        throw new TooManyEntitiesException(
            "Too many IOCs use the same Naming UUID! IOC Ids: "
                + iocEnts.stream().map(IocEntity::getId).collect(Collectors.toList()));
      }

      return new IdUsedByIoc(iocEnts.stream().findFirst().map(IocEntity::getId).orElse(null));

    } catch (TooManyEntitiesException e) {
      LOGGER.error(e.getMessage(), e);
      throw e;
    } catch (Exception e) {
      LOGGER.error("Error while trying to find IOC by Naming UUID", e);
      throw new ServiceException("Error while trying to find IOC by Naming UUID");
    }
  }
}
