package eu.ess.ics.ccce.rest.model.inventory.response;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public record IOCInventory(
    @JsonProperty("name") String namingName,
    @JsonProperty("git") String gitProjectUrl,
    @JsonProperty("version") String gitVersion,
    @JsonIgnore String fqdn) {}
