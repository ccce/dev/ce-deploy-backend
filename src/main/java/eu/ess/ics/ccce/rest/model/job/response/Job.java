/*
 * Copyright (C) 2024 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.rest.model.job.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import eu.ess.ics.ccce.rest.model.job.JobAction;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Objects;

/**
 * @author <a href="mailto:domonkos.gulyas@ess.eu">Domonkos Gulyas</a>
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Job extends BaseJob {
  private final String iocName;
  private final Long iocId;
  private final Long deploymentId;
  private final Long gitProjectId;
  private final String gitReference;
  private final DeploymentHostInfo host;
  private final List<JobEntry> jobs;

  private Job(
      Long id,
      String createdBy,
      ZonedDateTime startTime,
      ZonedDateTime createdAt,
      ZonedDateTime finishedAt,
      JobAction action,
      JobStatus status,
      String iocName,
      Long iocId,
      Long deploymentId,
      Long gitProjectId,
      String gitReference,
      DeploymentHostInfo host,
      List<JobEntry> jobs) {
    super(id, createdBy, startTime, createdAt, finishedAt, action, status);
    this.iocName = iocName;
    this.iocId = iocId;
    this.deploymentId = deploymentId;
    this.gitProjectId = gitProjectId;
    this.gitReference = gitReference;
    this.host = host;
    this.jobs = jobs;
  }

  public static Job createSingleJob(
      Long id,
      String createdBy,
      ZonedDateTime startTime,
      ZonedDateTime createdAt,
      ZonedDateTime finishedAt,
      JobAction action,
      String iocName,
      Long iocId,
      Long deploymentId,
      Long gitProjectId,
      String gitReference,
      DeploymentHostInfo host,
      JobStatus status) {
    return new Job(
        id,
        createdBy,
        startTime,
        createdAt,
        finishedAt,
        action,
        status,
        iocName,
        iocId,
        deploymentId,
        gitProjectId,
        gitReference,
        host,
        null);
  }

  public static Job createBatchJob(
      Long id,
      String createdBy,
      ZonedDateTime startTime,
      ZonedDateTime createdAt,
      ZonedDateTime finishedAt,
      JobAction action,
      JobStatus status,
      List<JobEntry> jobs) {
    return new Job(
        id,
        createdBy,
        startTime,
        createdAt,
        finishedAt,
        action,
        status,
        null,
        null,
        null,
        null,
        null,
        null,
        jobs);
  }

  public String getIocName() {
    return iocName;
  }

  public Long getIocId() {
    return iocId;
  }

  public Long getDeploymentId() {
    return deploymentId;
  }

  public Long getGitProjectId() {
    return gitProjectId;
  }

  public String getGitReference() {
    return gitReference;
  }

  public DeploymentHostInfo getHost() {
    return host;
  }

  public List<JobEntry> getJobs() {
    return jobs;
  }

  @Override
  public boolean equals(Object o) {
    if (o == null || getClass() != o.getClass()) return false;
    if (!super.equals(o)) return false;
    Job that = (Job) o;
    return Objects.equals(iocName, that.iocName)
        && Objects.equals(iocId, that.iocId)
        && Objects.equals(deploymentId, that.deploymentId)
        && Objects.equals(gitProjectId, that.gitProjectId)
        && Objects.equals(gitReference, that.gitReference)
        && Objects.equals(host, that.host)
        && Objects.equals(jobs, that.jobs);
  }

  @Override
  public int hashCode() {
    return Objects.hash(
        super.hashCode(), iocName, iocId, deploymentId, gitProjectId, gitReference, host, jobs);
  }
}
