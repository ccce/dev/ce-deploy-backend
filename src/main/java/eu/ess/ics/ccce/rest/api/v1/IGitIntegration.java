/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.rest.api.v1;

import eu.ess.ics.ccce.configuration.WebSecurityConfig;
import eu.ess.ics.ccce.configuration.exception.GeneralException;
import eu.ess.ics.ccce.rest.model.git.request.GitSearchMethodEnum;
import eu.ess.ics.ccce.rest.model.git.response.GitProject;
import eu.ess.ics.ccce.rest.model.git.response.GitReference;
import eu.ess.ics.ccce.rest.model.git.response.ReferenceTypeResponse;
import eu.ess.ics.ccce.rest.model.git.response.UserInfoResponse;
import eu.ess.ics.ccce.service.external.gitlab.model.dto.GitProjectDto;
import eu.ess.ics.ccce.service.internal.security.dto.UserDetails;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@RequestMapping("/api/v1/git_helper")
public interface IGitIntegration {
  @Operation(summary = "List Tags and CommitIds")
  @Tag(name = "Git")
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "401",
            description = "Unauthorized",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "503",
            description = "Git exception",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "500",
            description = "Service exception",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "200",
            description = "List of Tags and CommitIds for a specific GitLab repo",
            content =
                @Content(
                    array = @ArraySchema(schema = @Schema(implementation = GitReference.class))))
      })
  @GetMapping(
      value = "/{project_id}/tags_and_commits",
      produces = {"application/json"})
  List<GitReference> listTagsAndCommitIds(
      @AuthenticationPrincipal UserDetails user,
      @Parameter(in = ParameterIn.PATH, description = "Git repo project ID", required = true)
          @PathVariable("project_id")
          long projectId,
      @Parameter(description = "Git reference") @RequestParam(required = false) String reference,
      @Parameter(
              description =
                  "Search method - optional. Default value: filtering ref by EQUALS method")
          @RequestParam(required = false, name = "search_method")
          GitSearchMethodEnum searchMethod);

  @Operation(
      summary = "List IOC projects",
      security = {@SecurityRequirement(name = "bearerAuth")})
  @Tag(name = "Git")
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "401",
            description = "Unauthorized",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "503",
            description = "Git exception",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "500",
            description = "Service exception",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "200",
            description = "List of Gitlab projects of allowed groups",
            content =
                @Content(array = @ArraySchema(schema = @Schema(implementation = GitProject.class))))
      })
  @GetMapping(
      value = "/projects",
      produces = {"application/json"})
  @PreAuthorize(WebSecurityConfig.IS_ADMIN_OR_INTEGRATOR)
  List<GitProject> listProjects(
      @AuthenticationPrincipal UserDetails user,
      @Parameter(description = "Search text (Search for Git repository name)")
          @RequestParam(required = false)
          String query);

  @Operation(
      summary = "User information",
      security = {@SecurityRequirement(name = "bearerAuth")})
  @Tag(name = "Git")
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "401",
            description = "Unauthorized",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "404",
            description = "Not found",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "503",
            description = "Git exception",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "500",
            description = "Service exception",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "200",
            description = "Information about the current user",
            content = @Content(schema = @Schema(implementation = UserInfoResponse.class)))
      })
  @GetMapping(
      value = "/user_info",
      produces = {"application/json"})
  UserInfoResponse infoFromUserName(
      @AuthenticationPrincipal UserDetails user,
      @Parameter(
              description =
                  "The username to retrieve info from - optional. "
                      + "If no user name provided the backend will look up for currently logged in user name")
          @RequestParam(value = "user_name", required = false)
          String userName);

  @Operation(summary = "Git project detail by repository ID")
  @Tag(name = "Git")
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "404",
            description = "Project not found",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "503",
            description = "Git exception",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "500",
            description = "Service exception",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "200",
            description = "Git project details",
            content = @Content(schema = @Schema(implementation = GitProjectDto.class)))
      })
  @GetMapping(
      value = "/project_info/{project_id}",
      produces = {"application/json"})
  GitProjectDto gitProjectDetails(
      @Parameter(in = ParameterIn.PATH, description = "Git repo project ID", required = true)
          @PathVariable("project_id")
          long projectId);

  @Operation(summary = "Git reference tye")
  @Tag(name = "Git")
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "503",
            description = "Git exception",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "500",
            description = "Service exception",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "200",
            description = "Git reference type",
            content = @Content(schema = @Schema(implementation = ReferenceTypeResponse.class)))
      })
  @GetMapping(
      value = "/{project_id}/reference_type/{git_reference}",
      produces = {"application/json"})
  ReferenceTypeResponse gitReferenceType(
      @Parameter(in = ParameterIn.PATH, description = "Git repo project ID", required = true)
          @PathVariable("project_id")
          long projectId,
      @Parameter(in = ParameterIn.PATH, description = "Git reference", required = true)
          @PathVariable("git_reference")
          String reference);
}
