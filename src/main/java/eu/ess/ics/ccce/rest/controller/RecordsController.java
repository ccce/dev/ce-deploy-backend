/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.rest.controller;

import eu.ess.ics.ccce.exceptions.EntityNotFoundException;
import eu.ess.ics.ccce.exceptions.RemoteException;
import eu.ess.ics.ccce.exceptions.ServiceException;
import eu.ess.ics.ccce.rest.api.v1.IRecords;
import eu.ess.ics.ccce.rest.model.FeatureTag;
import eu.ess.ics.ccce.rest.model.record.PVStatus;
import eu.ess.ics.ccce.rest.model.record.response.PagedRecordResponse;
import eu.ess.ics.ccce.rest.model.record.response.RecordAlertResponse;
import eu.ess.ics.ccce.rest.model.record.response.RecordDetails;
import eu.ess.ics.ccce.service.internal.channel.ChannelService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 */
@RestController
@FeatureTag("records")
public class RecordsController implements IRecords {
  private static final Logger LOGGER = LoggerFactory.getLogger(RecordsController.class);
  private final ChannelService channelService;

  public RecordsController(ChannelService channelService) {
    this.channelService = channelService;
  }

  @Override
  public PagedRecordResponse findAllRecords(
      String text, String iocName, PVStatus pvStatus, Integer page, Integer limit) {
    try {
      return channelService.findAllRecords(text, iocName, pvStatus, page, limit);
    } catch (RemoteException e) {
      LOGGER.error("Error while trying to list all ChannelFinder records", e);
      throw e;
    } catch (Exception e) {
      LOGGER.error("Error while trying to list all records from ChannelFinder", e);
      throw new ServiceException(e.getMessage());
    }
  }

  @Override
  public RecordDetails getRecord(String name) throws EntityNotFoundException {
    try {
      return channelService.findRecord(name);
    } catch (EntityNotFoundException e) {
      LOGGER.error("Error while trying to fetch record details for {}", name, e);
      throw e;
    } catch (RemoteException e) {
      LOGGER.error("Error while trying to fetch ChannelFinder record by name", e);
      throw e;
    } catch (Exception e) {
      LOGGER.error("Error while trying to fetch record by name from ChannelFinder", e);
      throw new ServiceException(e.getMessage());
    }
  }

  @Override
  public RecordAlertResponse fetchRecordAlerts(String recordName) {

    try {
      return channelService.getAlerts(recordName);
    } catch (EntityNotFoundException e) {
      LOGGER.error("Record not found to fetch alerts", e);
      throw e;
    } catch (Exception e) {
      LOGGER.error("Error while trying to fetch Record alerts", e);
      throw new ServiceException("Error while trying to fetch Record alerts");
    }
  }
}
