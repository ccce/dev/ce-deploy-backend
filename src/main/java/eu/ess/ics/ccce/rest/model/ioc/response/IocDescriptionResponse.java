package eu.ess.ics.ccce.rest.model.ioc.response;

/**
 * @author <a href="mailto:domonkos.gulyas@ess.eu">Domonkos Gulyas</a>
 */
public class IocDescriptionResponse {

  private final String description;

  public IocDescriptionResponse(String description) {
    this.description = description;
  }

  public String getDescription() {
    return description;
  }
}
