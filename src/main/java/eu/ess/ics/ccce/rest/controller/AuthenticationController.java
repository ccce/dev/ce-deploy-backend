/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.rest.controller;

import static eu.ess.ics.ccce.common.Constants.COOKIE_AUTH_HEADER;

import eu.ess.ics.ccce.exceptions.*;
import eu.ess.ics.ccce.rest.api.v1.IAuthentication;
import eu.ess.ics.ccce.rest.model.authentication.request.Login;
import eu.ess.ics.ccce.rest.model.authentication.response.LoginResponse;
import eu.ess.ics.ccce.service.internal.security.AuthenticationService;
import eu.ess.ics.ccce.service.internal.security.dto.UserDetails;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseCookie;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 */
@RestController
public class AuthenticationController implements IAuthentication {

  private static final Logger LOGGER = LoggerFactory.getLogger(AuthenticationController.class);

  private final AuthenticationService authenticationService;

  public AuthenticationController(AuthenticationService authenticationService) {
    this.authenticationService = authenticationService;
  }

  @Override
  public ResponseEntity<LoginResponse> login(Login loginInfo) throws GitlabServiceException {
    try {
      LoginResponse login =
          authenticationService.login(loginInfo.getUserName(), loginInfo.getPassword());

      ResponseCookie ccceLoginCookie =
          ResponseCookie.from(COOKIE_AUTH_HEADER, login.getToken())
              .httpOnly(true)
              .secure(true)
              .path("/")
              .build();

      return ResponseEntity.ok()
          .header(HttpHeaders.SET_COOKIE, ccceLoginCookie.toString())
          .body(login);
    } catch (UnauthorizedException e) {
      LOGGER.error("User doesn't have permission to log in", e);
      throw e;
    } catch (AuthenticationException e) {
      LOGGER.error("Error while user tried to log in", e);
      throw e;
    } catch (RemoteException e) {
      LOGGER.error("Remote exception while user tried to log in", e);
      throw e;
    } catch (Exception e) {
      LOGGER.error("Error while trying to log in user", e);
      throw new ServiceException(e.getMessage());
    }
  }

  @Override
  public ResponseEntity<LoginResponse> tokenRenew(UserDetails user) {
    try {
      LoginResponse renew = authenticationService.renewToken(user);

      ResponseCookie ccceRenewCookie =
          ResponseCookie.from(COOKIE_AUTH_HEADER, renew.getToken())
              .httpOnly(true)
              .secure(true)
              .path("/")
              .build();

      return ResponseEntity.ok()
          .header(HttpHeaders.SET_COOKIE, ccceRenewCookie.toString())
          .body(renew);
    } catch (UnauthorizedException e) {
      LOGGER.error("Authorization error while trying to renew token", e);
      throw e;
    } catch (RemoteException e) {
      LOGGER.error("Remote exception while trying to renew token", e);
      throw e;
    } catch (Exception e) {
      LOGGER.error("Error while trying to renew token", e);
      throw new ServiceException(e.getMessage());
    }
  }

  @Override
  public List<String> getUserRoles(UserDetails user) {
    try {
      return authenticationService.getUserRoles(user);
    } catch (UnauthorizedException e) {
      LOGGER.error("Authorization error while trying to get user roles");
      throw e;
    } catch (RemoteException e) {
      LOGGER.error("Error while trying to get user roles from RBAC", e);
      throw e;
    } catch (Exception e) {
      LOGGER.error("Error while trying to retrieve user roles", e);
      throw new ServiceException(e.getMessage());
    }
  }

  @Override
  public ResponseEntity<Object> logout(UserDetails user) {

    try {
      authenticationService.logout(user);

      ResponseCookie deleteSpringCookie =
          ResponseCookie.from(COOKIE_AUTH_HEADER, null)
              .path("/")
              .httpOnly(true)
              .secure(true)
              .build();

      return ResponseEntity.ok()
          .header(HttpHeaders.SET_COOKIE, deleteSpringCookie.toString())
          .build();
    } catch (ServiceException | RemoteException e) {
      LOGGER.error(e.getMessage(), e);
      throw e;
    } catch (Exception e) {
      LOGGER.error("Error while trying to logout user", e);
      throw new ServiceException(e.getMessage());
    }
  }

  @Override
  public List<String> getToolUsers(UserDetails user, String query) {
    try {
      return authenticationService.getUsers(user, query);
    } catch (EntityNotFoundException e) {
      LOGGER.error(e.getMessage(), e);
      throw e;
    } catch (RemoteException e) {
      LOGGER.error("Error while trying to fetch tool users from RBAC", e);
      throw e;
    } catch (Exception e) {
      LOGGER.error("Error while trying to retrieve user list", e);
      throw new ServiceException(e.getMessage());
    }
  }
}
