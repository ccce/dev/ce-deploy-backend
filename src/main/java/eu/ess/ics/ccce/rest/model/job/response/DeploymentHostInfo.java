/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.rest.model.job.response;

import java.util.Objects;

/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 */
public class DeploymentHostInfo {
  private final String hostId;
  private final String hostName;
  private final String network;
  private final String fqdn;
  private static final boolean netBoxHostFromCache = true;

  public DeploymentHostInfo(String hostId, String hostName, String network, String fqdn) {
    this.hostId = hostId;
    this.hostName = hostName;
    this.network = network;
    this.fqdn = fqdn;
  }

  public String getHostId() {
    return hostId;
  }

  public String getHostName() {
    return hostName;
  }

  public String getNetwork() {
    return network;
  }

  public boolean isNetBoxHostFromCache() {
    return netBoxHostFromCache;
  }

  public String getFqdn() {
    return fqdn;
  }

  @Override
  public boolean equals(Object o) {
    if (o == null || getClass() != o.getClass()) return false;
    DeploymentHostInfo that = (DeploymentHostInfo) o;
    return Objects.equals(hostId, that.hostId)
        && Objects.equals(hostName, that.hostName)
        && Objects.equals(network, that.network)
        && Objects.equals(fqdn, that.fqdn);
  }

  @Override
  public int hashCode() {
    return Objects.hash(hostId, hostName, network, fqdn);
  }

  @Override
  public String toString() {
    return "DeploymentHostInfo{"
        + "hostId='"
        + hostId
        + '\''
        + ", hostName='"
        + hostName
        + '\''
        + ", network='"
        + network
        + '\''
        + ", fqdn='"
        + fqdn
        + '\''
        + '}';
  }
}
