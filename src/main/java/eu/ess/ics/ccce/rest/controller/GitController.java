/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.rest.controller;

import eu.ess.ics.ccce.exceptions.EntityNotFoundException;
import eu.ess.ics.ccce.exceptions.GitlabServiceException;
import eu.ess.ics.ccce.exceptions.ServiceException;
import eu.ess.ics.ccce.rest.api.v1.IGitIntegration;
import eu.ess.ics.ccce.rest.model.FeatureTag;
import eu.ess.ics.ccce.rest.model.git.request.GitSearchMethodEnum;
import eu.ess.ics.ccce.rest.model.git.response.GitProject;
import eu.ess.ics.ccce.rest.model.git.response.GitReference;
import eu.ess.ics.ccce.rest.model.git.response.ReferenceTypeResponse;
import eu.ess.ics.ccce.rest.model.git.response.UserInfoResponse;
import eu.ess.ics.ccce.service.external.gitlab.GitLabService;
import eu.ess.ics.ccce.service.external.gitlab.model.dto.GitProjectDto;
import eu.ess.ics.ccce.service.internal.security.UserService;
import eu.ess.ics.ccce.service.internal.security.dto.UserDetails;
import java.io.FileNotFoundException;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 */
@RestController
@FeatureTag("git")
public class GitController implements IGitIntegration {

  private static final Logger LOGGER = LoggerFactory.getLogger(GitController.class);

  private final GitLabService gitLabService;
  private final UserService userService;

  public GitController(GitLabService gitLabService, UserService userService) {
    this.gitLabService = gitLabService;
    this.userService = userService;
  }

  @Override
  public List<GitReference> listTagsAndCommitIds(
      UserDetails user, long projectId, String reference, GitSearchMethodEnum searchMethod) {
    try {
      return gitLabService.tagsAndCommitIds(
          projectId, reference, searchMethod == null ? GitSearchMethodEnum.EQUALS : searchMethod);
    } catch (GitlabServiceException e) {
      LOGGER.error(
          "GitException while trying to retrieve git tags and commits (project ID: "
              + projectId
              + ", ref: "
              + reference
              + ")",
          e);
      throw e;
    } catch (Exception e) {
      LOGGER.error(
          "Error while trying to retrieve git tags and commits (project ID: "
              + projectId
              + ", ref: "
              + reference
              + ")",
          e);
      throw new ServiceException(e.getMessage());
    }
  }

  @Override
  public List<GitProject> listProjects(UserDetails user, String query) {
    try {
      return gitLabService.projectsFromAllowedGroup(query);
    } catch (GitlabServiceException e) {
      LOGGER.error("GitException while trying to retrieve all git projects", e);
      throw e;
    } catch (Exception e) {
      LOGGER.error("Error while trying to retrieve all git projects", e);
      throw new ServiceException(e.getMessage());
    }
  }

  @Override
  public UserInfoResponse infoFromUserName(UserDetails user, String userName) {

    try {
      // no userName parameter -> get info for current user
      if (StringUtils.isEmpty(userName)) {
        // get the active user by userName from GitLab
        UserInfoResponse response = gitLabService.currentUserInfo(user.getUserName());

        // fallback: if no data provided by GitLab -> request RBAC for user info
        if ((StringUtils.isEmpty(response.getFullName()))
            && (StringUtils.isEmpty(response.getLoginName()))) {
          UserDetails userInfo = userService.getUserInfoFromToken(user.getToken());
          response.setFullName(userInfo.getFullName());
          response.setLoginName(userInfo.getUserName());
        }

        // returning current user info (single result)
        return response;
      }
      List<UserInfoResponse> gitlabResponse = gitLabService.infoFromUserName(userName);

      if (gitlabResponse.isEmpty()) {
        throw new EntityNotFoundException("GitLab user not found for name: " + userName);
      }

      return gitlabResponse.get(0);

    } catch (EntityNotFoundException e) {
      LOGGER.error("Gitlab user not found for name: " + userName, e);
      throw e;
    } catch (GitlabServiceException e) {
      LOGGER.error("Error while trying to retrieve info from username from GitLab", e);
      throw e;
    } catch (Exception e) {
      LOGGER.error("Error while trying to retrieve info from username from GitLab", e);
      throw new ServiceException("Error while trying to retrieve info from username from GitLab");
    }
  }

  @Override
  public GitProjectDto gitProjectDetails(long projectId) {
    try {
      return gitLabService.projectDetails(projectId);
    } catch (FileNotFoundException e) {
      LOGGER.error("Gitlab project not found by ID: " + projectId, e);
      throw new EntityNotFoundException("Failed to fetch gitlab project details");
    } catch (GitlabServiceException e) {
      LOGGER.error("Error while trying to retrieve project details from GitLab by ID", e);
      throw e;
    } catch (Exception e) {
      LOGGER.error("Error while trying to retrieve project details from GitLab By ID", e);
      throw new ServiceException("Error while trying to retrieve project details from GitLab");
    }
  }

  @Override
  public ReferenceTypeResponse gitReferenceType(long projectId, String reference) {
    try {
      return gitLabService.getReferenceType(projectId, reference);
    } catch (GitlabServiceException e) {
      LOGGER.error("Error while trying to retrieve git reference type from GitLab", e);
      throw e;
    } catch (Exception e) {
      LOGGER.error("Error while trying to retrieve git reference type from GitLab", e);
      throw new ServiceException("Error while trying to retrieve git reference type from GitLab");
    }
  }
}
