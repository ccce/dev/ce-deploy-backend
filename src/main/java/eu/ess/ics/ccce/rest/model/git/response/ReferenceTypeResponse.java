package eu.ess.ics.ccce.rest.model.git.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public record ReferenceTypeResponse(@JsonProperty("reference_type") ReferenceType referenceType) {}
