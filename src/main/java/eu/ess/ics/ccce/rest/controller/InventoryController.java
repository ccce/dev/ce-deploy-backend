package eu.ess.ics.ccce.rest.controller;

import eu.ess.ics.ccce.exceptions.ServiceException;
import eu.ess.ics.ccce.rest.api.v1.IInventory;
import eu.ess.ics.ccce.rest.model.FeatureTag;
import eu.ess.ics.ccce.rest.model.inventory.response.InventoryResponse;
import eu.ess.ics.ccce.service.internal.inventory.InventoryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 */
@RestController
@FeatureTag("inventory")
public class InventoryController implements IInventory {

  private static final Logger LOGGER = LoggerFactory.getLogger(InventoryController.class);

  private final InventoryService inventoryService;

  public InventoryController(InventoryService inventoryService) {
    this.inventoryService = inventoryService;
  }

  @Override
  public InventoryResponse fetchInventory() {
    try {
      LOGGER.debug("Trying to calculate inventory");

      return inventoryService.getInventory();
    } catch (Exception e) {
      LOGGER.error("Error while trying to calculate inventory", e);
      throw new ServiceException("Error while trying to calculate inventory");
    }
  }
}
