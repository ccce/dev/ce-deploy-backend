/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.rest.api.v1;

import eu.ess.ics.ccce.configuration.exception.GeneralException;
import eu.ess.ics.ccce.exceptions.EntityNotFoundException;
import eu.ess.ics.ccce.rest.model.record.PVStatus;
import eu.ess.ics.ccce.rest.model.record.response.PagedRecordResponse;
import eu.ess.ics.ccce.rest.model.record.response.RecordAlertResponse;
import eu.ess.ics.ccce.rest.model.record.response.RecordDetails;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 */
@RequestMapping("/api/v1/records")
@Tag(name = "Records")
public interface IRecords {
  @Operation(summary = "List all ChannelFinder records")
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "200",
            description = "A paged array of Channel Finder records",
            content = @Content(schema = @Schema(implementation = PagedRecordResponse.class))),
        @ApiResponse(
            responseCode = "503",
            description = "Remote server exception",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "500",
            description = "Service exception",
            content = @Content(schema = @Schema(implementation = GeneralException.class)))
      })
  @GetMapping(produces = {"application/json"})
  PagedRecordResponse findAllRecords(
      @Parameter(description = "Search text (Search for record name or description)")
          @RequestParam(required = false)
          String text,
      @Parameter(description = "Part of the IOC name that has to be searched")
          @RequestParam(required = false, name = "ioc_name")
          String iocName,
      @Parameter(description = "PV status") @RequestParam(required = false, name = "pv_status")
          PVStatus pvStatus,
      @Parameter(description = "Page offset") @RequestParam(required = false) Integer page,
      @Parameter(description = "Page size", schema = @Schema(maximum = "100"))
          @RequestParam(required = false)
          Integer limit);

  @Operation(summary = "Find ChannelFinder record details by record name")
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "404",
            description = "Record not found",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "500",
            description = "Service exception",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "200",
            description = "Found record",
            content = @Content(schema = @Schema(implementation = RecordDetails.class)))
      })
  @GetMapping(
      value = "/{name}",
      produces = {"application/json"})
  RecordDetails getRecord(
      @Parameter(
              in = ParameterIn.PATH,
              description = "Unique (part of the) name of the record",
              required = true)
          @PathVariable
          String name)
      throws EntityNotFoundException;

  @Operation(summary = "Fetches alerts of the Record")
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "200",
            description = "Alerts of the Record",
            content = @Content(schema = @Schema(implementation = RecordAlertResponse.class))),
        @ApiResponse(
            responseCode = "404",
            description = "Record not found",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "500",
            description = "Service exception",
            content =
                @Content(
                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = @Schema(implementation = GeneralException.class)))
      })
  @GetMapping(
      value = "/{record_name}/alerts",
      produces = {"application/json"})
  RecordAlertResponse fetchRecordAlerts(
      @Parameter(in = ParameterIn.PATH, description = "Name of the Record", required = true)
          @PathVariable("record_name")
          String recordName);
}
