/*
 * Copyright (C) 2024 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.rest.controller;

import eu.ess.ics.ccce.exceptions.*;
import eu.ess.ics.ccce.rest.api.v1.IMaintenance;
import eu.ess.ics.ccce.rest.model.maintenance.request.SetMaintenance;
import eu.ess.ics.ccce.rest.model.maintenance.response.MaintenanceMode;
import eu.ess.ics.ccce.service.internal.maintenance.MaintenanceService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author <a href="mailto:sky.brewer@ess.eu">Sky Brewer</a>
 */
@RestController
public class MaintenanceController implements IMaintenance {
  private static final Logger LOGGER = LoggerFactory.getLogger(MaintenanceController.class);
  private final MaintenanceService maintenanceService;

  public MaintenanceController(final MaintenanceService maintenanceService) {
    this.maintenanceService = maintenanceService;
  }

  @Override
  public MaintenanceMode getCurrentMode() {
    try {
      LOGGER.debug("Trying to get current maintenance mode");
      return maintenanceService.getCurrentMaintenanceMode();
    } catch (EntityNotFoundException e) {
      throw e;
    } catch (Exception e) {
      LOGGER.error("Error during getCurrentMode: " + e.getMessage(), e);
      throw new ServiceException(e);
    }
  }

  @Override
  public MaintenanceMode setMaintenanceMode(SetMaintenance body) {
    LOGGER.debug("Trying to set new Maintenance mode");
    checkModeBody(body);

    try {
      return maintenanceService.setMaintenanceMode(body);
    } catch (EntityNotFoundException e) {
      LOGGER.error(e.getMessage(), e);
      throw new UnprocessableEntityException(e.getMessage());
    } catch (DataIntegrityViolationException e) {
      LOGGER.error(e.getMessage(), e);
      throw transformDataIntegrityException(e);
    } catch (UnauthorizedException
        | IncompleteRequestException
        | RemoteException
        | ConflictException e) {
      LOGGER.error(e.getMessage(), e);
      throw e;
    } catch (Exception e) {
      LOGGER.error(e.getMessage(), e);
      throw new ServiceException("Error while trying to set the Maintenance Mode", e);
    }
  }

  private ConstraintViolationException transformDataIntegrityException(
      DataIntegrityViolationException e) {
    return new ConstraintViolationException(
        "Error while trying to set Maintenance Mode: database constraint violation - "
            + e.getMessage());
  }

  private void checkModeBody(SetMaintenance body) {
    if (body.startAt() == null) {
      throw new IncompleteRequestException("Valid 'start at' is required");
    }
    if (StringUtils.isEmpty(body.message())) {
      throw new IncompleteRequestException("Valid message is required");
    }
  }

  @Override
  public void endCurrentMaintenanceMode() {
    LOGGER.debug("Trying to end the current Maintenance Mode");
    try {
      maintenanceService.endMaintenanceMode();
    } catch (EntityNotFoundException e) {
      LOGGER.error("Not currently in Maintenance mode", e);
      throw e;
    } catch (Exception e) {
      LOGGER.error("Error while trying to modify Maintenance end mode", e);
      throw new ServiceException("Error while trying to end Maintenance mode");
    }
  }
}
