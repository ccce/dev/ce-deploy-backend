/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.rest.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import eu.ess.ics.ccce.exceptions.*;
import eu.ess.ics.ccce.rest.api.v1.IJobs;
import eu.ess.ics.ccce.rest.model.BatchType;
import eu.ess.ics.ccce.rest.model.FeatureTag;
import eu.ess.ics.ccce.rest.model.job.request.*;
import eu.ess.ics.ccce.rest.model.job.request.DeployJobRequest;
import eu.ess.ics.ccce.rest.model.job.response.*;
import eu.ess.ics.ccce.service.internal.deployment.DeploymentService;
import eu.ess.ics.ccce.service.internal.ioc.IocService;
import eu.ess.ics.ccce.service.internal.security.dto.UserDetails;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 */
@RestController
@FeatureTag("jobs")
public class JobsController implements IJobs {

  private static final Logger LOGGER = LoggerFactory.getLogger(JobsController.class);

  private final DeploymentService deploymentService;
  private final IocService iocService;

  public JobsController(DeploymentService deploymentService, IocService iocService) {
    this.deploymentService = deploymentService;
    this.iocService = iocService;
  }

  @Override
  public AwxJobDetails fetchAwxJobStatus(UserDetails user, long jobId) {
    try {
      return deploymentService.fetchAwxJobStatus(jobId);
    } catch (EntityNotFoundException e) {
      LOGGER.error(e.getMessage(), e);
      throw e;
    } catch (RemoteException e) {
      LOGGER.error("Remote service exception while trying to fetch Job status", e);
      throw e;
    } catch (Exception e) {
      LOGGER.error("Error during fetchJobStatus: {}", e.getMessage(), e);
      throw new ServiceException(e);
    }
  }

  @Override
  public AwxJobLog fetchAwxJobLog(UserDetails user, long jobId) {
    try {
      return deploymentService.fetchAwxJobLog(jobId);
    } catch (EntityNotFoundException e) {
      LOGGER.error(e.getMessage(), e);
      throw e;
    } catch (RemoteException e) {
      LOGGER.error("Remote service exception while trying to fetch AWX job log", e);
      throw e;
    } catch (Exception e) {
      LOGGER.error("Error during fetchDeploymentJobLog: " + e.getMessage(), e);
      throw new ServiceException(e);
    }
  }

  @Override
  public PagedJobResponse listJobs(
      UserDetails userDetails,
      Long iocId,
      JobKind type,
      JobStatusQuery status,
      String user,
      String query,
      LocalDateTime startDate,
      LocalDateTime endDate,
      String timeZone,
      String hostId,
      JobOrder orderBy,
      Boolean isAsc,
      Integer page,
      Integer limit) {
    try {
      return deploymentService.findAllJobs(
          iocId, type, status, user, query, startDate, endDate, timeZone, hostId, orderBy, isAsc,
          page, limit);
    } catch (InputValidationException e) {
      LOGGER.error("Input validation failed while trying to fetch Job list", e);
      throw e;
    } catch (Exception e) {
      LOGGER.error("Error while trying to fetch all Jobs", e);
      throw new ServiceException(e);
    }
  }

  @Override
  public JobDetails fetchJob(UserDetails user, long jobId) throws EntityNotFoundException {
    try {
      return deploymentService.findJobById(jobId);
    } catch (EmptyResultDataAccessException e) {
      LOGGER.error("Job not found for fetching details", e);
      throw new EntityNotFoundException("Job", jobId);
    } catch (Exception e) {
      LOGGER.error("Error while trying to get Job details", e);
      throw new ServiceException(e);
    }
  }

  @Override
  public Job iocStartJob(long iocId, UserDetails user) {
    return iocService.startIoc(user, iocId);
  }

  @Override
  public Job iocStopJob(long iocId, UserDetails user) {
    return iocService.stopIoc(user, iocId);
  }

  @Override
  public Job iocDeployJob(long iocId, DeployJobRequest request, UserDetails user) {
    return iocService.updateAndDeployIoc(user, iocId, request.hostId(), request.sourceVersion());
  }

  @Override
  public Job iocUndeployJob(long iocId, UserDetails user) {
    return iocService.createUndeployment(user, iocId);
  }

  @Override
  public Job startBatchDeployment(BatchDeploymentRequest request, UserDetails user) {
    try {
      return iocService.createBatchDeployment(user, request.deployments(), BatchType.DEPLOY);
    } catch (MetaDataFileException | JsonProcessingException e) {
      LOGGER.error("Error while trying to process metadata file content " + e.getMessage(), e);
      throw new MetaDataFileException(e.getMessage());
    } catch (EntityNotFoundException e) {
      LOGGER.error(e.getMessage(), e);
      throw new UnprocessableEntityException(e.getMessage());
    } catch (NetBoxServiceException
        | LimitExceededException
        | UnauthorizedException
        | AwxServiceException
        | OperationNotAllowedException
        | IncompleteRequestException
        | GitlabServiceException e) {
      LOGGER.error(e.getMessage(), e);
      throw e;
    } catch (Exception e) {
      LOGGER.error("Error while trying to update and deploy multiple IOCs", e);
      throw new ServiceException("Error while trying to update and deploy multiple IOCs", e);
    }
  }

  @Override
  public Job startBatchUndeployment(BatchUndeploymentRequest request, UserDetails user) {
    try {
      return iocService.createBatchDeployment(
          user, toDeploymentRequests(request.undeployments()), BatchType.UNDEPLOY);
    } catch (MetaDataFileException | JsonProcessingException e) {
      LOGGER.error("Error while trying to process metadata file content " + e.getMessage(), e);
      throw new MetaDataFileException(e.getMessage());
    } catch (EntityNotFoundException e) {
      LOGGER.error(e.getMessage(), e);
      throw new UnprocessableEntityException(e.getMessage());
    } catch (NetBoxServiceException
        | LimitExceededException
        | UnauthorizedException
        | AwxServiceException
        | OperationNotAllowedException
        | IncompleteRequestException
        | GitlabServiceException e) {
      LOGGER.error(e.getMessage(), e);
      throw e;
    } catch (Exception e) {
      LOGGER.error("Error while trying to update and undeploy multiple IOCs", e);
      throw new ServiceException("Error while trying to update and undeploy multiple IOCs", e);
    }
  }

  private static List<DeploymentRequest> toDeploymentRequests(
      List<UndeploymentRequest> undeploymentRequests) {
    return undeploymentRequests.stream()
        .map(
            undeploymentRequest ->
                new DeploymentRequest(
                    undeploymentRequest.iocId(), null, undeploymentRequest.hostId()))
        .collect(Collectors.toList());
  }
}
