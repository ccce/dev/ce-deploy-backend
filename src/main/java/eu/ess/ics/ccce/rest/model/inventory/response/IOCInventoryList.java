package eu.ess.ics.ccce.rest.model.inventory.response;

import java.util.List;

public record IOCInventoryList(List<IOCInventory> iocs) {}
