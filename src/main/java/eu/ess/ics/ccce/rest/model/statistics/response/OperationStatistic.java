package eu.ess.ics.ccce.rest.model.statistics.response;

import java.util.Date;

/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 */
public class OperationStatistic {
  private Long deploymentCount;
  private Long commandCount;
  private Date historyDate;

  public OperationStatistic(Long deploymentCount, Long commandCount, Date historyDate) {
    this.deploymentCount = deploymentCount;
    this.commandCount = commandCount;
    this.historyDate = historyDate;
  }

  public Long getDeploymentCount() {
    return deploymentCount;
  }

  public Long getCommandCount() {
    return commandCount;
  }

  public Date getHistoryDate() {
    return historyDate;
  }
}
