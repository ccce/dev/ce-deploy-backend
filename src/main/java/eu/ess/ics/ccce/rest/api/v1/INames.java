/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.rest.api.v1;

import eu.ess.ics.ccce.configuration.WebSecurityConfig;
import eu.ess.ics.ccce.configuration.exception.GeneralException;
import eu.ess.ics.ccce.exceptions.GitlabServiceException;
import eu.ess.ics.ccce.rest.model.job.response.IdUsedByIoc;
import eu.ess.ics.ccce.rest.model.naming.response.NameResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import java.util.List;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@RequestMapping("/api/v1/names")
public interface INames {

  @SecurityRequirement(name = "bearerAuth")
  @Operation(summary = "Fetches IOC names by name from the Names service")
  @Tag(name = "Names")
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "200",
            description = "Naming names, and UUIDs from the Names service",
            content =
                @Content(
                    array = @ArraySchema(schema = @Schema(implementation = NameResponse.class)))),
        @ApiResponse(
            responseCode = "401",
            description = "Unauthorized",
            content =
                @Content(
                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "500",
            description = "Service exception",
            content =
                @Content(
                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "503",
            description = "Remote service unreachable",
            content =
                @Content(
                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = @Schema(implementation = GeneralException.class)))
      })
  @GetMapping(
      value = "/ioc_names_by_name",
      produces = {"application/json"})
  @PreAuthorize(WebSecurityConfig.IS_ADMIN_OR_INTEGRATOR)
  List<NameResponse> fetchIOCByName(
      @Parameter(description = "IOC name") @RequestParam(required = false, name = "ioc_name")
          String iocName);

  @ApiResponses({
    @ApiResponse(
        responseCode = "200",
        description = "Ok",
        content = @Content(schema = @Schema(implementation = IdUsedByIoc.class))),
    @ApiResponse(
        responseCode = "422",
        description = "Too many IOCs use the same Naming UUID",
        content = @Content(schema = @Schema(implementation = GeneralException.class))),
    @ApiResponse(
        responseCode = "500",
        description = "Service exception",
        content = @Content(schema = @Schema(implementation = GeneralException.class)))
  })
  @Operation(
      summary = "Checks Naming UUID",
      description =
          """
          Checks Naming UUID if it is in use by IOC.\s

          Searching is based on EXACT match!\s

          If no IOC uses the Naming UUID then the iocId will be NULL""")
  @Tag(name = "Names")
  @GetMapping(
      value = "/check_naming_uuid/{naming_uuid}",
      produces = {"application/json"})
  IdUsedByIoc checkNamingUUId(
      @Parameter(in = ParameterIn.PATH, description = "Naming UUID", required = true)
          @PathVariable("naming_uuid")
          String namingUUId)
      throws GitlabServiceException;
}
