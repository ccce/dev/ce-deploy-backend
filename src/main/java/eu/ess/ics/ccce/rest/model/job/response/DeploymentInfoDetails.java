/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.rest.model.job.response;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import eu.ess.ics.ccce.common.conversion.ZonedDateTimeSerializer;
import eu.ess.ics.ccce.rest.model.Alert;
import java.time.ZonedDateTime;
import java.util.List;

/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 */
public class DeploymentInfoDetails extends BaseDeploymentInfo {

  private final @JsonSerialize(using = ZonedDateTimeSerializer.class) ZonedDateTime endDate;
  private final HostWithFqdn host;
  private final Long jobId;
  private final Long iocId;
  private final String jobUrl;
  private final String sourceUrl;
  private final String sourceVersionShort;
  private final List<Alert> alerts;

  public DeploymentInfoDetails(
      Long id,
      String createdBy,
      ZonedDateTime createdAt,
      ZonedDateTime startDate,
      ZonedDateTime endDate,
      Long awxJobId,
      HostWithFqdn host,
      boolean undeployment,
      String namingName,
      Long gitProjectId,
      String sourceUrl,
      String sourceVersion,
      String sourceVersionShort,
      Long iocId,
      String jobUrl,
      List<Alert> alerts,
      Long jobId) {
    super(
        id,
        createdBy,
        createdAt,
        startDate,
        undeployment,
        namingName,
        gitProjectId,
        sourceVersion,
        jobId);

    this.endDate = endDate;
    this.jobId = awxJobId;
    this.host = host;
    this.iocId = iocId;
    this.jobUrl = jobUrl;
    this.sourceUrl = sourceUrl;
    this.sourceVersionShort = sourceVersionShort;
    this.alerts = alerts;
  }

  public ZonedDateTime getEndDate() {
    return endDate;
  }

  public Long getJobId() {
    return jobId;
  }

  public HostWithFqdn getHost() {
    return host;
  }

  public Long getIocId() {
    return iocId;
  }

  public String getJobUrl() {
    return jobUrl;
  }

  public String getSourceUrl() {
    return sourceUrl;
  }

  public String getSourceVersionShort() {
    return sourceVersionShort;
  }

  public List<Alert> getAlerts() {
    return alerts;
  }
}
