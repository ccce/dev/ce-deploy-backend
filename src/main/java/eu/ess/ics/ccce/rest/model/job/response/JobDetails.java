/*
 * Copyright (C) 2024 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.rest.model.job.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import eu.ess.ics.ccce.rest.model.Alert;
import eu.ess.ics.ccce.rest.model.job.JobAction;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Objects;

/**
 * @author <a href="mailto:domonkos.gulyas@ess.eu">Domonkos Gulyas</a>
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class JobDetails extends BaseJob {
  private final String iocName;
  private final Long iocId;
  private final Long deploymentId;
  private final Long gitProjectId;
  private final String gitReference;
  private final String gitProjectUrl;
  private final String gitShortReference;
  private final HostWithFqdn host;
  private final Long awxJobId;
  private final String jobUrl;

  private final List<Alert> alerts;
  private final List<JobDetailsEntry> jobs;

  private JobDetails(
      Long id,
      String createdBy,
      ZonedDateTime startTime,
      ZonedDateTime createdAt,
      ZonedDateTime finishedAt,
      JobAction action,
      JobStatus status,
      String iocName,
      Long iocId,
      Long deploymentId,
      Long gitProjectId,
      String gitReference,
      String gitProjectUrl,
      String gitShortReference,
      HostWithFqdn host,
      Long awxJobId,
      String jobUrl,
      List<Alert> alerts,
      List<JobDetailsEntry> jobs) {
    super(id, createdBy, startTime, createdAt, finishedAt, action, status);
    this.iocName = iocName;
    this.iocId = iocId;
    this.deploymentId = deploymentId;
    this.gitProjectId = gitProjectId;
    this.gitReference = gitReference;
    this.gitProjectUrl = gitProjectUrl;
    this.gitShortReference = gitShortReference;
    this.host = host;
    this.awxJobId = awxJobId;
    this.jobUrl = jobUrl;
    this.alerts = alerts;
    this.jobs = jobs;
  }

  public static JobDetails createSingleJobDetails(
      Long id,
      String createdBy,
      ZonedDateTime startTime,
      ZonedDateTime createdAt,
      ZonedDateTime finishedAt,
      JobAction action,
      String iocName,
      Long iocId,
      Long deploymentId,
      Long gitProjectId,
      String gitReference,
      JobStatus status,
      String gitProjectUrl,
      String gitShortReference,
      HostWithFqdn host,
      Long awxJobId,
      String jobUrl,
      List<Alert> alerts) {
    return new JobDetails(
        id,
        createdBy,
        startTime,
        createdAt,
        finishedAt,
        action,
        status,
        iocName,
        iocId,
        deploymentId,
        gitProjectId,
        gitReference,
        gitProjectUrl,
        gitShortReference,
        host,
        awxJobId,
        jobUrl,
        alerts,
        null);
  }

  public static JobDetails createBatchJobDetails(
      Long id,
      String createdBy,
      ZonedDateTime startTime,
      ZonedDateTime createdAt,
      ZonedDateTime finishedAt,
      JobAction action,
      JobStatus status,
      String jobUrl,
      Long awxJobId,
      List<JobDetailsEntry> jobs) {
    return new JobDetails(
        id,
        createdBy,
        startTime,
        createdAt,
        finishedAt,
        action,
        status,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        awxJobId,
        jobUrl,
        null,
        jobs);
  }

  public String getIocName() {
    return iocName;
  }

  public Long getIocId() {
    return iocId;
  }

  public Long getDeploymentId() {
    return deploymentId;
  }

  public Long getGitProjectId() {
    return gitProjectId;
  }

  public String getGitReference() {
    return gitReference;
  }

  public String getGitProjectUrl() {
    return gitProjectUrl;
  }

  public String getGitShortReference() {
    return gitShortReference;
  }

  public HostWithFqdn getHost() {
    return host;
  }

  public Long getAwxJobId() {
    return awxJobId;
  }

  public String getJobUrl() {
    return jobUrl;
  }

  public List<Alert> getAlerts() {
    return alerts;
  }

  public List<JobDetailsEntry> getJobs() {
    return jobs;
  }

  @Override
  public boolean equals(Object o) {
    if (o == null || getClass() != o.getClass()) return false;
    if (!super.equals(o)) return false;
    JobDetails that = (JobDetails) o;
    return Objects.equals(iocName, that.iocName)
        && Objects.equals(iocId, that.iocId)
        && Objects.equals(deploymentId, that.deploymentId)
        && Objects.equals(gitProjectId, that.gitProjectId)
        && Objects.equals(gitReference, that.gitReference)
        && Objects.equals(gitProjectUrl, that.gitProjectUrl)
        && Objects.equals(gitShortReference, that.gitShortReference)
        && Objects.equals(host, that.host)
        && Objects.equals(awxJobId, that.awxJobId)
        && Objects.equals(jobUrl, that.jobUrl)
        && Objects.equals(alerts, that.alerts)
        && Objects.equals(jobs, that.jobs);
  }

  @Override
  public int hashCode() {
    return Objects.hash(
        super.hashCode(),
        iocName,
        iocId,
        deploymentId,
        gitProjectId,
        gitReference,
        gitProjectUrl,
        gitShortReference,
        host,
        awxJobId,
        jobUrl,
        alerts,
        jobs);
  }
}
