/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.rest.controller;

import eu.ess.ics.ccce.exceptions.*;
import eu.ess.ics.ccce.rest.api.v1.IHost;
import eu.ess.ics.ccce.rest.model.FeatureTag;
import eu.ess.ics.ccce.rest.model.host.request.HostIocFilter;
import eu.ess.ics.ccce.rest.model.host.response.*;
import eu.ess.ics.ccce.rest.model.host.response.HostInfoWithId;
import eu.ess.ics.ccce.rest.model.host.response.PagedNetBoxHostResponse;
import eu.ess.ics.ccce.rest.model.ioc.request.IocOrder;
import eu.ess.ics.ccce.rest.model.loki.response.LokiResponse;
import eu.ess.ics.ccce.service.external.loki.LokiService;
import eu.ess.ics.ccce.service.internal.host.HostService;
import eu.ess.ics.ccce.service.internal.ioc.IocService;
import eu.ess.ics.ccce.service.internal.security.dto.UserDetails;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 */
@RestController
@FeatureTag("host")
public class HostController implements IHost {
  private static final Logger LOGGER = LoggerFactory.getLogger(HostController.class);

  private final HostService hostService;
  private final IocService iocService;
  private final LokiService lokiService;

  public HostController(HostService hostService, IocService iocService, LokiService lokiService) {
    this.hostService = hostService;
    this.iocService = iocService;
    this.lokiService = lokiService;
  }

  @Override
  public void checkHostExists(String hostId) {
    try {
      hostService.checkHostExists(hostId);
    } catch (EntityNotFoundException e) {
      throw e;
    } catch (InputValidationException | IllegalArgumentException e) {
      LOGGER.error("Host check failed, invalid arguments", e);
      throw new EntityNotFoundException("Host not found");
    } catch (RemoteException e) {
      LOGGER.error("Error while trying check if host exists", e);
      throw e;
    } catch (Exception e) {
      LOGGER.error("Error while trying check if host exists from NetBox", e);
      throw new ServiceException(e.getMessage());
    }
  }

  @Override
  public PagedNetBoxHostResponse listHosts(
      UserDetails user,
      String text,
      HostIocFilter filter,
      Integer page,
      Integer limit,
      boolean listAll) {
    try {
      return hostService.findAllFromNetBoxPaged(text, filter, page, limit, user);
    } catch (RemoteException e) {
      LOGGER.error("Error while trying to list all hosts", e);
      throw e;
    } catch (Exception e) {
      LOGGER.error("Error while trying to list all hosts from NetBox", e);
      throw new ServiceException(e.getMessage());
    }
  }

  @Override
  public HostStatusResponse findHostStatus(String hostId) {
    try {
      return hostService.findHostStatus(hostId);
    } catch (EntityNotFoundException e) {
      LOGGER.error(e.getMessage(), e);
      throw e;
    } catch (InputValidationException e) {
      LOGGER.error("Host ID couldn't be found to fetch status", e);
      throw new EntityNotFoundException("Host not found");
    } catch (RemoteException e) {
      LOGGER.error("Error while trying to fetch host status by ID", e);
      throw e;
    } catch (Exception e) {
      LOGGER.error("Error while trying to fetch host status from NetBox by ID", e);
      throw new ServiceException(e.getMessage());
    }
  }

  @Override
  public HostAlertResponse findHostAlerts(String hostId) {
    try {
      return hostService.findHostAlerts(hostId);
    } catch (EntityNotFoundException e) {
      LOGGER.error(e.getMessage(), e);
      throw e;
    } catch (InputValidationException | IllegalArgumentException e) {
      LOGGER.error("Host ID couldn't be found to fetch alerts", e);
      throw new EntityNotFoundException("Host not found");
    } catch (RemoteException e) {
      LOGGER.error("Error while trying to fetch host alerts by ID", e);
      throw e;
    } catch (Exception e) {
      LOGGER.error("Error while trying to fetch host alerts from NetBox by ID", e);
      throw new ServiceException(e.getMessage());
    }
  }

  @Override
  public PagedAssociatedIocs findAssociatedIocsByHostId(
      UserDetails user,
      String hostId,
      String iocName,
      IocOrder orderBy,
      Boolean isAsc,
      Integer page,
      Integer limit) {
    try {
      return iocService.getAssociatedIocs(hostId, iocName, orderBy, isAsc, page, limit);
    } catch (EntityNotFoundException e) {
      LOGGER.error(e.getMessage(), e);
      throw e;
    } catch (Exception e) {
      LOGGER.error("Error while trying to find associated IOCs by host ID", e);
      throw new ServiceException(e.getMessage());
    }
  }

  @Override
  public HostInfoWithId findNetBoxHostByFqdn(UserDetails user, String fqdn)
      throws EntityNotFoundException, NetBoxServiceException {
    try {
      return hostService.findAndConvertHostByFQDN(fqdn);
    } catch (EntityNotFoundException e) {
      return null;
    } catch (RemoteException e) {
      LOGGER.error("Error while trying to fetch host by FQDN", e);
      throw e;
    } catch (Exception e) {
      LOGGER.error("Error while trying to fetch host by FQDN", e);
      throw new ServiceException(e.getMessage());
    }
  }

  @Override
  public LokiResponse fetchSyslogLines(String hostName, Integer timeRange) {
    try {
      return lokiService.getLogLines(hostName, null, timeRange);
    } catch (LokiServiceException e) {
      LOGGER.error("Remote exception while trying to fetch SysLogs from Loki", e);
      throw e;
    } catch (Exception e) {
      LOGGER.error("Error while trying to get SysLogs from Loki", e);
      throw new ServiceException("Error while trying to get SysLogs");
    }
  }

  @Override
  public HostInfoWithId findNetBoxHostByHostId(UserDetails user, String hostId)
      throws EntityNotFoundException, NetBoxServiceException {
    try {
      return hostService.getHostByNetBoxId(hostId);
    } catch (EntityNotFoundException e) {
      LOGGER.error("Host not found by ID", e);
      throw e;
    } catch (InputValidationException | IllegalArgumentException e) {
      LOGGER.error("Host not found by Host ID", e);
      throw new EntityNotFoundException("Host not found");
    } catch (RemoteException e) {
      LOGGER.error("Error while trying to fetch host by Host ID", e);
      throw e;
    } catch (Exception e) {
      LOGGER.error("Error while trying to fetch host by Host ID", e);
      throw new ServiceException(e.getMessage());
    }
  }
}
