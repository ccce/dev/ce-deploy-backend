package eu.ess.ics.ccce.rest.model.migration.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

public record MigrationDeviance(
    @JsonProperty("not_found") Integer notFound,
    @JsonProperty("network_name_changed") Integer networkNameChanged,
    @JsonProperty("hostname_changed") Integer hostnameChanged,
    @JsonProperty("both_changed") Integer bothChanged,
    @JsonProperty("all_jobs") Integer allJobs,
    @JsonProperty("unresolvable_iocs") List<String> unresolvableIocs,
    @JsonProperty("network_scopes") List<String> networkScopes) {}
