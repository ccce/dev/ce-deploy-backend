package eu.ess.ics.ccce.rest.model.host.response;

import eu.ess.ics.ccce.rest.model.BasePagedResponse;
import java.util.List;

public class PagedNetBoxHostResponse extends BasePagedResponse {
  private List<HostInfoWithId> netBoxHosts;

  public PagedNetBoxHostResponse(
      long totalCount, int listSize, int pageNumber, int limit, List<HostInfoWithId> netBoxHosts) {
    super(totalCount, listSize, pageNumber, limit);
    this.netBoxHosts = netBoxHosts;
  }

  public List<HostInfoWithId> getNetBoxHosts() {
    return netBoxHosts;
  }
}
