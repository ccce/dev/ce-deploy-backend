package eu.ess.ics.ccce.rest.api.v1;

import eu.ess.ics.ccce.configuration.WebSecurityConfig;
import eu.ess.ics.ccce.configuration.exception.GeneralException;
import eu.ess.ics.ccce.rest.model.migration.response.MigrationDeviance;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 */
@RequestMapping("/api/v1/migration")
@Tag(name = "Migration")
public interface IMigrationController {

  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "200",
            description = "Compares the stored host information about the Jobs against NetBox",
            content = @Content(schema = @Schema(implementation = MigrationDeviance.class))),
        @ApiResponse(
            responseCode = "500",
            description = "Service exception during deviance calculation",
            content = @Content(schema = @Schema(implementation = GeneralException.class)))
      })
  @Operation(
      description =
          """
          Response description:

          **all_jobs** - Number of all Job hosts that will be checked in NetBox

          **unresolvable_iocs** - List of IOC IDs where the hosts can not be resolved in NetBox

          **network_scopes** - Set of IOC network-scopes that were resolved from all Jobs

          **network_name_changed** - Number of hosts where the Network-name has changed (compared to when an operation was performed on the IOC)

          **hostname_changed** - Number of hosts where the Hostname has changed (compared to when an operation was performed on the IOC)

          **both_changed** - Number of hosts where Hostname and Network-name has been changed (compared to when an operation was performed on the IOC)

          [*The IOC counted here is also counted in the Hostname change, and Network change fields*]

          **not_found** - Number of Jobs for the Host can not be resolved in NetBox
          """)
  @GetMapping(
      value = "/deviance",
      produces = {"application/json"})
  MigrationDeviance hostDeviance();

  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "204",
            description = "Removes all IOCs that has been ever deployed to the network scope"),
        @ApiResponse(
            responseCode = "500",
            description = "Service exception during delete",
            content = @Content(schema = @Schema(implementation = GeneralException.class)))
      })
  @DeleteMapping(
      value = "/{network_scope_to_remove}/remove_iocs",
      produces = {"application/json"})
  @PreAuthorize(WebSecurityConfig.IS_ADMIN)
  @ResponseStatus(HttpStatus.NO_CONTENT)
  void removeIocs(
      @Parameter(
              in = ParameterIn.PATH,
              description = "Removing all IOCs that has even been deployed to this network scope")
          @PathVariable(value = "network_scope_to_remove")
          String networkScope,
      @RequestParam(value = "ioc_name_prefix", required = false)
          @Parameter(
              description =
                  """
                    Remove IOCs that's CURRENT name starts like the parameter AND was NEVER deployed
                     -- leave empty if don't want to delete such IOCs

                    This is independent from networkScope parameter!
                    """)
          String iocPrefix);
}
