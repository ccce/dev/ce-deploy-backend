/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.rest.model.job.response;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 */
public class Host {
  private final String hostId;
  private final Boolean externalIdValid;
  private final String fqdn;
  private final String hostName;
  private final String network;

  public Host(
      String hostId, Boolean externalIdValid, String fqdn, String hostName, String network) {
    this.hostId = hostId;
    this.externalIdValid = externalIdValid;
    this.fqdn = fqdn;
    this.hostName = hostName;
    this.network = network;
  }

  public Host(String hostId) {
    this.hostId = hostId;
    this.externalIdValid = null;
    this.network = null;
    this.hostName = null;
    this.fqdn = null;
  }

  public String getHostId() {
    return hostId;
  }

  public String getHostName() {
    return hostName;
  }

  public String getNetwork() {
    return network;
  }

  public String getFqdn() {
    return fqdn;
  }

  public Boolean isExternalIdValid() {
    return externalIdValid;
  }
}
