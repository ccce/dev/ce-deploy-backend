/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.rest.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import eu.ess.ics.ccce.exceptions.EntityNotFoundException;
import eu.ess.ics.ccce.exceptions.ServiceException;
import eu.ess.ics.ccce.rest.api.v1.IAwx;
import eu.ess.ics.ccce.rest.model.FeatureTag;
import eu.ess.ics.ccce.rest.model.awx.request.AwxJobMeta;
import eu.ess.ics.ccce.service.internal.deployment.DeploymentService;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 */
@RestController
@FeatureTag("awx")
public class AwxController extends BaseController implements IAwx {
  private static final Logger LOGGER = LoggerFactory.getLogger(AwxController.class);

  @Autowired private DeploymentService deploymentService;

  public AwxController(ObjectMapper objectMapper, HttpServletRequest request) {
    super(objectMapper, request);
  }

  @Override
  public void updateJob(@Valid AwxJobMeta body) {
    checkToken();
    try {
      deploymentService.updateAwxJobOrCommand(body);
    } catch (EntityNotFoundException e) {
      LOGGER.error(e.getMessage(), e);
      throw e;
    } catch (Exception e) {
      LOGGER.error("Error while trying to update AWX job", e);
      throw new ServiceException(e);
    }
  }
}
