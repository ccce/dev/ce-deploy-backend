/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.rest.model.job.response;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import eu.ess.ics.ccce.common.Utils;
import eu.ess.ics.ccce.common.conversion.ZonedDateTimeSerializer;
import java.time.ZonedDateTime;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 */
public class Deployment extends BaseDeploymentInfo {
  private final @JsonSerialize(using = ZonedDateTimeSerializer.class) ZonedDateTime endDate;
  private final String iocName;
  private final Host host;
  private final Long awxJobId;
  private final String sourceUrl;
  private final String sourceVersionShort;
  private final JobStatus jobStatus;

  public Deployment(
      Long id,
      String createdBy,
      ZonedDateTime createdAt,
      ZonedDateTime startDate,
      ZonedDateTime endDate,
      String iocName,
      String namingName,
      Long gitProjectId,
      String sourceUrl,
      String sourceVersion,
      String sourceVersionShort,
      Host host,
      JobStatus jobStatus,
      Long awxJobId,
      boolean undeployment,
      Long jobId) {
    super(
        id,
        createdBy,
        createdAt,
        startDate,
        undeployment,
        namingName,
        gitProjectId,
        sourceVersion,
        jobId);
    this.endDate = Utils.cloneDate(endDate);
    this.iocName = iocName;
    this.host = host;
    this.jobStatus = jobStatus;
    this.awxJobId = awxJobId;
    this.sourceUrl = sourceUrl;
    this.sourceVersionShort = sourceVersionShort;
  }

  public Deployment(
      Long id,
      String createdBy,
      ZonedDateTime createdAt,
      ZonedDateTime startDate,
      ZonedDateTime endDate,
      String iocName,
      String namingName,
      Long gitProjectId,
      String sourceVersion,
      Host host,
      JobStatus jobStatus,
      Long awxJobId,
      boolean undeployment,
      Long jobId) {
    super(
        id,
        createdBy,
        createdAt,
        startDate,
        undeployment,
        namingName,
        gitProjectId,
        sourceVersion,
        jobId);
    this.endDate = Utils.cloneDate(endDate);
    this.iocName = iocName;
    this.host = host;
    this.jobStatus = jobStatus;
    this.awxJobId = awxJobId;
    this.sourceUrl = null;
    this.sourceVersionShort = null;
  }

  public String getIocName() {
    return iocName;
  }

  public ZonedDateTime getEndDate() {
    return Utils.cloneDate(endDate);
  }

  public Host getHost() {
    return host;
  }

  public Long getAwxJobId() {
    return awxJobId;
  }

  public String getSourceUrl() {
    return sourceUrl;
  }

  public String getSourceVersionShort() {
    return sourceVersionShort;
  }

  public JobStatus getOperationStatus() {
    return jobStatus;
  }
}
