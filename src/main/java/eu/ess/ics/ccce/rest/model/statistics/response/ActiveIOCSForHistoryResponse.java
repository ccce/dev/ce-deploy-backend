/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.rest.model.statistics.response;

import java.util.Date;
import java.util.Objects;

/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 */
public class ActiveIOCSForHistoryResponse {
  private Long iocCount;
  private Date historyDate;

  public ActiveIOCSForHistoryResponse(Long iocCount, Date historyDate) {
    this.iocCount = iocCount;
    this.historyDate = historyDate;
  }

  public Long getIocCount() {
    return iocCount;
  }

  public Date getHistoryDate() {
    return historyDate;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    ActiveIOCSForHistoryResponse that = (ActiveIOCSForHistoryResponse) o;
    return Objects.equals(iocCount, that.iocCount) && Objects.equals(historyDate, that.historyDate);
  }

  @Override
  public int hashCode() {
    return Objects.hash(iocCount, historyDate);
  }

  @Override
  public String toString() {
    return "ActiveIOCSForHistoryResponse{"
        + "iocCount="
        + iocCount
        + ", historyDate="
        + historyDate
        + '}';
  }
}
