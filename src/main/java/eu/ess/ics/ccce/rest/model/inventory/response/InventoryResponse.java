package eu.ess.ics.ccce.rest.model.inventory.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public record InventoryResponse(
    @JsonProperty("_meta") MetaStructure meta, @JsonProperty("all") HostInventoryList allHosts) {}
