package eu.ess.ics.ccce.rest.model.host.response;

import java.util.List;

public class HostInfoWithId extends HostInfo {
  private final String hostId;

  public HostInfoWithId(
      long id,
      String fqdn,
      String name,
      String scope,
      String description,
      String network,
      String deviceType,
      Boolean isVm,
      String hostId,
      List<String> tags) {
    super(id, fqdn, name, scope, description, network, deviceType, isVm, tags);
    this.hostId = hostId;
  }

  public HostInfoWithId(HostInfo hostInfo, String hostId) {
    super(
        hostInfo.getId(),
        hostInfo.getFqdn(),
        hostInfo.getName(),
        hostInfo.getScope(),
        hostInfo.getDescription(),
        hostInfo.getNetwork(),
        hostInfo.getDeviceType(),
        hostInfo.isVm(),
        hostInfo.getTags());
    this.hostId = hostId;
  }

  public String getHostId() {
    return hostId;
  }
}
