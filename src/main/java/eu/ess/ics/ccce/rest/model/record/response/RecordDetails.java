/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.rest.model.record.response;

import eu.ess.ics.ccce.rest.model.record.PVStatus;
import java.util.List;
import java.util.Map;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 */
public class RecordDetails extends Record {
  private final Map<String, String> properties;
  private final String hostId;
  private final List<String> aliases;

  public RecordDetails(
      String name,
      String iocName,
      String hostName,
      PVStatus pvStatus,
      String iocVersion,
      String description,
      String recordType,
      String alias,
      Long iocId,
      String hostId,
      Map<String, String> properties,
      List<String> aliases) {
    super(name, iocName, iocId, hostName, pvStatus, iocVersion, description, recordType, alias);
    this.properties = properties;
    this.hostId = hostId;
    this.aliases = aliases;
  }

  public Map<String, String> getProperties() {
    return properties;
  }

  public String getHostId() {
    return hostId;
  }

  public List<String> getAliases() {
    return aliases;
  }
}
