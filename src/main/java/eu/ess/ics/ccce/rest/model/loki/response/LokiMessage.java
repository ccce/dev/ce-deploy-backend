/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.rest.model.loki.response;

import eu.ess.ics.ccce.common.Utils;
import java.time.ZonedDateTime;

/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 */
public class LokiMessage {
  private ZonedDateTime logDate;
  private String logMessage = "";

  public LokiMessage() {}

  public LokiMessage(ZonedDateTime logDate, String logMessage) {
    this.logDate = Utils.cloneDate(logDate);
    this.logMessage = logMessage;
  }

  public ZonedDateTime getLogDate() {
    return Utils.cloneDate(logDate);
  }

  public String getLogMessage() {
    return logMessage;
  }

  public void setLogDate(ZonedDateTime logDate) {
    this.logDate = Utils.cloneDate(logDate);
  }

  public void setLogMessage(String logMessage) {
    this.logMessage = logMessage;
  }
}
