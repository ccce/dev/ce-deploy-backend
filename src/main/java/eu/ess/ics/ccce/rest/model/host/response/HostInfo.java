/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.rest.model.host.response;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.gson.annotations.SerializedName;
import eu.ess.ics.ccce.service.external.netbox.model.NetBoxHostIdentifier;
import java.util.List;

/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 */
public class HostInfo {
  private final long id;
  private final String fqdn;
  private final String name;
  private final String scope;
  private final String description;
  private final String network;

  @SerializedName("device_type")
  private final String deviceType;

  @JsonIgnore private final Boolean isVm;
  private final List<String> tags;

  public HostInfo(
      long id,
      String fqdn,
      String name,
      String scope,
      String description,
      String network,
      String deviceType,
      Boolean isVm,
      List<String> tags) {
    this.id = id;
    this.fqdn = fqdn;
    this.name = name;
    this.scope = scope;
    this.description = description;
    this.network = network;
    this.deviceType = deviceType;
    this.isVm = isVm;
    this.tags = tags;
  }

  public long getId() {
    return id;
  }

  public String getFqdn() {
    return fqdn;
  }

  public String getName() {
    return name;
  }

  public String getScope() {
    return scope;
  }

  public String getDescription() {
    return description;
  }

  public String getNetwork() {
    return network;
  }

  public String getDeviceType() {
    return deviceType;
  }

  public List<String> getTags() {
    return tags;
  }

  @JsonIgnore
  public NetBoxHostIdentifier getHostIdentifier() {
    return new NetBoxHostIdentifier(id, isVm());
  }

  public boolean isVm() {
    return isVm;
  }
}
