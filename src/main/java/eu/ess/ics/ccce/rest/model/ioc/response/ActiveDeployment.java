/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.rest.model.ioc.response;

import eu.ess.ics.ccce.rest.model.job.response.DeploymentHostInfo;

/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 */
public class ActiveDeployment {
  private final DeploymentHostInfo host;
  private final String sourceVersion;
  private final String sourceVersionShort;

  public ActiveDeployment(
      DeploymentHostInfo host, String sourceVersion, String sourceVersionShort) {
    this.host = host;
    this.sourceVersion = sourceVersion;
    this.sourceVersionShort = sourceVersionShort;
  }

  public DeploymentHostInfo getHost() {
    return host;
  }

  public String getSourceVersion() {
    return sourceVersion;
  }

  public String getSourceVersionShort() {
    return sourceVersionShort;
  }
}
