/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.rest.model.record.response;

import eu.ess.ics.ccce.rest.model.record.PVStatus;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 */
public class Record {
  private final String name;
  private final String iocName;
  private final Long iocId;
  private final String hostName;
  private final PVStatus pvStatus;
  private final String iocVersion;
  private final String description;
  private final String recordType;
  private final String aliasFor;

  public Record(
      String name,
      String iocName,
      Long iocId,
      String hostName,
      PVStatus pvStatus,
      String iocVersion,
      String description,
      String recordType,
      String aliasFor) {
    this.name = name;
    this.iocName = iocName;
    this.iocId = iocId;
    this.hostName = hostName;
    this.pvStatus = pvStatus;
    this.iocVersion = iocVersion;
    this.description = description;
    this.recordType = recordType;
    this.aliasFor = aliasFor;
  }

  public String getName() {
    return name;
  }

  public String getIocName() {
    return iocName;
  }

  public Long getIocId() {
    return iocId;
  }

  public String getHostName() {
    return hostName;
  }

  public PVStatus getPvStatus() {
    return pvStatus;
  }

  public String getIocVersion() {
    return iocVersion;
  }

  public String getDescription() {
    return description;
  }

  public String getRecordType() {
    return recordType;
  }

  public String getAliasFor() {
    return aliasFor;
  }
}
