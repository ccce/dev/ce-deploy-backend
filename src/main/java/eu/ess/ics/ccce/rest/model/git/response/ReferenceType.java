package eu.ess.ics.ccce.rest.model.git.response;

public enum ReferenceType {
  TAG,
  COMMIT,
  UNKNOWN
}
