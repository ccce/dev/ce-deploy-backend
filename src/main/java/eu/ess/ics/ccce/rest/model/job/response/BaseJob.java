/*
 * Copyright (C) 2023 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.rest.model.job.response;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import eu.ess.ics.ccce.common.conversion.ZonedDateTimeSerializer;
import eu.ess.ics.ccce.rest.model.job.JobAction;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 */
public abstract class BaseJob {

  private final Long id;
  private final String createdBy;
  private final @JsonSerialize(using = ZonedDateTimeSerializer.class) ZonedDateTime startTime;
  private final @JsonSerialize(using = ZonedDateTimeSerializer.class) ZonedDateTime createdAt;
  private final @JsonSerialize(using = ZonedDateTimeSerializer.class) ZonedDateTime finishedAt;
  private final JobAction action;
  private final JobStatus status;

  public BaseJob(
      Long id,
      String createdBy,
      ZonedDateTime startTime,
      ZonedDateTime createdAt,
      ZonedDateTime finishedAt,
      JobAction action,
      JobStatus status) {
    this.id = id;
    this.createdBy = createdBy;
    this.startTime = startTime;
    this.createdAt = createdAt;
    this.finishedAt = finishedAt;
    this.action = action;
    this.status = status;
  }

  public Long getId() {
    return id;
  }

  public String getCreatedBy() {
    return createdBy;
  }

  public ZonedDateTime getStartTime() {
    return startTime;
  }

  public ZonedDateTime getCreatedAt() {
    return createdAt;
  }

  public JobAction getAction() {
    return action;
  }

  public JobStatus getStatus() {
    return status;
  }

  public ZonedDateTime getFinishedAt() {
    return finishedAt;
  }

  @Override
  public boolean equals(Object o) {
    if (o == null || getClass() != o.getClass()) return false;
    BaseJob baseJob = (BaseJob) o;
    return Objects.equals(id, baseJob.id)
        && Objects.equals(createdBy, baseJob.createdBy)
        && Objects.equals(startTime, baseJob.startTime)
        && Objects.equals(createdAt, baseJob.createdAt)
        && Objects.equals(finishedAt, baseJob.finishedAt)
        && action == baseJob.action
        && status == baseJob.status;
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, createdBy, startTime, createdAt, finishedAt, action, status);
  }

  @Override
  public String toString() {
    return "BaseJob{"
        + "id="
        + id
        + ", createdBy='"
        + createdBy
        + '\''
        + ", startTime="
        + startTime
        + ", createdAt="
        + createdAt
        + ", finishedAt="
        + finishedAt
        + ", action="
        + action
        + ", status="
        + status
        + '}';
  }
}
