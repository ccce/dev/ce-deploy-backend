package eu.ess.ics.ccce.rest.controller;

import eu.ess.ics.ccce.exceptions.ServiceException;
import eu.ess.ics.ccce.rest.api.v1.IMigrationController;
import eu.ess.ics.ccce.rest.model.migration.response.MigrationDeviance;
import eu.ess.ics.ccce.service.internal.migration.MigrationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 */
@RestController
public class MigrationController implements IMigrationController {

  private static final Logger LOGGER = LoggerFactory.getLogger(MigrationController.class);

  private final MigrationService migrationService;

  @Autowired
  public MigrationController(MigrationService migrationService) {
    this.migrationService = migrationService;
  }

  @Override
  public MigrationDeviance hostDeviance() {
    try {
      return migrationService.deviance();
    } catch (Exception e) {
      LOGGER.error("Error while trying to compare host info to NetBox", e);
      throw new ServiceException(e);
    }
  }

  @Override
  public void removeIocs(String networkScope, String iocPrefix) {
    try {
      migrationService.removeIocs(networkScope, iocPrefix);
    } catch (Exception e) {
      LOGGER.error("Error while trying to remove IOCs by scope", e);
      throw new ServiceException(e);
    }
  }
}
