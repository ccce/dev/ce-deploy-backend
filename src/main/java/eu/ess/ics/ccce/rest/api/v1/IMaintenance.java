/*
 * Copyright (C) 2024 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.rest.api.v1;

import eu.ess.ics.ccce.configuration.WebSecurityConfig;
import eu.ess.ics.ccce.configuration.exception.GeneralException;
import eu.ess.ics.ccce.exceptions.EntityNotFoundException;
import eu.ess.ics.ccce.rest.model.maintenance.request.SetMaintenance;
import eu.ess.ics.ccce.rest.model.maintenance.response.MaintenanceMode;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

/**
 * @author <a href="mailto:sky.brewer@ess.eu">Sky Brewer</a>
 */
@RequestMapping("/api/v1/maintenance_mode")
@Tag(name = "Maintenance")
public interface IMaintenance {

  @Operation(summary = "Get Current Maintenance Mode")
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "500",
            description = "Service exception",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "404",
            description = "Maintenance Mode not found",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "200",
            description = "Found Mode",
            content = @Content(schema = @Schema(implementation = MaintenanceMode.class)))
      })
  @GetMapping(
      value = "/current",
      produces = {"application/json"})
  MaintenanceMode getCurrentMode() throws EntityNotFoundException;

  @Operation(
      summary = "Set Maintenance Mode",
      security = {@SecurityRequirement(name = "bearerAuth")})
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "201",
            description = "Maintenance Mode Changed",
            content = @Content(schema = @Schema(implementation = MaintenanceMode.class))),
        @ApiResponse(
            responseCode = "400",
            description = "Incomplete request",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "401",
            description = "Unauthorized",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "403",
            description = "Forbidden: User doesn't have the necessary permissions",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "409",
            description = "Maintenance Mode already set to that value",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "422",
            description = "Unprocessable request",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "500",
            description = "Service exception",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "503",
            description = "Remote service exception",
            content = @Content(schema = @Schema(implementation = GeneralException.class)))
      })
  @PutMapping(
      produces = {"application/json"},
      consumes = {"application/json"})
  @ResponseStatus(HttpStatus.CREATED)
  @PreAuthorize(WebSecurityConfig.IS_ADMIN)
  MaintenanceMode setMaintenanceMode(
      @Parameter(
              in = ParameterIn.DEFAULT,
              description = "Maintenance Mode to set to",
              required = true,
              schema = @Schema(implementation = SetMaintenance.class))
          @Valid
          @RequestBody
          SetMaintenance body);

  @Operation(
      summary = "End Current Maintenance Mode",
      security = {@SecurityRequirement(name = "bearerAuth")})
  @ApiResponses(
      value = {
        @ApiResponse(responseCode = "204", description = "Maintenance Mode ended"),
        @ApiResponse(
            responseCode = "400",
            description = "Incomplete request",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "401",
            description = "Unauthorized",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "403",
            description = "Forbidden: User doesn't have the necessary permissions",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "422",
            description = "Unprocessable request",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "500",
            description = "Service exception",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "503",
            description = "Remote service exception",
            content = @Content(schema = @Schema(implementation = GeneralException.class)))
      })
  @DeleteMapping(
      value = "/current",
      produces = {"application/json"})
  @ResponseStatus(HttpStatus.NO_CONTENT)
  @PreAuthorize(WebSecurityConfig.IS_ADMIN)
  void endCurrentMaintenanceMode();
}
