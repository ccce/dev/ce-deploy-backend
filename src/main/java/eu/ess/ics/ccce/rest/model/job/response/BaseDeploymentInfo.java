/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.rest.model.job.response;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import eu.ess.ics.ccce.common.Utils;
import eu.ess.ics.ccce.common.conversion.ZonedDateTimeSerializer;
import java.time.ZonedDateTime;

/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 */
public class BaseDeploymentInfo {
  private final Long id;
  private final String createdBy;
  private final @JsonSerialize(using = ZonedDateTimeSerializer.class) ZonedDateTime startDate;
  private final @JsonSerialize(using = ZonedDateTimeSerializer.class) ZonedDateTime createdAt;
  private final boolean undeployment;
  private final String namingName;
  private final Long gitProjectId;
  private final String sourceVersion;
  private final Long jobId;

  public BaseDeploymentInfo(
      Long id,
      String createdBy,
      ZonedDateTime createdAt,
      ZonedDateTime startDate,
      boolean undeployment,
      String namingName,
      Long gitProjectId,
      String sourceVersion,
      Long jobId) {
    this.id = id;
    this.createdBy = createdBy;
    this.createdAt = Utils.cloneDate(createdAt);
    this.startDate = Utils.cloneDate(startDate);
    this.undeployment = undeployment;
    this.namingName = namingName;
    this.gitProjectId = gitProjectId;
    this.sourceVersion = sourceVersion;
    this.jobId = jobId;
  }

  public Long getId() {
    return id;
  }

  public String getCreatedBy() {
    return createdBy;
  }

  public ZonedDateTime getStartDate() {
    return startDate;
  }

  public boolean isUndeployment() {
    return undeployment;
  }

  public String getNamingName() {
    return namingName;
  }

  public Long getGitProjectId() {
    return gitProjectId;
  }

  public String getSourceVersion() {
    return sourceVersion;
  }

  public ZonedDateTime getCreatedAt() {
    return createdAt;
  }

  public Long getJobId() {
    return jobId;
  }
}
