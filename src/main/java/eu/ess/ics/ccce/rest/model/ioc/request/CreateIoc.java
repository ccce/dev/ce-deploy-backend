/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.rest.model.ioc.request;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 */
public class CreateIoc {
  private String namingUuid;
  private Long gitProjectId;

  @JsonProperty("repository_name")
  private String repoName;

  public CreateIoc(String namingUuid, Long gitProjectId, String repoName) {
    this.namingUuid = namingUuid;
    this.gitProjectId = gitProjectId;
    this.repoName = repoName;
  }

  public String getNamingUuid() {
    return namingUuid;
  }

  public void setNamingUuid(String namingUuid) {
    this.namingUuid = namingUuid;
  }

  public Long getGitProjectId() {
    return gitProjectId;
  }

  public void setGitProjectId(Long gitProjectId) {
    this.gitProjectId = gitProjectId;
  }

  public String getRepoName() {
    return repoName;
  }

  public void setRepoName(String repoName) {
    this.repoName = repoName;
  }
}
