/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.rest.api.v1;

import eu.ess.ics.ccce.configuration.WebSecurityConfig;
import eu.ess.ics.ccce.configuration.exception.GeneralException;
import eu.ess.ics.ccce.exceptions.GitlabServiceException;
import eu.ess.ics.ccce.rest.model.authentication.request.Login;
import eu.ess.ics.ccce.rest.model.authentication.response.LoginResponse;
import eu.ess.ics.ccce.service.internal.security.dto.UserDetails;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import java.util.List;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 */
@RequestMapping("/api/v1/authentication")
@Tag(name = "Authentication")
public interface IAuthentication {

  @ApiResponses({
    @ApiResponse(
        responseCode = "200",
        description = "Ok",
        content = @Content(schema = @Schema(implementation = LoginResponse.class))),
    @ApiResponse(
        responseCode = "401",
        description = "Bad username, and/or password",
        content = @Content(schema = @Schema(implementation = GeneralException.class))),
    @ApiResponse(
        responseCode = "403",
        description = "User doesn't have permission to log in",
        content = @Content(schema = @Schema(implementation = GeneralException.class))),
    @ApiResponse(
        responseCode = "503",
        description = "Login server unavailable",
        content = @Content(schema = @Schema(implementation = GeneralException.class))),
    @ApiResponse(
        responseCode = "500",
        description = "Service exception",
        content = @Content(schema = @Schema(implementation = GeneralException.class)))
  })
  @Operation(
      summary = "Login user and acquire JWT token",
      description = "Login user with username, and password in order to acquire JWT token")
  @PostMapping(
      value = "/login",
      produces = {"application/json"})
  ResponseEntity<LoginResponse> login(@RequestBody Login loginInfo) throws GitlabServiceException;

  @ApiResponses({
    @ApiResponse(
        responseCode = "200",
        description = "Ok",
        content = @Content(schema = @Schema(implementation = LoginResponse.class))),
    @ApiResponse(
        responseCode = "401",
        description = "Unauthorized",
        content = @Content(schema = @Schema(implementation = GeneralException.class))),
    @ApiResponse(
        responseCode = "403",
        description = "Permission denied",
        content = @Content(schema = @Schema(implementation = GeneralException.class))),
    @ApiResponse(
        responseCode = "503",
        description = "Login server unavailable",
        content = @Content(schema = @Schema(implementation = GeneralException.class))),
    @ApiResponse(
        responseCode = "500",
        description = "Service exception",
        content = @Content(schema = @Schema(implementation = GeneralException.class)))
  })
  @Operation(
      summary = "Renewing JWT token",
      description = "Renewing valid, non-expired JWT token",
      security = {@SecurityRequirement(name = "bearerAuth")})
  @PostMapping(
      value = "/renew",
      produces = {"application/json"})
  ResponseEntity<LoginResponse> tokenRenew(@AuthenticationPrincipal UserDetails user);

  @ApiResponses({
    @ApiResponse(
        responseCode = "200",
        description = "Ok",
        content = @Content(array = @ArraySchema(schema = @Schema(implementation = String.class)))),
    @ApiResponse(
        responseCode = "401",
        description = "Unauthorized",
        content = @Content(schema = @Schema(implementation = GeneralException.class))),
    @ApiResponse(
        responseCode = "403",
        description = "Permission denied",
        content = @Content(schema = @Schema(implementation = GeneralException.class))),
    @ApiResponse(
        responseCode = "503",
        description = "Login server unavailable",
        content = @Content(schema = @Schema(implementation = GeneralException.class))),
    @ApiResponse(
        responseCode = "500",
        description = "Service exception",
        content = @Content(schema = @Schema(implementation = GeneralException.class)))
  })
  @Operation(
      summary = "Get user roles",
      description = "Get user roles from authorization service",
      security = {@SecurityRequirement(name = "bearerAuth")})
  @GetMapping(
      value = "/roles",
      produces = {"application/json"})
  List<String> getUserRoles(@AuthenticationPrincipal UserDetails user);

  @ApiResponses({
    @ApiResponse(responseCode = "200", description = "Ok"),
    @ApiResponse(
        responseCode = "503",
        description = "Login server unavailable or remote error",
        content = @Content(schema = @Schema(implementation = GeneralException.class))),
    @ApiResponse(
        responseCode = "500",
        description = "Service exception",
        content = @Content(schema = @Schema(implementation = GeneralException.class)))
  })
  @Operation(
      summary = "Logout",
      description = "Logging out the user",
      security = {@SecurityRequirement(name = "bearerAuth")})
  @DeleteMapping(
      value = "/logout",
      produces = {"application/json"})
  ResponseEntity<Object> logout(@AuthenticationPrincipal UserDetails user);

  @ApiResponses({
    @ApiResponse(
        responseCode = "200",
        description = "Ok",
        content = @Content(array = @ArraySchema(schema = @Schema(implementation = String.class)))),
    @ApiResponse(
        responseCode = "401",
        description = "Unauthorized",
        content = @Content(schema = @Schema(implementation = GeneralException.class))),
    @ApiResponse(
        responseCode = "404",
        description = "Role not found by authorization service",
        content = @Content(schema = @Schema(implementation = GeneralException.class))),
    @ApiResponse(
        responseCode = "503",
        description = "Login server unavailable",
        content = @Content(schema = @Schema(implementation = GeneralException.class))),
    @ApiResponse(
        responseCode = "500",
        description = "Service exception",
        content = @Content(schema = @Schema(implementation = GeneralException.class)))
  })
  @Operation(
      summary = "Get Deployment Tool users",
      description = "Get Deployment Tool users from authorization service",
      security = {@SecurityRequirement(name = "bearerAuth")})
  @GetMapping(
      value = "/users",
      produces = {"application/json"})
  @PreAuthorize(WebSecurityConfig.IS_ADMIN_OR_INTEGRATOR)
  List<String> getToolUsers(
      @AuthenticationPrincipal UserDetails user,
      @Parameter(description = "User name query") @RequestParam(required = false) String query);
}
