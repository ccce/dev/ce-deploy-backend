/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.rest.model;

import java.util.Objects;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 */
public class Alert {
  private final AlertSeverity type;
  private final String message;
  private final String link;

  public Alert(AlertSeverity type, String message) {
    this.type = type;
    this.message = message;
    this.link = null;
  }

  public Alert(AlertSeverity type, String message, String link) {
    this.type = type;
    this.message = message;
    this.link = link;
  }

  public AlertSeverity getType() {
    return type;
  }

  public String getMessage() {
    return message;
  }

  public String getLink() {
    return link;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Alert alert = (Alert) o;
    return type == alert.type
        && Objects.equals(message, alert.message)
        && Objects.equals(link, alert.link);
  }

  @Override
  public int hashCode() {
    return Objects.hash(type, message, link);
  }

  @Override
  public String toString() {
    return "Alert{"
        + "type="
        + type
        + ", message='"
        + message
        + '\''
        + ", link='"
        + link
        + '\''
        + '}';
  }
}
