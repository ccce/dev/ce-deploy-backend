/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.rest.api.v1;

import eu.ess.ics.ccce.configuration.WebSecurityConfig;
import eu.ess.ics.ccce.configuration.exception.GeneralException;
import eu.ess.ics.ccce.exceptions.EntityNotFoundException;
import eu.ess.ics.ccce.rest.model.job.request.BatchDeploymentRequest;
import eu.ess.ics.ccce.rest.model.job.request.BatchUndeploymentRequest;
import eu.ess.ics.ccce.rest.model.job.request.DeployJobRequest;
import eu.ess.ics.ccce.rest.model.job.request.JobKind;
import eu.ess.ics.ccce.rest.model.job.request.JobOrder;
import eu.ess.ics.ccce.rest.model.job.request.JobStatusQuery;
import eu.ess.ics.ccce.rest.model.job.response.*;
import eu.ess.ics.ccce.service.internal.security.dto.UserDetails;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import java.time.LocalDateTime;
import javax.validation.Valid;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 */
@RequestMapping("/api/v1/jobs")
@Tag(name = "Jobs")
public interface IJobs {

  @Operation(summary = "Get AWX job status")
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "401",
            description = "Unauthorized",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "404",
            description = "Job not found",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "500",
            description = "Service exception",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "503",
            description = "Remote service exception",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "200",
            description = "Job details",
            content = @Content(schema = @Schema(implementation = AwxJobDetails.class)))
      })
  @GetMapping(
      value = "/{job_id}/awx/status",
      produces = {"application/json"})
  AwxJobDetails fetchAwxJobStatus(
      @AuthenticationPrincipal UserDetails user,
      @Parameter(in = ParameterIn.PATH, description = "Unique job ID", required = true)
          @PathVariable("job_id")
          long jobId);

  @Operation(
      summary = "Get job log from AWX",
      security = {@SecurityRequirement(name = "bearerAuth")})
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "401",
            description = "Unauthorized",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "404",
            description = "Deployment not found",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "500",
            description = "Service exception",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "503",
            description = "Remote service exception",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "200",
            description = "Job log",
            content = @Content(schema = @Schema(implementation = AwxJobLog.class)))
      })
  @GetMapping(
      value = "/{job_id}/awx/log",
      produces = {"application/json"})
  @PreAuthorize(WebSecurityConfig.IS_ADMIN_OR_INTEGRATOR)
  AwxJobLog fetchAwxJobLog(
      @AuthenticationPrincipal UserDetails user,
      @Parameter(in = ParameterIn.PATH, description = "Unique job ID", required = true)
          @PathVariable("job_id")
          long jobId);

  @Operation(summary = "List of deployments and commands")
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "401",
            description = "Unauthorized",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "200",
            description = "A paged array of deployments",
            content = @Content(schema = @Schema(implementation = PagedJobResponse.class))),
        @ApiResponse(
            responseCode = "500",
            description = "Service exception",
            content = @Content(schema = @Schema(implementation = GeneralException.class)))
      })
  @GetMapping(produces = {"application/json"})
  PagedJobResponse listJobs(
      @AuthenticationPrincipal UserDetails userDetails,
      @Parameter(description = "IOC ID") @RequestParam(required = false, name = "ioc_id")
          Long iocId,
      @Parameter(description = "Type") @RequestParam(required = false) JobKind type,
      @Parameter(description = "Status") @RequestParam(required = false) JobStatusQuery status,
      @Parameter(description = "User name") @RequestParam(required = false) String user,
      @Parameter(description = "Search text (Search for IOC name, Created by)")
          @RequestParam(required = false)
          String query,
      @Parameter(description = "Start date")
          @RequestParam(required = false, name = "start_date")
          @DateTimeFormat(iso = DateTimeFormat.ISO.DATE, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS")
          LocalDateTime startDate,
      @Parameter(description = "End date")
          @RequestParam(required = false, name = "end_date")
          @DateTimeFormat(iso = DateTimeFormat.ISO.DATE, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS")
          LocalDateTime endDate,
      @Parameter(description = "Time zone ID") @RequestParam(required = false, name = "time_zone")
          String timeZone,
      @Parameter(description = "Host ID for the host")
          @RequestParam(required = false, name = "host_id")
          String hostId,
      @Parameter(description = "Order by") @RequestParam(required = false, name = "order_by")
          JobOrder orderBy,
      @Parameter(description = "Order Ascending")
          @RequestParam(required = false, name = "order_asc")
          Boolean isAsc,
      @Parameter(description = "Page offset") @RequestParam(required = false) Integer page,
      @Parameter(description = "Page size") @RequestParam(required = false) Integer limit);

  @Operation(summary = "Get job details")
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "401",
            description = "Unauthorized",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "404",
            description = "Not found",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "200",
            description = "Job details",
            content = @Content(schema = @Schema(implementation = JobDetails.class))),
        @ApiResponse(
            responseCode = "500",
            description = "Service exception",
            content = @Content(schema = @Schema(implementation = GeneralException.class)))
      })
  @GetMapping(
      value = "/{job_id}",
      produces = {"application/json"})
  JobDetails fetchJob(
      @AuthenticationPrincipal UserDetails user,
      @Parameter(in = ParameterIn.PATH, description = "Unique ID of the job", required = true)
          @PathVariable("job_id")
          long jobId)
      throws EntityNotFoundException;

  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "201",
            description = "IOC job initiated",
            content = @Content(schema = @Schema(implementation = Job.class))),
        @ApiResponse(
            responseCode = "400",
            description = "Incomplete request",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "401",
            description = "Unauthorized",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "403",
            description = "Forbidden: User doesn't have the necessary permissions in Gitlab",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "404",
            description = "IOC not found",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "409",
            description = "Concurrent deployment, undeployment, start or stop for IOC is ongoing",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "422",
            description =
                "IOC has no active / NetBox host not found / IOC is not deployed correctly",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "424",
            description = "Metadata file not found, or not processable",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "500",
            description = "Service exception",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "503",
            description = "Remote service exception",
            content = @Content(schema = @Schema(implementation = GeneralException.class)))
      })
  @Operation(
      summary = "Starts an IOC job",
      security = {@SecurityRequirement(name = "bearerAuth")})
  @PreAuthorize(WebSecurityConfig.IS_ADMIN_OR_INTEGRATOR)
  @ResponseStatus(HttpStatus.CREATED)
  @PostMapping(
      value = "start/{ioc_id}",
      produces = {"application/json"})
  Job iocStartJob(
      @Parameter(
              in = ParameterIn.PATH,
              description = "The ID of the IOC to start a job on",
              required = true)
          @PathVariable("ioc_id")
          long iocId,
      @AuthenticationPrincipal UserDetails user);

  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "201",
            description = "IOC job initiated",
            content = @Content(schema = @Schema(implementation = Job.class))),
        @ApiResponse(
            responseCode = "400",
            description = "Incomplete request",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "401",
            description = "Unauthorized",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "403",
            description = "Forbidden: User doesn't have the necessary permissions in Gitlab",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "404",
            description = "IOC not found",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "409",
            description = "Concurrent deployment, undeployment, start or stop for IOC is ongoing",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "422",
            description =
                "IOC has no active / NetBox host not found / IOC is not deployed correctly",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "424",
            description = "Metadata file not found, or not processable",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "500",
            description = "Service exception",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "503",
            description = "Remote service exception",
            content = @Content(schema = @Schema(implementation = GeneralException.class)))
      })
  @Operation(
      summary = "Stops an IOC job",
      security = {@SecurityRequirement(name = "bearerAuth")})
  @PreAuthorize(WebSecurityConfig.IS_ADMIN_OR_INTEGRATOR)
  @ResponseStatus(HttpStatus.CREATED)
  @PostMapping(
      value = "stop/{ioc_id}",
      produces = {"application/json"})
  Job iocStopJob(
      @Parameter(
              in = ParameterIn.PATH,
              description = "The ID of the IOC to start a job on",
              required = true)
          @PathVariable("ioc_id")
          long iocId,
      @AuthenticationPrincipal UserDetails user);

  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "201",
            description = "IOC job initiated",
            content = @Content(schema = @Schema(implementation = Job.class))),
        @ApiResponse(
            responseCode = "400",
            description = "Incomplete request",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "401",
            description = "Unauthorized",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "403",
            description = "Forbidden: User doesn't have the necessary permissions in Gitlab",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "404",
            description = "IOC not found",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "409",
            description = "Concurrent deployment, undeployment, start or stop for IOC is ongoing",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "422",
            description =
                "IOC has no active / NetBox host not found / IOC is not deployed correctly",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "424",
            description = "Metadata file not found, or not processable",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "500",
            description = "Service exception",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "503",
            description = "Remote service exception",
            content = @Content(schema = @Schema(implementation = GeneralException.class)))
      })
  @Operation(
      summary = "Deploy an IOC job",
      security = {@SecurityRequirement(name = "bearerAuth")})
  @PreAuthorize(WebSecurityConfig.IS_ADMIN_OR_INTEGRATOR)
  @ResponseStatus(HttpStatus.CREATED)
  @PostMapping(
      value = "deploy/{ioc_id}",
      produces = {"application/json"})
  Job iocDeployJob(
      @Parameter(
              in = ParameterIn.PATH,
              description = "The ID of the IOC to start a job on",
              required = true)
          @PathVariable("ioc_id")
          long iocId,
      @RequestBody DeployJobRequest request,
      @AuthenticationPrincipal UserDetails user);

  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "201",
            description = "IOC job initiated",
            content = @Content(schema = @Schema(implementation = Job.class))),
        @ApiResponse(
            responseCode = "400",
            description = "Incomplete request",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "401",
            description = "Unauthorized",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "403",
            description = "Forbidden: User doesn't have the necessary permissions in Gitlab",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "404",
            description = "IOC not found",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "409",
            description = "Concurrent deployment, undeployment, start or stop for IOC is ongoing",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "422",
            description =
                "IOC has no active / NetBox host not found / IOC is not deployed correctly",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "424",
            description = "Metadata file not found, or not processable",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "500",
            description = "Service exception",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "503",
            description = "Remote service exception",
            content = @Content(schema = @Schema(implementation = GeneralException.class)))
      })
  @Operation(
      summary = "Undeploy an IOC job",
      security = {@SecurityRequirement(name = "bearerAuth")})
  @PreAuthorize(WebSecurityConfig.IS_ADMIN_OR_INTEGRATOR)
  @ResponseStatus(HttpStatus.CREATED)
  @PostMapping(
      value = "undeploy/{ioc_id}",
      produces = {"application/json"})
  Job iocUndeployJob(
      @Parameter(
              in = ParameterIn.PATH,
              description = "The ID of the IOC to start a job on",
              required = true)
          @PathVariable("ioc_id")
          long iocId,
      @AuthenticationPrincipal UserDetails user);

  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "201",
            description = "IOC job initiated",
            content = @Content(schema = @Schema(implementation = Job.class))),
        @ApiResponse(
            responseCode = "400",
            description = "Incomplete request",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "401",
            description = "Unauthorized",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "403",
            description = "Forbidden: User doesn't have the necessary permissions in Gitlab",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "409",
            description = "Concurrent deployment, undeployment, start or stop for IOC is ongoing",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "422",
            description =
                "IOC has no active / NetBox host not found / IOC is not deployed correctly",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "424",
            description = "Metadata file not found, or not processable",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "500",
            description = "Service exception",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "503",
            description = "Remote service exception",
            content = @Content(schema = @Schema(implementation = GeneralException.class)))
      })
  @Operation(
      summary = "Starts multiple IOC deployments",
      security = {@SecurityRequirement(name = "bearerAuth")})
  @PreAuthorize(WebSecurityConfig.IS_ADMIN_OR_INTEGRATOR)
  @ResponseStatus(HttpStatus.CREATED)
  @PostMapping(
      value = "/batch/deploy",
      produces = {"application/json"})
  Job startBatchDeployment(
      @Valid @RequestBody BatchDeploymentRequest request,
      @AuthenticationPrincipal UserDetails user);

  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "201",
            description = "IOC undeployment initiated",
            content = @Content(schema = @Schema(implementation = Job.class))),
        @ApiResponse(
            responseCode = "400",
            description = "Incomplete request",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "401",
            description = "Unauthorized",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "403",
            description = "Forbidden: User doesn't have the necessary permissions in Gitlab",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "409",
            description = "Concurrent deployment, undeployment, start or stop for IOC is ongoing",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "422",
            description =
                "IOC has no active / NetBox host not found / IOC is not deployed correctly",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "424",
            description = "Metadata file not found, or not processable",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "500",
            description = "Service exception",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "503",
            description = "Remote service exception",
            content = @Content(schema = @Schema(implementation = GeneralException.class)))
      })
  @Operation(
      summary = "Starts multiple IOC undeployments",
      security = {@SecurityRequirement(name = "bearerAuth")})
  @PreAuthorize(WebSecurityConfig.IS_ADMIN_OR_INTEGRATOR)
  @ResponseStatus(HttpStatus.CREATED)
  @PostMapping(
      value = "/batch/undeploy",
      produces = {"application/json"})
  Job startBatchUndeployment(
      @Valid @RequestBody BatchUndeploymentRequest request,
      @AuthenticationPrincipal UserDetails user);
}
