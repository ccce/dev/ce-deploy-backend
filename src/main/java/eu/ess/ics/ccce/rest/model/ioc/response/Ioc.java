/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.rest.model.ioc.response;

import eu.ess.ics.ccce.rest.model.job.response.Deployment;

/**
 * DTO for IOC related data
 *
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 */
public class Ioc {
  private final Long id;
  protected String description;
  protected String createdBy;
  private final String namingName;
  private final String namingUuid;
  private final Long gitProjectId;
  private final String sourceUrl;
  private final Deployment activeDeployment;
  private final Boolean deployedWithOldPlaybook;

  public Ioc(
      String description,
      String createdBy,
      Long id,
      String namingName,
      String namingUuid,
      Long gitProjectId,
      String sourceUrl,
      Deployment activeDeployment,
      Boolean deployedWithOldPlaybook) {
    this.id = id;
    this.description = description;
    this.createdBy = createdBy;
    this.activeDeployment = activeDeployment;
    this.deployedWithOldPlaybook = deployedWithOldPlaybook;
    this.namingName = namingName;
    this.namingUuid = namingUuid;
    this.gitProjectId = gitProjectId;
    this.sourceUrl = sourceUrl;
  }

  public Ioc(
      String createdBy,
      Long id,
      String namingName,
      String namingUuid,
      Long gitProjectId,
      Deployment activeDeployment,
      Boolean deployedWithOldPlaybook) {
    this.id = id;
    this.description = null;
    this.createdBy = createdBy;
    this.activeDeployment = activeDeployment;
    this.deployedWithOldPlaybook = deployedWithOldPlaybook;
    this.namingName = namingName;
    this.namingUuid = namingUuid;
    this.gitProjectId = gitProjectId;
    this.sourceUrl = null;
  }

  public Long getId() {
    return id;
  }

  public Deployment getActiveDeployment() {
    return activeDeployment;
  }

  public Boolean isDeployedWithOldPlaybook() {
    return deployedWithOldPlaybook;
  }

  public String getDescription() {
    return description;
  }

  public String getCreatedBy() {
    return createdBy;
  }

  public String getNamingName() {
    return namingName;
  }

  public String getNamingUuid() {
    return namingUuid;
  }

  public Long getGitProjectId() {
    return gitProjectId;
  }

  public String getSourceUrl() {
    return sourceUrl;
  }
}
