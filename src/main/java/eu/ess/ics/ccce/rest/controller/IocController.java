/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.rest.controller;

import eu.ess.ics.ccce.configuration.ValidationConstants;
import eu.ess.ics.ccce.exceptions.*;
import eu.ess.ics.ccce.repository.entity.IocDeploymentEntity;
import eu.ess.ics.ccce.repository.entity.IocEntity;
import eu.ess.ics.ccce.rest.api.v1.IIoc;
import eu.ess.ics.ccce.rest.model.FeatureTag;
import eu.ess.ics.ccce.rest.model.ioc.request.CreateIoc;
import eu.ess.ics.ccce.rest.model.ioc.request.IOCUpdateRequest;
import eu.ess.ics.ccce.rest.model.ioc.request.IocDeploymentStatus;
import eu.ess.ics.ccce.rest.model.ioc.request.IocOrder;
import eu.ess.ics.ccce.rest.model.ioc.request.UpdateHostRequest;
import eu.ess.ics.ccce.rest.model.ioc.response.Ioc;
import eu.ess.ics.ccce.rest.model.ioc.response.IocAlertResponse;
import eu.ess.ics.ccce.rest.model.ioc.response.IocDescriptionResponse;
import eu.ess.ics.ccce.rest.model.ioc.response.IocDetails;
import eu.ess.ics.ccce.rest.model.ioc.response.IocStatusResponse;
import eu.ess.ics.ccce.rest.model.ioc.response.PagedIocResponse;
import eu.ess.ics.ccce.rest.model.job.response.IdUsedByIoc;
import eu.ess.ics.ccce.rest.model.loki.response.LokiResponse;
import eu.ess.ics.ccce.service.external.loki.LokiService;
import eu.ess.ics.ccce.service.internal.deployment.DeploymentService;
import eu.ess.ics.ccce.service.internal.ioc.IocService;
import eu.ess.ics.ccce.service.internal.security.dto.UserDetails;
import jakarta.validation.Valid;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 */
@RestController
@FeatureTag("ioc")
public class IocController implements IIoc {

  private static final Logger LOGGER = LoggerFactory.getLogger(IocController.class);

  private final IocService iocService;
  private final DeploymentService deploymentService;
  private final LokiService lokiService;

  public IocController(
      IocService iocService, DeploymentService deploymentService, LokiService lokiService) {
    this.iocService = iocService;
    this.deploymentService = deploymentService;
    this.lokiService = lokiService;
  }

  @Override
  public PagedIocResponse listIocs(
      UserDetails user,
      IocDeploymentStatus deploymentStatus,
      String createdBy,
      String network,
      String networkScope,
      String hostType,
      String hostName,
      String query,
      IocOrder orderBy,
      Boolean isAsc,
      Integer page,
      Integer limit,
      boolean listAll) {

    try {
      LOGGER.debug("Trying to list all IOCs");
      return iocService.findAll(
          deploymentStatus,
          createdBy,
          network,
          networkScope,
          hostType,
          hostName,
          query,
          orderBy,
          isAsc,
          page,
          limit,
          listAll);
    } catch (Exception e) {
      LOGGER.error(e.getMessage(), e);
      throw new ServiceException("Error while trying to list all IOCs");
    }
  }

  @Override
  public IocDetails getIoc(UserDetails user, long iocId) {

    try {
      LOGGER.debug("Trying to get IOC with ID: {}", iocId);
      return iocService.getIoc(iocId);
    } catch (EntityNotFoundException e) {
      LOGGER.error("Error trying to return IOC details. IOC ID {} not found.", iocId, e);
      throw e;
    } catch (Exception e) {
      LOGGER.error("Error while trying to get IOC details by ID", e);
      throw new ServiceException(e.getMessage());
    }
  }

  @Override
  public IocDescriptionResponse getIocDescription(long iocId) {
    try {
      return iocService.getIocDescription(iocId);
    } catch (EntityNotFoundException e) {
      LOGGER.error("IOC not found to get description by ID: {}", iocId, e);
      throw e;
    } catch (Exception e) {
      LOGGER.error("Error while trying to get IOC Description by ID", e);
      throw new ServiceException(e.getMessage());
    }
  }

  @Override
  public Ioc createIoc(UserDetails user, @Valid CreateIoc body) {
    LOGGER.debug("Trying to create new IOC");
    checkIocCreateBody(body);

    try {
      return iocService.createNewIoc(body, user);
    } catch (MetaDataFileException e) {
      LOGGER.error("Error while trying to process metadata file: " + e.getMessage(), e);
      throw new MetaDataFileException(e.getMessage());
    } catch (EntityNotFoundException e) {
      LOGGER.error(e.getMessage(), e);
      throw new UnprocessableEntityException(e.getMessage());
    } catch (DataIntegrityViolationException e) {
      LOGGER.error(e.getMessage(), e);
      throw transformDataIntegrityException(e);
    } catch (UnauthorizedException
        | IncompleteRequestException
        | RemoteException
        | ConflictException
        | InputValidationException e) {
      LOGGER.error(e.getMessage(), e);
      throw e;
    } catch (Exception e) {
      LOGGER.error("Error while trying to create IOC", e);
      throw new ServiceException("Error while trying to create new IOC", e);
    }
  }

  @Override
  public ResponseEntity deleteIoc(UserDetails user, long iocId) {
    LOGGER.debug("Trying to delete IOC, id: [{}]", iocId);

    try {
      iocService.deleteIoc(user, iocId);
      return new ResponseEntity(HttpStatus.NO_CONTENT);
    } catch (EntityNotFoundException | EmptyResultDataAccessException e) {
      LOGGER.error("IOC couldn't be found by Id [{}] to delete", iocId, e);
      throw new EntityNotFoundException("IOC", iocId);
    } catch (OperationNotAllowedException e) {
      LOGGER.error(e.getMessage(), e);
      throw e;
    } catch (UnauthorizedException e) {
      LOGGER.error("IOC is not allowed to be deleted because lack of rights", e);
      throw e;
    } catch (GitlabServiceException e) {
      LOGGER.error("Gitlab repo can not be deleted", e);
      throw e;
    } catch (Exception e) {
      LOGGER.error(e.getMessage(), e);
      throw new ServiceException("Error while trying to delete IOC", e);
    }
  }

  @Override
  public Ioc updateIoc(UserDetails user, long iocId, IOCUpdateRequest updateBody) {
    try {
      IocEntity ioc = iocService.findIocById(iocId);
      return iocService.checkMetadata(ioc, updateBody, user);
    } catch (DataIntegrityViolationException e) {
      LOGGER.error(e.getMessage(), e);
      throw transformDataIntegrityException(e);
    } catch (UnauthorizedException
        | UnprocessableEntityException
        | AccessDeniedException
        | OperationNotAllowedException
        | ConflictException
        | EntityNotFoundException e) {
      LOGGER.error(e.getMessage(), e);
      throw e;
    } catch (GitlabServiceException e) {
      LOGGER.error("Error while trying to check permissions for updating IOC", e);
      throw new RemoteException(
          "Permission check exception", "Can not check users permission in GitLab");
    } catch (Exception e) {
      LOGGER.error("Error while trying to update IOC", e);
      throw new ServiceException("Error while trying to update IOC");
    }
  }

  @Override
  public void unDeployInDb(UserDetails user, long iocId) {
    LOGGER.debug("Trying to mark IOC as undeployed in DB, iocId: [{}]", iocId);
    try {
      IocEntity iocentity = iocService.findIocById(iocId);

      deploymentService.checkConcurrency(iocentity);

      deploymentService.undeployInDb(iocId);

    } catch (EntityNotFoundException e) {
      LOGGER.error("IOC not found to mark it as undeployed in the DB, iocId: " + iocId, e);
      throw e;
    } catch (IocNotDeployedException e) {
      LOGGER.error("IOC is not deployed, can not mark it as undeployed in DB, iocId: " + iocId, e);
      throw e;
    } catch (ConcurrentOperationFoundException e) {
      LOGGER.error(
          "Ongoing operation for IOC, can not mark it as undeployed in DB, iocId: " + iocId, e);
      throw e;
    } catch (Exception e) {
      LOGGER.error("Error while trying to mark IOC as undeployed in DB, iocId: " + iocId, e);
      throw new ServiceException("Error while trying to mark IOC as undeployed");
    }
  }

  @Override
  public Ioc updateActiveDeploymentHost(UserDetails user, long iocId, UpdateHostRequest body) {
    LOGGER.debug("Trying to update IOC deployment host in DB, iocId: [{}]", iocId);
    try {
      IocEntity iocEntity = iocService.findIocById(iocId);

      deploymentService.checkConcurrency(iocEntity);

      IocDeploymentEntity activeDeployment =
          deploymentService.findActiveDeploymentEntityForIocId(iocId);

      if (activeDeployment != null) {
        deploymentService.updateActiveDeploymentHost(
            iocEntity, activeDeployment, body.getHostId(), user.getUserName());
        return iocService.searchIocById(iocId);
      } else {
        throw new OperationNotAllowedException(
            "Deployment host modification in database is not allowed, IOC has no active deployment");
      }

    } catch (EntityNotFoundException e) {
      LOGGER.error("IOC host modification error, IOC not found with ID: " + iocId, e);
      throw e;
    } catch (IocNotDeployedException e) {
      LOGGER.error("IOC is not deployed, cannot modify deployment host in DB, IOC ID: " + iocId, e);
      throw e;
    } catch (ConcurrentOperationFoundException e) {
      LOGGER.error("Ongoing operation for IOC, cannot modify deployment host, IOC ID: " + iocId, e);
      throw e;
    } catch (Exception e) {
      LOGGER.error("Error while trying to modify deployment host in DB, IOC ID: " + iocId, e);
      throw new ServiceException("Error while trying to modify deployment host");
    }
  }

  private ConstraintViolationException transformDataIntegrityException(
      DataIntegrityViolationException e) {
    return new ConstraintViolationException(
        "Error while trying to create new IOC: database constraint violation - " + e.getMessage());
  }

  private void checkIocCreateBody(CreateIoc ioc) {
    if (ioc.getNamingUuid() == null) {
      throw new IncompleteRequestException("Valid naming-name has to be selected!");
    }
    if ((ioc.getGitProjectId() == null) && (StringUtils.isEmpty(ioc.getRepoName()))) {
      throw new IncompleteRequestException("Git project ID required!");
    }

    if (StringUtils.isNotEmpty(ioc.getRepoName())) {
      Pattern pattern = Pattern.compile(ValidationConstants.REPO_NAME_REGEX);
      Matcher matcher = pattern.matcher(ioc.getRepoName());

      if (!matcher.matches()) {
        throw new InputValidationException(ValidationConstants.REPO_NAME_CHECK_ERR_MESSAGE);
      }
    }
  }

  @Override
  public IdUsedByIoc checkGitRepoId(long repoId) throws GitlabServiceException {
    try {
      IocEntity iocEnt = iocService.findIocByRepoId(repoId);

      Long iocId = null;

      if (iocEnt != null) {
        iocId = iocEnt.getId();
      }

      return new IdUsedByIoc(iocId);
    } catch (Exception e) {
      LOGGER.error("Error while trying to find IOC by Git repo Id", e);
      throw new ServiceException("Error while trying to find IOC by Git repo Id");
    }
  }

  @Override
  public IocStatusResponse fetchIocStatus(long iocId) {
    try {
      return iocService.getStatus(iocId);
    } catch (EntityNotFoundException e) {
      LOGGER.error("IOC not found to check status", e);
      throw e;
    } catch (Exception e) {
      LOGGER.error("Error while trying to get fetch IOC status", e);
      throw new ServiceException("Error while trying get fetch IOC status");
    }
  }

  @Override
  public IocAlertResponse fetchIocAlerts(long iocId) {
    try {
      return iocService.getAlerts(iocId);
    } catch (EntityNotFoundException e) {
      LOGGER.error("IOC not found to fetch alerts", e);
      throw e;
    } catch (Exception e) {
      LOGGER.error("Error while trying to fetch IOC alerts", e);
      throw new ServiceException("Error while trying to fetch IOC alerts");
    }
  }

  @Override
  public LokiResponse fetchProcServLogLines(String hostName, String iocName, Integer timeRange) {
    try {
      return lokiService.getLogLines(hostName, iocName, timeRange);
    } catch (LokiServiceException e) {
      LOGGER.error("Remote exception while trying to fetch ProcServ logs from Loki", e);
      throw e;
    } catch (Exception e) {
      LOGGER.error("Error while trying to get ProcServ logs from Loki", e);
      throw new ServiceException("Error while trying to get ProcServ logs");
    }
  }
}
