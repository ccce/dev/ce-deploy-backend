package eu.ess.ics.ccce.rest.model.inventory.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Map;

public class MetaStructure {
  @JsonProperty("hostvars")
  private Map<String, IOCInventoryList> hostVars;

  public MetaStructure(Map<String, IOCInventoryList> hostVars) {
    this.hostVars = hostVars;
  }

  public Map<String, IOCInventoryList> getHostVars() {
    return hostVars;
  }

  public void setHostVars(Map<String, IOCInventoryList> hostVars) {
    this.hostVars = hostVars;
  }
}
