package eu.ess.ics.ccce.repository.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import java.util.Date;
import org.hibernate.annotations.Immutable;
import org.hibernate.annotations.Subselect;
import org.hibernate.annotations.Synchronize;

@Entity
@Immutable
@Synchronize("ioc_deployment")
@Subselect(
    "SELECT d1.ioc AS ioc_id, MAX(d1.created_at) AS last_deployment_time"
        + " FROM ioc_deployment d1"
        + " GROUP BY d1.ioc")
public class LatestDeploymentView {

  @Id private Long iocId;

  private Date lastDeploymentTime;

  public Long getIocId() {
    return iocId;
  }

  public Date getLastDeploymentTime() {
    return lastDeploymentTime;
  }
}
