/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.repository.impl;

import static eu.ess.ics.ccce.common.Constants.UNDEPLOYMENT_TYPES;

import eu.ess.ics.ccce.common.Constants;
import eu.ess.ics.ccce.repository.api.IIocDeploymentRepository;
import eu.ess.ics.ccce.repository.entity.AwxJobEntity;
import eu.ess.ics.ccce.repository.entity.IocDeploymentEntity;
import eu.ess.ics.ccce.repository.entity.IocEntity;
import eu.ess.ics.ccce.rest.model.job.request.DeploymentOrder;
import eu.ess.ics.ccce.rest.model.job.request.JobKind;
import eu.ess.ics.ccce.rest.model.job.request.JobOrder;
import eu.ess.ics.ccce.rest.model.job.request.JobStatusQuery;
import eu.ess.ics.ccce.service.external.awx.model.JobStatus;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.Query;
import jakarta.persistence.TypedQuery;
import jakarta.persistence.criteria.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.stereotype.Repository;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 */
@Repository
public class IocDeploymentRepository implements IIocDeploymentRepository {
  @PersistenceContext private EntityManager em;

  @Override
  public void createDeployment(IocDeploymentEntity deployment) {
    em.persist(deployment);
  }

  @Override
  public void updateDeployment(IocDeploymentEntity deployment) {
    em.merge(deployment);
  }

  @Override
  public List<IocDeploymentEntity> findAll() {
    return em.createQuery("SELECT d FROM IocDeploymentEntity d", IocDeploymentEntity.class)
        .getResultList();
  }

  @Override
  public IocDeploymentEntity findLatestDeploymentForIoc(Long iocId) {
    return em
        .createQuery(
            "SELECT d FROM IocDeploymentEntity d "
                + "WHERE d.ioc.id = :pIocId ORDER BY d.createdAt DESC",
            IocDeploymentEntity.class)
        .setParameter("pIocId", iocId)
        .getResultList()
        .stream()
        .findFirst()
        .orElse(null);
  }

  @Override
  public IocDeploymentEntity findPendingDeploymentForIoc(Long iocId) {
    return em.createQuery(
            "SELECT d FROM IocDeploymentEntity d WHERE d.ioc.id = :pIocId AND "
                + " d.pending = true",
            IocDeploymentEntity.class)
        .setParameter("pIocId", iocId)
        .getSingleResult();
  }

  @Override
  public List<AwxJobEntity> findOngoingDeploymentsForIoc(IocEntity ioc) {
    return em.createQuery(
            "SELECT c FROM AwxJobEntity c WHERE "
                + "c.deployment.ioc.id = :pId AND c.status NOT IN (:pFinishedStatuses) "
                + "AND c.jobType IN (:pDeploymentTypes) ORDER BY c.createdAt DESC",
            AwxJobEntity.class)
        .setParameter("pId", ioc.getId())
        .setParameter("pFinishedStatuses", Constants.FINISHED_AWX_STATUSES)
        .setParameter("pDeploymentTypes", Constants.ALL_DEPLOYMENT_TYPES)
        .getResultList();
  }

  @Override
  public List<AwxJobEntity> findConcurrentPendingAwxJobsForDeployments(
      List<IocDeploymentEntity> deployments) {
    List<Long> iocIds =
        deployments.stream().map(de -> de.getIoc().getId()).collect(Collectors.toList());
    return em.createQuery(
            "SELECT j FROM AwxJobEntity j WHERE j.deployment.ioc.id IN (:pIocIds) AND "
                + "j.status = 'PENDING'",
            AwxJobEntity.class)
        .setParameter("pIocIds", iocIds)
        .getResultList();
  }

  @Override
  public List<IocDeploymentEntity> findAllPendingOrQueuedDeployments() {
    return em.createQuery(
            "SELECT d FROM IocDeploymentEntity d WHERE "
                + " (d.pending = true OR d.awxJob.status = :pQueuedStatus)",
            IocDeploymentEntity.class)
        .setParameter("pQueuedStatus", JobStatus.QUEUED_BY_BACKEND.name())
        .getResultList();
  }

  @Override
  public List<AwxJobEntity> findAllPendingAwxJobs() {
    return em.createQuery(
            "SELECT j FROM AwxJobEntity j WHERE " + " j.status IN :pendingStatuses",
            AwxJobEntity.class)
        .setParameter("pendingStatuses", Constants.PENDING_STATUSES)
        .getResultList();
  }

  @Override
  public IocDeploymentEntity findFirstQueued() {
    return em
        .createQuery(
            "SELECT d FROM IocDeploymentEntity d WHERE d.awxJob.status = :pQueuedStatus ORDER BY d.createdAt ASC",
            IocDeploymentEntity.class)
        .setParameter("pQueuedStatus", JobStatus.QUEUED_BY_BACKEND.name())
        .getResultList()
        .stream()
        .findFirst()
        .orElse(null);
  }

  @Override
  public AwxJobEntity findFirstQueuedAwxJob() {
    return em
        .createQuery(
            "SELECT j FROM AwxJobEntity j WHERE j.status = :pQueuedStatus ORDER BY j.createdAt ASC",
            AwxJobEntity.class)
        .setParameter("pQueuedStatus", JobStatus.QUEUED_BY_BACKEND.name())
        .getResultList()
        .stream()
        .findFirst()
        .orElse(null);
  }

  @Override
  public List<IocDeploymentEntity> findDeploymentsForBatchJob(long jobId) {
    return em.createQuery(
            "SELECT d From IocDeploymentEntity  d WHERE d.awxJob.id = :jobId",
            IocDeploymentEntity.class)
        .setParameter("jobId", jobId)
        .getResultList();
  }

  @Override
  public IocDeploymentEntity findDeploymentById(long deploymentId) {
    return em.createQuery(
            "SELECT d FROM IocDeploymentEntity d WHERE d.id = :pId", IocDeploymentEntity.class)
        .setParameter("pId", deploymentId)
        .getSingleResult();
  }

  private <T, R> TypedQuery<T> pagingQueryDeployments(
      CriteriaBuilder cb,
      CriteriaQuery<T> select,
      Root<R> from,
      String searchText,
      Long iocId,
      Long hostCSEntryId,
      String status,
      String user,
      CriteriaQuery<T> cq,
      DeploymentOrder orderBy,
      Boolean isAsc) {
    List<Predicate> predicates = new ArrayList<>();
    Join<AwxJobEntity, IocDeploymentEntity> awxJob = from.join("awxJob", JoinType.LEFT);

    if (searchText != null) {
      predicates.add(
          cb.or(
              cb.like(cb.lower(from.get("namingName")), "%" + searchText.toLowerCase() + "%"),
              cb.like(cb.lower(from.get("sourceVersion")), "%" + searchText.toLowerCase() + "%"),
              cb.like(cb.lower(from.get("createdBy")), "%" + searchText.toLowerCase() + "%")));
    }

    if (iocId != null) {
      predicates.add(cb.equal(from.get("ioc"), iocId));
    }

    if (hostCSEntryId != null) {
      predicates.add(cb.equal(from.get("hostExternalId"), hostCSEntryId));
    }

    if (user != null) {
      Predicate userPredicate =
          cb.like(cb.lower(from.get("createdBy")), "%" + user.toLowerCase() + "%");
      predicates.add(userPredicate);
    }

    if (status != null) {
      Predicate statusPredicate;

      if (JobStatusQuery.FINISHED.name().equalsIgnoreCase(status)) {
        statusPredicate = cb.in(awxJob.get("status")).value(Constants.FINISHED_AWX_STATUSES);
      } else if (JobStatusQuery.QUEUED.name().equalsIgnoreCase(status)) {
        statusPredicate = cb.in(awxJob.get("status")).value(Constants.QUEUED_STATUSES);
      } else {
        statusPredicate = cb.equal(awxJob.get("status"), status);
      }

      predicates.add(statusPredicate);
    }

    select.where(cb.and(predicates.toArray(new Predicate[0])));
    return em.createQuery(cq);
  }

  @Override
  public void createAwxJob(AwxJobEntity awxJob) {
    em.persist(awxJob);
  }

  @Override
  public void updateAwxJob(AwxJobEntity awxJob) {
    em.merge(awxJob);
  }

  @Override
  public AwxJobEntity findByAwxJobId(long awxId) {
    return em.createQuery(
            "SELECT j from AwxJobEntity j " + " WHERE j.awxJobId = :pAwJobId", AwxJobEntity.class)
        .setParameter("pAwJobId", awxId)
        .getSingleResult();
  }

  @Override
  public AwxJobEntity findJobById(long id) {
    return em.createQuery(
            "SELECT j from AwxJobEntity j " + " WHERE j.id = :pId", AwxJobEntity.class)
        .setParameter("pId", id)
        .getSingleResult();
  }

  @Override
  public long countAllDeployments() {
    Query query =
        em.createQuery(
                "SELECT COUNT(*) FROM IocDeploymentEntity d WHERE d.deploymentType NOT IN (:undeploymentTypes)")
            .setParameter("undeploymentTypes", UNDEPLOYMENT_TYPES);
    return (Long) query.getSingleResult();
  }

  @Override
  public long countSuccessfulDeployments(LocalDate requestedDay) {
    Query query =
        em.createQuery(
            "SELECT COUNT(*) FROM IocDeploymentEntity d WHERE d.awxJob.awxJobFinishedAt IS NOT NULL"
                + " AND YEAR(d.awxJob.awxJobFinishedAt)=:pFinishYear AND MONTH(d.awxJob.awxJobFinishedAt)=:pFinishMonth "
                + " AND DAY(d.awxJob.awxJobFinishedAt)=:pFinishDay "
                + " AND d.awxJob.status=:pJobState");
    query.setParameter("pFinishDay", requestedDay.getDayOfMonth());
    query.setParameter("pFinishYear", requestedDay.getYear());
    query.setParameter("pFinishMonth", requestedDay.getMonthValue());
    query.setParameter("pJobState", JobStatus.SUCCESSFUL.toString());

    return (Long) query.getSingleResult();
  }

  @Override
  public long countAllDeploymentsForIoc(long iocId) {
    return em.createQuery(
            "SELECT COUNT(*) FROM IocDeploymentEntity d WHERE d.ioc.id = :pIocId", Long.class)
        .setParameter("pIocId", iocId)
        .getSingleResult();
  }

  @Override
  public List<AwxJobEntity> findAllOngoing() {
    return em.createQuery(
            "SELECT j from AwxJobEntity j" + " WHERE j.status NOT IN (:pFinishedStates)",
            AwxJobEntity.class)
        .setParameter("pFinishedStates", Constants.FINISHED_AWX_STATUSES)
        .getResultList();
  }

  @Override
  public long countAllDeployments(LocalDate requestedDay) {
    Query query =
        em.createQuery(
            "SELECT COUNT(*) FROM IocDeploymentEntity d WHERE d.awxJob.awxJobFinishedAt IS NOT NULL"
                + " AND YEAR(d.awxJob.awxJobFinishedAt)=:pFinishYear AND MONTH(d.awxJob.awxJobFinishedAt)=:pFinishMonth "
                + " AND DAY(d.awxJob.awxJobFinishedAt)=:pFinishDay");
    query.setParameter("pFinishDay", requestedDay.getDayOfMonth());
    query.setParameter("pFinishYear", requestedDay.getYear());
    query.setParameter("pFinishMonth", requestedDay.getMonthValue());

    return (Long) query.getSingleResult();
  }

  @Override
  public List<AwxJobEntity> findAllAwxJobs(
      Long iocId,
      JobKind type,
      String status,
      String user,
      String query,
      Date startDate,
      Date endDate,
      Long netBoxId,
      Boolean isVm,
      JobOrder orderBy,
      Boolean isAsc,
      int page,
      int limit) {
    CriteriaBuilder cb = em.getCriteriaBuilder();

    CriteriaQuery<AwxJobEntity> cq = cb.createQuery(AwxJobEntity.class);
    Root<AwxJobEntity> from = cq.from(AwxJobEntity.class);

    CriteriaQuery<AwxJobEntity> select = cq.select(from);

    if (orderBy != null) {
      if (BooleanUtils.toBoolean(isAsc)) {
        cq.orderBy(cb.asc(from.get(orderBy.columnName)));
      } else {
        cq.orderBy(cb.desc(from.get(orderBy.columnName)));
      }
    } else {
      if (BooleanUtils.toBoolean(isAsc)) {
        cq.orderBy(cb.asc(from.get("createdAt")));
      } else {
        cq.orderBy(cb.desc(from.get("createdAt")));
      }
    }

    TypedQuery<AwxJobEntity> q =
        pagingQueryAwxOperations(
            cb, select, from, cq, iocId, type, status, user, query, startDate, endDate, netBoxId,
            isVm, orderBy, isAsc);
    q.setFirstResult(page * limit);
    q.setMaxResults(limit);

    return q.getResultList();
  }

  private <T, R> TypedQuery<T> pagingQueryAwxOperations(
      CriteriaBuilder cb,
      CriteriaQuery<T> select,
      Root<R> from,
      CriteriaQuery<T> cq,
      Long iocId,
      JobKind type,
      String status,
      String user,
      String query,
      Date startDate,
      Date endDate,
      Long netBoxId,
      Boolean isVm,
      JobOrder orderBy,
      Boolean isAsc) {
    List<Predicate> predicates = new ArrayList<>();

    if (query != null) {
      predicates.add(
          cb.or(
              cb.like(
                  cb.lower(from.get("deployment").get("namingName")),
                  "%" + query.toLowerCase() + "%"),
              cb.like(cb.lower(from.get("createdBy")), "%" + query.toLowerCase() + "%")));
    }

    if (iocId != null) {
      predicates.add(cb.equal(from.get("deployment").get("ioc").get("id"), iocId));
    }

    if (type != null) {
      Predicate typePredicate;

      if (JobKind.DEPLOYMENT.equals(type)) {
        typePredicate = cb.in(from.get("jobType")).value(Constants.ALL_DEPLOYMENT_TYPES);
      } else {
        typePredicate = cb.in(from.get("jobType")).value(Constants.COMMAND_TYPES);
      }

      predicates.add(typePredicate);
    }

    if (user != null) {
      Predicate userPredicate =
          cb.like(cb.lower(from.get("createdBy")), "%" + user.toLowerCase() + "%");
      predicates.add(userPredicate);
    }

    if (startDate != null) {
      predicates.add(cb.greaterThanOrEqualTo(from.get("finishedAt"), startDate));
    }

    if (endDate != null) {
      predicates.add(cb.lessThanOrEqualTo(from.get("finishedAt"), endDate));
    }

    if (netBoxId != null && isVm != null) {
      predicates.add(cb.equal(from.get("deployment").get("netBoxHostId"), netBoxId));
      predicates.add(cb.equal(from.get("deployment").get("isVm"), isVm));
    }

    if (status != null) {
      Predicate statusPredicate;

      if (JobStatusQuery.FINISHED.name().equalsIgnoreCase(status)) {
        statusPredicate = cb.in(from.get("status")).value(Constants.FINISHED_AWX_STATUSES);
      } else if (JobStatusQuery.ONGOING.name().equalsIgnoreCase(status)) {
        statusPredicate = cb.not(cb.in(from.get("status")).value(Constants.FINISHED_AWX_STATUSES));
      } else if (JobStatusQuery.QUEUED.name().equalsIgnoreCase(status)) {
        statusPredicate = cb.in(from.get("status")).value(Constants.QUEUED_STATUSES);
      } else {
        statusPredicate = cb.equal(from.get("status"), status);
      }

      predicates.add(statusPredicate);
    }

    select.where(cb.and(predicates.toArray(new Predicate[0])));
    return em.createQuery(cq);
  }

  @Override
  public long countJobsForPaging(
      Long iocId,
      JobKind type,
      String status,
      String user,
      String query,
      Date startDate,
      Date endDate,
      Long netBoxId,
      Boolean isVm) {
    CriteriaBuilder cb = em.getCriteriaBuilder();

    CriteriaQuery<Long> cq = cb.createQuery(Long.class);
    Root<AwxJobEntity> from = cq.from(AwxJobEntity.class);
    Expression<Long> count1 = cb.count(from);
    CriteriaQuery<Long> select = cq.select(count1);

    return pagingQueryAwxOperations(
            cb, select, from, cq, iocId, type, status, user, query, startDate, endDate, netBoxId,
            isVm, null, null)
        .getSingleResult();
  }

  @Override
  public List<AwxJobEntity> findOngoingCommandsByIoc(IocEntity ioc) {
    return em.createQuery(
            "SELECT c FROM AwxJobEntity c WHERE "
                + "c.deployment.ioc.id = :pId AND c.status NOT IN (:pFinishedStatuses) "
                + "AND c.jobType IN (:pCommandTypes) ORDER BY c.createdAt DESC",
            AwxJobEntity.class)
        .setParameter("pId", ioc.getId())
        .setParameter("pFinishedStatuses", Constants.FINISHED_AWX_STATUSES)
        .setParameter("pCommandTypes", Constants.COMMAND_TYPES)
        .getResultList();
  }

  @Override
  public List<String> findAllHostsWithActualDeployments() {
    return em.createQuery(
            "SELECT DISTINCT(v.latestSuccessfulDeployment.hostFqdn) FROM IocEntity v "
                + " WHERE v.latestSuccessfulDeployment.deploymentType NOT IN (:undeploymentTypes)"
                + " GROUP BY (v.latestSuccessfulDeployment.hostFqdn) ",
            String.class)
        .setParameter("undeploymentTypes", UNDEPLOYMENT_TYPES)
        .getResultList();
  }

  @Override
  public void deleteIocDeployment(IocDeploymentEntity entity) {
    em.remove(entity);
  }

  @Override
  public void deleteAwxJob(AwxJobEntity entity) {
    em.remove(entity);
  }

  @Override
  public long countAwxJobsByType(final String jobType) {
    return em.createQuery(
            "SELECT COUNT(*) FROM AwxJobEntity job WHERE job.jobType = :pJobType", Long.class)
        .setParameter("pJobType", jobType)
        .getSingleResult();
  }

  @Override
  public long countAwxJobsByTypeAndStatus(final String jobType, final List<String> jobStatuses) {
    return em.createQuery(
            "SELECT COUNT(*) FROM AwxJobEntity job WHERE job.jobType = :pJobType AND job.status IN :pJobStatuses",
            Long.class)
        .setParameter("pJobType", jobType)
        .setParameter("pJobStatuses", jobStatuses)
        .getSingleResult();
  }

  @Override
  public List<AwxJobEntity> findAllAwxJobsByIocDeploymentId(Long deploymentId) {
    return em.createQuery(
            "SELECT j FROM AwxJobEntity j WHERE j.deployment.id = :deploymentId",
            AwxJobEntity.class)
        .setParameter("deploymentId", deploymentId)
        .getResultList();
  }

  @Override
  public List<IocDeploymentEntity> findAllByIocId(Long iocId) {
    return em.createQuery(
            "SELECT d FROM IocDeploymentEntity d " + "WHERE d.ioc.id = :pIocId",
            IocDeploymentEntity.class)
        .setParameter("pIocId", iocId)
        .getResultList();
  }

  @Override
  public List<AwxJobEntity> findAllQueuedOrPendingCommands() {
    return em.createQuery(
            "SELECT j FROM AwxJobEntity j "
                + "WHERE j.status IN (:queuedStatuses) AND j.jobType IN (:commandTypes)",
            AwxJobEntity.class)
        .setParameter("queuedStatuses", Constants.QUEUED_STATUSES)
        .setParameter("commandTypes", Constants.COMMAND_TYPES)
        .getResultList();
  }

  @Override
  public List<AwxJobEntity> findAllQueuedOrPendingCommandsForIoc(IocEntity ioc) {
    return em.createQuery(
            "SELECT j FROM AwxJobEntity j "
                + "WHERE j.status IN (:queuedStatuses) AND j.jobType IN (:commandTypes) AND "
                + "j.deployment.ioc.id = :iocId",
            AwxJobEntity.class)
        .setParameter("iocId", ioc.getId())
        .setParameter("queuedStatuses", Constants.QUEUED_STATUSES)
        .setParameter("commandTypes", Constants.COMMAND_TYPES)
        .getResultList();
  }

  @Override
  public long countAllEntities() {
    return em.createQuery("SELECT COUNT(*) FROM IocDeploymentEntity", Long.class).getSingleResult();
  }

  @Override
  public List<IocDeploymentEntity> listEntities(int page, int limit) {
    return em.createQuery("FROM IocDeploymentEntity", IocDeploymentEntity.class)
        .setFirstResult(limit * page)
        .setMaxResults(limit)
        .getResultList();
  }

  @Override
  public void deleteNeverDeployedWithPrefix(String iocPrefix) {
    em.createQuery(
            "DELETE FROM IocEntity ie WHERE LOWER(ie.namingName) LIKE LOWER( :pNamePrefix ) "
                + " AND ie NOT IN (SELECT de.ioc FROM IocDeploymentEntity de )")
        .setParameter("pNamePrefix", iocPrefix + "%")
        .executeUpdate();
  }

  @Override
  public void deleteIocsFromANetwork(List<Long> iocIds) {
    if (iocIds.isEmpty()) {
      return;
    }

    // remove cross reference to be able to delete
    em.createQuery(
            "UPDATE IocDeploymentEntity de SET de.awxJob = NULL WHERE de.ioc.id IN ( :pIdList )")
        .setParameter("pIdList", iocIds)
        .executeUpdate();

    em.createQuery(
            "DELETE FROM AwxJobEntity aj WHERE aj.deployment.id IN "
                + " ( SELECT de.id FROM  IocDeploymentEntity de WHERE de.ioc.id IN ( :pIdList ))")
        .setParameter("pIdList", iocIds)
        .executeUpdate();

    // remove cross reference to be able to delete
    em.createQuery(
            "UPDATE IocEntity ie SET ie.latestSuccessfulDeployment = NULL WHERE ie.id IN ( :pIdList )")
        .setParameter("pIdList", iocIds)
        .executeUpdate();

    em.createQuery("DELETE FROM IocDeploymentEntity de WHERE de.ioc.id IN ( :pIdList )")
        .setParameter("pIdList", iocIds)
        .executeUpdate();

    em.createQuery("DELETE FROM IocEntity ie WHERE ie.id IN ( :pIdList )")
        .setParameter("pIdList", iocIds)
        .executeUpdate();
  }
}
