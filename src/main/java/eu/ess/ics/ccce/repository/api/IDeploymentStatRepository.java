/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.repository.api;

import eu.ess.ics.ccce.repository.entity.DeploymentStatisticsEntity;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import org.springframework.transaction.annotation.Transactional;

/** Class used for generating statistics about deployments (for stakeholders) */
public interface IDeploymentStatRepository {
  @Transactional
  void saveStatistics(DeploymentStatisticsEntity statistics);

  /**
   * Finds all statistics entry (that is generated every night) that has been stored after a certain
   * date. (Including that day in the parameter)
   *
   * @param fromDay from which day should the deployment statistics be collected
   * @return List of deployment statistics after a certain date
   */
  List<DeploymentStatisticsEntity> collectDeploymentStat(Date fromDay);

  /**
   * Counts from the deployment statistics DB table how many IOCs were running on a certain date
   *
   * @param actualDate for which date should the running IOC count be collected
   * @return the number of running IOCs on a certain date - according to deployment statistics DB
   *     table
   */
  Long countActiveIocs(Date actualDate);

  /**
   * Counts how many successful deployments were made before the specific date. (Does not count
   * unsuccessful, unfinished deployments or undeployments. The response will contain deployment
   * statistics including the date that is in the parameter.)
   *
   * @param actualDate the last date for which the statistics should be calculated
   * @return number of successful deployments before the specific date
   */
  Long countSuccessfulIocDeployments(LocalDate actualDate);

  Long countAllIocDeployments(LocalDate actualDate);

  Long countAllCommands(LocalDate actualDate);
}
