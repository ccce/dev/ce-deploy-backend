/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.repository.entity;

import eu.ess.ics.ccce.common.Utils;
import eu.ess.ics.ccce.service.external.netbox.model.NetBoxHostIdentifier;
import jakarta.persistence.*;
import java.util.Date;
import org.hibernate.annotations.CreationTimestamp;

/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 */
@Entity
@Table(name = "ioc_deployment")
public class IocDeploymentEntity {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @ManyToOne
  @JoinColumn(name = "ioc")
  private IocEntity ioc;

  @Column(name = "host_fqdn")
  private String hostFqdn;

  @Column(name = "host_name")
  private String hostName;

  @Column(name = "host_network")
  private String hostNetwork;

  @Column(name = "created_by")
  private String createdBy;

  @CreationTimestamp
  @Column(name = "created_at")
  private Date createdAt;

  /** This is variable is set to true when the AWX job is being queued on the AWX side. */
  @Column(name = "pending")
  private Boolean pending;

  @Column(name = "git_project_id")
  private Long gitProjectId;

  @Column(name = "source_version")
  private String sourceVersion;

  @Column(name = "naming_name")
  private String namingName;

  @OneToOne
  @JoinColumn(name = "job_id")
  private AwxJobEntity awxJob;

  @Column(name = "git_project_url")
  private String gitProjectUrl;

  @Column(name = "netbox_host_id")
  private Long netBoxHostId;

  @Column(name = "is_vm")
  private Boolean isVm;

  @Column(name = "last_known_host_name")
  private String lastKnownHostName;

  @Column(name = "deployment_type")
  private String deploymentType;

  public void setId(Long id) {
    this.id = id;
  }

  public Long getId() {
    return id;
  }

  public IocEntity getIoc() {
    return ioc;
  }

  public void setIoc(IocEntity ioc) {
    this.ioc = ioc;
  }

  public String getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

  public Date getCreatedAt() {
    return Utils.cloneDate(createdAt);
  }

  public void setCreatedAt(Date createdAt) {
    this.createdAt = Utils.cloneDate(createdAt);
  }

  public Boolean isPending() {
    return pending;
  }

  public void setPending(Boolean pending) {
    this.pending = pending;
  }

  public String getDeploymentType() {
    return deploymentType;
  }

  public void setDeploymentType(String deploymentType) {
    this.deploymentType = deploymentType;
  }

  public Long getGitProjectId() {
    return gitProjectId;
  }

  public void setGitProjectId(Long gitProjectId) {
    this.gitProjectId = gitProjectId;
  }

  public String getSourceVersion() {
    return sourceVersion;
  }

  public void setSourceVersion(String sourceVersion) {
    this.sourceVersion = sourceVersion;
  }

  public String getNamingName() {
    return namingName;
  }

  public void setNamingName(String namingName) {
    this.namingName = namingName;
  }

  public AwxJobEntity getAwxJob() {
    return awxJob;
  }

  public void setAwxJob(AwxJobEntity awxJob) {
    this.awxJob = awxJob;
  }

  public String getHostFqdn() {
    return hostFqdn;
  }

  public void setHostFqdn(String hostFqdn) {
    this.hostFqdn = hostFqdn;
  }

  public String getHostName() {
    return hostName;
  }

  public void setHostName(String hostName) {
    this.hostName = hostName;
  }

  public String getHostNetwork() {
    return hostNetwork;
  }

  public void setHostNetwork(String hostNetwork) {
    this.hostNetwork = hostNetwork;
  }

  public String getGitProjectUrl() {
    return gitProjectUrl;
  }

  public void setGitProjectUrl(String gitProjectUrl) {
    this.gitProjectUrl = gitProjectUrl;
  }

  public Long getNetBoxHostId() {
    return netBoxHostId;
  }

  public void setNetBoxHostId(Long netBoxHostId) {
    this.netBoxHostId = netBoxHostId;
  }

  public Boolean isVm() {
    return isVm;
  }

  public void setVm(Boolean vm) {
    isVm = vm;
  }

  public String getLastKnownHostName() {
    return lastKnownHostName;
  }

  public void setLastKnownHostName(String lastKnownHostName) {
    this.lastKnownHostName = lastKnownHostName;
  }

  public NetBoxHostIdentifier getNetBoxHostIdentifier() {
    return new NetBoxHostIdentifier(netBoxHostId, isVm);
  }

  @Override
  public String toString() {
    return "IOC Deployment record -"
        + ", IOC: "
        + ioc.getId()
        + ", PENDING: "
        + pending
        + ", GIT PROJECT ID: "
        + gitProjectId
        + ", SOURCE VERSION: "
        + sourceVersion
        + ", NAMING NAME: "
        + namingName;
  }
}
