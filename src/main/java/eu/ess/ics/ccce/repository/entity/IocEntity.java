/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.repository.entity;

import eu.ess.ics.ccce.common.Utils;
import jakarta.persistence.*;
import java.util.Date;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 */
@Entity
@Table(name = "ioc")
public class IocEntity {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @CreationTimestamp
  @Column(name = "created_at")
  private Date createdAt;

  @Column(name = "created_by")
  private String createdBy;

  @Column(name = "git_project_id")
  private Long gitProjectId;

  @Column(name = "naming_name")
  private String namingName;

  @Column(name = "naming_uuid")
  private String namingUuid;

  @ManyToOne()
  @Fetch(FetchMode.JOIN)
  @JoinColumn(name = "latest_successful_deployment")
  private IocDeploymentEntity latestSuccessfulDeployment;

  @Column(name = "deployed_with_old_playbook")
  private Boolean deployedWithOldPlaybook;

  public void setId(Long id) {
    this.id = id;
  }

  public Long getId() {
    return id;
  }

  public Date getCreatedAt() {
    return Utils.cloneDate(createdAt);
  }

  public void setCreatedAt(Date createdAt) {
    this.createdAt = Utils.cloneDate(createdAt);
  }

  public String getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

  public Long getGitProjectId() {
    return gitProjectId;
  }

  public void setGitProjectId(Long gitProjectId) {
    this.gitProjectId = gitProjectId;
  }

  public String getNamingName() {
    return namingName;
  }

  public void setNamingName(String namingName) {
    this.namingName = namingName;
  }

  public String getNamingUuid() {
    return namingUuid;
  }

  public void setNamingUuid(String namingUuid) {
    this.namingUuid = namingUuid;
  }

  public IocDeploymentEntity getLatestSuccessfulDeployment() {
    return latestSuccessfulDeployment;
  }

  public void setLatestSuccessfulDeployment(IocDeploymentEntity latestSuccessfulDeployment) {
    this.latestSuccessfulDeployment = latestSuccessfulDeployment;
  }

  public Boolean isDeployedWithOldPlaybook() {
    return deployedWithOldPlaybook;
  }

  public void setDeployedWithOldPlaybook(Boolean deployedWithOldPlaybook) {
    this.deployedWithOldPlaybook = deployedWithOldPlaybook;
  }
}
