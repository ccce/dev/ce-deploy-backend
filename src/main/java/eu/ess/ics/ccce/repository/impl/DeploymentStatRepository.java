/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.repository.impl;

import static eu.ess.ics.ccce.common.Constants.UNDEPLOYMENT_TYPES;

import eu.ess.ics.ccce.repository.api.IDeploymentStatRepository;
import eu.ess.ics.ccce.repository.entity.DeploymentStatisticsEntity;
import eu.ess.ics.ccce.service.external.awx.model.JobStatus;
import eu.ess.ics.ccce.service.internal.deployment.dto.AwxJobType;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import org.springframework.stereotype.Repository;

/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 */
@Repository
public class DeploymentStatRepository implements IDeploymentStatRepository {

  @PersistenceContext private EntityManager em;

  @Override
  public void saveStatistics(DeploymentStatisticsEntity statistics) {
    em.persist(statistics);
  }

  @Override
  public List<DeploymentStatisticsEntity> collectDeploymentStat(Date fromDay) {
    return em.createQuery(
            "SELECT d FROM DeploymentStatisticsEntity d WHERE d.createdAt>=:pReqDay",
            DeploymentStatisticsEntity.class)
        .setParameter("pReqDay", fromDay)
        .getResultList();
  }

  @Override
  public Long countActiveIocs(Date actualDate) {
    List<Long> countList =
        em.createQuery(
                "SELECT SUM(d.iocCount) from DeploymentStatisticsEntity d GROUP BY d.createdAt "
                    + " HAVING (d.createdAt =:pActDate)",
                Long.class)
            .setParameter("pActDate", actualDate)
            .getResultList();

    if (countList.isEmpty()) {
      return 0L;
    }

    return countList.get(0);
  }

  @Override
  public Long countSuccessfulIocDeployments(LocalDate actualDate) {
    List<Long> countList =
        em.createQuery(
                "SELECT COUNT(*) FROM IocDeploymentEntity d "
                    + " WHERE d.deploymentType NOT IN (:undeploymentTypes)"
                    + " AND d.awxJob.status = :pJobStatus"
                    + " AND d.createdAt <= :pActualDate ",
                Long.class)
            .setParameter(
                "pActualDate",
                Date.from(actualDate.atTime(23, 59, 59).atZone(ZoneId.systemDefault()).toInstant()))
            .setParameter("pJobStatus", JobStatus.SUCCESSFUL.toString())
            .setParameter("undeploymentTypes", UNDEPLOYMENT_TYPES)
            .getResultList();

    if (countList.isEmpty()) {
      return 0L;
    }

    return countList.get(0);
  }

  @Override
  public Long countAllIocDeployments(LocalDate actualDate) {
    return em.createQuery(
            "SELECT COUNT(*) FROM IocDeploymentEntity d "
                + " WHERE d.deploymentType NOT IN (:undeploymentTypes) "
                + " AND d.createdAt <= :pActualDate ",
            Long.class)
        .setParameter(
            "pActualDate",
            Date.from(actualDate.atTime(23, 59, 59).atZone(ZoneId.systemDefault()).toInstant()))
        .setParameter("undeploymentTypes", UNDEPLOYMENT_TYPES)
        .getSingleResult();
  }

  @Override
  public Long countAllCommands(LocalDate actualDate) {

    return em.createQuery(
            "SELECT COUNT(*) FROM AwxJobEntity a "
                + " WHERE a.jobType IN (:pCommandType) "
                + " AND a.deployment.createdAt <= :pActualDate ",
            Long.class)
        .setParameter(
            "pActualDate",
            Date.from(actualDate.atTime(23, 59, 59).atZone(ZoneId.systemDefault()).toInstant()))
        .setParameter(
            "pCommandType", Arrays.asList(AwxJobType.START.toString(), AwxJobType.STOP.toString()))
        .getSingleResult();
  }
}
