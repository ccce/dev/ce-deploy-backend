/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.repository.api;

import eu.ess.ics.ccce.repository.entity.IocDeploymentEntity;
import eu.ess.ics.ccce.repository.entity.IocEntity;
import eu.ess.ics.ccce.rest.model.ioc.request.IocDeploymentStatus;
import eu.ess.ics.ccce.rest.model.ioc.request.IocOrder;
import eu.ess.ics.ccce.service.external.netbox.model.NetBoxHostIdentifier;
import java.util.List;
import java.util.Optional;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 */
public interface IIocRepository {

  /**
   * Finds IOC entity in DB by ID
   *
   * @param id the ID of the IOC that has to be found
   * @return the IOC entity that has the specific ID
   */
  IocEntity findById(Long id);

  /**
   * Finds all IOCs that meet the searching parameters. Response is paginated!
   *
   * @param deploymentStatus the IOC deployment status (deployed, non-deployed,...)
   * @param createdBy the user who created the IOC
   * @param searchText searching in naming-name
   * @param orderBy the result should be ordered by which field
   * @param isAsc the order should be and ASC or in DESC
   * @param page the requested pade number
   * @param limit how many entries should appear on the requested page
   * @param listAll fetch all resource?
   * @return List of IOCs that meet the searching parameters
   */
  List<IocEntity> findAll(
      IocDeploymentStatus deploymentStatus,
      String createdBy,
      List<NetBoxHostIdentifier> netBoxHostIds,
      String searchText,
      IocOrder orderBy,
      Boolean isAsc,
      int page,
      Integer limit,
      boolean listAll);

  /**
   * Counts all IOCs that is stored in the DB (Including non-deployed ones)
   *
   * @return number of IOCs created/stored in the DB
   */
  Long countAll();

  /**
   * Counts the number of IOCs that have been created by a specific user (created_by)
   *
   * @param createdBy the user who created the IOC(s)
   * @return number of IOCs that have been created by a specific user (created_by)
   */
  Long countAllByCreatedBy(String createdBy);

  /**
   * Counts how many IOCs are meeting the parameter requirements. Used for pagination.
   *
   * @param deploymentStatus the deployment status (deployed/non-deployed,...)
   * @param createdBy the user who created the IOCs
   * @param searchText searching by naming-name
   * @return number of IOCs that meets the parameter requirements.
   */
  long countForPaging(
      IocDeploymentStatus deploymentStatus,
      String createdBy,
      List<NetBoxHostIdentifier> netBoxHostIds,
      String searchText);

  @Transactional
  void createIoc(IocEntity ioc);

  @Transactional
  void updateIoc(IocEntity iocEnt);

  /**
   * Deletes IOC entry from the DB. (If the IOC has been [previously] deployed the function call
   * will result in exception)
   *
   * @param ioc the IOC entity that has to be deleted from the DB
   */
  @Transactional
  void deleteIoc(IocEntity ioc);

  /**
   * Finding IOCEntry by repository ID
   *
   * @param repoId the ID of the git repository
   * @return IOCEntity that uses the git repo Id
   */
  IocEntity findIocByRepoId(long repoId);

  /**
   * Finding all IOCEntries that use the NamingUUID given as parameter
   *
   * @param namingUUID the NamingUUID that has to be searched
   * @return List of IOCEntries that use the NamingUUID
   */
  List<IocEntity> findIocByNamingUUID(String namingUUID);

  IocEntity findIocByNamingName(String namingName);

  Optional<IocEntity> findOptionalIocByNamingName(String namingName);

  long countIocByNamingName(String namingName);

  /**
   * Finds all successful, active deployment entities that has been done on a certain NetBox ID
   *
   * @param netBoxHostId the host's NetBox ID
   * @return List of deployment entities
   */
  List<IocDeploymentEntity> findAllActualIocDeploymentsByHostId(Long netBoxHostId, Boolean isVm);

  /**
   * Counts all (currently) successful, active deployments are in DB. (Does not count the
   * undeployments!)
   *
   * @return number of successful active entities
   */
  long countActualDeployments();

  /**
   * Counting all successful, active deployment for a certain host. (Not counting the
   * undeployments!)
   *
   * @param netBoxHostId the NetBox ID of the host
   * @return number of active, successful deployments
   */
  long countAssociatedDeployments(Long netBoxHostId, Boolean isVm);

  /**
   * Finds all IOCs with active deployments. (All deployments are finished successfully, and not
   * including the undeployments!)
   *
   * @return all IOCs with active deployments
   */
  List<IocDeploymentEntity> findIocsWithActiveIocDeployments();

  List<String> findNetworkContainingDeployedIocs();

  long countIocsWithActiveIocDeploymentsOnNetwork(String network);

  List<IocDeploymentEntity> findIocsWithActiveIocDeploymentsOnNetwork(String network);

  /**
   * Finds all host IDs that have active IOC deployments on them. (Only successful deployments will
   * appear, and not including the undeployments!)
   *
   * @return List of host IDs that have active deployments on them.
   */
  List<NetBoxHostIdentifier> findAllHostIdWithLatestSuccessfulDeployments();

  /**
   * Finds all host IDs on which IOCs are active and have been created by the specific user. (Will
   * only find host on which IOCs are successfully deployed, not including the undeployments! The
   * user is searched in the CURRENT IOC created_by list, not who was at the time the IOC was
   * deployed!)
   *
   * @param createdBy the user who created the IOCs
   * @return List of host IDs on which the user has active deployments
   */
  List<NetBoxHostIdentifier> findAllHostIdLatestSuccessfulDeploymentForUser(String createdBy);

  List<IocEntity> findIocsByName(List<String> iocNames);

  List<IocDeploymentEntity> findIOCsForInventory();
}
