/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.repository.impl;

import static eu.ess.ics.ccce.common.Constants.DEPLOYMENT_TYPES;
import static eu.ess.ics.ccce.common.Constants.UNDEPLOYMENT_TYPES;

import eu.ess.ics.ccce.repository.api.IIocRepository;
import eu.ess.ics.ccce.repository.entity.IocDeploymentEntity;
import eu.ess.ics.ccce.repository.entity.IocEntity;
import eu.ess.ics.ccce.rest.model.ioc.request.IocDeploymentStatus;
import eu.ess.ics.ccce.rest.model.ioc.request.IocOrder;
import eu.ess.ics.ccce.service.external.awx.model.JobStatus;
import eu.ess.ics.ccce.service.external.netbox.model.NetBoxHostIdentifier;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.Query;
import jakarta.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Repository;

/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 */
@Repository
public class IocRepository implements IIocRepository {

  @PersistenceContext private EntityManager em;

  @Override
  public IocEntity findById(Long id) {
    return em.createQuery("SELECT i FROM IocEntity i WHERE i.id =:pId", IocEntity.class)
        .setParameter("pId", id)
        .getSingleResult();
  }

  @Override
  public List<IocEntity> findAll(
      IocDeploymentStatus deploymentStatus,
      String createdBy,
      List<NetBoxHostIdentifier> netBoxHostIds,
      String searchText,
      IocOrder orderBy,
      Boolean isAsc,
      int page,
      Integer limit,
      boolean listAll) {
    String query = findIocsQuery(deploymentStatus, createdBy, netBoxHostIds, searchText);

    // order by default (created at) field
    if (orderBy == null) {
      if (BooleanUtils.toBoolean(isAsc)) {
        query += " ORDER BY i.createdAt ASC";

      } else {
        query += " ORDER BY i.createdAt DESC";
      }
      // using other field to order by
    } else {
      if (orderBy == IocOrder.ID) {
        query += " ORDER BY i.id";
      }

      if (orderBy == IocOrder.CREATED_BY) {
        query += " ORDER BY i.createdBy";
      }

      if (orderBy == IocOrder.IOC_NAME) {
        query += " ORDER BY i.namingName";
      }

      if (BooleanUtils.toBoolean(isAsc)) {
        query += " ASC";
      } else {
        query += " DESC";
      }
    }

    TypedQuery<IocEntity> response = em.createQuery("SELECT i " + query, IocEntity.class);

    if (listAll) {
      return response.getResultList();
    }

    // paginated list was requested
    return em.createQuery("SELECT i " + query, IocEntity.class)
        .setFirstResult(page * limit)
        .setMaxResults(limit)
        .getResultList();
  }

  @NotNull
  private static String findIocsQuery(
      IocDeploymentStatus deploymentStatus,
      String createdBy,
      List<NetBoxHostIdentifier> netBoxHostIds,
      String searchText) {
    // joining tables
    String query = "FROM IocEntity i ";

    // this join needed only when the latest successful deployment has to be investigated
    if ((deploymentStatus != null) && (!IocDeploymentStatus.ALL.equals(deploymentStatus))) {
      query +=
          " LEFT JOIN IocDeploymentEntity latestSuccessfulDeployment ON latestSuccessfulDeployment.id = i.latestSuccessfulDeployment.id "
              + " LEFT JOIN AwxJobEntity activeJob ON latestSuccessfulDeployment.awxJob.id = activeJob.id ";
    }

    // this join needed only when deployment state has to be investigated
    if ((deploymentStatus != null) && (!IocDeploymentStatus.ALL.equals(deploymentStatus))) {
      query +=
          " LEFT JOIN LatestDeploymentView lastDeploymentPerIoc"
              + " ON i.id = lastDeploymentPerIoc.iocId"
              + " LEFT JOIN IocDeploymentEntity lastDeployment "
              + " ON (lastDeployment.ioc.id = i.id AND lastDeploymentPerIoc.lastDeploymentTime = lastDeployment.createdAt)"
              + " LEFT JOIN AwxJobEntity lastJob ON (lastDeployment.awxJob.id = lastJob.id)";
    }

    // collecting the WHERE clauses into a list that will be joined in the end
    List<String> whereList = new ArrayList<>();

    // never deployed or successfully undeployed IOCs
    String notDeployedIocsQuery =
        "((lastDeployment IS NULL) OR"
            + " (lastDeployment.deploymentType IN ('UNDEPLOY', 'BATCH_UNDEPLOY') AND lastJob.status = 'SUCCESSFUL')) OR"
            + " (latestSuccessfulDeployment IS NULL AND lastDeployment.deploymentType IN ('DEPLOY', 'BATCH_DEPLOY') AND lastJob.status = 'SUCCESSFUL')";

    // looking for never deployed or successfully undeployed IOCs
    if (IocDeploymentStatus.NOT_DEPLOYED.equals(deploymentStatus)) {
      whereList.add(" " + notDeployedIocsQuery);
    }
    // looking for all other IOCs
    else if (IocDeploymentStatus.DEPLOYED.equals(deploymentStatus)) {
      whereList.add(" NOT(" + notDeployedIocsQuery + ")");
      if (netBoxHostIds != null) {
        whereList.add(hostFilterString(netBoxHostIds));
      }
    }

    // using searchText field if user entered something
    if (StringUtils.isNotEmpty(searchText)) {
      whereList.add(" (lower(i.namingName) LIKE lower('%" + searchText + "%'))");
    }

    // use createdBy field if user entered a name
    if (StringUtils.isNotEmpty(createdBy)) {
      whereList.add(" lower(i.createdBy) like lower('%" + createdBy + "%')");
    }

    // no where clause
    if (whereList.isEmpty()) {
      return query;
    }

    return query + " WHERE " + StringUtils.join(whereList, " AND ");
  }

  private static String hostFilterString(List<NetBoxHostIdentifier> netBoxIds) {
    return netBoxIds.stream()
        .map(
            hostId ->
                (" latestSuccessfulDeployment.netBoxHostId = %s "
                        + "AND latestSuccessfulDeployment.isVm = %s")
                    .formatted(hostId.getNetBoxHostId(), hostId.isVm()))
        .collect(Collectors.joining(" OR ", "(", ")"));
  }

  @Override
  public Long countAll() {
    return em.createQuery("SELECT COUNT(*) FROM IocEntity", Long.class).getSingleResult();
  }

  @Override
  public Long countAllByCreatedBy(String createdBy) {
    return em.createQuery(
            "SELECT COUNT(*) FROM IocEntity ioc WHERE ioc.createdBy = :pCreatedBy", Long.class)
        .setParameter("pCreatedBy", createdBy)
        .getSingleResult();
  }

  @Override
  public long countForPaging(
      IocDeploymentStatus deploymentStatus,
      String createdBy,
      List<NetBoxHostIdentifier> netBoxHostIds,
      String searchText) {

    String queryString = findIocsQuery(deploymentStatus, createdBy, netBoxHostIds, searchText);

    return em.createQuery("SELECT COUNT(*) " + queryString, Long.class).getSingleResult();
  }

  @Override
  public void createIoc(IocEntity ioc) {
    em.persist(ioc);
  }

  @Override
  public void updateIoc(IocEntity iocEnt) {
    em.merge(iocEnt);
  }

  @Override
  public void deleteIoc(IocEntity ioc) {
    em.remove(ioc);
  }

  @Override
  public IocEntity findIocByRepoId(long repoId) {
    return em.createQuery(
            "SELECT i FROM IocEntity i" + " WHERE i.gitProjectId =:pGitProjectId", IocEntity.class)
        .setParameter("pGitProjectId", repoId)
        .getResultStream()
        .findFirst()
        .orElse(null);
  }

  @Override
  public List<IocEntity> findIocByNamingUUID(String namingUUID) {
    return em.createQuery(
            "SELECT i FROM IocEntity i" + " WHERE lower(i.namingUuid) = lower(:pNamingUUID)",
            IocEntity.class)
        .setParameter("pNamingUUID", namingUUID)
        .getResultList();
  }

  @Override
  public IocEntity findIocByNamingName(String namingName) {
    return em.createQuery(
            "SELECT i FROM IocEntity i WHERE i.namingName = :pNamingName", IocEntity.class)
        .setParameter("pNamingName", namingName)
        .getSingleResult();
  }

  @Override
  public Optional<IocEntity> findOptionalIocByNamingName(String namingName) {
    return em.createQuery(
            "SELECT i FROM IocEntity i WHERE i.namingName = :pNamingName", IocEntity.class)
        .setParameter("pNamingName", namingName)
        .getResultStream()
        .filter(Objects::nonNull)
        .findFirst();
  }

  @Override
  public long countIocByNamingName(String namingName) {
    return em.createQuery(
            "SELECT COUNT(*) FROM IocEntity ioc " + "WHERE ioc.namingName = :pNamingName",
            Long.class)
        .setParameter("pNamingName", namingName)
        .getSingleResult();
  }

  public List<IocDeploymentEntity> findAllActualIocDeploymentsByHostId(
      Long netBoxHostId, Boolean isVm) {
    return em.createQuery(
            "SELECT ioc.latestSuccessfulDeployment from IocEntity ioc "
                + " WHERE ioc.latestSuccessfulDeployment IS NOT NULL AND ioc.latestSuccessfulDeployment.deploymentType NOT IN (:undeploymentTypes) "
                + " AND ioc.latestSuccessfulDeployment.netBoxHostId = :netBoxHostId AND ioc.latestSuccessfulDeployment.isVm = :isVm "
                + "AND ioc.latestSuccessfulDeployment.awxJob.status = :pSuccessful",
            IocDeploymentEntity.class)
        .setParameter("netBoxHostId", netBoxHostId)
        .setParameter("isVm", isVm)
        .setParameter("pSuccessful", JobStatus.SUCCESSFUL.toString())
        .setParameter("undeploymentTypes", UNDEPLOYMENT_TYPES)
        .getResultList();
  }

  public long countActualDeployments() {
    Query query =
        em.createQuery(
            "SELECT COUNT(*) FROM IocEntity ioc WHERE ioc.latestSuccessfulDeployment IS NOT NULL"
                + " AND ioc.latestSuccessfulDeployment.deploymentType NOT IN (:undeploymentTypes) "
                + " AND ioc.latestSuccessfulDeployment.awxJob.status = :pSuccessful");
    query.setParameter("pSuccessful", JobStatus.SUCCESSFUL.toString());
    query.setParameter("undeploymentTypes", UNDEPLOYMENT_TYPES);
    return (Long) query.getSingleResult();
  }

  public long countAssociatedDeployments(Long netBoxHostID, Boolean isVm) {
    return em.createQuery(
            "SELECT COUNT(*) from IocEntity ioc "
                + " WHERE ioc.latestSuccessfulDeployment IS NOT NULL AND ioc.latestSuccessfulDeployment.netBoxHostId = :pNetBoxHostId "
                + " AND ioc.latestSuccessfulDeployment.isVm = :isVm"
                + " AND ((ioc.latestSuccessfulDeployment.deploymentType NOT IN (:undeploymentTypes) AND ioc.latestSuccessfulDeployment.awxJob.status = :pSuccessful) "
                + " OR (ioc.latestSuccessfulDeployment.awxJob.status != :pSuccessful))",
            Long.class)
        .setParameter("pNetBoxHostId", netBoxHostID)
        .setParameter("isVm", isVm)
        .setParameter("pSuccessful", JobStatus.SUCCESSFUL.toString())
        .setParameter("undeploymentTypes", UNDEPLOYMENT_TYPES)
        .getSingleResult();
  }

  @Override
  public List<IocDeploymentEntity> findIocsWithActiveIocDeployments() {
    return em.createQuery(
            "SELECT ioc.latestSuccessfulDeployment from IocEntity ioc "
                + " WHERE ioc.latestSuccessfulDeployment IS NOT NULL AND ioc.latestSuccessfulDeployment.deploymentType NOT IN (:undeploymentTypes)"
                + " AND ioc.latestSuccessfulDeployment.awxJob.status = :pSuccessful",
            IocDeploymentEntity.class)
        .setParameter("pSuccessful", JobStatus.SUCCESSFUL.toString())
        .setParameter("undeploymentTypes", UNDEPLOYMENT_TYPES)
        .getResultList();
  }

  @Override
  public List<String> findNetworkContainingDeployedIocs() {
    return em.createQuery(
            "SELECT DISTINCT ioc.latestSuccessfulDeployment.hostNetwork from IocEntity ioc "
                + " WHERE ioc.latestSuccessfulDeployment IS NOT NULL AND ioc.latestSuccessfulDeployment.deploymentType NOT IN (:undeploymentTypes)"
                + " AND ioc.latestSuccessfulDeployment.awxJob.status = :pSuccessful",
            String.class)
        .setParameter("pSuccessful", JobStatus.SUCCESSFUL.toString())
        .setParameter("undeploymentTypes", UNDEPLOYMENT_TYPES)
        .getResultList();
  }

  @Override
  public long countIocsWithActiveIocDeploymentsOnNetwork(String network) {
    return em.createQuery(
            "SELECT COUNT(ioc.latestSuccessfulDeployment) from IocEntity ioc "
                + " WHERE ioc.latestSuccessfulDeployment IS NOT NULL AND ioc.latestSuccessfulDeployment.deploymentType NOT IN (:undeploymentTypes)"
                + " AND ioc.latestSuccessfulDeployment.awxJob.status = :pSuccessful"
                + " AND ioc.latestSuccessfulDeployment.hostNetwork = :network",
            Long.class)
        .setParameter("pSuccessful", JobStatus.SUCCESSFUL.toString())
        .setParameter("undeploymentTypes", UNDEPLOYMENT_TYPES)
        .setParameter("network", network)
        .getSingleResult();
  }

  @Override
  public List<IocDeploymentEntity> findIocsWithActiveIocDeploymentsOnNetwork(String network) {
    return em.createQuery(
            "SELECT ioc.latestSuccessfulDeployment from IocEntity ioc "
                + " WHERE ioc.latestSuccessfulDeployment IS NOT NULL AND ioc.latestSuccessfulDeployment.deploymentType NOT IN (:undeploymentTypes)"
                + " AND ioc.latestSuccessfulDeployment.awxJob.status = :pSuccessful"
                + " AND ioc.latestSuccessfulDeployment.hostNetwork = :network",
            IocDeploymentEntity.class)
        .setParameter("pSuccessful", JobStatus.SUCCESSFUL.toString())
        .setParameter("undeploymentTypes", UNDEPLOYMENT_TYPES)
        .setParameter("network", network)
        .getResultList();
  }

  @Override
  public List<NetBoxHostIdentifier> findAllHostIdWithLatestSuccessfulDeployments() {
    return em.createQuery(
            "SELECT ioc.latestSuccessfulDeployment FROM IocEntity ioc "
                + " WHERE ioc.latestSuccessfulDeployment IS NOT NULL AND ioc.latestSuccessfulDeployment.deploymentType NOT IN (:undeploymentTypes) "
                + " AND ioc.latestSuccessfulDeployment.awxJob.status = :pSuccessful",
            IocDeploymentEntity.class)
        .setParameter("pSuccessful", JobStatus.SUCCESSFUL.toString())
        .setParameter("undeploymentTypes", UNDEPLOYMENT_TYPES)
        .getResultStream()
        .map(IocDeploymentEntity::getNetBoxHostIdentifier)
        .collect(Collectors.toList());
  }

  @Override
  public List<NetBoxHostIdentifier> findAllHostIdLatestSuccessfulDeploymentForUser(
      String createdBy) {
    return em.createQuery(
            "SELECT ioc.latestSuccessfulDeployment FROM IocEntity ioc "
                + " WHERE ioc.latestSuccessfulDeployment IS NOT NULL AND ioc.latestSuccessfulDeployment.deploymentType NOT IN (:undeploymentTypes) "
                + " AND ioc.latestSuccessfulDeployment.awxJob.status = :pSuccessful "
                + " AND ioc.createdBy = :pUser",
            IocDeploymentEntity.class)
        .setParameter("pSuccessful", JobStatus.SUCCESSFUL.toString())
        .setParameter("undeploymentTypes", UNDEPLOYMENT_TYPES)
        .setParameter("pUser", createdBy)
        .getResultStream()
        .map(IocDeploymentEntity::getNetBoxHostIdentifier)
        .collect(Collectors.toList());
  }

  @Override
  public List<IocEntity> findIocsByName(List<String> iocNames) {
    return em.createQuery("FROM IocEntity WHERE namingName IN (:pNames)", IocEntity.class)
        .setParameter("pNames", iocNames)
        .getResultList();
  }

  @Override
  public List<IocDeploymentEntity> findIOCsForInventory() {
    return em.createQuery(
            "SELECT dep FROM IocDeploymentEntity dep"
                + " WHERE dep.id IN"
                + "(SELECT MAX(d.id) FROM IocDeploymentEntity d GROUP BY d.ioc.id)"
                + " AND dep.deploymentType IN :pDeploymentType",
            IocDeploymentEntity.class)
        .setParameter("pDeploymentType", DEPLOYMENT_TYPES)
        .getResultList();
  }
}
