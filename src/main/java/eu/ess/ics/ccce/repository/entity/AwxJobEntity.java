/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.repository.entity;

import eu.ess.ics.ccce.common.Utils;
import jakarta.persistence.*;
import java.util.Date;
import org.hibernate.annotations.CreationTimestamp;

/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 */
@Entity
@Table(name = "awx_job")
public class AwxJobEntity {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "awx_job_id")
  private Long awxJobId;

  @Column(name = "job_type")
  private String jobType;

  @Column(name = "created_by")
  private String createdBy;

  @CreationTimestamp
  @Column(name = "created_at")
  private Date createdAt;

  @Column(name = "awx_job_started_at")
  private Date awxJobStartedAt;

  @Column(name = "awx_job_finished_at")
  private Date awxJobFinishedAt;

  @ManyToOne
  @JoinColumn(name = "deployment")
  private IocDeploymentEntity deployment;

  @Column(name = "webhook_update_at")
  private Date webhookUpdateAt;

  @Column(name = "status")
  private String status;

  @Column(name = "error_details")
  private String errorDetails;

  public void setId(Long id) {
    this.id = id;
  }

  public Long getId() {
    return id;
  }

  public Long getAwxJobId() {
    return awxJobId;
  }

  public void setAwxJobId(Long awxJobId) {
    this.awxJobId = awxJobId;
  }

  public String getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

  public Date getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(Date createdAt) {
    this.createdAt = createdAt;
  }

  public Date getAwxJobStartedAt() {
    return Utils.cloneDate(awxJobStartedAt);
  }

  public void setAwxJobStartedAt(Date awxJobStartedAt) {
    this.awxJobStartedAt = Utils.cloneDate(awxJobStartedAt);
  }

  public Date getAwxJobFinishedAt() {
    return Utils.cloneDate(awxJobFinishedAt);
  }

  public void setAwxJobFinishedAt(Date awxJobFinishedAt) {
    this.awxJobFinishedAt = Utils.cloneDate(awxJobFinishedAt);
  }

  public Date getWebhookUpdateAt() {
    return Utils.cloneDate(webhookUpdateAt);
  }

  public void setWebhookUpdateAt(Date webhookUpdateAt) {
    this.webhookUpdateAt = Utils.cloneDate(webhookUpdateAt);
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public IocDeploymentEntity getDeployment() {
    return deployment;
  }

  public void setDeployment(IocDeploymentEntity deployment) {
    this.deployment = deployment;
  }

  public String getJobType() {
    return jobType;
  }

  public void setJobType(String jobType) {
    this.jobType = jobType;
  }

  public String getErrorDetails() {
    return errorDetails;
  }

  public void setErrorDetails(String errorDetails) {
    this.errorDetails = errorDetails;
  }
}
