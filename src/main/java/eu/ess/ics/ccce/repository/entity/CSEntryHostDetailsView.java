package eu.ess.ics.ccce.repository.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import org.hibernate.annotations.Immutable;
import org.hibernate.annotations.Subselect;
import org.hibernate.annotations.Synchronize;

/**
 * @author <a href="mailto:domonkos.gulyas@ess.eu">Domonkos Gulyas</a>
 */
@Entity
@Immutable
@Synchronize("ioc_deployment")
@Subselect(
    "SELECT deployment.host_external_id, deployment.host_name "
        + "FROM ioc_deployment AS deployment "
        + "JOIN ("
        + "    SELECT host_external_id, MAX(created_at) AS latest_date "
        + "    FROM ioc_deployment "
        + "    GROUP BY host_external_id "
        + ") AS latest_hostname_per_csentry_id "
        + "ON deployment.host_external_id = latest_hostname_per_csentry_id.host_external_id "
        + "AND deployment.created_at = latest_hostname_per_csentry_id.latest_date")
public class CSEntryHostDetailsView {

  @Id private Long hostExternalId;

  private String hostName;

  public Long getHostExternalId() {
    return hostExternalId;
  }

  public String getHostName() {
    return hostName;
  }
}
