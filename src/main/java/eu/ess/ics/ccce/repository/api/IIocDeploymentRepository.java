/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.repository.api;

import eu.ess.ics.ccce.repository.entity.AwxJobEntity;
import eu.ess.ics.ccce.repository.entity.IocDeploymentEntity;
import eu.ess.ics.ccce.repository.entity.IocEntity;
import eu.ess.ics.ccce.rest.model.job.request.JobKind;
import eu.ess.ics.ccce.rest.model.job.request.JobOrder;
import jakarta.persistence.LockModeType;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 */
public interface IIocDeploymentRepository {
  List<IocDeploymentEntity> findAll();

  @Transactional
  @Lock(LockModeType.PESSIMISTIC_WRITE)
  void createDeployment(IocDeploymentEntity deployment);

  @Transactional
  @Lock(LockModeType.PESSIMISTIC_WRITE)
  void updateDeployment(IocDeploymentEntity deployment);

  /**
   * Finds the LATEST (un)deployment entry for a certain IOC by ID
   *
   * @param iocId the ID of the IOC for which the LATEST (un)deployment entry has to be found
   * @return the latest (un)deployment entity
   */
  IocDeploymentEntity findLatestDeploymentForIoc(Long iocId);

  /**
   * Finds the INTENDED deployment entity for a certain IOC by ID
   *
   * @param iocId the ID of the IOC for which the INTENDED deployment entry has to be found
   * @return the intended deployment entity
   */
  IocDeploymentEntity findPendingDeploymentForIoc(Long iocId);

  @Lock(LockModeType.PESSIMISTIC_WRITE)
  List<AwxJobEntity> findOngoingDeploymentsForIoc(IocEntity ioc);

  @Lock(LockModeType.PESSIMISTIC_WRITE)
  List<AwxJobEntity> findConcurrentPendingAwxJobsForDeployments(
      List<IocDeploymentEntity> deployments);

  @Lock(LockModeType.PESSIMISTIC_WRITE)
  List<IocDeploymentEntity> findAllPendingOrQueuedDeployments();

  @Lock(LockModeType.PESSIMISTIC_WRITE)
  List<AwxJobEntity> findAllPendingAwxJobs();

  @Lock(LockModeType.PESSIMISTIC_WRITE)
  IocDeploymentEntity findFirstQueued();

  @Lock(LockModeType.PESSIMISTIC_WRITE)
  AwxJobEntity findFirstQueuedAwxJob();

  List<IocDeploymentEntity> findDeploymentsForBatchJob(long jobId);

  /**
   * Finds a Deployment entity by ID
   *
   * @param deploymentId the ID of the deployment that has to be found
   * @return the deployment entity
   */
  IocDeploymentEntity findDeploymentById(long deploymentId);

  @Transactional
  void createAwxJob(AwxJobEntity awxJob);

  @Transactional
  void updateAwxJob(AwxJobEntity awxJob);

  /**
   * Finds AWX job entity by ID
   *
   * @param awxId the ID of the AWX job
   * @return the AWX job entity
   */
  AwxJobEntity findByAwxJobId(long awxId);

  /**
   * Finds AWX job entity by ID
   *
   * @param id the database ID of the AWX job
   * @return the AWX job entity
   */
  AwxJobEntity findJobById(long id);

  /**
   * Counts the number of deployments in the DB. (Including the not successful, unfinished ones, but
   * will not count undeployments!)
   *
   * @return number of deployment entities in the DB.
   */
  long countAllDeployments();

  /**
   * Counts all finished (un)deployments in the DB for a certain day. (Including unsuccessful,and
   * undeployment entities!)
   *
   * @param requestedDay the day for which the count has to be made for (un)deployments.
   * @return number of (un)deployments on a certain day.
   */
  long countAllDeployments(LocalDate requestedDay);

  /**
   * Counts all finished, successful (un)deployments in the DB for a certain day. (Including
   * undeployment entities!)
   *
   * @param requestedDay the day for which the count has to be made for (un)deployments.
   * @return number of successful (un)deployments on a certain day.
   */
  long countSuccessfulDeployments(LocalDate requestedDay);

  /**
   * Counts all deployment entities for a certain IOC. (Including non-successful, unfinished, and
   * undeployment entities!)
   *
   * @param iocId the ID of the IOC
   * @return number of deployment entities.
   */
  long countAllDeploymentsForIoc(long iocId);

  /**
   * Finds all ongoing (un)deployments. (Including undeployments!)
   *
   * @return List of ongoing (un)deployments
   */
  List<AwxJobEntity> findAllOngoing();

  /**
   * Finds all operations (deployment or command) depending on the parameters
   *
   * @param iocId the ID of the IOC for which operations has to be found
   * @param type selected operation type
   * @param status the status of the command (finished, running,..)
   * @param user search operations that were created by certain user
   * @param query searching in naming-name, git revision, description and created by fields
   * @param orderBy the result should be ordered by which field
   * @param isAsc the order should be and ASC or in DESC
   * @param page the requested pade number
   * @param limit how many entries should appear on the requested page
   * @return List of AWX jobs that meets the parameters
   */
  List<AwxJobEntity> findAllAwxJobs(
      Long iocId,
      JobKind type,
      String status,
      String user,
      String query,
      Date startDate,
      Date endDate,
      Long netBoxId,
      Boolean isVm,
      JobOrder orderBy,
      Boolean isAsc,
      int page,
      int limit);

  /**
   * Utility function for paging. Calculates the available entries for the given criteria in the DB.
   * It should be called with the same parameters as the findAllJobs(...) function!
   *
   * @param iocId the ID of the IOC for which operations has to be found
   * @param type selected operation type
   * @param status the status of the command (finished, running,..)
   * @param user search operations that were created by certain user
   * @param query searching in naming-name, git revision, description and created by fields
   * @return number of operations that matches the criteria
   */
  long countJobsForPaging(
      Long iocId,
      JobKind type,
      String status,
      String user,
      String query,
      Date startDate,
      Date endDate,
      Long netBoxId,
      Boolean isVm);

  List<AwxJobEntity> findOngoingCommandsByIoc(IocEntity ioc);

  List<String> findAllHostsWithActualDeployments();

  List<IocDeploymentEntity> findAllByIocId(Long iocId);

  List<AwxJobEntity> findAllAwxJobsByIocDeploymentId(Long deploymentId);

  @Transactional
  @Lock(LockModeType.PESSIMISTIC_WRITE)
  void deleteIocDeployment(IocDeploymentEntity entity);

  @Transactional
  @Lock(LockModeType.PESSIMISTIC_WRITE)
  void deleteAwxJob(AwxJobEntity entity);

  long countAwxJobsByType(String jobType);

  long countAwxJobsByTypeAndStatus(String jobType, List<String> jobStatuses);

  List<AwxJobEntity> findAllQueuedOrPendingCommands();

  List<AwxJobEntity> findAllQueuedOrPendingCommandsForIoc(IocEntity ioc);

  long countAllEntities();

  List<IocDeploymentEntity> listEntities(int page, int limit);

  @Transactional
  void deleteNeverDeployedWithPrefix(String iocPrefix);

  @Transactional
  void deleteIocsFromANetwork(List<Long> iocIds);
}
