package eu.ess.ics.ccce.exceptions;

public class LimitExceededException extends CcceException {
  public LimitExceededException(String description) {
    super("Limit exceeded", description);
  }
}
