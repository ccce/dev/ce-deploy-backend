package eu.ess.ics.ccce.exceptions;

/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 */
public class UnprocessableRequestException extends CcceException {
  public UnprocessableRequestException(String error, String description) {
    super(error, description);
  }
}
