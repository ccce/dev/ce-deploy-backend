package eu.ess.ics.ccce.exceptions;

/**
 * This exception is raised when there requested name from the Naming service is inactive
 *
 * @author <a href="mailto:domonkos.gulyas@ess.eu">Domonkos Gulyas</a>
 */
public class NameInactiveException extends CcceException {

  private static final String NAME_INACTIVE_ERROR_MESSAGE = "Name inactive";
  private static final String NAME_INACTIVE_DESCRIPTION_TEMPLATE =
      "Name with UUID: '%s' has an inactive status: %s.";

  /**
   * Constructor
   *
   * @param uuid The unique UUID of the name
   * @param status The status of the name
   */
  public NameInactiveException(final String uuid, final String status) {
    super(NAME_INACTIVE_ERROR_MESSAGE, NAME_INACTIVE_DESCRIPTION_TEMPLATE.formatted(uuid, status));
  }
}
