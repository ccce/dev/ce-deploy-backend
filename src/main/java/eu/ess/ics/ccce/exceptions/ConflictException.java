package eu.ess.ics.ccce.exceptions;

/**
 * @author <a href="mailto:domonkos.gulyas@ess.eu">Domonkos Gulyas</a>
 */
public class ConflictException extends CcceException {

  /**
   * Constructor
   *
   * @param description Detailed error message
   */
  public ConflictException(String description) {
    super("Conflict", description);
  }
}
