package eu.ess.ics.ccce.exceptions;

/**
 * @author <a href="mailto:domonkos.gulyas@ess.eu">Domonkos Gulyas</a>
 */
public class NetBoxServiceException extends RemoteException {
  private static final String NETBOX_SERVICE_ERROR = "NetBox service error";

  /**
   * Constructor
   *
   * @param description Detailed error message
   */
  public NetBoxServiceException(String description) {
    super(NETBOX_SERVICE_ERROR, description);
  }
}
