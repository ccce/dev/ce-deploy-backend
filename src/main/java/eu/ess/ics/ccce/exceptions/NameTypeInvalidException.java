package eu.ess.ics.ccce.exceptions;

import static eu.ess.ics.ccce.common.Constants.IOC;
import static eu.ess.ics.ccce.common.Constants.SC;

import eu.ess.ics.ccce.service.external.naming.model.NamingIocResponse;

/**
 * This exception is raised when there requested name from the Naming service is not an SC-IOC
 *
 * @author <a href="mailto:domonkos.gulyas@ess.eu">Domonkos Gulyas</a>
 */
public class NameTypeInvalidException extends CcceException {

  private static final String NAME_TYPE_INVALID_ERROR_MESSAGE =
      "The requested name must be an SC-IOC";

  /**
   * Constructor
   *
   * @param namingIocResponse Response from the Naming service
   */
  public NameTypeInvalidException(final NamingIocResponse namingIocResponse) {
    super(NAME_TYPE_INVALID_ERROR_MESSAGE, createDescription(namingIocResponse));
  }

  private static String createDescription(final NamingIocResponse namingIocResponse) {
    final StringBuilder descriptionBuilder =
        new StringBuilder(
            "Name with UUID: '%s' has an invalid ".formatted(namingIocResponse.getUuid()));

    final String deviceType = namingIocResponse.getDeviceType();
    final String discipline = namingIocResponse.getDiscipline();

    if (!IOC.equals(deviceType)) {
      descriptionBuilder.append("device type: ").append(deviceType);
    }
    if (!SC.equals(discipline)) {
      descriptionBuilder.append(" and an invalid discipline: ").append(discipline);
    }
    return descriptionBuilder.toString();
  }
}
