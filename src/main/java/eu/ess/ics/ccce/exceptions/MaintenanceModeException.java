package eu.ess.ics.ccce.exceptions;

public class MaintenanceModeException extends CcceException {
  public MaintenanceModeException(String message) {
    super("MaintenanceMode", message);
  }
}
