/*
 *
 *  * Copyright (C) 2024 European Spallation Source ERIC.
 *  *
 *  * This program is free software; you can redistribute it and/or
 *  * modify it under the terms of the GNU General Public License
 *  * as published by the Free Software Foundation; either version 2
 *  * of the License, or (at your option) any later version.
 *  *
 *  * This program is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program; if not, write to the Free Software
 *  * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 *
 */

package eu.ess.ics.ccce.common.alert;

import eu.ess.ics.ccce.rest.model.Alert;
import eu.ess.ics.ccce.rest.model.AlertSeverity;
import eu.ess.ics.ccce.rest.model.record.response.Record;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Service;

@Service
public class RecordAlertGenerator {
  private final Alert recordIsAnAlias;
  protected static final String RECORD_IS_AN_ALIAS = "Record is an alias";

  RecordAlertGenerator() {
    recordIsAnAlias = new Alert(AlertSeverity.INFO, RECORD_IS_AN_ALIAS);
  }

  /**
   * Checks if there are any alerts to be shown for channelRecord details (warning/error).
   *
   * @param channelRecord channelRecord details
   * @return List of Alerts
   */
  public List<Alert> generateAlerts(Record channelRecord) {
    List<Alert> alerts = new ArrayList<>();
    // ERRORS

    // WARNINGS

    // INFO
    if ((channelRecord != null && channelRecord.getAliasFor() != null)) {
      alerts.add(recordIsAnAlias);
    }

    return alerts;
  }
}
