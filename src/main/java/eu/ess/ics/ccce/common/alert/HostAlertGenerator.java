/*
 *
 *  * Copyright (C) 2024 European Spallation Source ERIC.
 *  *
 *  * This program is free software; you can redistribute it and/or
 *  * modify it under the terms of the GNU General Public License
 *  * as published by the Free Software Foundation; either version 2
 *  * of the License, or (at your option) any later version.
 *  *
 *  * This program is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program; if not, write to the Free Software
 *  * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 *
 */

package eu.ess.ics.ccce.common.alert;

import static eu.ess.ics.ccce.common.alert.IOCAlertGenerator.exporterProblem;

import com.google.common.collect.ImmutableMap;
import eu.ess.ics.ccce.rest.model.Alert;
import eu.ess.ics.ccce.rest.model.AlertSeverity;
import eu.ess.ics.ccce.service.internal.status.StatusService;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class HostAlertGenerator {

  protected static final String HOST_NOT_REACHABLE_ALERT_MESSAGE = "Host not reachable";

  protected static final String NO_IOC_DEPLOYED_ON_HOST_ALERT_MESSAGE =
      "There are no IOCs deployed to host";

  protected static final String HOST_CAPACITY_REACHED_ALERT_MESSAGE_TEMPLATE =
      "Host has exceeded the maximum number of IOCs (%d).";

  private final Alert noIocDeployedToHost;
  private final Alert hostNotReachable;
  private final Alert hostMaxCapacityReached;
  private final StatusService statusService;
  private final Integer iocsOnHostLimit;

  HostAlertGenerator(
      @Value("#{${alert.help.links}}") Map<String, String> alertHelpLinks,
      StatusService statusService,
      Integer iocsOnHostLimit) {

    Map<String, String> links = alertHelpLinks != null ? alertHelpLinks : ImmutableMap.of();
    this.statusService = statusService;
    this.iocsOnHostLimit = iocsOnHostLimit;
    hostMaxCapacityReached =
        new Alert(
            AlertSeverity.WARNING,
            HOST_CAPACITY_REACHED_ALERT_MESSAGE_TEMPLATE.formatted(iocsOnHostLimit));
    noIocDeployedToHost = new Alert(AlertSeverity.INFO, NO_IOC_DEPLOYED_ON_HOST_ALERT_MESSAGE);
    hostNotReachable = new Alert(AlertSeverity.INFO, HOST_NOT_REACHABLE_ALERT_MESSAGE);
  }

  public List<Alert> generateHostDetailsAlert(String fqdn, long deploymentsOnHost) {
    return generateHostDetailsAlert(
        statusService.checkIOCExporterForHost(fqdn) != null,
        BooleanUtils.toBoolean(statusService.checkHostIsActive(fqdn)),
        deploymentsOnHost == 0,
        deploymentsOnHost >= iocsOnHostLimit);
  }

  /**
   * Checks if there are any alerts to be shown about a Host (warning/error).
   *
   * @param iocExporterWorking iocExporter response from Prometheus
   * @param noIocsDeployed tells if no IOC is deployed to the specific host
   * @param hostReachable tells if a host is reachable according to Prometheus
   * @return List of Alerts
   */
  private List<Alert> generateHostDetailsAlert(
      Boolean iocExporterWorking,
      boolean hostReachable,
      boolean noIocsDeployed,
      boolean maxCapacityReached) {
    List<Alert> alerts = new ArrayList<>();

    // ERRORS

    // WARNINGS
    if (hostReachable && !noIocsDeployed && !iocExporterWorking) {
      alerts.add(exporterProblem);
    }

    if (maxCapacityReached) {
      alerts.add(hostMaxCapacityReached);
    }

    // INFO
    if (!hostReachable) {
      alerts.add(hostNotReachable);
    }

    if (hostReachable && noIocsDeployed) {
      alerts.add(noIocDeployedToHost);
    }

    return alerts;
  }
}
