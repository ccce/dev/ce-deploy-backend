/*
 *
 *  * Copyright (C) 2024 European Spallation Source ERIC.
 *  *
 *  * This program is free software; you can redistribute it and/or
 *  * modify it under the terms of the GNU General Public License
 *  * as published by the Free Software Foundation; either version 2
 *  * of the License, or (at your option) any later version.
 *  *
 *  * This program is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program; if not, write to the Free Software
 *  * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 *
 */

package eu.ess.ics.ccce.common.alert;

import static eu.ess.ics.ccce.common.Constants.UNDEPLOYMENT_TYPES;

import eu.ess.ics.ccce.common.Constants;
import eu.ess.ics.ccce.repository.entity.IocDeploymentEntity;
import eu.ess.ics.ccce.rest.model.Alert;
import eu.ess.ics.ccce.rest.model.AlertSeverity;
import eu.ess.ics.ccce.rest.model.job.response.Deployment;
import eu.ess.ics.ccce.service.external.awx.model.JobStatus;
import eu.ess.ics.ccce.service.internal.ioc.dto.NamingResponseDTO;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.gitlab.api.models.GitlabProject;
import org.springframework.stereotype.Service;

/**
 * IOC and host related alert creator
 *
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 */
@Service
public class IOCAlertGenerator {

  protected static final String GIT_PROJECT_ID_INVALID_ALERT_MESSAGE =
      "Cannot find the repository in Gitlab";
  protected static final String GIT_REFERENCE_ID_ALERT_MESSAGE = "Cannot find tag/commit in Gitlab";
  protected static final String LATEST_DEPLOYMENT_FAILED_ALERT_MESSAGE =
      "IOC's latest deployment failed, IOC may be in inconsistent state";
  protected static final String LATEST_UNDEPLOYMENT_FAILED_ALERT_MESSAGE =
      "IOC's latest undeployment failed, IOC may be in inconsistent state";
  protected static final String IOC_HAS_CORE_DUMP_ALERT_MESSAGE =
      "IOC has generated a coredump (inspect locally)";
  protected static final String IOC_SHOULD_NOT_BE_RUNNING_ALERT_MESSAGE =
      "IOC should not be running, contact administrators";
  protected static final String EXPORTER_PROBLEM_ALERT_MESSAGE =
      "Prometheus exporters cannot be found (IOC and host details cannot be acquired)";
  protected static final String GIT_REPOSITORY_DIRTY_ALERT_MESSAGE =
      "IOC repository is dirty (there are untracked changes, inspect your repository locally)";
  protected static final String GIT_LOCAL_COMMIT_ALERT_MESSAGE =
      "IOC repository has local (unpushed) commits (make sure to push modifications, and redeploy)";
  protected static final String ESSIOC_MISSING_ALERT_MESSAGE =
      "IOC services (autosave, channelfinder, etc.) are not configured correctly (make sure the \"essioc\" module is loaded)";
  protected static final String CELLMODE_IS_BEING_USED_ALERT_MESSAGE = "Cellmode is being used";
  protected static final String NAME_CHANGED_IN_NAMING_ALERT_MESSAGE =
      "IOC name has been changed, please take action and redeploy IOC";
  protected static final String IOC_DELETED_FROM_NAMING_ALERT_MESSAGE =
      "IOC name has been deleted, please contact the CCCE team";
  protected static final String NAME_DIFFERS_FROM_DEPLOYED_VERSION_ALERT_MESSAGE =
      "IOC name has changed upstream (redeploy at the earliest opportunity)";
  protected static final String GIT_PROJECT_ARCHIVED_ALERT_MESSAGE = "IOC repository is archived";
  protected static final String NAME_HAS_INVALID_TYPE_ALERT_MESSAGE =
      "IOC names should be in the Software Controller (SC) discipline";
  protected static final String NAME_INACTIVATED_IN_NAMING_ALERT_MESSAGE =
      "IOC name has been inactivated, please contact the CCCE team";
  protected static final String HOST_NOT_ACCESSIBLE_ALERT_MESSAGE =
      "NetBox host is not accessible, please contact the admins";
  protected static final String HOST_INFORMATION_CHANGED_ALERT_MESSAGE =
      "Host information have been changed, please take action and redeploy IOC";
  protected static final String IOC_NOT_DEPLOYED_ALERT_MESSAGE = "IOC is not deployed";
  public static final Alert exporterProblem =
      new Alert(AlertSeverity.WARNING, EXPORTER_PROBLEM_ALERT_MESSAGE);

  private final Alert gitlabProjectIdInvalid;
  private final Alert gitlabProjectArchived;
  private final Alert gitlabReferenceInvalid;
  private final Alert latestDeploymentFailedAlert;
  private final Alert latestUndeploymentFailedAlert;
  private final Alert gitDirtyAlert;
  private final Alert gitLocalCommitAlert;
  private final Alert essiocMissingAlert;
  private final Alert cellmodeUsedAlert;
  private final Alert nameChangeInNamingAlert;
  private final Alert nameDeletedFromNamingAlert;
  private final Alert nameDiffersFromDeployedVersion;
  private final Alert nameHasInvalidTypeAlert;
  private final Alert nameInactiveInNamingAlert;
  private final Alert targetHostNetBoxIdInvalid;
  private final Alert targetHostRenamedAlert;
  private final Alert iocHasCoreDumpAlert;
  private final Alert iocNotDeployed;
  private final Alert iocShouldNotBeRunning;

  public IOCAlertGenerator() {

    gitlabProjectIdInvalid = new Alert(AlertSeverity.ERROR, GIT_PROJECT_ID_INVALID_ALERT_MESSAGE);
    gitlabReferenceInvalid = new Alert(AlertSeverity.ERROR, GIT_REFERENCE_ID_ALERT_MESSAGE);
    latestDeploymentFailedAlert =
        new Alert(AlertSeverity.ERROR, LATEST_DEPLOYMENT_FAILED_ALERT_MESSAGE);
    latestUndeploymentFailedAlert =
        new Alert(AlertSeverity.ERROR, LATEST_UNDEPLOYMENT_FAILED_ALERT_MESSAGE);
    iocHasCoreDumpAlert = new Alert(AlertSeverity.ERROR, IOC_HAS_CORE_DUMP_ALERT_MESSAGE);
    iocShouldNotBeRunning = new Alert(AlertSeverity.ERROR, IOC_SHOULD_NOT_BE_RUNNING_ALERT_MESSAGE);
    gitDirtyAlert = new Alert(AlertSeverity.WARNING, GIT_REPOSITORY_DIRTY_ALERT_MESSAGE);
    gitLocalCommitAlert = new Alert(AlertSeverity.WARNING, GIT_LOCAL_COMMIT_ALERT_MESSAGE);
    essiocMissingAlert = new Alert(AlertSeverity.WARNING, ESSIOC_MISSING_ALERT_MESSAGE);
    cellmodeUsedAlert = new Alert(AlertSeverity.ERROR, CELLMODE_IS_BEING_USED_ALERT_MESSAGE);
    nameChangeInNamingAlert =
        new Alert(AlertSeverity.WARNING, NAME_CHANGED_IN_NAMING_ALERT_MESSAGE);
    nameDeletedFromNamingAlert =
        new Alert(AlertSeverity.ERROR, IOC_DELETED_FROM_NAMING_ALERT_MESSAGE);
    nameDiffersFromDeployedVersion =
        new Alert(AlertSeverity.WARNING, NAME_DIFFERS_FROM_DEPLOYED_VERSION_ALERT_MESSAGE);
    gitlabProjectArchived = new Alert(AlertSeverity.WARNING, GIT_PROJECT_ARCHIVED_ALERT_MESSAGE);
    nameHasInvalidTypeAlert = new Alert(AlertSeverity.ERROR, NAME_HAS_INVALID_TYPE_ALERT_MESSAGE);
    nameInactiveInNamingAlert =
        new Alert(AlertSeverity.ERROR, NAME_INACTIVATED_IN_NAMING_ALERT_MESSAGE);
    targetHostNetBoxIdInvalid = new Alert(AlertSeverity.ERROR, HOST_NOT_ACCESSIBLE_ALERT_MESSAGE);
    targetHostRenamedAlert =
        new Alert(AlertSeverity.WARNING, HOST_INFORMATION_CHANGED_ALERT_MESSAGE);
    iocNotDeployed = new Alert(AlertSeverity.INFO, IOC_NOT_DEPLOYED_ALERT_MESSAGE);
  }

  /**
   * Checks if there are any alerts to be shown about an IOC (warning/error).
   *
   * @param iocNamingName IOC name (from naming service)
   * @param activeDeployment IOC's active deployment from DB
   * @param latestDeployment IOC's latest deployment from DB
   * @param targetHostRenamed host has been renamed on which IOC has been deployed to
   * @param isDirtyRepo dirty flag from status service
   * @param hasLocalCommits local_commit flag from status service
   * @param isCellmodeUsed cellmode flag from status service
   * @param isEssiocMissing ess_ioc_missing flag from status service
   * @param nameServiceResponse information about name changes or deletion in Naming service
   * @param project Gitlab project
   * @param isGitReferenceValid is Gitlab reference (tag, commit ID) vali
   * @param iocExporterWorking is iocExporter working on host according to Prometheus
   * @param isActive indicates if IOC active according to monitoring
   * @return <code>error</code> if there is an error with the IOC <code>warning</code> if there is a
   *     warning with the IOC <code>null</code> if there is no error, nor warning with the IOC
   * @return List of the alerts
   */
  public List<Alert> generateAlerts(
      String iocNamingName,
      Deployment activeDeployment,
      IocDeploymentEntity latestDeployment,
      Boolean targetHostRenamed,
      Boolean isDirtyRepo,
      Boolean hasCoreDump,
      Boolean hasLocalCommits,
      Boolean isCellmodeUsed,
      Boolean isEssiocMissing,
      NamingResponseDTO nameServiceResponse,
      GitlabProject project,
      boolean isGitReferenceValid,
      boolean iocExporterWorking,
      Boolean isActive,
      Boolean isRunningOnCorrectHost) {
    List<Alert> alerts = new ArrayList<>();

    boolean isGitProjectIdValid = project != null;
    // ERRORS
    if (activeDeployment != null
        && !activeDeployment.isUndeployment()
        && Boolean.FALSE.equals(activeDeployment.getHost().isExternalIdValid())) {
      alerts.add(targetHostNetBoxIdInvalid);
    }
    if ((activeDeployment != null)
        && (latestDeployment != null)
        && (!UNDEPLOYMENT_TYPES.contains(latestDeployment.getDeploymentType()))
        && (latestDeployment.getAwxJob() != null)
        && lastJobFailed(latestDeployment)) {
      alerts.add(latestDeploymentFailedAlert);
    }
    if ((activeDeployment != null)
        && (latestDeployment != null)
        && (UNDEPLOYMENT_TYPES.contains(latestDeployment.getDeploymentType()))
        && (latestDeployment.getAwxJob() != null)
        && lastJobFailed(latestDeployment)) {
      alerts.add(latestUndeploymentFailedAlert);
    }
    if (BooleanUtils.isFalse(isRunningOnCorrectHost) && BooleanUtils.toBoolean(isActive)) {
      alerts.add(iocShouldNotBeRunning);
    }
    if (!isGitProjectIdValid) {
      alerts.add(gitlabProjectIdInvalid);
    }
    if (isGitProjectIdValid && !isGitReferenceValid && (activeDeployment != null)) {
      alerts.add(gitlabReferenceInvalid);
    }
    if ((activeDeployment != null)
        && (latestDeployment != null)
        && (!UNDEPLOYMENT_TYPES.contains(latestDeployment.getDeploymentType()))
        && !iocExporterWorking) {
      alerts.add(exporterProblem);
    }
    if (nameServiceResponse.isNameDeleted()) {
      alerts.add(nameDeletedFromNamingAlert);
    }
    if (nameServiceResponse.isNameInactive()) {
      alerts.add(nameInactiveInNamingAlert);
    }
    if (nameServiceResponse.isTypeInvalid()) {
      alerts.add(nameHasInvalidTypeAlert);
    }
    if (activeDeployment != null
        && !activeDeployment.isUndeployment()
        && Boolean.TRUE.equals(hasCoreDump)) {
      alerts.add(iocHasCoreDumpAlert);
    }
    if (activeDeployment != null
        && !activeDeployment.isUndeployment()
        && Boolean.TRUE.equals(isCellmodeUsed)) {
      alerts.add(cellmodeUsedAlert);
    }

    // WARNINGS
    if (activeDeployment != null
        && !activeDeployment.isUndeployment()
        && Boolean.TRUE.equals(hasLocalCommits)) {
      alerts.add(gitLocalCommitAlert);
    }
    if (activeDeployment != null
        && !activeDeployment.isUndeployment()
        && Boolean.TRUE.equals(isDirtyRepo)) {
      alerts.add(gitDirtyAlert);
    }
    if (activeDeployment != null
        && !activeDeployment.isUndeployment()
        && Boolean.TRUE.equals(isEssiocMissing)) {
      alerts.add(essiocMissingAlert);
    }
    if ((activeDeployment != null) && (nameServiceResponse.isNameChanged())) {
      if (StringUtils.isNotEmpty(nameServiceResponse.getCurrentIocName())) {
        alerts.add(
            new Alert(
                nameChangeInNamingAlert.getType(),
                "IOC name has been changed to %s, please take action and redeploy IOC"
                    .formatted(nameServiceResponse.getCurrentIocName())));
      } else {
        alerts.add(nameChangeInNamingAlert);
      }
    }

    if (isGitProjectIdValid && project.isArchived()) {
      alerts.add(gitlabProjectArchived);
    }

    if (iocHasNameDeviation(activeDeployment, iocNamingName)) {
      alerts.add(nameDiffersFromDeployedVersion);
    }

    if (activeDeployment != null
        && !activeDeployment.isUndeployment()
        && Boolean.TRUE.equals(targetHostRenamed)) {
      alerts.add(targetHostRenamedAlert);
    }

    // INFO
    if (BooleanUtils.isNotTrue(isActive)
        && isIocNotDeployed(activeDeployment, latestDeployment)
        && (isRunningOnCorrectHost == null)) {
      alerts.add(iocNotDeployed);
    }

    return alerts;
  }

  private static boolean iocHasNameDeviation(Deployment activeDeployment, String iocNamingName) {

    if (activeDeployment == null) {
      return false;
    }

    return !iocNamingName.equals(activeDeployment.getIocName());
  }

  private static boolean isIocNotDeployed(
      Deployment activeDeployment, IocDeploymentEntity latestDeployment) {
    return latestDeployment == null
        || (UNDEPLOYMENT_TYPES.contains(latestDeployment.getDeploymentType())
            && lastJobWasSuccessful(latestDeployment))
        || (activeDeployment == null
            && !UNDEPLOYMENT_TYPES.contains(latestDeployment.getDeploymentType())
            && lastJobWasSuccessful(latestDeployment));
  }

  private static boolean lastJobWasSuccessful(IocDeploymentEntity latestDeployment) {
    return JobStatus.SUCCESSFUL.equals(JobStatus.valueOf(latestDeployment.getAwxJob().getStatus()));
  }

  private static boolean lastJobFailed(IocDeploymentEntity latestDeployment) {
    return Constants.FAILED_JOB_STATES.contains(
        JobStatus.valueOf(latestDeployment.getAwxJob().getStatus()));
  }
}
