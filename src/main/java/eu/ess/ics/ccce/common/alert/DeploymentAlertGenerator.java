/*
 *
 *  * Copyright (C) 2024 European Spallation Source ERIC.
 *  *
 *  * This program is free software; you can redistribute it and/or
 *  * modify it under the terms of the GNU General Public License
 *  * as published by the Free Software Foundation; either version 2
 *  * of the License, or (at your option) any later version.
 *  *
 *  * This program is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program; if not, write to the Free Software
 *  * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 *
 */

package eu.ess.ics.ccce.common.alert;

import eu.ess.ics.ccce.rest.model.Alert;
import eu.ess.ics.ccce.rest.model.AlertSeverity;
import eu.ess.ics.ccce.rest.model.job.response.Deployment;
import eu.ess.ics.ccce.service.external.netbox.model.NetBoxHostIdentifier;
import eu.ess.ics.ccce.service.internal.host.HostService;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

@Service
public class DeploymentAlertGenerator {
  private final Alert hostUnresolvable;
  protected static final String HOST_NOT_FOUND_ALERT_MESSAGE = "Host cannot be found";

  DeploymentAlertGenerator() {
    hostUnresolvable = new Alert(AlertSeverity.INFO, HOST_NOT_FOUND_ALERT_MESSAGE);
  }

  /**
   * Checks if there are any alerts to be shown for deployment details (warning/error).
   *
   * @param deployment deployment details
   * @return List of Alerts
   */
  public List<Alert> generateDeploymentDetailsAlert(Deployment deployment) {
    List<Alert> alerts = new ArrayList<>();
    // ERRORS

    // WARNINGS

    // INFO
    if (deployment != null
        && deployment.getHost() != null
        && Boolean.TRUE.equals(!deployment.getHost().isExternalIdValid())) {
      String hostId =
          deployment.getHost().getHostId() == null ? "" : deployment.getHost().getHostId();

      String postFix = extractHostInfoForAlert(hostId);

      alerts.add(new Alert(hostUnresolvable.getType(), hostUnresolvable.getMessage() + postFix));
    }

    return alerts;
  }

  public String extractHostInfoForAlert(String hostId) {
    String result = "";

    if (StringUtils.isNotEmpty(hostId)) {
      NetBoxHostIdentifier hostInfo = HostService.decodeHostIdentifier(hostId);
      result =
          " ( "
              + (hostInfo.isVm() ? "VM" : "Device")
              + ", ID: "
              + hostInfo.getNetBoxHostId()
              + " )";
    }

    return result;
  }
}
