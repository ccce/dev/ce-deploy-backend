/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.common.conversion;

import eu.ess.ics.ccce.service.internal.PagingLimitDto;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

/**
 * Calculation of paging parameters
 *
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 */
@Component
public class PagingUtil {

  private static final String LIST_LIMIT = "response.list.max.size";

  private Environment env;

  private final int responseListMaxSize;

  /**
   * Constructor
   *
   * @param env Environment variables representation
   */
  public PagingUtil(Environment env) {
    this.env = env;
    this.responseListMaxSize = Integer.parseInt(env.getProperty(LIST_LIMIT));
  }

  /**
   * Limit maximum page size according to CE backend configuration
   *
   * @param page Desired page
   * @param limit Desired page size
   * @param listAll List all resource?
   * @return Calculated paging parameters (limited if needed)
   */
  public PagingLimitDto pageLimitConverter(Integer page, Integer limit, boolean listAll) {
    return pageLimitConverter(page, limit, null, listAll);
  }

  public PagingLimitDto pageLimitConverter(Integer page, Integer limit) {
    return pageLimitConverter(page, limit, null, false);
  }

  /**
   * Limit maximum page size according to an external service API's configuration
   *
   * @param page Desired page
   * @param limit Desired page size
   * @param listAll List all resource?
   * @param customPageLimit External service API's page size limit
   * @return Calculated paging parameters (limited if needed)
   */
  public PagingLimitDto pageLimitConverter(
      Integer page, Integer limit, Integer customPageLimit, boolean listAll) {

    int maxLimitSize = customPageLimit != null ? customPageLimit : responseListMaxSize;

    int pageSize = page == null ? 0 : page;
    int limitSize = limit == null ? maxLimitSize : Math.min(limit, maxLimitSize);

    return new PagingLimitDto(pageSize, limitSize, listAll);
  }
}
