/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.common;

import eu.ess.ics.ccce.exceptions.InputValidationException;
import java.net.URL;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;
import org.apache.commons.lang3.StringUtils;

/**
 * Application level utility functions
 *
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 */
public class Utils {

  // Util class, don't instantiate it!
  private Utils() {}

  /**
   * Clones a Date to make object immutable.
   *
   * @param dateToClone the date that has to be cloned.
   * @return <code>null</code>, if date was null <code>cloned date</code>, if date was not null
   */
  public static Date cloneDate(Date dateToClone) {
    return dateToClone == null ? null : new Date(dateToClone.getTime());
  }

  public static ZonedDateTime cloneDate(ZonedDateTime dateToClone) {
    return dateToClone == null ? null : ZonedDateTime.from(dateToClone);
  }

  public static ZonedDateTime validateAndApplyTimeZone(
      final LocalDateTime dateTime, final String timeZone) {
    if (dateTime == null) {
      return null;
    }
    try {
      return ZonedDateTime.of(dateTime, ZoneId.of(timeZone));
    } catch (IllegalArgumentException e) {
      throw new InputValidationException(e.getMessage());
    }
  }

  public static Long parseNumericId(final String idAsString, final String fieldName) {
    if (idAsString == null) {
      return null;
    }
    try {
      return Long.parseLong(idAsString);
    } catch (NumberFormatException e) {
      throw new InputValidationException("'%s' must be a number.".formatted(fieldName));
    }
  }

  public static Date normalizeDate(final ZonedDateTime dateTime, final String serverTimeZone) {
    if (dateTime == null) {
      return null;
    }

    return Date.from(dateTime.withZoneSameInstant(ZoneId.of(serverTimeZone)).toInstant());
  }

  public static ZonedDateTime now(final String serverTimeZone) {
    return ZonedDateTime.now().withZoneSameInstant(ZoneId.of(serverTimeZone));
  }

  public static ZonedDateTime toZonedDateTime(final Date date, final String serverTimeZone) {
    if (date == null) {
      return null;
    }
    return date.toInstant().atZone(ZoneId.of(serverTimeZone));
  }

  public static Date toUtilDate(final ZonedDateTime dateTime) {
    if (dateTime == null) {
      return null;
    }
    return Date.from(dateTime.toInstant());
  }

  public static boolean checkTextContains(String string, String subString) {
    if (subString != null) {
      return StringUtils.containsIgnoreCase(string, subString);
    }
    return true;
  }

  public static boolean isUrl(String urlToCheck) {
    try {
      URL url = new URL(urlToCheck);
      url.toURI();
      return true;
    } catch (Exception e) {
      return false;
    }
  }

  public static boolean isDeployedWithOldPlaybook(
      ZonedDateTime deploymentTime, ZonedDateTime playbookTransitionTime) {
    return playbookTransitionTime.isAfter(deploymentTime);
  }

  public static String getIocMetricPrefix(
      ZonedDateTime deploymentTime, ZonedDateTime playbookTransitionTime) {
    return Utils.isDeployedWithOldPlaybook(deploymentTime, playbookTransitionTime)
        ? Constants.OLD_IOC_METRICS_PREFIX
        : Constants.NEW_IOC_METRICS_PREFIX;
  }
}
