/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.common.conversion;

import eu.ess.ics.ccce.rest.model.Alert;
import eu.ess.ics.ccce.rest.model.AlertSeverity;
import eu.ess.ics.ccce.service.external.csentry.model.CSEntryHost;
import eu.ess.ics.ccce.service.internal.deployment.dto.TargetHost;
import java.util.Comparator;
import java.util.List;

/**
 * Utility class for conversion of DTOs and BOs
 *
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 */
public class ConversionUtil {

  // not intended to be instantiated
  private ConversionUtil() {
    throw new IllegalStateException("Utility class");
  }

  /**
   * This method is deprecated and no longer performs any operations. It is retained only to prevent
   * modifications to the Flyway migration script (V10__Store_deployment_host_historical_data).
   *
   * @deprecated This method has no functional purpose.
   */
  @Deprecated
  public static TargetHost convertToTargetHost(CSEntryHost csEntryHost) {
    return null;
  }

  public static AlertSeverity determineAlertSeverity(List<Alert> alerts) {
    if (alerts != null && !alerts.isEmpty()) {

      Comparator<Alert> compareAlertSeverity =
          Comparator.comparingInt((Alert o) -> o.getType().getValue());

      alerts.sort(compareAlertSeverity);

      return alerts.get(0).getType();
    }
    return null;
  }
}
