/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.common;

import com.google.common.collect.ImmutableList;
import eu.ess.ics.ccce.service.external.awx.model.JobStatus;
import eu.ess.ics.ccce.service.internal.deployment.dto.AwxJobType;
import eu.ess.ics.ccce.service.internal.deployment.dto.DeploymentType;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Application level constants
 *
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 */
public class Constants {
  private Constants() {
    throw new IllegalStateException("Utility class");
  }

  public static final String COOKIE_AUTH_HEADER = "ce-deploy-auth";
  public static final List<AwxJobType> COMMAND_JOB_TYPES =
      ImmutableList.of(AwxJobType.START, AwxJobType.STOP);

  public static final List<AwxJobType> BATCH_JOB_TYPES =
      ImmutableList.of(AwxJobType.BATCH_DEPLOY, AwxJobType.BATCH_UNDEPLOY);

  public static final List<DeploymentType> BATCH_DEPLOYMENT_TYPES =
      ImmutableList.of(DeploymentType.BATCH_DEPLOY, DeploymentType.BATCH_UNDEPLOY);

  public static final List<String> DEPLOYMENT_TYPES =
      ImmutableList.of(DeploymentType.DEPLOY, DeploymentType.BATCH_DEPLOY).stream()
          .map(DeploymentType::name)
          .toList();

  public static final List<String> UNDEPLOYMENT_TYPES =
      ImmutableList.of(DeploymentType.UNDEPLOY, DeploymentType.BATCH_UNDEPLOY).stream()
          .map(DeploymentType::name)
          .toList();

  public static final List<AwxJobType> ALL_DEPLOYMENT_JOB_TYPES =
      ImmutableList.of(
          AwxJobType.DEPLOY,
          AwxJobType.UNDEPLOY,
          AwxJobType.BATCH_DEPLOY,
          AwxJobType.BATCH_UNDEPLOY);
  public static final List<JobStatus> FAILED_JOB_STATES =
      ImmutableList.of(JobStatus.FAILED, JobStatus.CANCELED, JobStatus.ERROR);
  public static final List<JobStatus> FINISHED_JOB_STATES =
      new ImmutableList.Builder<JobStatus>()
          .addAll(FAILED_JOB_STATES)
          .add(JobStatus.SUCCESSFUL)
          .build();

  public static final List<String> FINISHED_AWX_STATUSES =
      FINISHED_JOB_STATES.stream().map(JobStatus::name).collect(Collectors.toList());

  public static final List<String> COMMAND_TYPES =
      COMMAND_JOB_TYPES.stream().map(AwxJobType::name).collect(Collectors.toList());

  public static final List<String> ALL_DEPLOYMENT_TYPES =
      ALL_DEPLOYMENT_JOB_TYPES.stream().map(AwxJobType::name).collect(Collectors.toList());

  public static final List<String> QUEUED_STATUSES =
      ImmutableList.of(
          JobStatus.QUEUED_BY_BACKEND.name(),
          JobStatus.PENDING.name(),
          JobStatus.WAITING.name(),
          JobStatus.NEW.name());

  public static final List<String> PENDING_STATUSES =
      ImmutableList.of(JobStatus.PENDING.name(), JobStatus.NEW.name());

  public static final String SC = "SC";

  public static final String IOC = "IOC";

  public static final String OLD_IOC_METRICS_PREFIX = "ioc@";
  public static final String NEW_IOC_METRICS_PREFIX = "ioc-";
}
