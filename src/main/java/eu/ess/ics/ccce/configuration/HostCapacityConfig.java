package eu.ess.ics.ccce.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class HostCapacityConfig {

  @Value("${monitor.host-max-capacity}")
  private Integer hostMaxCapacity;

  @Bean
  Integer iocsOnHostLimit() {
    return hostMaxCapacity;
  }
}
