package eu.ess.ics.ccce.configuration;

import eu.ess.ics.ccce.configuration.exception.handler.GlobalControllerExceptionHandler;
import eu.ess.ics.ccce.exceptions.MaintenanceModeException;
import eu.ess.ics.ccce.service.internal.maintenance.MaintenanceService;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

@Component
public class MaintenanceFilter extends OncePerRequestFilter {
  private static final Logger LOGGER = LoggerFactory.getLogger(MaintenanceService.class);

  private final MaintenanceService maintenanceService;
  private final GlobalControllerExceptionHandler globalControllerExceptionHandler;

  public MaintenanceFilter(
      MaintenanceService maintenanceService,
      GlobalControllerExceptionHandler globalControllerExceptionHandler) {
    this.maintenanceService = maintenanceService;
    this.globalControllerExceptionHandler = globalControllerExceptionHandler;
  }

  @Override
  protected void doFilterInternal(
      HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
      throws ServletException, IOException {
    if (request.getMethod().equals(HttpMethod.GET.name())) {
      filterChain.doFilter(request, response);
      return;
    }

    String message = this.maintenanceService.checkMaintenanceMode(request.getRequestURI());
    if (message != null) {
      LOGGER.debug("Filtering out maintenance mode: {}", message);
      globalControllerExceptionHandler.setResponseToException(
          request, response, new MaintenanceModeException("App is in read-only mode!"));
    } else {
      filterChain.doFilter(request, response);
    }
  }
}
