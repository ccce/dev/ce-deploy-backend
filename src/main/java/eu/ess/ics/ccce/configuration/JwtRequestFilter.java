/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.configuration;

import eu.ess.ics.ccce.common.Constants;
import eu.ess.ics.ccce.common.security.EncryptUtil;
import eu.ess.ics.ccce.configuration.exception.handler.GlobalControllerExceptionHandler;
import eu.ess.ics.ccce.service.internal.security.JwtTokenService;
import eu.ess.ics.ccce.service.internal.security.JwtUserDetailsService;
import eu.ess.ics.ccce.service.internal.security.dto.UserDetails;
import io.jsonwebtoken.ExpiredJwtException;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.util.WebUtils;

/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 */
@Component
public class JwtRequestFilter extends OncePerRequestFilter {

  private static final Logger LOGGER = LoggerFactory.getLogger(JwtRequestFilter.class);

  private final JwtUserDetailsService jwtUserDetailsService;
  private final JwtTokenService jwtTokenService;
  private final EncryptUtil encryptUtil;
  private final GlobalControllerExceptionHandler globalControllerExceptionHandler;

  public JwtRequestFilter(
      JwtUserDetailsService jwtUserDetailsService,
      JwtTokenService jwtTokenService,
      EncryptUtil encryptUtil,
      GlobalControllerExceptionHandler globalControllerExceptionHandler) {
    this.jwtUserDetailsService = jwtUserDetailsService;
    this.jwtTokenService = jwtTokenService;
    this.encryptUtil = encryptUtil;
    this.globalControllerExceptionHandler = globalControllerExceptionHandler;
  }

  private static class TokenInfo {
    private String userName;
    private String rbacToken;

    public String getUserName() {
      return userName;
    }

    public void setUserName(String userName) {
      this.userName = userName;
    }

    public String getRbacToken() {
      return rbacToken;
    }

    public void setRbacToken(String rbacToken) {
      this.rbacToken = rbacToken;
    }
  }

  private TokenInfo extractTokenInfo(String jwtToken) {
    TokenInfo result = new TokenInfo();
    try {
      result.setUserName(jwtTokenService.getUsernameFromToken(jwtToken));
      result.setRbacToken(
          encryptUtil.decrypt(
              jwtTokenService.getClaim(jwtToken, JwtTokenService.TOKEN, String.class)));
    } catch (IllegalArgumentException e) {
      LOGGER.error("Unable to get JWT Token", e);
      return null;
    } catch (ExpiredJwtException e) {
      return null;
    }

    return result;
  }

  @Override
  protected void doFilterInternal(
      HttpServletRequest httpServletRequest,
      HttpServletResponse httpServletResponse,
      FilterChain filterChain)
      throws ServletException, IOException {
    final String requestTokenHeader = httpServletRequest.getHeader("Authorization");

    String jwtToken = null;
    TokenInfo tokenInfo = null;
    try {
      // JWT Token is in the form "Bearer token". Remove Bearer word and get
      // only the Token
      if (requestTokenHeader != null && requestTokenHeader.startsWith("Bearer ")) {
        jwtToken = requestTokenHeader.substring(7);
        tokenInfo = extractTokenInfo(jwtToken);
      } else {
        Cookie cookie = WebUtils.getCookie(httpServletRequest, Constants.COOKIE_AUTH_HEADER);

        if (cookie != null) {
          String cookieValue = cookie.getValue();
          if (StringUtils.isNotEmpty(cookieValue)) {
            jwtToken = cookieValue;
            tokenInfo = extractTokenInfo(cookieValue);
          }
        }
      }

      // Once we get the token validate it.
      if (tokenInfo != null
          && tokenInfo.getRbacToken() != null
          && SecurityContextHolder.getContext().getAuthentication() == null) {

        UserDetails userDetails =
            this.jwtUserDetailsService.loadUserByToken(tokenInfo.getRbacToken());

        // if token is valid configure Spring Security to manually set
        // authentication
        if (jwtTokenService.validateToken(jwtToken, userDetails)) {

          UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken =
              new UsernamePasswordAuthenticationToken(
                  userDetails, null, userDetails.getAuthorities());
          usernamePasswordAuthenticationToken.setDetails(
              new WebAuthenticationDetailsSource().buildDetails(httpServletRequest));
          // After setting the Authentication in the context, we specify
          // that the current user is authenticated. So it passes the
          // Spring Security Configurations successfully.
          SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
        }
      }
    } catch (RuntimeException e) {
      // MyObject is whatever the output of the below method
      ResponseEntity<Object> objectResponseEntity = null;
      try {
        globalControllerExceptionHandler.setResponseToException(
            httpServletRequest, httpServletResponse, e);

        return;
      } catch (Exception ex) {
        LOGGER.error("JWT Request filter processing error", ex);
      }
    }
    filterChain.doFilter(httpServletRequest, httpServletResponse);
  }
}
