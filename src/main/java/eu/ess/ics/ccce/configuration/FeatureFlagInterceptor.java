/*
 * Copyright (C) 2024 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.configuration;

import eu.ess.ics.ccce.rest.model.FeatureTag;
import eu.ess.ics.ccce.service.internal.featureflags.FeatureFlagsService;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author <a href="mailto:domonkos.gulyas@ess.eu">Domonkos Gulyas</a>
 */
@Component
public class FeatureFlagInterceptor implements HandlerInterceptor {

  private final FeatureFlagsService featureFlagsService;

  public FeatureFlagInterceptor(FeatureFlagsService featureFlagsService) {
    this.featureFlagsService = featureFlagsService;
  }

  @Override
  public boolean preHandle(
      HttpServletRequest request, HttpServletResponse response, Object handler) {
    if (handler instanceof HandlerMethod handlerMethod) {
      FeatureTag tagAnnotation = handlerMethod.getBeanType().getAnnotation(FeatureTag.class);
      if (tagAnnotation != null) {
        Method method = handlerMethod.getMethod();
        String methodName = method.getName();
        featureFlagsService.checkFeature(tagAnnotation.value(), methodName);
      }
    }
    return true;
  }

  @Override
  public void postHandle(
      HttpServletRequest request,
      HttpServletResponse response,
      Object object,
      ModelAndView model) {}

  @Override
  public void afterCompletion(
      HttpServletRequest request,
      HttpServletResponse response,
      Object object,
      Exception exception) {}
}
