/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.configuration;

import static org.springframework.security.config.Customizer.withDefaults;
import static org.springframework.security.web.util.matcher.AntPathRequestMatcher.antMatcher;

import java.util.Arrays;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 */
@Configuration
@EnableWebSecurity
@EnableMethodSecurity
public class WebSecurityConfig {
  public static final String ROLE_DEPLOYMENT_TOOL_ADMIN = "DeploymentToolAdmin";
  public static final String ROLE_DEPLOYMENT_TOOL_INTEGRATOR = "DeploymentToolIntegrator";
  public static final String IS_ADMIN = "hasAuthority('" + ROLE_DEPLOYMENT_TOOL_ADMIN + "')";
  public static final String IS_INTEGRATOR =
      "hasAuthority('" + ROLE_DEPLOYMENT_TOOL_INTEGRATOR + "')";
  public static final String IS_ADMIN_OR_INTEGRATOR = IS_ADMIN + " or " + IS_INTEGRATOR;
  public static final List<String> ALLOWED_ROLES_TO_LOGIN =
      Arrays.asList(
          WebSecurityConfig.ROLE_DEPLOYMENT_TOOL_ADMIN,
          WebSecurityConfig.ROLE_DEPLOYMENT_TOOL_INTEGRATOR);

  @Autowired private JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint;

  @Autowired private UserDetailsService jwtUserDetailsService;

  @Autowired private JwtRequestFilter jwtRequestFilter;

  @Value("${springdoc.api-docs.path}")
  private String apiPath;

  @Value("${springdoc.swagger-ui.path}")
  private String swaggerPath;

  @Autowired
  public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
    // configure AuthenticationManager so that it knows from where to load
    // user for matching credentials
    // Use BCryptPasswordEncoder
    auth.userDetailsService(jwtUserDetailsService);
  }

  @Bean
  PasswordEncoder passwordEncoder() {
    return new BCryptPasswordEncoder();
  }

  @Bean
  SecurityFilterChain filterChain(HttpSecurity httpSecurity) throws Exception {
    // We don't need CSRF
    httpSecurity
        .csrf(AbstractHttpConfigurer::disable)
        // dont authenticate this particular request
        .authorizeHttpRequests(
            requests ->
                requests
                    // openAPI web UI
                    .requestMatchers(
                        "/v3/api-docs/**",
                        "/swagger-ui/**",
                        "/api/swagger-ui/**",
                        swaggerPath,
                        apiPath + "/**")
                    .permitAll()
                    // no auth required for login endpoint
                    .requestMatchers(HttpMethod.POST, "/api/v1/authentication/login")
                    .permitAll()
                    // announcement
                    .requestMatchers(HttpMethod.GET, "/api/v1/broadcasts/announcements")
                    .permitAll()
                    // actuator endpoints
                    .requestMatchers(HttpMethod.GET, "/api/actuator/**")
                    .permitAll()
                    // hosts endpoint
                    .requestMatchers(HttpMethod.GET, "/api/v1/hosts")
                    .permitAll()
                    // IOCs that are deployed to certain hosts
                    .requestMatchers(HttpMethod.GET, "/api/v1/hosts/**")
                    .permitAll()
                    // host log endpoint
                    .requestMatchers(antMatcher(HttpMethod.GET, "/api/v1/hosts/{.+}/log"))
                    .authenticated()
                    // IOCs endpoint
                    .requestMatchers(HttpMethod.GET, "/api/v1/iocs")
                    .permitAll()
                    // IOC description endpoint
                    .requestMatchers(antMatcher(HttpMethod.GET, "/api/v1/iocs/{\\d+}/description"))
                    .permitAll()
                    // My IOCs must be authenticated
                    .requestMatchers(HttpMethod.GET, "/api/v1/iocs/my_iocs_with_alarms")
                    .authenticated()
                    // ioc details
                    .requestMatchers(antMatcher(HttpMethod.GET, "/api/v1/iocs/{\\d+}"))
                    .permitAll()
                    // monitoring IOC status
                    .requestMatchers(antMatcher(HttpMethod.GET, "/api/v1/iocs/{\\d+}/status"))
                    .permitAll()
                    // monitoring IOC alerts
                    .requestMatchers(antMatcher(HttpMethod.GET, "/api/v1/iocs/{\\d+}/alerts"))
                    .permitAll()
                    // checking if git repo id is being used
                    .requestMatchers(
                        antMatcher(HttpMethod.GET, "/api/v1/iocs/check_git_repo_id/{\\d+}"))
                    .permitAll()
                    // jobs endpoint
                    .requestMatchers(HttpMethod.GET, "/api/v1/jobs/**")
                    .permitAll()
                    .requestMatchers(antMatcher(HttpMethod.GET, "/api/v1/jobs/{\\d+}/awx/log"))
                    .authenticated()
                    // statistics endpoints
                    .requestMatchers(HttpMethod.GET, "/api/v1/statistics/general")
                    .permitAll()
                    .requestMatchers(HttpMethod.GET, "/api/v1/statistics/general/**")
                    .permitAll()
                    // records
                    .requestMatchers(HttpMethod.GET, "/api/v1/records/**")
                    .permitAll()
                    // monitoring IOC status
                    .requestMatchers(
                        antMatcher(HttpMethod.GET, "/api/v1/git_helper/{\\d+}/tags_and_commits"))
                    .permitAll()
                    .requestMatchers(
                        antMatcher(HttpMethod.GET, "/api/v1/git_helper/project_info/{\\d+}"))
                    .permitAll()
                    .requestMatchers(
                        antMatcher(HttpMethod.GET, "/api/v1/git_helper/{\\d+}/reference_type/**"))
                    .permitAll()
                    // no JWT login for AWX endpoint calls
                    .requestMatchers(HttpMethod.POST, "/api/v1/awx/**")
                    .permitAll()
                    // checking naming uuid
                    .requestMatchers(HttpMethod.GET, "/api/v1/names/check_naming_uuid/**")
                    .permitAll()
                    // Maintenance Mode
                    .requestMatchers(HttpMethod.GET, "/api/v1/maintenance_mode/**")
                    .permitAll()
                    // inventory related endpoints
                    .requestMatchers(HttpMethod.GET, "/api/v1/inventory/**")
                    .permitAll()
                    .requestMatchers(HttpMethod.GET, "/api/v1/migration/**")
                    .permitAll()
                    // all other requests need to be authenticated
                    .anyRequest()
                    .authenticated())
        // Add a filter to validate the tokens with every request
        .addFilterBefore(jwtRequestFilter, UsernamePasswordAuthenticationFilter.class)
        // make sure we use stateless session; session won't be used to
        // store user's state.
        .exceptionHandling(
            handling -> handling.authenticationEntryPoint(jwtAuthenticationEntryPoint))
        .sessionManagement(
            management -> management.sessionCreationPolicy(SessionCreationPolicy.STATELESS));
    httpSecurity.cors(withDefaults());

    return httpSecurity.build();
  }
}
