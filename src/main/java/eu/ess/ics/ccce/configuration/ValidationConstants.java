package eu.ess.ics.ccce.configuration;

/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 */
public class ValidationConstants {
  public static final String REPO_NAME_CHECK_ERR_MESSAGE =
      "Only lowercase alphanumeric chars, hyphen and underscores are allowed in repository name (min 4, max 20 chars)";
  public static final String REPO_NAME_REGEX = "^(?=.{4,20}$)[a-z0-9]+([a-z0-9_-]+)*[a-z0-9]$";
}
