/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.configuration.exception.handler;

import com.fasterxml.jackson.databind.ObjectMapper;
import eu.ess.ics.ccce.configuration.exception.GeneralException;
import eu.ess.ics.ccce.exceptions.*;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * Handler of API level exceptions
 *
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 */
@RestControllerAdvice
public class GlobalControllerExceptionHandler extends ResponseEntityExceptionHandler {

  /**
   * Transforms API level exception to HTTP response (sets status code and response body)
   *
   * @param ex Raised exception to be handled
   * @param request Request descriptor
   * @return HTTP response object
   */
  @ExceptionHandler
  public ResponseEntity<Object> handleConflict(RuntimeException ex, WebRequest request) {

    GeneralException generalException = new GeneralException();
    if (ex instanceof CcceException ccceException) {
      generalException.setError(ccceException.getError());
      generalException.setDescription(ccceException.getDescription());
    } else {
      generalException.setError("Generic error");
      generalException.setDescription(ex.getMessage());
    }

    HttpStatus resultStatus = HttpStatus.INTERNAL_SERVER_ERROR;

    // your exception comes here
    if (ex instanceof UnauthorizedException || ex instanceof AccessDeniedException) {
      resultStatus = HttpStatus.FORBIDDEN;
    }

    if (ex instanceof AuthenticationException) {
      resultStatus = HttpStatus.UNAUTHORIZED;
    }

    if (ex instanceof GitlabServiceException
        || ex instanceof AwxServiceException
        || ex instanceof CSEntryServiceException) {
      resultStatus = HttpStatus.SERVICE_UNAVAILABLE;
    }

    if (ex instanceof EntityNotFoundException) {
      resultStatus = HttpStatus.NOT_FOUND;
    }

    if (ex instanceof FeatureDisabledException) {
      resultStatus = HttpStatus.BAD_REQUEST;
    }

    if (ex instanceof RemoteException) {
      resultStatus = HttpStatus.SERVICE_UNAVAILABLE;
    }

    if (ex instanceof ConstraintViolationException || ex instanceof OperationNotAllowedException) {
      resultStatus = HttpStatus.CONFLICT;
    }

    if (ex instanceof IncompleteRequestException
        || ex instanceof IocNotDeployedException
        || ex instanceof UnprocessableEntityException
        || ex instanceof TooManyEntitiesException
        || ex instanceof UnprocessableRequestException) {
      resultStatus = HttpStatus.UNPROCESSABLE_ENTITY;
    }

    if (ex instanceof MetaDataFileException) {
      resultStatus = HttpStatus.FAILED_DEPENDENCY;
    }

    if (ex instanceof NamingServiceException || ex instanceof MaintenanceModeException) {
      resultStatus = HttpStatus.SERVICE_UNAVAILABLE;
    }

    if (ex instanceof UnsupportedTypeException) {
      resultStatus = HttpStatus.BAD_REQUEST;
    }

    if (ex instanceof InputValidationException) {
      resultStatus = HttpStatus.BAD_REQUEST;
    }

    if (ex instanceof ConflictException) {
      resultStatus = HttpStatus.CONFLICT;
    }

    if (ex instanceof LimitExceededException) {
      resultStatus = HttpStatus.BAD_REQUEST;
    }

    return new ResponseEntity<>(generalException, resultStatus);
  }

  @ExceptionHandler({MethodArgumentTypeMismatchException.class})
  public ResponseEntity<Object> handleMethodArgumentTypeMismatch(
      MethodArgumentTypeMismatchException ex, WebRequest request) {

    // queryparam transformation error should respond with 400
    // pathparam transformation error should respond with 404
    HttpStatus status =
        ex.getParameter().getParameterAnnotation(PathVariable.class) != null
            ? HttpStatus.NOT_FOUND
            : HttpStatus.BAD_REQUEST;

    GeneralException generalException = new GeneralException();
    generalException.setError("Request type problem");
    generalException.setDescription("Requested resource cannot be found");

    return new ResponseEntity<Object>(generalException, new HttpHeaders(), status);
  }

  public void setResponseToException(
      HttpServletRequest httpServletRequest,
      HttpServletResponse httpServletResponse,
      RuntimeException e)
      throws IOException {
    ResponseEntity<Object> objectResponseEntity =
        handleConflict(e, new ServletWebRequest(httpServletRequest));
    // set the response object
    httpServletResponse.setStatus(objectResponseEntity.getStatusCode().value());
    httpServletResponse.setContentType("application/json");

    // pass down the actual obj that exception handler normally send
    ObjectMapper mapper = new ObjectMapper();
    PrintWriter out = httpServletResponse.getWriter();
    out.print(mapper.writeValueAsString(objectResponseEntity.getBody()));
    out.flush();
  }
}
