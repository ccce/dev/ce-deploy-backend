/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.configuration;

import com.github.benmanes.caffeine.cache.Caffeine;
import java.util.concurrent.TimeUnit;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.caffeine.CaffeineCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;

/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 */

/** Class is used for cache-ing values temporarily in memory. */
@EnableCaching
@Configuration
public class CaffeineConfiguration {

  private static final String CACHE_EXPIRATION_IN_SEC = "cache.expiration.in.sec";
  private static final String NETBOX_CACHE_EXPIRATION_IN_SEC = "netbox.cache.expiration.in.sec";

  private Environment env;

  /**
   * Constructor for the Caching.
   *
   * @param env Environment variable to read configuration values.
   */
  public CaffeineConfiguration(Environment env) {
    this.env = env;
  }

  /**
   * Creates the cache manager with the proper configuration-
   *
   * @return the set-up cache-manager.
   */
  @Bean
  @Primary
  @Qualifier("defaultCacheManager")
  CacheManager defaultCacheManager() {
    CaffeineCacheManager caffeineCacheManager = new CaffeineCacheManager();
    caffeineCacheManager.setCaffeine(
        Caffeine.newBuilder()
            .expireAfterWrite(
                Integer.parseInt(env.getProperty(CACHE_EXPIRATION_IN_SEC)), TimeUnit.SECONDS));
    return caffeineCacheManager;
  }

  /**
   * Creates the cache manager with the proper configuration-
   *
   * @return the set-up cache-manager.
   */
  @Bean
  @Qualifier("netBoxCacheManager")
  CacheManager netBoxCacheManager() {
    CaffeineCacheManager caffeineCacheManager = new CaffeineCacheManager();
    caffeineCacheManager.setCaffeine(
        Caffeine.newBuilder()
            .expireAfterWrite(
                Integer.parseInt(env.getProperty(NETBOX_CACHE_EXPIRATION_IN_SEC)),
                TimeUnit.SECONDS));
    return caffeineCacheManager;
  }
}
