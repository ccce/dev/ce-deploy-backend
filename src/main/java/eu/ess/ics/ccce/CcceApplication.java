/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce;

import com.google.common.collect.ImmutableList;
import eu.ess.ics.ccce.configuration.FeatureFlagInterceptor;
import eu.ess.ics.ccce.configuration.FeatureFlags;
import eu.ess.ics.ccce.service.internal.featureflags.FeatureFlagsService;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.servers.Server;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/** Application class, instantiated by spring boot */
@SpringBootApplication(exclude = {SecurityAutoConfiguration.class})
@EnableScheduling
@EnableConfigurationProperties(FeatureFlags.class)
public class CcceApplication {
  private static final String OPENAPI_SERVER = "openapi.server";
  private final Environment env;
  private final FeatureFlagsService featureFlagsService;

  /**
   * Application constructor
   *
   * @param env object for getting environment variables
   */
  public CcceApplication(Environment env, FeatureFlagsService featureFlagsService) {
    this.env = env;
    this.featureFlagsService = featureFlagsService;
  }

  /**
   * Main function
   *
   * @param args Initial arguments
   */
  public static void main(String[] args) {
    SpringApplication.run(CcceApplication.class, args);
  }

  /**
   * Open API configuration
   *
   * @param appVersion application version from application properties
   * @return Open API descriptor
   */
  @Bean
  OpenAPI customOpenAPI(@Value("${api.version}") String appVersion) {
    return new OpenAPI()
        .info(
            new Info()
                .title("CE deploy & monitor API")
                .version(appVersion)
                .description("CE deploy & monitor backend"))
        .servers(
            ImmutableList.of(
                new Server()
                    .url(env.getProperty(OPENAPI_SERVER))
                    .description("CE deploy & monitor backend")));
  }

  /**
   * CORS configuration
   *
   * @return MVC configurer
   */
  @Bean
  WebMvcConfigurer corsConfigurer() {
    return new WebMvcConfigurer() {
      @Override
      public void addCorsMappings(CorsRegistry registry) {
        registry
            .addMapping("/**")
            .allowedOrigins("*")
            .allowedMethods("GET", "POST", "PUT", "PATCH", "DELETE");
      }

      @Override
      public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new FeatureFlagInterceptor(featureFlagsService));
      }
    };
  }
}
