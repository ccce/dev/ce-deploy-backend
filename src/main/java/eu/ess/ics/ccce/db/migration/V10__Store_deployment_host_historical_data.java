/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.db.migration;

import eu.ess.ics.ccce.common.conversion.ConversionUtil;
import eu.ess.ics.ccce.service.external.csentry.CSEntryService;
import eu.ess.ics.ccce.service.internal.deployment.dto.TargetHost;
import java.util.List;
import java.util.stream.Collectors;
import org.flywaydb.core.api.migration.BaseJavaMigration;
import org.flywaydb.core.api.migration.Context;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.SingleConnectionDataSource;
import org.springframework.stereotype.Component;

/**
 * Java based flyway migration for storing deployment host details from CSentry service
 *
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 */
@Component
public class V10__Store_deployment_host_historical_data extends BaseJavaMigration {
  private final CSEntryService csEntryService;

  /**
   * Constructor called by Spring framework
   *
   * @param csEntryService CSentry external service reference
   */
  public V10__Store_deployment_host_historical_data(CSEntryService csEntryService) {
    this.csEntryService = csEntryService;
  }

  @Override
  public void migrate(Context context) throws Exception {
    JdbcTemplate jdbcTemplate =
        new JdbcTemplate(new SingleConnectionDataSource(context.getConnection(), true));
    jdbcTemplate.execute("ALTER TABLE deployment ADD host_fqdn text NULL;");
    jdbcTemplate.execute("ALTER TABLE deployment ADD host_name text NULL;");
    jdbcTemplate.execute("ALTER TABLE deployment ADD host_network text NULL;");

    List<Long> cSentryIds = findAllCSentryIds(jdbcTemplate);
    for (Long csEntryId : cSentryIds) {
      TargetHost host = ConversionUtil.convertToTargetHost(csEntryService.findHostById(csEntryId));
      jdbcTemplate.execute(
          "UPDATE deployment SET"
              + " host_fqdn = '"
              + host.getFqdn()
              + "'"
              + ", host_name = '"
              + host.getHostName()
              + "'"
              + ", host_network = '"
              + host.getNetwork()
              + "' WHERE host_external_id = "
              + csEntryId);
    }

    jdbcTemplate.execute("ALTER TABLE deployment ALTER COLUMN host_fqdn SET NOT NULL;");
    jdbcTemplate.execute("ALTER TABLE deployment ALTER COLUMN host_name SET NOT NULL;");
    jdbcTemplate.execute("ALTER TABLE deployment ALTER COLUMN host_network SET NOT NULL;");
  }

  private List<Long> findAllCSentryIds(JdbcTemplate template) {
    String sql = "SELECT * FROM deployment";

    return template.query(sql, (rs, rowNum) -> rs.getLong("host_external_id")).stream()
        .distinct()
        .collect(Collectors.toList());
  }
}
