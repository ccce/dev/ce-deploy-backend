/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.db.migration;

import eu.ess.ics.ccce.service.external.gitlab.GitLabService;
import java.util.List;
import java.util.stream.Collectors;
import org.flywaydb.core.api.migration.BaseJavaMigration;
import org.flywaydb.core.api.migration.Context;
import org.gitlab.api.models.GitlabProject;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.SingleConnectionDataSource;
import org.springframework.stereotype.Component;

/**
 * Java based flyway migration for replacing git project URLs with project IDs in the database.
 *
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 */
@Component
public class V7__Git_project_ID_instead_of_URL extends BaseJavaMigration {
  private final GitLabService gitLabService;

  /**
   * Constructor called by Spring framework
   *
   * @param gitLabService Gitlab external service reference
   */
  public V7__Git_project_ID_instead_of_URL(GitLabService gitLabService) {
    this.gitLabService = gitLabService;
  }

  @Override
  public void migrate(Context context) throws Exception {
    JdbcTemplate jdbcTemplate =
        new JdbcTemplate(new SingleConnectionDataSource(context.getConnection(), true));
    migrateGitProjectsForIocVersions(jdbcTemplate);
    migrateGitProjectsForDeployments(jdbcTemplate);
  }

  private void migrateGitProjectsForIocVersions(JdbcTemplate jdbcTemplate) {
    jdbcTemplate.execute("ALTER TABLE ioc_version ADD git_project_id int8 NULL;");
    jdbcTemplate.execute(
        "ALTER TABLE ioc_version ADD CONSTRAINT ioc_version_project_id_un UNIQUE (git_project_id);");
    List<String> gitUrls = findAllGitUrls(jdbcTemplate, "ioc_version");
    for (String url : gitUrls) {
      GitlabProject project = gitLabService.projectInfo(url);
      jdbcTemplate.execute(
          "UPDATE ioc_version SET git_project_id ="
              + project.getId()
              + " WHERE source_url = '"
              + url
              + "'");
    }
    jdbcTemplate.execute("ALTER TABLE ioc_version ALTER COLUMN git_project_id SET NOT NULL;");
    jdbcTemplate.execute("ALTER TABLE ioc_version DROP CONSTRAINT ioc_version_un;");
    jdbcTemplate.execute("ALTER TABLE ioc_version DROP COLUMN source_url;");
  }

  private void migrateGitProjectsForDeployments(JdbcTemplate jdbcTemplate) {
    jdbcTemplate.execute("ALTER TABLE deployment ADD git_project_id int8 NULL;");
    List<String> gitUrls = findAllGitUrls(jdbcTemplate, "deployment");
    for (String url : gitUrls) {
      GitlabProject project = gitLabService.projectInfo(url);
      jdbcTemplate.execute(
          "UPDATE deployment SET git_project_id ="
              + project.getId()
              + " WHERE source_url = '"
              + url
              + "'");
    }
    jdbcTemplate.execute("ALTER TABLE deployment ALTER COLUMN git_project_id SET NOT NULL;");
    jdbcTemplate.execute("ALTER TABLE deployment DROP COLUMN source_url;");
  }

  private List<String> findAllGitUrls(JdbcTemplate template, String table) {
    String sql = "SELECT * FROM " + table;

    return template.query(sql, (rs, rowNum) -> rs.getString("source_url")).stream()
        .distinct()
        .collect(Collectors.toList());
  }
}
