/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.db.migration;

import eu.ess.ics.ccce.service.external.gitlab.GitLabService;
import java.util.List;
import java.util.stream.Collectors;
import org.flywaydb.core.api.migration.BaseJavaMigration;
import org.flywaydb.core.api.migration.Context;
import org.gitlab.api.models.GitlabProject;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.SingleConnectionDataSource;
import org.springframework.stereotype.Component;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 */
@Component
public class V17__Store_deployment_git_project_url_remove_ioc_owner extends BaseJavaMigration {
  private final GitLabService gitLabService;

  /**
   * Constructor called by Spring framework
   *
   * @param gitLabService Gitlab external service reference
   */
  public V17__Store_deployment_git_project_url_remove_ioc_owner(GitLabService gitLabService) {
    this.gitLabService = gitLabService;
  }

  @Override
  public void migrate(Context context) {
    JdbcTemplate jdbcTemplate =
        new JdbcTemplate(new SingleConnectionDataSource(context.getConnection(), true));
    jdbcTemplate.execute("ALTER TABLE deployment DROP COLUMN ioc_owner;");
    jdbcTemplate.execute("ALTER TABLE deployment ADD git_project_url text NULL;");
    List<Long> gitProjectIds = findAllGitProjectIds(jdbcTemplate);
    for (Long projectId : gitProjectIds) {
      GitlabProject project = gitLabService.projectInfo(projectId);
      jdbcTemplate.execute(
          "UPDATE deployment SET git_project_url ='"
              + project.getHttpUrl()
              + "' WHERE git_project_id = "
              + projectId);
    }
    jdbcTemplate.execute("ALTER TABLE deployment ALTER COLUMN git_project_url SET NOT NULL;");
  }

  private List<Long> findAllGitProjectIds(JdbcTemplate template) {
    java.lang.String sql = "SELECT * FROM deployment";

    return template.query(sql, (rs, rowNum) -> rs.getLong("git_project_id")).stream()
        .distinct()
        .collect(Collectors.toList());
  }
}
