/*
 *
 *  * Copyright (C) 2024 European Spallation Source ERIC.
 *  *
 *  * This program is free software; you can redistribute it and/or
 *  * modify it under the terms of the GNU General Public License
 *  * as published by the Free Software Foundation; either version 2
 *  * of the License, or (at your option) any later version.
 *  *
 *  * This program is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program; if not, write to the Free Software
 *  * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 *
 */

package eu.ess.ics.ccce.common.alert;

import static eu.ess.ics.ccce.common.alert.DeploymentAlertGenerator.HOST_NOT_FOUND_ALERT_MESSAGE;
import static org.junit.jupiter.api.Assertions.assertEquals;

import eu.ess.ics.ccce.rest.model.Alert;
import eu.ess.ics.ccce.rest.model.AlertSeverity;
import eu.ess.ics.ccce.rest.model.job.response.Deployment;
import eu.ess.ics.ccce.rest.model.job.response.Host;
import eu.ess.ics.ccce.rest.model.job.response.JobStatus;
import java.time.ZonedDateTime;
import java.util.List;
import org.junit.jupiter.api.Test;

public class DeploymentAlertGeneratorUT {

  private static final Long ID_1 = 1L;
  private static final String IOC_NAME_CHANGED = "TEST-IOC:2";
  private static final String HOST_ID = "MTZfdHJ1ZQ==";
  private static final String HOST_FQDN = "fqdn1";
  private static final String HOST_NAME = "hostname";
  private static final String NETWORK = "network";
  private static final Host HOST_WITH_INVALID_EXTERNAL_ID =
      new Host(HOST_ID, false, HOST_FQDN, HOST_NAME, NETWORK);
  private static final ZonedDateTime DATE_TIME = ZonedDateTime.now();
  private static final String CREATED_BY = "me";
  private static final String SOURCE_URL = "url";
  private static final String SOURCE_VERSION = "version";
  private static final String SOURCE_VERSION_SHORT = "shortVersion";
  private static final Deployment FAILED_DEPLOYMENT =
      new Deployment(
          ID_1,
          CREATED_BY,
          DATE_TIME,
          DATE_TIME,
          DATE_TIME,
          IOC_NAME_CHANGED,
          IOC_NAME_CHANGED,
          ID_1,
          SOURCE_URL,
          SOURCE_VERSION,
          SOURCE_VERSION_SHORT,
          HOST_WITH_INVALID_EXTERNAL_ID,
          JobStatus.FAILED,
          ID_1,
          false,
          ID_1);

  @Test
  public void generateDeploymentDetailsAlert() {
    DeploymentAlertGenerator deploymentAlertGenerator = new DeploymentAlertGenerator();
    List<Alert> actual = deploymentAlertGenerator.generateDeploymentDetailsAlert(FAILED_DEPLOYMENT);
    List<Alert> expected =
        List.of(
            new Alert(
                AlertSeverity.INFO,
                HOST_NOT_FOUND_ALERT_MESSAGE
                    + deploymentAlertGenerator.extractHostInfoForAlert(HOST_ID)));
    assertEquals(expected, actual);
  }
}
