/*
 *
 *  * Copyright (C) 2024 European Spallation Source ERIC.
 *  *
 *  * This program is free software; you can redistribute it and/or
 *  * modify it under the terms of the GNU General Public License
 *  * as published by the Free Software Foundation; either version 2
 *  * of the License, or (at your option) any later version.
 *  *
 *  * This program is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program; if not, write to the Free Software
 *  * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 *
 */

package eu.ess.ics.ccce.common.alert;

import static eu.ess.ics.ccce.common.alert.IOCAlertGenerator.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import eu.ess.ics.ccce.repository.entity.AwxJobEntity;
import eu.ess.ics.ccce.repository.entity.IocDeploymentEntity;
import eu.ess.ics.ccce.rest.model.Alert;
import eu.ess.ics.ccce.rest.model.AlertSeverity;
import eu.ess.ics.ccce.rest.model.job.response.Deployment;
import eu.ess.ics.ccce.rest.model.job.response.Host;
import eu.ess.ics.ccce.rest.model.job.response.JobStatus;
import eu.ess.ics.ccce.service.internal.deployment.dto.DeploymentType;
import eu.ess.ics.ccce.service.internal.ioc.dto.NamingResponseDTO;
import java.time.ZonedDateTime;
import java.util.List;
import org.gitlab.api.models.GitlabProject;
import org.junit.jupiter.api.Test;

/**
 * @author <a href="mailto:domonkos.gulyas@ess.eu">Domonkos Gulyas</a>
 */
public class IOCAlertGeneratorUT {

  private static final Long ID_1 = 1L;
  private static final Long ID_2 = 2L;
  private static final String IOC_NAME = "TEST-IOC:1";
  private static final String IOC_NAME_CHANGED = "TEST-IOC:2";
  private static final String HOST_ID = "MTZfdHJ1ZQ==";
  private static final GitlabProject GITLAB_PROJECT = new GitlabProject();
  private static final GitlabProject ARCHIVED_GITLAB_PROJECT = new GitlabProject();
  private static final String HOST_FQDN = "fqdn1";
  private static final String HOST_NAME = "hostname";
  private static final String NETWORK = "network";
  private static final Host HOST = new Host(HOST_ID, true, HOST_FQDN, HOST_NAME, NETWORK);
  private static final Host HOST_WITH_INVALID_EXTERNAL_ID =
      new Host(HOST_ID, false, HOST_FQDN, HOST_NAME, NETWORK);
  private static final ZonedDateTime DATE_TIME = ZonedDateTime.now();
  private static final String CREATED_BY = "me";
  private static final String SOURCE_URL = "url";
  private static final String SOURCE_VERSION = "version";
  private static final String SOURCE_VERSION_SHORT = "shortVersion";
  private static final Deployment FAILED_DEPLOYMENT =
      new Deployment(
          ID_1,
          CREATED_BY,
          DATE_TIME,
          DATE_TIME,
          DATE_TIME,
          IOC_NAME_CHANGED,
          IOC_NAME_CHANGED,
          ID_1,
          SOURCE_URL,
          SOURCE_VERSION,
          SOURCE_VERSION_SHORT,
          HOST_WITH_INVALID_EXTERNAL_ID,
          JobStatus.FAILED,
          ID_1,
          false,
          ID_1);
  private static final Deployment SUCCESSFUL_DEPLOYMENT =
      new Deployment(
          ID_2,
          CREATED_BY,
          DATE_TIME,
          DATE_TIME,
          DATE_TIME,
          IOC_NAME,
          IOC_NAME,
          ID_2,
          SOURCE_URL,
          SOURCE_VERSION,
          SOURCE_VERSION_SHORT,
          HOST,
          JobStatus.SUCCESSFUL,
          ID_2,
          false,
          ID_2);

  private static final Deployment SUCCESSFUL_UNDEPLOYMENT =
      new Deployment(
          ID_2,
          CREATED_BY,
          DATE_TIME,
          DATE_TIME,
          DATE_TIME,
          IOC_NAME,
          IOC_NAME,
          ID_2,
          SOURCE_URL,
          SOURCE_VERSION,
          SOURCE_VERSION_SHORT,
          HOST,
          JobStatus.SUCCESSFUL,
          ID_2,
          true,
          ID_2);

  private static final String IOC_DESCRIPTION = "Test IOC";
  private static final NamingResponseDTO NAMING_RESPONSE =
      new NamingResponseDTO.NamingResponseBuilder()
          .currentIocName(IOC_NAME)
          .description(IOC_DESCRIPTION)
          .build();
  private static final NamingResponseDTO NAMING_RESPONSE_WITH_ISSUES =
      new NamingResponseDTO.NamingResponseBuilder()
          .currentIocName(IOC_NAME_CHANGED)
          .description(IOC_DESCRIPTION)
          .nameInactive()
          .typeInvalid()
          .nameChanged()
          .nameDeleted()
          .build();
  private static final IocDeploymentEntity LATEST_DEPLOYMENT = new IocDeploymentEntity();
  private static final IocDeploymentEntity LATEST_UNDEPLOYMENT = new IocDeploymentEntity();
  private static final IocDeploymentEntity LATEST_SUCCESFUL_UNDEPLOYMENT =
      new IocDeploymentEntity();

  static {
    GITLAB_PROJECT.setArchived(false);
    GITLAB_PROJECT.setId(1);
    GITLAB_PROJECT.setHttpUrl("url1");

    ARCHIVED_GITLAB_PROJECT.setArchived(true);
    ARCHIVED_GITLAB_PROJECT.setId(2);
    ARCHIVED_GITLAB_PROJECT.setHttpUrl("url2");

    LATEST_DEPLOYMENT.setDeploymentType(DeploymentType.DEPLOY.name());
    AwxJobEntity awxJobEntity = new AwxJobEntity();
    awxJobEntity.setStatus("FAILED");
    LATEST_DEPLOYMENT.setAwxJob(awxJobEntity);

    LATEST_UNDEPLOYMENT.setDeploymentType(DeploymentType.UNDEPLOY.name());
    LATEST_UNDEPLOYMENT.setAwxJob(awxJobEntity);

    AwxJobEntity undeployAwxJobEntity = new AwxJobEntity();
    undeployAwxJobEntity.setStatus("SUCCESSFUL");

    LATEST_SUCCESFUL_UNDEPLOYMENT.setDeploymentType(DeploymentType.UNDEPLOY.name());
    LATEST_SUCCESFUL_UNDEPLOYMENT.setAwxJob(undeployAwxJobEntity);
  }

  @Test
  public void generateIOCAlertsForUndeployed() {
    IOCAlertGenerator iocAlertGenerator = createAlertGenerator();

    List<Alert> actual =
        iocAlertGenerator.generateAlerts(
            IOC_NAME,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            NAMING_RESPONSE,
            GITLAB_PROJECT,
            false,
            false,
            false,
            null);
    List<Alert> expected = List.of(new Alert(AlertSeverity.INFO, IOC_NOT_DEPLOYED_ALERT_MESSAGE));
    assertEquals(expected, actual);
  }

  @Test
  public void generateIOCAlertsForWithUndeploymentFailed() {
    IOCAlertGenerator iocAlertGenerator = createAlertGenerator();

    List<Alert> actual =
        iocAlertGenerator.generateAlerts(
            IOC_NAME,
            SUCCESSFUL_DEPLOYMENT,
            LATEST_UNDEPLOYMENT,
            null,
            null,
            null,
            null,
            null,
            null,
            NAMING_RESPONSE,
            GITLAB_PROJECT,
            true,
            false,
            false,
            null);
    List<Alert> expected =
        List.of(new Alert(AlertSeverity.ERROR, LATEST_UNDEPLOYMENT_FAILED_ALERT_MESSAGE));
    assertEquals(expected, actual);
  }

  @Test
  public void generateAlertsForDeployedIocWithIOCAlerts() {
    IOCAlertGenerator iocAlertGenerator = createAlertGenerator();
    List<Alert> actual =
        iocAlertGenerator.generateAlerts(
            IOC_NAME,
            FAILED_DEPLOYMENT,
            LATEST_DEPLOYMENT,
            true,
            true,
            true,
            true,
            true,
            true,
            NAMING_RESPONSE_WITH_ISSUES,
            ARCHIVED_GITLAB_PROJECT,
            false,
            false,
            true,
            false);

    List<Alert> expected =
        List.of(
            new Alert(AlertSeverity.ERROR, HOST_NOT_ACCESSIBLE_ALERT_MESSAGE),
            new Alert(AlertSeverity.ERROR, LATEST_DEPLOYMENT_FAILED_ALERT_MESSAGE),
            new Alert(AlertSeverity.ERROR, IOC_SHOULD_NOT_BE_RUNNING_ALERT_MESSAGE),
            new Alert(AlertSeverity.ERROR, GIT_REFERENCE_ID_ALERT_MESSAGE),
            new Alert(AlertSeverity.ERROR, IOC_DELETED_FROM_NAMING_ALERT_MESSAGE),
            new Alert(AlertSeverity.ERROR, NAME_INACTIVATED_IN_NAMING_ALERT_MESSAGE),
            new Alert(AlertSeverity.ERROR, NAME_HAS_INVALID_TYPE_ALERT_MESSAGE),
            new Alert(AlertSeverity.ERROR, IOC_HAS_CORE_DUMP_ALERT_MESSAGE),
            new Alert(AlertSeverity.ERROR, CELLMODE_IS_BEING_USED_ALERT_MESSAGE),
            new Alert(AlertSeverity.WARNING, EXPORTER_PROBLEM_ALERT_MESSAGE),
            new Alert(AlertSeverity.WARNING, GIT_LOCAL_COMMIT_ALERT_MESSAGE),
            new Alert(AlertSeverity.WARNING, GIT_REPOSITORY_DIRTY_ALERT_MESSAGE),
            new Alert(AlertSeverity.WARNING, ESSIOC_MISSING_ALERT_MESSAGE),
            new Alert(
                AlertSeverity.WARNING,
                "IOC name has been changed to %s, please take action and redeploy IOC"
                    .formatted(IOC_NAME_CHANGED)),
            new Alert(AlertSeverity.WARNING, GIT_PROJECT_ARCHIVED_ALERT_MESSAGE),
            new Alert(AlertSeverity.WARNING, NAME_DIFFERS_FROM_DEPLOYED_VERSION_ALERT_MESSAGE),
            new Alert(AlertSeverity.WARNING, HOST_INFORMATION_CHANGED_ALERT_MESSAGE));

    // assert equals ignore order
    assertEquals(expected.size(), actual.size());
    assertTrue(expected.containsAll(actual));
    assertTrue(actual.containsAll(expected));
  }

  @Test
  void iocShouldNotRun() {
    IOCAlertGenerator iocAlertGenerator = createAlertGenerator();

    List<Alert> actual =
        iocAlertGenerator.generateAlerts(
            IOC_NAME,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            NAMING_RESPONSE,
            GITLAB_PROJECT,
            false,
            false,
            true,
            false);
    List<Alert> expected =
        List.of(new Alert(AlertSeverity.ERROR, IOC_SHOULD_NOT_BE_RUNNING_ALERT_MESSAGE));
    assertEquals(expected, actual);
  }

  @Test
  void undeployedIoc() {
    IOCAlertGenerator iocAlertGenerator = createAlertGenerator();

    List<Alert> actual =
        iocAlertGenerator.generateAlerts(
            IOC_NAME,
            SUCCESSFUL_UNDEPLOYMENT,
            LATEST_SUCCESFUL_UNDEPLOYMENT,
            null,
            null,
            null,
            null,
            null,
            null,
            NAMING_RESPONSE,
            GITLAB_PROJECT,
            true,
            false,
            false,
            null);
    List<Alert> expected = List.of(new Alert(AlertSeverity.INFO, IOC_NOT_DEPLOYED_ALERT_MESSAGE));
    assertEquals(expected, actual);
  }

  private IOCAlertGenerator createAlertGenerator() {
    return new IOCAlertGenerator();
  }
}
