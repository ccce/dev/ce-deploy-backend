/*
 *
 *  * Copyright (C) 2024 European Spallation Source ERIC.
 *  *
 *  * This program is free software; you can redistribute it and/or
 *  * modify it under the terms of the GNU General Public License
 *  * as published by the Free Software Foundation; either version 2
 *  * of the License, or (at your option) any later version.
 *  *
 *  * This program is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program; if not, write to the Free Software
 *  * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 *
 */

package eu.ess.ics.ccce.common.alert;

import static eu.ess.ics.ccce.common.alert.HostAlertGenerator.*;
import static eu.ess.ics.ccce.common.alert.IOCAlertGenerator.EXPORTER_PROBLEM_ALERT_MESSAGE;
import static org.junit.jupiter.api.Assertions.assertEquals;

import eu.ess.ics.ccce.rest.model.Alert;
import eu.ess.ics.ccce.rest.model.AlertSeverity;
import eu.ess.ics.ccce.service.internal.status.StatusService;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class HostAlertGeneratorUT {
  private static final int IOCS_ON_HOST_LIMIT = 20;
  private static final String HOST_FQDN = "fqdn1";

  @Mock private StatusService statusServiceMock;

  private HostAlertGenerator createAlertGenerator() {
    return new HostAlertGenerator(null, statusServiceMock, IOCS_ON_HOST_LIMIT);
  }

  @Test
  public void generateHostDetailsAlertWithoutAnyDeployments() {
    HostAlertGenerator hostAlertGenerator = createAlertGenerator();
    Mockito.when(statusServiceMock.checkIOCExporterForHost(HOST_FQDN)).thenReturn(true);
    Mockito.when(statusServiceMock.checkHostIsActive(HOST_FQDN)).thenReturn(true);
    List<Alert> actual = hostAlertGenerator.generateHostDetailsAlert(HOST_FQDN, 0);
    List<Alert> expected =
        List.of(new Alert(AlertSeverity.INFO, NO_IOC_DEPLOYED_ON_HOST_ALERT_MESSAGE));
    assertEquals(expected, actual);
  }

  @Test
  public void generateHostDetailsAlertForHostWithDeploymentLimitExceeded() {
    HostAlertGenerator hostAlertGenerator = createAlertGenerator();
    Mockito.when(statusServiceMock.checkIOCExporterForHost(HOST_FQDN)).thenReturn(true);
    Mockito.when(statusServiceMock.checkHostIsActive(HOST_FQDN)).thenReturn(true);
    List<Alert> actual = hostAlertGenerator.generateHostDetailsAlert(HOST_FQDN, 100);
    List<Alert> expected =
        List.of(
            new Alert(
                AlertSeverity.WARNING,
                HOST_CAPACITY_REACHED_ALERT_MESSAGE_TEMPLATE.formatted(IOCS_ON_HOST_LIMIT)));
    assertEquals(expected, actual);
  }

  @Test
  public void generateHostDetailsAlertForHostWithExporterNotWorking() {
    HostAlertGenerator hostAlertGenerator = createAlertGenerator();
    Mockito.when(statusServiceMock.checkIOCExporterForHost(HOST_FQDN)).thenReturn(null);
    Mockito.when(statusServiceMock.checkHostIsActive(HOST_FQDN)).thenReturn(true);
    List<Alert> actual = hostAlertGenerator.generateHostDetailsAlert(HOST_FQDN, 10);
    List<Alert> expected =
        List.of(new Alert(AlertSeverity.WARNING, EXPORTER_PROBLEM_ALERT_MESSAGE));
    assertEquals(expected, actual);
  }

  @Test
  public void generateHostDetailsAlertForHostWithUnreacheableHost() {
    HostAlertGenerator hostAlertGenerator = createAlertGenerator();
    Mockito.when(statusServiceMock.checkIOCExporterForHost(HOST_FQDN)).thenReturn(true);
    Mockito.when(statusServiceMock.checkHostIsActive(HOST_FQDN)).thenReturn(false);
    List<Alert> actual = hostAlertGenerator.generateHostDetailsAlert(HOST_FQDN, 10);
    List<Alert> expected = List.of(new Alert(AlertSeverity.INFO, HOST_NOT_REACHABLE_ALERT_MESSAGE));
    assertEquals(expected, actual);
  }
}
