package eu.ess.ics.ccce.common;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class UtilsTest {

  @Test
  void cloneFromJavaSqlDate() {

    long millis = 123456789;

    java.sql.Date sqlDate = new java.sql.Date(millis);

    assertEquals(sqlDate.getTime(), millis);

    java.util.Date cloneDate = Utils.cloneDate(sqlDate);

    assertEquals(cloneDate.getTime(), millis);
    assertTrue(cloneDate.equals(sqlDate));
  }

  @Test
  void cloneFromJavaUtilDate() {

    long millis = 123456789;

    java.util.Date utilDate = new java.sql.Date(millis);

    assertEquals(utilDate.getTime(), millis);

    java.util.Date cloneDate = Utils.cloneDate(utilDate);

    assertEquals(cloneDate.getTime(), millis);
    assertTrue(cloneDate.equals(utilDate));
  }
}
