/*
 *
 *  * Copyright (C) 2024 European Spallation Source ERIC.
 *  *
 *  * This program is free software; you can redistribute it and/or
 *  * modify it under the terms of the GNU General Public License
 *  * as published by the Free Software Foundation; either version 2
 *  * of the License, or (at your option) any later version.
 *  *
 *  * This program is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program; if not, write to the Free Software
 *  * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 *
 */

package eu.ess.ics.ccce.common.alert;

import static eu.ess.ics.ccce.common.alert.RecordAlertGenerator.RECORD_IS_AN_ALIAS;
import static org.junit.jupiter.api.Assertions.assertEquals;

import eu.ess.ics.ccce.rest.model.Alert;
import eu.ess.ics.ccce.rest.model.AlertSeverity;
import eu.ess.ics.ccce.rest.model.record.PVStatus;
import eu.ess.ics.ccce.rest.model.record.response.Record;
import java.util.List;
import org.junit.jupiter.api.Test;

class RecordAlertGeneratorUT {

  @Test
  void generateRecordIsAnAliasAlert() {
    RecordAlertGenerator recordAlertGenerator = new RecordAlertGenerator();
    List<Alert> actual =
        recordAlertGenerator.generateAlerts(
            new Record(
                "test_record",
                "iocName",
                1L,
                "hostname",
                PVStatus.ACTIVE,
                "iocVersion",
                "description",
                "recordType",
                "alias"));
    List<Alert> expected = List.of(new Alert(AlertSeverity.INFO, RECORD_IS_AN_ALIAS));
    assertEquals(expected, actual);
  }
}
