/*
 * Copyright (C) 2024 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.repository;

import eu.ess.ics.ccce.common.Utils;
import eu.ess.ics.ccce.repository.entity.MaintenanceModeEntity;
import eu.ess.ics.ccce.repository.impl.MaintenanceModeRepository;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.ZonedDateTime;
import java.util.Date;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.EmptyResultDataAccessException;

@SpringBootTest
class MaintenanceRepoIT {

  @Autowired MaintenanceModeRepository maintenanceModeRepository;

  @AfterEach
  public void tearDown() throws SQLException {
    Connection conn = DriverManager.getConnection("jdbc:h2:mem:db:public", "ccceuser", "sa");
    Statement st = conn.createStatement();
    st.executeUpdate("set referential_integrity = false");
    st.executeUpdate("delete from maintenance_mode");
    st.executeUpdate("set referential_integrity = true");
    conn.close();
  }

  private static final ZonedDateTime zonedDateTimeNow = ZonedDateTime.now();
  private static final Date dateNow = Utils.toUtilDate(zonedDateTimeNow);

  @Test
  void createMaintenanceMode() {
    MaintenanceModeEntity maintenanceModeEntity = new MaintenanceModeEntity();
    maintenanceModeEntity.setStartAt(dateNow);
    maintenanceModeEntity.setMessage("Message 1");
    maintenanceModeRepository.createMaintenanceMode(maintenanceModeEntity);
    MaintenanceModeEntity foundMaintenanceModeEntity = maintenanceModeRepository.find();
    Assertions.assertEquals(
        foundMaintenanceModeEntity.getStartAt().getTime(),
        maintenanceModeEntity.getStartAt().getTime());
    Assertions.assertEquals(
        foundMaintenanceModeEntity.getMessage(), maintenanceModeEntity.getMessage());
  }

  @Test
  void updateMaintenanceMode() {
    MaintenanceModeEntity maintenanceModeEntity = new MaintenanceModeEntity();
    maintenanceModeEntity.setStartAt(dateNow);
    maintenanceModeEntity.setMessage("Message 0");
    maintenanceModeRepository.createMaintenanceMode(maintenanceModeEntity);
    MaintenanceModeEntity maintenanceModeEntity1 = new MaintenanceModeEntity();
    maintenanceModeEntity1.setStartAt(dateNow);
    maintenanceModeEntity1.setMessage("Message 1");
    maintenanceModeRepository.updateMaintenanceMode(maintenanceModeEntity1);
    MaintenanceModeEntity foundMaintenanceModeEntity = maintenanceModeRepository.find();
    Assertions.assertEquals(
        foundMaintenanceModeEntity.getStartAt().getTime(),
        maintenanceModeEntity1.getStartAt().getTime());
    Assertions.assertEquals(
        foundMaintenanceModeEntity.getMessage(), maintenanceModeEntity1.getMessage());
  }

  @Test
  void deleteMaintenanceMode() {

    MaintenanceModeEntity maintenanceModeEntity = new MaintenanceModeEntity();
    maintenanceModeEntity.setStartAt(dateNow);
    maintenanceModeEntity.setMessage("Message 1");
    maintenanceModeRepository.createMaintenanceMode(maintenanceModeEntity);
    MaintenanceModeEntity foundMaintenanceModeEntity = maintenanceModeRepository.find();
    Assertions.assertNotNull(foundMaintenanceModeEntity);
    maintenanceModeRepository.deleteMaintenanceMode();
    Assertions.assertThrows(
        EmptyResultDataAccessException.class, () -> maintenanceModeRepository.find());
  }
}
