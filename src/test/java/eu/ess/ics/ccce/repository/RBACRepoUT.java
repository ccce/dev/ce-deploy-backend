/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.repository;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import eu.ess.ics.ccce.exceptions.*;
import eu.ess.ics.ccce.service.external.common.HttpClientService;
import eu.ess.ics.ccce.service.external.common.HttpClientService.*;
import eu.ess.ics.ccce.service.external.rbac.RBACService;
import eu.ess.ics.ccce.service.external.rbac.model.RBACToken;
import java.net.SocketTimeoutException;
import okhttp3.Headers;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.env.Environment;

/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 */
@SpringBootTest
public class RBACRepoUT {
  @Mock private Environment env;
  @Mock private HttpClientService httpClientService;

  @Test
  void successfulLogin() throws RemoteServiceException, JsonProcessingException, ParseException {

    RBACService rbacRepo = createRepo();
    Mockito.when(env.getProperty(ArgumentMatchers.anyString())).thenReturn("test-env-value");

    ServiceResponse<RBACToken> loginToken = createToken();

    Mockito.when(
            httpClientService.executePostRequest(
                ArgumentMatchers.any(Headers.class),
                ArgumentMatchers.anyString(),
                ArgumentMatchers.isNull(),
                ArgumentMatchers.eq(HttpClientService.XML_MEDIA_TYPE),
                ArgumentMatchers.eq(RBACToken.class)))
        .thenReturn(loginToken);

    RBACToken rbacToken = rbacRepo.loginUser("testUser", "testPassword");

    assertEquals(loginToken.getEntity().getId(), rbacToken.getId());
    assertEquals(loginToken.getEntity().getUserName(), rbacToken.getUserName());
    assertEquals(loginToken.getEntity().getSignature(), rbacToken.getSignature());
  }

  @Test
  void badLogin() throws RemoteServiceException, JsonProcessingException, ParseException {

    RBACService rbacRepo = createRepo();
    Mockito.when(env.getProperty(ArgumentMatchers.anyString())).thenReturn("test-env-value");

    ServiceResponse<RBACToken> loginToken = badToken();

    Mockito.when(
            httpClientService.executePostRequest(
                ArgumentMatchers.any(Headers.class),
                ArgumentMatchers.anyString(),
                ArgumentMatchers.isNull(),
                ArgumentMatchers.eq(HttpClientService.XML_MEDIA_TYPE),
                ArgumentMatchers.eq(RBACToken.class)))
        .thenReturn(loginToken);

    assertThrows(
        AuthenticationException.class,
        () -> {
          rbacRepo.loginUser("testUser", "testPassword");
        });
  }

  @Test
  void userInfoFromTokenSuccess()
      throws RemoteServiceException,
          JsonProcessingException,
          ParseException,
          SocketTimeoutException {
    RBACService rbacRepo = createRepo();
    Mockito.when(env.getProperty(ArgumentMatchers.anyString())).thenReturn("test-env-value");
    ServiceResponse<RBACToken> loginToken = createToken();

    Mockito.when(
            httpClientService.executeGetRequest(
                ArgumentMatchers.any(Headers.class),
                ArgumentMatchers.anyString(),
                ArgumentMatchers.eq(HttpClientService.XML_MEDIA_TYPE),
                ArgumentMatchers.eq(RBACToken.class)))
        .thenReturn(loginToken);
    RBACToken token = rbacRepo.userInfoFromToken(loginToken.getEntity().getId());
    assertNotNull(token);
    assertEquals(loginToken.getEntity().getId(), token.getId());
  }

  @Test
  void userInfoFromTokenFail()
      throws RemoteServiceException,
          JsonProcessingException,
          ParseException,
          SocketTimeoutException {
    RBACService rbacRepo = createRepo();
    Mockito.when(env.getProperty(ArgumentMatchers.anyString())).thenReturn("test-env-value");
    ServiceResponse<RBACToken> loginToken = createToken();

    Mockito.when(
            httpClientService.executeGetRequest(
                ArgumentMatchers.any(Headers.class),
                ArgumentMatchers.anyString(),
                ArgumentMatchers.eq(HttpClientService.XML_MEDIA_TYPE),
                ArgumentMatchers.eq(RBACToken.class)))
        .thenReturn(tokenNotFound());
    assertThrows(
        UnauthorizedException.class,
        () -> rbacRepo.userInfoFromToken(loginToken.getEntity().getId()));
  }

  @Test
  void renewTokenSuccess() throws RemoteServiceException, JsonProcessingException, ParseException {
    RBACService rbacRepo = createRepo();
    Mockito.when(env.getProperty(ArgumentMatchers.anyString())).thenReturn("test-env-value");
    ServiceResponse<RBACToken> loginToken = createToken();
    ServiceResponse<RBACToken> renewToken = renewToken();

    Mockito.when(
            httpClientService.executePostRequest(
                ArgumentMatchers.any(Headers.class),
                ArgumentMatchers.anyString(),
                ArgumentMatchers.isNull(),
                ArgumentMatchers.eq(HttpClientService.XML_MEDIA_TYPE),
                ArgumentMatchers.eq(RBACToken.class)))
        .thenReturn(renewToken);
    RBACToken token = rbacRepo.renewToken(loginToken.getEntity().getId());
    assertNotNull(token);
    assertEquals(loginToken.getEntity().getId(), token.getId());
    assertTrue(token.getExpirationTime().getTime() > token.getCreationTime().getTime());
  }

  @Test
  void renewTokenFail() throws RemoteServiceException, JsonProcessingException, ParseException {
    RBACService rbacRepo = createRepo();
    Mockito.when(env.getProperty(ArgumentMatchers.anyString())).thenReturn("test-env-value");
    ServiceResponse<RBACToken> loginToken = createToken();

    Mockito.when(
            httpClientService.executePostRequest(
                ArgumentMatchers.any(Headers.class),
                ArgumentMatchers.anyString(),
                ArgumentMatchers.isNull(),
                ArgumentMatchers.eq(HttpClientService.XML_MEDIA_TYPE),
                ArgumentMatchers.eq(RBACToken.class)))
        .thenReturn(tokenNotFound());
    assertThrows(
        UnauthorizedException.class, () -> rbacRepo.renewToken(loginToken.getEntity().getId()));
  }

  @Test
  void deleteTokenSuccess() throws RemoteServiceException, JsonProcessingException, ParseException {
    RBACService rbacRepo = createRepo();
    Mockito.when(env.getProperty(ArgumentMatchers.anyString())).thenReturn("test-env-value");
    ServiceResponse<RBACToken> loginToken = createToken();

    Mockito.when(
            httpClientService.executeDeleteRequest(
                ArgumentMatchers.any(Headers.class), ArgumentMatchers.anyString()))
        .thenReturn(204);
    assertDoesNotThrow(() -> rbacRepo.deleteToken(loginToken.getEntity().getId()));
  }

  @Test
  void deleteTokenFail() throws RemoteServiceException, JsonProcessingException, ParseException {
    RBACService rbacRepo = createRepo();
    Mockito.when(env.getProperty(ArgumentMatchers.anyString())).thenReturn("test-env-value");
    ServiceResponse<RBACToken> loginToken = createToken();

    Mockito.when(
            httpClientService.executeDeleteRequest(
                ArgumentMatchers.any(Headers.class), ArgumentMatchers.anyString()))
        .thenReturn(500);
    assertThrows(RemoteException.class, () -> rbacRepo.deleteToken(loginToken.getEntity().getId()));
  }

  private ServiceResponse<RBACToken> createToken() throws JsonProcessingException {
    String xml =
        """
        <?xml version="1.0" encoding="UTF-8" standalone="yes"?>
        <token>
            <id>AAAA-BBBBB-CCCC-DDDD-EEFFGGHH</id>
            <username>johndoe</username>
            <firstName>John</firstName>
            <lastName>Doe</lastName>
            <creationTime>1620823391053</creationTime>
            <expirationTime>1620852191053</expirationTime>
            <ip>10.0.42.5</ip>
            <roles>
                <role>CCDBAdministrator</role>
                <role>CableAdministrator</role>
                <role>IOCFactoryAdministrator</role>
                <role>NamingAdministrator</role>
            </roles>
            <signature>764AAF5B29498D3424226C6FF54E5BD7FC0751C35E8986CEBFA25E8867FBEBC2F9482C6189CDEFC8312BEB244D3E122B2E1876344BD8CC7C9B08420A3A536169</signature>
        </token>""";

    XmlMapper xmlMapper = new XmlMapper();

    RBACToken rbacToken = xmlMapper.readValue(xml, RBACToken.class);

    return new ServiceResponse<>(rbacToken, 200, null);
  }

  private ServiceResponse<RBACToken> renewToken() throws JsonProcessingException {
    String xml =
        """
        <?xml version="1.0" encoding="UTF-8" standalone="yes"?>
        <token>
            <id>AAAA-BBBBB-CCCC-DDDD-EEFFGGHH</id>
            <username>johndoe</username>
            <firstName>John</firstName>
            <lastName>Doe</lastName>
            <creationTime>1620823391053</creationTime>
            <expirationTime>1620852291053</expirationTime>
            <ip>10.0.42.5</ip>
            <roles>
                <role>CCDBAdministrator</role>
                <role>CableAdministrator</role>
                <role>IOCFactoryAdministrator</role>
                <role>NamingAdministrator</role>
            </roles>
            <signature>764AAF5B29498D3424226C6FF54E5BD7FC0751C35E8986CEBFA25E8867FBEBC2F9482C6189CDEFC8312BEB244D3E122B2E1876344BD8CC7C9B08420A3A536169</signature>
        </token>""";

    XmlMapper xmlMapper = new XmlMapper();

    RBACToken rbacToken = xmlMapper.readValue(xml, RBACToken.class);

    return new ServiceResponse<>(rbacToken, 200, null);
  }

  private ServiceResponse<RBACToken> badToken() throws JsonProcessingException {

    return new ServiceResponse<>(null, 401, null);
  }

  private ServiceResponse<RBACToken> tokenNotFound() throws JsonProcessingException {

    return new ServiceResponse<>(null, 404, null);
  }

  private RBACService createRepo() {
    return new RBACService(env, httpClientService);
  }
}
