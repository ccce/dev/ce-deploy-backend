package eu.ess.ics.ccce.repository;

import static org.junit.jupiter.api.Assertions.*;

import eu.ess.ics.ccce.repository.api.IIocDeploymentRepository;
import eu.ess.ics.ccce.repository.entity.IocDeploymentEntity;
import eu.ess.ics.ccce.repository.entity.IocEntity;
import eu.ess.ics.ccce.repository.impl.IocRepository;
import eu.ess.ics.ccce.rest.model.ioc.request.IocDeploymentStatus;
import eu.ess.ics.ccce.rest.model.ioc.request.IocOrder;
import eu.ess.ics.ccce.service.external.awx.model.JobStatus;
import eu.ess.ics.ccce.service.internal.deployment.dto.DeploymentType;
import eu.ess.ics.ccce.service.internal.deployment.dto.TargetHost;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 */
@SpringBootTest
public class IocRepoIT {

  @Autowired private IocRepository repository;

  @Autowired private IIocDeploymentRepository deploymentRepository;

  @AfterEach
  void clean() throws SQLException {
    Connection conn = DriverManager.getConnection("jdbc:h2:mem:db:public", "ccceuser", "sa");
    Statement st = conn.createStatement();
    st.executeUpdate("set referential_integrity false");
    st.executeUpdate("delete from ioc");
    st.executeUpdate("alter table ioc alter column id restart with 1");
    st.executeUpdate("delete from ioc_deployment");
    st.executeUpdate("alter table ioc_deployment alter column id restart with 1");
    st.executeUpdate("delete from awx_job");
    st.executeUpdate("alter table awx_job alter column id restart with 1");
    st.executeUpdate("set referential_integrity true");
    conn.close();
  }

  @Test
  public void saveIoc() {
    IocEntity ioc = EntityFactory.createIoc(repository, "test-ioc", "user", 10L);
    List<IocEntity> iocs = repository.findAll(null, null, null, null, null, null, 0, 10, false);
    IocEntity savedIoc =
        iocs.stream().filter(i -> ioc.getId().equals(i.getId())).findFirst().orElse(null);
    assertNotNull(savedIoc);
    assertEquals(ioc.getCreatedBy(), savedIoc.getCreatedBy());
  }

  @Test
  public void updateIoc() {
    IocEntity ioc = EntityFactory.createIoc(repository, "test-ioc", "user");
    List<IocEntity> iocs = repository.findAll(null, null, null, null, null, null, 0, 10, false);
    IocEntity savedIoc =
        iocs.stream().filter(i -> ioc.getId().equals(i.getId())).findFirst().orElse(null);
    assertNotNull(savedIoc);
    assertEquals(ioc.getCreatedBy(), savedIoc.getCreatedBy());
    savedIoc.setCreatedBy("user2");
    repository.updateIoc(savedIoc);
    IocEntity updatedIoc =
        iocs.stream().filter(i -> savedIoc.getId().equals(i.getId())).findFirst().orElse(null);
    assertNotNull(updatedIoc);
    assertEquals(savedIoc.getCreatedBy(), updatedIoc.getCreatedBy());
  }

  @Test
  public void findIocById() {
    IocEntity ioc = EntityFactory.createIoc(repository, "test-ioc", "user");
    List<IocEntity> iocs = repository.findAll(null, null, null, null, null, null, 0, 10, false);
    IocEntity iocFound =
        iocs.stream().filter(i -> ioc.getId().equals(i.getId())).findFirst().orElse(null);
    assertNotNull(iocFound);
    IocEntity savedIoc = repository.findById(iocFound.getId());
    assertNotNull(savedIoc);
    assertEquals(ioc.getCreatedBy(), savedIoc.getCreatedBy());
  }

  @Test
  public void findAll() {
    createIocs();
    List<IocEntity> iocs =
        repository.findAll(null, "ccce", null, "test", IocOrder.ID, false, 0, 10, false);
    assertEquals(5, iocs.size());
    assertEquals("ccce", iocs.get(0).getCreatedBy());
    assertEquals("ccce", iocs.get(1).getCreatedBy());
    assertEquals("ccce", iocs.get(2).getCreatedBy());
    assertEquals("ccce", iocs.get(3).getCreatedBy());
    assertEquals("ccce", iocs.get(4).getCreatedBy());
  }

  @Test
  public void countForPaging() {
    createIocs();
    assertEquals(5, repository.countForPaging(null, "ccce", null, "test"));
  }

  @Test
  public void countEmptyTable() {
    Long emptyCount = repository.countAll();
    assertEquals(0L, emptyCount);
  }

  @Test
  public void countIocs() {
    int realNumberOfIocs = createIocs();
    Long iocCount = repository.countAll();
    assertEquals(realNumberOfIocs, iocCount);
  }

  @Test
  public void findIocByNamingUUID() {
    IocEntity expected = EntityFactory.createIoc(repository, "test-ioc", "user", null, "UUID");
    List<IocEntity> actualList = repository.findIocByNamingUUID("UUID");

    assertEquals(1, actualList.size());
    IocEntity actual = actualList.get(0);

    assertEquals(expected.getId(), actual.getId());
    assertEquals(expected.getCreatedBy(), actual.getCreatedBy());
    assertEquals(expected.getGitProjectId(), actual.getGitProjectId());
    assertEquals(expected.getNamingName(), actual.getNamingName());
  }

  @Test
  public void findIocByNamingName() {
    IocEntity expected = EntityFactory.createIoc(repository, "test-ioc", "user", null, "");
    IocEntity actual = repository.findIocByNamingName("test-ioc");

    assertEquals(expected.getId(), actual.getId());
    assertEquals(expected.getCreatedBy(), actual.getCreatedBy());
    assertEquals(expected.getGitProjectId(), actual.getGitProjectId());
    assertEquals(expected.getNamingName(), actual.getNamingName());
  }

  private int createIocs() {
    int createdIocCount = 0;
    for (int i = 0; i < 5; i++) {
      EntityFactory.createIoc(repository, "ioc-" + i, "user");
      createdIocCount++;
    }
    for (int i = 5; i < 10; i++) {
      EntityFactory.createIoc(repository, "test-" + i, "ccce");
      createdIocCount++;
    }

    return createdIocCount;
  }

  private int createDeployments() {
    createIocs();

    List<IocEntity> iocList =
        repository.findAll(IocDeploymentStatus.ALL, null, null, null, null, null, 0, 0, true);

    int createdDeployments = 0;

    for (int i = 0; i < iocList.size(); i++) {
      TargetHost host =
          EntityFactory.createHost("abc.test-" + i + ".com", "abc" + i, i, "TestNetwork");
      EntityFactory.createDeployment(
          deploymentRepository,
          repository,
          iocList.get(i),
          i % 2 == 0,
          i % 2 == 0 ? DeploymentType.UNDEPLOY : DeploymentType.DEPLOY,
          JobStatus.SUCCESSFUL,
          host,
          "ccce");
      createdDeployments++;
    }

    return createdDeployments;
  }

  @Test
  void findIOCsForInventory() {
    int noOfDeployments = createDeployments();
    List<IocDeploymentEntity> iocsForInventory = repository.findIOCsForInventory();

    assertEquals(noOfDeployments / 2, iocsForInventory.size());
  }
}
