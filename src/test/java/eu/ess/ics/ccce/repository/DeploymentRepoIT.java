/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.repository;

import static org.junit.jupiter.api.Assertions.*;

import com.google.common.collect.ImmutableList;
import eu.ess.ics.ccce.repository.entity.AwxJobEntity;
import eu.ess.ics.ccce.repository.entity.IocDeploymentEntity;
import eu.ess.ics.ccce.repository.entity.IocEntity;
import eu.ess.ics.ccce.repository.impl.IocDeploymentRepository;
import eu.ess.ics.ccce.repository.impl.IocRepository;
import eu.ess.ics.ccce.service.external.awx.model.JobStatus;
import eu.ess.ics.ccce.service.internal.deployment.dto.DeploymentType;
import eu.ess.ics.ccce.service.internal.deployment.dto.TargetHost;
import java.sql.*;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 */
@SpringBootTest
public class DeploymentRepoIT {
  public static final String CC_E_2_AB_CD = "CC-E2:AB-CD";
  @Autowired private IocDeploymentRepository deploymentRepository;
  @Autowired private IocRepository iocRepository;

  @AfterEach
  void clean() throws SQLException {
    Connection conn = DriverManager.getConnection("jdbc:h2:mem:db:public", "ccceuser", "sa");
    Statement st = conn.createStatement();
    st.executeUpdate("update ioc set latest_successful_deployment = NULL");
    st.executeUpdate("update ioc_deployment set job_id = NULL");
    st.executeUpdate("delete from awx_job");
    st.executeUpdate("alter table awx_job alter column id restart with 1");
    st.executeUpdate("delete from ioc_deployment");
    st.executeUpdate("alter table ioc_deployment alter column id restart with 1");
    st.executeUpdate("delete from ioc");
    conn.close();
  }

  @Test
  public void findAwxJobById() {
    AwxJobEntity awxJob = EntityFactory.createAwxJob(deploymentRepository, 10L);
    AwxJobEntity foundAwxJob = deploymentRepository.findJobById(awxJob.getId());

    assertEquals(awxJob.getId(), foundAwxJob.getId());
    assertEquals(awxJob.getAwxJobId(), foundAwxJob.getAwxJobId());
    assertEquals(awxJob.getStatus(), foundAwxJob.getStatus());
    assertEquals(awxJob.getAwxJobStartedAt(), foundAwxJob.getAwxJobStartedAt());
  }

  @Test
  public void findLatestDeploymentForIocSuccess() {
    IocEntity ioc = createTestIoc1();
    createDeploymentByIoc1(ioc);
    IocDeploymentEntity expectedLatestDeployment = createDeploymentByIoc2(ioc);
    IocDeploymentEntity actualLatestDeployment =
        deploymentRepository.findLatestDeploymentForIoc(ioc.getId());

    assertEquals(expectedLatestDeployment.getId(), actualLatestDeployment.getId());
    assertEquals(expectedLatestDeployment.getCreatedBy(), actualLatestDeployment.getCreatedBy());
    assertEquals(
        expectedLatestDeployment.getIoc().getId(), actualLatestDeployment.getIoc().getId());
    assertEquals(expectedLatestDeployment.isPending(), actualLatestDeployment.isPending());
    assertEquals(
        expectedLatestDeployment.getDeploymentType(), actualLatestDeployment.getDeploymentType());
  }

  @Test
  public void findDeploymentByIdSuccess() {
    IocDeploymentEntity deployment = createDeployment();
    IocDeploymentEntity savedDeployment =
        deploymentRepository.findDeploymentById(deployment.getId());
    assertEquals(deployment.getId(), savedDeployment.getId());
    assertEquals(deployment.getCreatedBy(), savedDeployment.getCreatedBy());
    assertEquals(deployment.getIoc().getId(), savedDeployment.getIoc().getId());
    assertEquals(deployment.isPending(), savedDeployment.isPending());
    assertEquals(deployment.getDeploymentType(), savedDeployment.getDeploymentType());
  }

  @Test
  public void updateDeploymentSuccess() {
    IocDeploymentEntity deployment = createDeployment();
    deployment.setPending(true);
    deploymentRepository.updateDeployment(deployment);

    IocDeploymentEntity foundDeployment =
        deploymentRepository.findDeploymentById(deployment.getId());
    assertEquals(deployment.getId(), foundDeployment.getId());
    assertEquals(deployment.isPending(), foundDeployment.isPending());
  }

  @Test
  public void findAllFilter() {
    createDeployments();
    List<AwxJobEntity> operations =
        deploymentRepository.findAllAwxJobs(
            null, null, null, null, "test", null, null, null, null, null, false, 0, 100);
    assertEquals(5, operations.size());
  }

  @Test
  public void countForPaging() {
    createDeployments();
    assertEquals(
        5,
        deploymentRepository.countJobsForPaging(
            null, null, null, "test", null, null, null, null, null));
  }

  @Test
  public void countAllDeployments() {
    createDeployments();
    assertEquals(10, deploymentRepository.countAllDeployments());
  }

  @Test
  public void countActiveDeployments() {
    createDeployments();
    assertEquals(2, iocRepository.countActualDeployments());
  }

  @Test
  public void findAllWithMultipleFilters() {
    createDeployments();
    List<AwxJobEntity> operations =
        deploymentRepository.findAllAwxJobs(
            null, null, null, "test", null, null, null, null, null, null, true, 0, 100);
  }

  @Test
  public void findAllWithIocFilter() {
    IocEntity ioc1 = createTestIoc1();
    IocEntity ioc2 = createTestIoc2();
    createDeployments(ioc1, ioc2);
    List<AwxJobEntity> operations =
        deploymentRepository.findAllAwxJobs(
            ioc1.getId(), null, null, null, null, null, null, null, null, null, true, 0, 100);
    assertEquals(5, operations.size());
  }

  @Test
  public void findActiveDeploymentForIocVersions() {
    IocEntity ioc1 = createTestIoc1();
    TargetHost host1 = EntityFactory.createHost("abc.test-01.com", "abc", 10L, "TestNetwork");
    EntityFactory.createDeployment(
        deploymentRepository,
        iocRepository,
        ioc1,
        false,
        DeploymentType.UNDEPLOY,
        JobStatus.SUCCESSFUL,
        host1,
        "test user");
    EntityFactory.createDeployment(
        deploymentRepository,
        iocRepository,
        ioc1,
        true,
        DeploymentType.DEPLOY,
        JobStatus.SUCCESSFUL,
        host1,
        "test user");
    IocDeploymentEntity deployment =
        iocRepository.findById(ioc1.getId()).getLatestSuccessfulDeployment();
  }

  @Test
  public void findIntendedDeploymentForIocVersions() {
    IocEntity ioc1 = createTestIoc1();
    TargetHost host1 = EntityFactory.createHost("abc.test-01.com", "abc", 10L, "TestNetwork");
    EntityFactory.createDeployment(
        deploymentRepository,
        iocRepository,
        ioc1,
        false,
        DeploymentType.UNDEPLOY,
        JobStatus.SUCCESSFUL,
        host1,
        "test user");
    EntityFactory.createDeployment(
        deploymentRepository,
        iocRepository,
        ioc1,
        false,
        DeploymentType.DEPLOY,
        JobStatus.PENDING,
        host1,
        "test user");
    IocDeploymentEntity deployment = deploymentRepository.findPendingDeploymentForIoc(ioc1.getId());
  }

  @Test
  public void findIntendedOrQueuedDeploymentForIocVersions() {
    IocEntity ioc1 = createTestIoc1();
    TargetHost host1 = EntityFactory.createHost("abc.test-01.com", "abc", 10L, "TestNetwork");
    EntityFactory.createDeployment(
        deploymentRepository,
        iocRepository,
        ioc1,
        false,
        DeploymentType.UNDEPLOY,
        JobStatus.SUCCESSFUL,
        host1,
        "test user");
    EntityFactory.createDeployment(
        deploymentRepository,
        iocRepository,
        ioc1,
        false,
        DeploymentType.UNDEPLOY,
        JobStatus.QUEUED_BY_BACKEND,
        host1,
        "test user");
    EntityFactory.createDeployment(
        deploymentRepository,
        iocRepository,
        ioc1,
        false,
        DeploymentType.UNDEPLOY,
        JobStatus.QUEUED_BY_BACKEND,
        host1,
        "test user");
    List<AwxJobEntity> deployments = deploymentRepository.findOngoingDeploymentsForIoc(ioc1);
    assertEquals(2, deployments.size());
  }

  @Test
  public void findAllIntendedOrQueuedDeployments() {
    IocEntity ioc1 = createTestIoc1();
    TargetHost host1 = EntityFactory.createHost("abc.test-01.com", "abc", 10L, "TestNetwork");
    EntityFactory.createDeployment(
        deploymentRepository,
        iocRepository,
        ioc1,
        false,
        DeploymentType.UNDEPLOY,
        JobStatus.PENDING,
        host1,
        "test user");
    EntityFactory.createDeployment(
        deploymentRepository,
        iocRepository,
        ioc1,
        false,
        DeploymentType.UNDEPLOY,
        JobStatus.PENDING,
        host1,
        "test user");
    EntityFactory.createDeployment(
        deploymentRepository,
        iocRepository,
        ioc1,
        false,
        DeploymentType.UNDEPLOY,
        JobStatus.SUCCESSFUL,
        host1,
        "test user");
    List<IocDeploymentEntity> deployments =
        deploymentRepository.findAllPendingOrQueuedDeployments();
    assertEquals(2, deployments.size());
  }

  @Test
  public void findAllIntendedDeployments() {
    IocEntity ioc1 = createTestIoc1();
    IocEntity ioc2 = createTestIoc2();

    TargetHost host1 = EntityFactory.createHost("abc.test-01.com", "abc", 10L, "TestNetwork");
    EntityFactory.createDeployment(
        deploymentRepository,
        iocRepository,
        ioc1,
        false,
        DeploymentType.UNDEPLOY,
        JobStatus.SUCCESSFUL,
        host1,
        "test user");
    EntityFactory.createDeployment(
        deploymentRepository,
        iocRepository,
        ioc1,
        false,
        DeploymentType.UNDEPLOY,
        JobStatus.PENDING,
        host1,
        "test user");
    EntityFactory.createDeployment(
        deploymentRepository,
        iocRepository,
        ioc2,
        false,
        DeploymentType.UNDEPLOY,
        JobStatus.SUCCESSFUL,
        host1,
        "test user");
    EntityFactory.createDeployment(
        deploymentRepository,
        iocRepository,
        ioc2,
        false,
        DeploymentType.UNDEPLOY,
        JobStatus.PENDING,
        host1,
        "test user");
    List<AwxJobEntity> deployments = deploymentRepository.findAllPendingAwxJobs();
    assertEquals(2, deployments.size());
  }

  @Test
  public void findFirstQueued() {
    IocEntity ioc1 = createTestIoc1();

    TargetHost host1 = EntityFactory.createHost("abc.test-01.com", "abc", 10L, "TestNetwork");
    long now = new Date().getTime();
    EntityFactory.createDeployment(
        deploymentRepository,
        iocRepository,
        ioc1,
        false,
        DeploymentType.UNDEPLOY,
        JobStatus.QUEUED_BY_BACKEND,
        host1,
        "test user",
        now);
    EntityFactory.createDeployment(
        deploymentRepository,
        iocRepository,
        ioc1,
        false,
        DeploymentType.UNDEPLOY,
        JobStatus.QUEUED_BY_BACKEND,
        host1,
        "test user",
        now + 1);
    EntityFactory.createDeployment(
        deploymentRepository,
        iocRepository,
        ioc1,
        false,
        DeploymentType.UNDEPLOY,
        JobStatus.QUEUED_BY_BACKEND,
        host1,
        "test user",
        now + 2);
    IocDeploymentEntity deployment = deploymentRepository.findFirstQueued();
  }

  @Test
  public void findAllActiveByHostId() {
    IocEntity ioc1 = createTestIoc1();
    TargetHost host1 = EntityFactory.createHost("abc.test-01.com", "abc", 10L, "TestNetwork");
    TargetHost host2 = EntityFactory.createHost("abc.test-02.com", "abc", 20L, "TestNetwork");
    EntityFactory.createDeployment(
        deploymentRepository,
        iocRepository,
        ioc1,
        true,
        DeploymentType.DEPLOY,
        JobStatus.SUCCESSFUL,
        host1,
        "test user");
    EntityFactory.createDeployment(
        deploymentRepository,
        iocRepository,
        ioc1,
        true,
        DeploymentType.DEPLOY,
        JobStatus.SUCCESSFUL,
        host2,
        "test user");
    EntityFactory.createDeployment(
        deploymentRepository,
        iocRepository,
        ioc1,
        true,
        DeploymentType.DEPLOY,
        JobStatus.SUCCESSFUL,
        host2,
        "test user");
    List<IocDeploymentEntity> deployments =
        iocRepository.findAllActualIocDeploymentsByHostId(host2.getExternalId(), host2.isVm());
    assertEquals(1, deployments.size());
  }

  @Test
  public void findConcurrentIntendedDeploymentsForDeployments() {
    IocEntity ioc1 = createTestIoc1();

    TargetHost host1 = EntityFactory.createHost("abc.test-01.com", "abc", 10L, "TestNetwork");
    UUID uuid = UUID.randomUUID();
    long now = new Date().getTime();
    IocDeploymentEntity deployment =
        EntityFactory.createDeployment(
            deploymentRepository,
            iocRepository,
            ioc1,
            false,
            DeploymentType.UNDEPLOY,
            JobStatus.SUCCESSFUL,
            host1,
            "test user");
    EntityFactory.createDeployment(
        deploymentRepository,
        iocRepository,
        ioc1,
        false,
        DeploymentType.UNDEPLOY,
        JobStatus.PENDING,
        host1,
        "test user");
    EntityFactory.createDeployment(
        deploymentRepository,
        iocRepository,
        ioc1,
        false,
        DeploymentType.UNDEPLOY,
        JobStatus.SUCCESSFUL,
        host1,
        "test user");
    List<AwxJobEntity> deployments =
        deploymentRepository.findConcurrentPendingAwxJobsForDeployments(
            ImmutableList.of(deployment));
    assertEquals(1, deployments.size());
  }

  @Test
  public void findJobForDeployment() {
    IocDeploymentEntity deployment = createDeployment();
    AwxJobEntity awxJob = EntityFactory.createAwxJob(deploymentRepository, 10L);
    deployment.setAwxJob(awxJob);

    AwxJobEntity foundJob = deployment.getAwxJob();
    assertEquals(awxJob.getId(), foundJob.getId());
    assertEquals(awxJob.getAwxJobId(), foundJob.getAwxJobId());
    assertEquals(awxJob.getStatus(), foundJob.getStatus());
    assertEquals(awxJob.getAwxJobStartedAt(), foundJob.getAwxJobStartedAt());
  }

  @Test
  public void findByAwxJobId() {
    AwxJobEntity awxJob = EntityFactory.createAwxJob(deploymentRepository, 10L);
    AwxJobEntity foundJob = deploymentRepository.findByAwxJobId(awxJob.getAwxJobId());
    assertEquals(awxJob.getId(), foundJob.getId());
    assertEquals(awxJob.getAwxJobId(), foundJob.getAwxJobId());
  }

  @Test
  public void updateAwxJob() {
    AwxJobEntity awxJob = EntityFactory.createAwxJob(deploymentRepository, 10L);
    awxJob.setStatus("RUNNING");
    deploymentRepository.updateAwxJob(awxJob);
    AwxJobEntity foundJob = deploymentRepository.findByAwxJobId(awxJob.getAwxJobId());
    assertEquals(awxJob.getId(), foundJob.getId());
    assertEquals(awxJob.getStatus(), foundJob.getStatus());
  }

  private IocDeploymentEntity createDeployment() {
    IocEntity ioc = createTestIoc1();
    TargetHost host = EntityFactory.createHost("abc.test-01.com", "abc", 10L, "TestNetwork");
    IocDeploymentEntity deployment =
        EntityFactory.createDeployment(
            deploymentRepository,
            iocRepository,
            ioc,
            false,
            DeploymentType.UNDEPLOY,
            JobStatus.SUCCESSFUL,
            host,
            "test user");
    return deployment;
  }

  private IocDeploymentEntity createDeploymentByIoc1(IocEntity ioc) {
    TargetHost host = EntityFactory.createHost("abc.test-01.com", "abc", 10L, "TestNetwork");
    IocDeploymentEntity deployment =
        EntityFactory.createDeployment(
            deploymentRepository,
            iocRepository,
            ioc,
            false,
            DeploymentType.UNDEPLOY,
            JobStatus.SUCCESSFUL,
            host,
            "test user");
    return deployment;
  }

  private IocDeploymentEntity createDeploymentByIoc2(IocEntity ioc) {
    TargetHost host = EntityFactory.createHost("abc.test-02.com", "def", 20L, "TestNetwork");
    IocDeploymentEntity deployment =
        EntityFactory.createDeployment(
            deploymentRepository,
            iocRepository,
            ioc,
            false,
            DeploymentType.UNDEPLOY,
            JobStatus.SUCCESSFUL,
            host,
            "test user");
    return deployment;
  }

  private int createDeployments() {
    IocEntity ioc1 = createTestIoc1();
    IocEntity ioc2 = createTestIoc2();
    return createDeployments(ioc1, ioc2);
  }

  private int createDeployments(IocEntity ioc1, IocEntity ioc2) {
    TargetHost host1 = EntityFactory.createHost("abc.test-01.com", "abc", 10L, "TestNetwork");
    TargetHost host2 = EntityFactory.createHost("abc.dep-01.com", "abc", 20L, "Network");

    int createdCount = 0;
    for (int i = 0; i < 5; i++) {
      EntityFactory.createDeployment(
          deploymentRepository,
          iocRepository,
          ioc1,
          true,
          DeploymentType.DEPLOY,
          JobStatus.SUCCESSFUL,
          host1,
          "test user");
      createdCount++;
    }
    for (int i = 5; i < 10; i++) {
      EntityFactory.createDeployment(
          deploymentRepository,
          iocRepository,
          ioc2,
          true,
          DeploymentType.DEPLOY,
          JobStatus.SUCCESSFUL,
          host2,
          "user");
      createdCount++;
    }

    return createdCount;
  }

  private IocEntity createTestIoc1() {
    return EntityFactory.createIoc(iocRepository, "IOC TO DEPLOY 1", "test user");
  }

  private IocEntity createTestIoc2() {
    return EntityFactory.createIoc(iocRepository, "IOC TO DEPLOY 2", "test user");
  }
}
