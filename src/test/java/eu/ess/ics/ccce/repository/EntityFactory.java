/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.repository;

import eu.ess.ics.ccce.common.Constants;
import eu.ess.ics.ccce.repository.api.IDeploymentStatRepository;
import eu.ess.ics.ccce.repository.api.IIocDeploymentRepository;
import eu.ess.ics.ccce.repository.api.IIocRepository;
import eu.ess.ics.ccce.repository.entity.AwxJobEntity;
import eu.ess.ics.ccce.repository.entity.DeploymentStatisticsEntity;
import eu.ess.ics.ccce.repository.entity.IocDeploymentEntity;
import eu.ess.ics.ccce.repository.entity.IocEntity;
import eu.ess.ics.ccce.rest.model.job.JobAction;
import eu.ess.ics.ccce.service.external.awx.model.JobStatus;
import eu.ess.ics.ccce.service.internal.deployment.dto.DeploymentType;
import eu.ess.ics.ccce.service.internal.deployment.dto.TargetHost;
import java.util.Date;
import java.util.Random;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 */
public class EntityFactory {

  public static IocEntity createIoc(IIocRepository iocRepository, String name, String createdBy) {
    return createIoc(iocRepository, name, createdBy, null);
  }

  public static IocEntity createIoc(
      IIocRepository iocRepository,
      String name,
      String createdBy,
      Long gitProjectId,
      String namingUUID) {
    IocEntity ioc = new IocEntity();
    ioc.setCreatedAt(new Date());
    ioc.setGitProjectId(gitProjectId);
    ioc.setCreatedBy(createdBy);
    ioc.setNamingName(name);
    ioc.setNamingUuid(namingUUID);
    ioc.setDeployedWithOldPlaybook(false);
    iocRepository.createIoc(ioc);
    return iocRepository.findById(ioc.getId());
  }

  public static IocEntity createIoc(
      IIocRepository iocRepository, String name, String createdBy, Long externalNameId) {
    IocEntity ioc = new IocEntity();
    ioc.setCreatedAt(new Date());
    ioc.setCreatedBy(createdBy);
    ioc.setNamingName(name);
    ioc.setDeployedWithOldPlaybook(false);
    iocRepository.createIoc(ioc);
    return iocRepository.findById(ioc.getId());
  }

  public static IocDeploymentEntity createDeployment(
      IIocDeploymentRepository repository,
      IIocRepository iocRepository,
      IocEntity ioc,
      boolean active,
      DeploymentType deploymentType,
      JobStatus jobStatus,
      TargetHost host,
      String user) {
    return createDeployment(
        repository,
        iocRepository,
        ioc,
        active,
        deploymentType,
        jobStatus,
        host,
        user,
        new Date().getTime());
  }

  public static IocDeploymentEntity createDeployment(
      IIocDeploymentRepository repository,
      IIocRepository iocRepository,
      IocEntity ioc,
      boolean active,
      DeploymentType deploymentType,
      JobStatus jobStatus,
      TargetHost host,
      String user,
      long createdAt) {
    IocDeploymentEntity iocDeploymentEntity = new IocDeploymentEntity();
    iocDeploymentEntity.setIoc(ioc);
    iocDeploymentEntity.setCreatedAt(new Date(createdAt));
    iocDeploymentEntity.setDeploymentType(deploymentType.name());
    iocDeploymentEntity.setCreatedBy(user);
    iocDeploymentEntity.setPending(Constants.PENDING_STATUSES.contains(jobStatus.name()));
    iocDeploymentEntity.setNetBoxHostId(host.getExternalId());
    iocDeploymentEntity.setVm(host.isVm());
    repository.createDeployment(iocDeploymentEntity);
    iocDeploymentEntity.setAwxJob(
        createAwxJob(
            repository,
            iocDeploymentEntity,
            new Random().nextLong(),
            jobStatus,
            JobAction.DEPLOY,
            user));
    repository.updateDeployment(iocDeploymentEntity);
    ioc.setLatestSuccessfulDeployment(active ? iocDeploymentEntity : null);
    iocRepository.updateIoc(ioc);
    return iocDeploymentEntity;
  }

  public static TargetHost createHost(
      String fqdn, String hostName, long csEntryId, String network) {
    return new TargetHost(csEntryId, true, fqdn, hostName, network);
  }

  public static AwxJobEntity createAwxJob(
      IIocDeploymentRepository repository,
      IocDeploymentEntity deployment,
      long awxId,
      JobStatus jobStatus,
      JobAction jobAction,
      String user) {
    AwxJobEntity awxJob = new AwxJobEntity();
    awxJob.setAwxJobId(awxId);
    awxJob.setStatus(jobStatus.name());
    awxJob.setAwxJobStartedAt(new Date());
    awxJob.setDeployment(deployment);
    awxJob.setCreatedBy(user);
    awxJob.setJobType(jobAction.name());

    repository.createAwxJob(awxJob);
    return awxJob;
  }

  public static AwxJobEntity createAwxJob(IIocDeploymentRepository repository, long awxId) {
    AwxJobEntity awxJob = new AwxJobEntity();
    awxJob.setAwxJobId(awxId);
    awxJob.setStatus("SUCCESSFUL");
    awxJob.setAwxJobStartedAt(new Date());

    repository.createAwxJob(awxJob);
    return awxJob;
  }

  public static DeploymentStatisticsEntity createDeploymentStatEntity(
      IDeploymentStatRepository repository) {
    DeploymentStatisticsEntity deploymentStatisticsEntity = new DeploymentStatisticsEntity();
    deploymentStatisticsEntity.setNetwork("test network");
    deploymentStatisticsEntity.setIocCount(1L);
    deploymentStatisticsEntity.setCreatedAt(new Date());
    repository.saveStatistics(deploymentStatisticsEntity);
    return deploymentStatisticsEntity;
  }
}
