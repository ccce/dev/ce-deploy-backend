package eu.ess.ics.ccce.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;

import eu.ess.ics.ccce.repository.entity.DeploymentStatisticsEntity;
import eu.ess.ics.ccce.repository.entity.IocEntity;
import eu.ess.ics.ccce.repository.impl.DeploymentStatRepository;
import eu.ess.ics.ccce.repository.impl.IocDeploymentRepository;
import eu.ess.ics.ccce.repository.impl.IocRepository;
import eu.ess.ics.ccce.service.external.awx.model.JobStatus;
import eu.ess.ics.ccce.service.internal.deployment.dto.DeploymentType;
import eu.ess.ics.ccce.service.internal.deployment.dto.TargetHost;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class DeploymentStatRepoIT {

  public static final Date DATE_FROM = new Date(946681200000L);

  @Autowired private DeploymentStatRepository deploymentStatRepository;
  @Autowired private IocDeploymentRepository deploymentRepository;
  @Autowired private IocRepository iocRepository;

  @AfterEach
  void clean() throws SQLException {
    Connection conn = DriverManager.getConnection("jdbc:h2:mem:db:public", "ccceuser", "sa");
    Statement st = conn.createStatement();
    st.executeUpdate("set referential_integrity false");
    st.executeUpdate("delete from deployment_statistics");
    st.executeUpdate("alter table deployment_statistics alter column id restart with 1");
    st.executeUpdate("delete from ioc");
    st.executeUpdate("alter table ioc alter column id restart with 1");
    st.executeUpdate("delete from ioc_deployment");
    st.executeUpdate("alter table ioc_deployment alter column id restart with 1");
    st.executeUpdate("delete from awx_job");
    st.executeUpdate("alter table awx_job alter column id restart with 1");
    st.executeUpdate("set referential_integrity true");
    conn.close();
  }

  @Test
  public void collectDeploymentStatSuccess() {
    EntityFactory.createDeploymentStatEntity(deploymentStatRepository);
    List<DeploymentStatisticsEntity> actual =
        deploymentStatRepository.collectDeploymentStat(DATE_FROM);

    assertEquals(1, actual.size());
    assertEquals(1L, actual.get(0).getId());
    assertEquals(1L, actual.get(0).getIocCount());
    assertEquals("test network", actual.get(0).getNetwork());
  }

  @Test
  public void countActiveIocsEmpty() {
    Date actualDate =
        Date.from(LocalDate.now().atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
    Long activeIocs = deploymentStatRepository.countActiveIocs(actualDate);
    assertEquals(0L, activeIocs);
  }

  @Test
  public void countIocDeploymentsSuccess() {
    IocEntity ioc1 = createTestIoc();

    TargetHost host1 = EntityFactory.createHost("abc.test-01.com", "abc", 10L, "TestNetwork");
    EntityFactory.createDeployment(
        deploymentRepository,
        iocRepository,
        ioc1,
        true,
        DeploymentType.DEPLOY,
        JobStatus.SUCCESSFUL,
        host1,
        "test user");

    Long actual =
        deploymentStatRepository.countSuccessfulIocDeployments(
            LocalDate.now().atStartOfDay().toLocalDate());

    assertEquals(1L, actual);
  }

  @Test
  public void countIocDeploymentsEmpty() {
    IocEntity ioc1 = createTestIoc();

    TargetHost host1 = EntityFactory.createHost("abc.test-01.com", "abc", 10L, "TestNetwork");
    EntityFactory.createDeployment(
        deploymentRepository,
        iocRepository,
        ioc1,
        false,
        DeploymentType.UNDEPLOY,
        JobStatus.SUCCESSFUL,
        host1,
        "test user");

    Long actual =
        deploymentStatRepository.countSuccessfulIocDeployments(
            LocalDate.now().minusDays(2).atStartOfDay().toLocalDate());

    assertEquals(0L, actual);
  }

  private IocEntity createTestIoc() {
    return EntityFactory.createIoc(iocRepository, "IOC TO DEPLOY 1", "test user");
  }
}
