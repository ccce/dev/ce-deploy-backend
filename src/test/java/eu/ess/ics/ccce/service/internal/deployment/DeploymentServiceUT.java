/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.service.internal.deployment;

import static org.junit.jupiter.api.Assertions.*;

import com.google.common.collect.ImmutableList;
import com.google.gson.Gson;
import eu.ess.ics.ccce.common.alert.DeploymentAlertGenerator;
import eu.ess.ics.ccce.common.conversion.PagingUtil;
import eu.ess.ics.ccce.exceptions.LimitExceededException;
import eu.ess.ics.ccce.repository.api.IIocDeploymentRepository;
import eu.ess.ics.ccce.repository.api.IIocRepository;
import eu.ess.ics.ccce.repository.entity.AwxJobEntity;
import eu.ess.ics.ccce.repository.entity.IocDeploymentEntity;
import eu.ess.ics.ccce.repository.entity.IocEntity;
import eu.ess.ics.ccce.rest.model.host.response.HostInfoWithId;
import eu.ess.ics.ccce.rest.model.job.JobAction;
import eu.ess.ics.ccce.rest.model.job.request.JobKind;
import eu.ess.ics.ccce.rest.model.job.request.JobOrder;
import eu.ess.ics.ccce.rest.model.job.request.JobStatusQuery;
import eu.ess.ics.ccce.rest.model.job.response.Deployment;
import eu.ess.ics.ccce.rest.model.job.response.PagedJobResponse;
import eu.ess.ics.ccce.service.external.awx.AwxService;
import eu.ess.ics.ccce.service.external.awx.model.JobStatus;
import eu.ess.ics.ccce.service.external.awx.model.response.Job;
import eu.ess.ics.ccce.service.external.awx.model.response.JobTemplate;
import eu.ess.ics.ccce.service.external.gitlab.GitLabService;
import eu.ess.ics.ccce.service.external.naming.NamingService;
import eu.ess.ics.ccce.service.external.naming.model.NamingIocResponse;
import eu.ess.ics.ccce.service.external.netbox.model.NetBoxHostIdentifier;
import eu.ess.ics.ccce.service.internal.PagingLimitDto;
import eu.ess.ics.ccce.service.internal.deployment.dto.DeploymentTask;
import eu.ess.ics.ccce.service.internal.deployment.dto.DeploymentType;
import eu.ess.ics.ccce.service.internal.deployment.dto.TargetHost;
import eu.ess.ics.ccce.service.internal.host.HostService;
import eu.ess.ics.ccce.service.internal.security.AuthorizationService;
import eu.ess.ics.ccce.service.internal.security.dto.UserDetails;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.List;
import org.gitlab.api.models.GitlabProject;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 */
@ExtendWith(MockitoExtension.class)
public class DeploymentServiceUT {
  @Mock private PagingUtil utils;
  @Mock private IIocDeploymentRepository deploymentRepository;
  @Mock private IIocRepository iocRepository;
  @Mock private HostService hostService;
  @Mock private AwxService awxService;
  @Mock private AuthorizationService authorizationService;
  @Mock private GitLabService gitLabService;
  @Mock private DeploymentAlertGenerator deploymentAlertGenerator;
  @Mock private NamingService namingService;

  private final TargetHost host1;
  private final TargetHost host2;
  private final IocEntity ioc1;
  private final IocEntity ioc2;
  private final IocDeploymentEntity iocDeploymentEntityActive1;
  private final IocDeploymentEntity iocDeploymentEntityActive2;
  private final IocDeploymentEntity iocDeploymentEntityIntended1;
  private final IocDeploymentEntity iocDeploymentEntityIntended2;
  private final Job job1;
  private final AwxJobEntity jobEntity1;
  private final AwxJobEntity jobEntity2;
  private final AwxJobEntity jobEntity3;

  private final String GIT_PROJECT_1111_URL = "http://gitlab.test.com/test-project.git";
  private final String GIT_PROJECT_2222_URL = "http://gitlab.test.com/test-project-2.git";

  private final String IOC1_DESCRIPTION = "First IOC description";
  private final String IOC2_DESCRIPTION = "Second IOC description";

  private final NamingIocResponse namingResponse1;
  private final NamingIocResponse namingResponse2;

  DeploymentServiceUT() {
    host1 = new TargetHost(10L, true, "demo-1.esss.lu.se", "demo-1", "CSLab");
    host2 = new TargetHost(20L, true, "demo-2.esss.lu.se", "demo-1", "CSLab");

    ioc1 = new IocEntity();
    ioc1.setCreatedBy("Test User 1");
    ioc1.setId(1L);
    ioc1.setNamingName("TEST-IOC-1");
    ioc1.setGitProjectId(1111L);
    ioc1.setNamingUuid("ABC-DEF-123-456");

    ioc2 = new IocEntity();
    ioc2.setCreatedBy("Test User 2");
    ioc2.setId(2L);
    ioc2.setNamingName("TEST-IOC-2");
    ioc2.setGitProjectId(2222L);
    ioc1.setNamingUuid("987-654-012-ABC");

    job1 = new Job();
    job1.setId(100);
    job1.setStatus(JobStatus.PENDING);

    jobEntity1 = new AwxJobEntity();
    jobEntity1.setId(1L);
    jobEntity1.setAwxJobId(job1.getId());
    jobEntity1.setJobType(JobAction.DEPLOY.name());
    jobEntity1.setStatus(JobStatus.PENDING.name());

    jobEntity2 = new AwxJobEntity();
    jobEntity2.setId(2L);
    jobEntity2.setAwxJobId(job1.getId());
    jobEntity2.setJobType(JobAction.DEPLOY.name());
    jobEntity2.setStatus(JobStatus.PENDING.name());

    jobEntity3 = new AwxJobEntity();
    jobEntity3.setId(3L);
    jobEntity3.setAwxJobId(job1.getId());
    jobEntity3.setJobType(JobAction.DEPLOY.name());
    jobEntity3.setStatus(JobStatus.ERROR.name());

    iocDeploymentEntityActive1 = new IocDeploymentEntity();
    iocDeploymentEntityActive1.setId(1L);
    iocDeploymentEntityActive1.setIoc(ioc1);
    iocDeploymentEntityActive1.setGitProjectId(ioc1.getGitProjectId());
    ioc1.setLatestSuccessfulDeployment(iocDeploymentEntityActive1);
    iocDeploymentEntityActive1.setPending(false);
    iocDeploymentEntityActive1.setNetBoxHostId(host1.getExternalId());
    iocDeploymentEntityActive1.setVm(true);
    iocDeploymentEntityActive1.setHostFqdn(host1.getFqdn());
    iocDeploymentEntityActive1.setHostName(host1.getHostName());
    iocDeploymentEntityActive1.setHostNetwork(host1.getNetwork());
    iocDeploymentEntityActive1.setSourceVersion("1.0.0");
    iocDeploymentEntityActive1.setDeploymentType(DeploymentType.DEPLOY.name());
    iocDeploymentEntityActive1.setAwxJob(jobEntity1);
    jobEntity1.setDeployment(iocDeploymentEntityActive1);

    iocDeploymentEntityActive2 = new IocDeploymentEntity();
    iocDeploymentEntityActive2.setId(2L);
    iocDeploymentEntityActive2.setIoc(ioc2);
    iocDeploymentEntityActive2.setGitProjectId(ioc2.getGitProjectId());
    ioc2.setLatestSuccessfulDeployment(iocDeploymentEntityActive2);
    iocDeploymentEntityActive2.setPending(false);
    iocDeploymentEntityActive2.setNetBoxHostId(host2.getExternalId());
    iocDeploymentEntityActive2.setHostFqdn(host2.getFqdn());
    iocDeploymentEntityActive2.setHostName(host2.getHostName());
    iocDeploymentEntityActive2.setHostNetwork(host2.getNetwork());
    iocDeploymentEntityActive2.setSourceVersion("2.0.0");
    iocDeploymentEntityActive2.setAwxJob(jobEntity3);
    iocDeploymentEntityActive2.setDeploymentType(DeploymentType.DEPLOY.name());
    jobEntity3.setDeployment(iocDeploymentEntityActive2);

    iocDeploymentEntityIntended1 = new IocDeploymentEntity();
    iocDeploymentEntityIntended1.setId(3L);
    iocDeploymentEntityIntended1.setCreatedBy("testuser");
    iocDeploymentEntityIntended1.setIoc(ioc1);
    iocDeploymentEntityIntended1.setPending(true);
    iocDeploymentEntityIntended1.setNetBoxHostId(host2.getExternalId());
    iocDeploymentEntityIntended1.setVm(true);
    iocDeploymentEntityIntended1.setHostFqdn(host2.getFqdn());
    iocDeploymentEntityIntended1.setHostName(host2.getHostName());
    iocDeploymentEntityIntended1.setHostNetwork(host2.getNetwork());
    iocDeploymentEntityIntended1.setDeploymentType(DeploymentType.DEPLOY.name());
    iocDeploymentEntityIntended1.setSourceVersion("0.9.0");
    iocDeploymentEntityIntended1.setGitProjectId(ioc1.getGitProjectId());
    iocDeploymentEntityIntended1.setAwxJob(jobEntity2);
    jobEntity2.setDeployment(iocDeploymentEntityIntended1);

    iocDeploymentEntityIntended2 = new IocDeploymentEntity();
    iocDeploymentEntityIntended2.setId(4L);
    iocDeploymentEntityIntended2.setCreatedBy("testuser");
    iocDeploymentEntityIntended2.setIoc(ioc2);
    iocDeploymentEntityIntended2.setPending(true);
    iocDeploymentEntityIntended2.setNetBoxHostId(host1.getExternalId());
    iocDeploymentEntityIntended2.setHostFqdn(host1.getFqdn());
    iocDeploymentEntityIntended2.setHostName(host1.getHostName());
    iocDeploymentEntityIntended2.setHostNetwork(host1.getNetwork());
    iocDeploymentEntityIntended2.setSourceVersion("1.9.0");
    iocDeploymentEntityIntended2.setDeploymentType(DeploymentType.DEPLOY.name());

    namingResponse1 =
        new NamingIocResponse(
            ioc1.getNamingUuid(),
            ioc1.getNamingName(),
            "DEVICE",
            "Discipline",
            IOC1_DESCRIPTION,
            "Valid");
    namingResponse2 =
        new NamingIocResponse(
            ioc2.getNamingUuid(),
            ioc2.getNamingName(),
            "DEVICE",
            "Discipline",
            IOC2_DESCRIPTION,
            "Valid");
  }

  @Test
  public void createDeploymentsSuccess() {
    DeploymentService deploymentService = createDeploymentService();
    Mockito.when(gitLabService.projectInfo(ioc1.getGitProjectId()))
        .thenReturn(
            new GitlabProject() {
              {
                setHttpUrl(GIT_PROJECT_1111_URL);
              }
            });

    HostInfoWithId hostInfoWithId = createHostWithId(host1);
    IocDeploymentEntity iocDeploymentEntity =
        deploymentService.createDeployments(
            new DeploymentService.DeploymentRequest(ioc1, hostInfoWithId),
            "testuser",
            false,
            false,
            "test_source_version");
    ArgumentCaptor<IocDeploymentEntity> argument =
        ArgumentCaptor.forClass(IocDeploymentEntity.class);
    Mockito.verify(deploymentRepository, Mockito.times(1)).createDeployment(argument.capture());
  }

  @Test
  public void createDeploymentSuccess() throws Exception {
    DeploymentService deploymentService = createDeploymentService();
    JobTemplate template = createJobTemplate();

    Mockito.when(awxService.getJobTemplateName()).thenReturn(template.getName());
    Mockito.when(awxService.getJobTemplate(template.getName())).thenReturn(template);
    Mockito.when(
            awxService.launchJob(
                "http://awx.job/1",
                "demo-1.esss.lu.se",
                new Gson().toJson(ImmutableList.of(ioc1.getNamingName())),
                DeploymentType.DEPLOY))
        .thenReturn(job1);

    Mockito.when(gitLabService.projectInfo(ioc1.getGitProjectId()))
        .thenReturn(
            new GitlabProject() {
              {
                setHttpUrl(GIT_PROJECT_1111_URL);
              }
            });

    Mockito.when(gitLabService.getNullableGitlabProject(ioc1.getGitProjectId()))
        .thenReturn(
            new GitlabProject() {
              {
                setId(Math.toIntExact(ioc1.getGitProjectId()));
              }
            });

    UserDetails user = createUser();
    HostInfoWithId hostInfoWithId = createHostWithId(host1);

    Deployment deployment =
        deploymentService.createDeployment(
            DeploymentTask.createDeploymentTask(ioc1, hostInfoWithId, user),
            iocDeploymentEntityActive1.getSourceVersion());
    assertNotNull(deployment);
    assertEquals(job1.getId(), deployment.getAwxJobId());
    assertNull(deployment.getEndDate());
    assertEquals(host1.getFqdn(), deployment.getHost().getFqdn());
    assertEquals(iocDeploymentEntityActive1.getSourceVersion(), deployment.getSourceVersion());
  }

  @Test
  public void redeployIOCOnHostThatReachedItsMaximumCapacitySuccess() throws Exception {
    DeploymentService deploymentService = createDeploymentService(1);
    UserDetails user = createUser();
    HostInfoWithId hostInfoWithId = createHostWithId(host1);
    JobTemplate template = createJobTemplate();

    Mockito.when(iocRepository.findById(ioc1.getId())).thenReturn(ioc1);
    Mockito.when(awxService.getJobTemplateName()).thenReturn(template.getName());
    Mockito.when(awxService.getJobTemplate(template.getName())).thenReturn(template);
    Mockito.when(
            awxService.launchJob(
                "http://awx.job/1",
                "demo-1.esss.lu.se",
                new Gson().toJson(ImmutableList.of(ioc1.getNamingName())),
                DeploymentType.DEPLOY))
        .thenReturn(job1);

    Mockito.when(gitLabService.projectInfo(ioc1.getGitProjectId()))
        .thenReturn(
            new GitlabProject() {
              {
                setHttpUrl(GIT_PROJECT_1111_URL);
              }
            });

    Mockito.when(gitLabService.getNullableGitlabProject(ioc1.getGitProjectId()))
        .thenReturn(
            new GitlabProject() {
              {
                setId(Math.toIntExact(ioc1.getGitProjectId()));
              }
            });

    Deployment deployment =
        deploymentService.createDeployment(
            DeploymentTask.createDeploymentTask(ioc1, hostInfoWithId, user),
            iocDeploymentEntityActive1.getSourceVersion());
    assertNotNull(deployment);
    assertEquals(job1.getId(), deployment.getAwxJobId());
    assertNull(deployment.getEndDate());
    assertEquals(host1.getFqdn(), deployment.getHost().getFqdn());
    assertEquals(iocDeploymentEntityActive1.getSourceVersion(), deployment.getSourceVersion());
  }

  @Test
  public void createDeploymentFailedDueToHostCapacityIsFull() throws Exception {
    DeploymentService deploymentService = createDeploymentService(1);
    UserDetails user = createUser();
    HostInfoWithId hostInfoWithId = createHostWithId(host1);

    Mockito.when(
            iocRepository.countAssociatedDeployments(hostInfoWithId.getId(), hostInfoWithId.isVm()))
        .thenReturn(1L);
    try {
      deploymentService.createDeployment(
          DeploymentTask.createDeploymentTask(ioc1, hostInfoWithId, user),
          iocDeploymentEntityActive1.getSourceVersion());
    } catch (LimitExceededException e) {
      assertEquals(
          "Limit exceeded: Max number of IOCs per host is %d.".formatted(1), e.getMessage());
      Mockito.verify(iocRepository)
          .countAssociatedDeployments(hostInfoWithId.getId(), hostInfoWithId.isVm());
    }
  }

  @Test
  public void undeployInDb() {
    Mockito.when(iocRepository.findById(1L)).thenReturn(ioc1);
    Mockito.doNothing().when(iocRepository).updateIoc(ioc1);
    createDeploymentService().undeployInDb(1L);
    Mockito.verify(iocRepository).findById(1L);
    Mockito.verify(iocRepository).updateIoc(ioc1);
    ioc1.setLatestSuccessfulDeployment(iocDeploymentEntityActive1);
  }

  @Test
  public void createAwxJobSuccess() {
    DeploymentService deploymentService = createDeploymentService();
    AwxJobEntity jobEntity =
        deploymentService.updateDeploymentAwxJob(iocDeploymentEntityIntended1.getAwxJob(), job1);
    assertEquals(job1.getId(), jobEntity.getAwxJobId());
  }

  @Test
  public void updateAwxJobAndDeploymentToRunning() {
    DeploymentService deploymentService = createDeploymentService();
    deploymentService.updateDeployment(iocDeploymentEntityIntended1, JobStatus.RUNNING, null);

    ArgumentCaptor<IocDeploymentEntity> argument2 =
        ArgumentCaptor.forClass(IocDeploymentEntity.class);
    Mockito.verify(deploymentRepository, Mockito.times(0)).updateDeployment(argument2.capture());
  }

  @Test
  public void updateAwxJobAndDeploymentToFailed() {
    DeploymentService deploymentService = createDeploymentService();
    deploymentService.updateDeployment(iocDeploymentEntityIntended1, JobStatus.FAILED, false);

    ArgumentCaptor<IocDeploymentEntity> argument2 =
        ArgumentCaptor.forClass(IocDeploymentEntity.class);
    Mockito.verify(deploymentRepository, Mockito.times(1)).updateDeployment(argument2.capture());
    assertEquals(false, argument2.getValue().isPending());
  }

  @Test
  public void updateAwxJobAndDeploymentToSuccessful() {
    DeploymentService deploymentService = createDeploymentService();
    deploymentService.updateDeployment(iocDeploymentEntityIntended1, JobStatus.SUCCESSFUL, true);

    ArgumentCaptor<IocDeploymentEntity> deploymentCaptor =
        ArgumentCaptor.forClass(IocDeploymentEntity.class);
    Mockito.verify(deploymentRepository, Mockito.times(1))
        .updateDeployment(deploymentCaptor.capture());
    ArgumentCaptor<IocEntity> iocCaptor = ArgumentCaptor.forClass(IocEntity.class);
    Mockito.verify(iocRepository, Mockito.times(1)).updateIoc(iocCaptor.capture());
    List<IocDeploymentEntity> capturedDeployments = deploymentCaptor.getAllValues();

    assertEquals(false, capturedDeployments.get(0).isPending());
  }

  @Test
  public void findDeploymentForIocVersions() {
    DeploymentService deploymentService = createDeploymentService();
    Mockito.when(gitLabService.getNullableGitlabProject(ioc1.getGitProjectId()))
        .thenReturn(
            new GitlabProject() {
              {
                setId(Math.toIntExact(ioc1.getGitProjectId()));
              }
            });
    Mockito.when(iocRepository.findById(ioc1.getId())).thenReturn(ioc1);
    Mockito.when(
            hostService.getHostByNetBoxId(
                ioc1.getLatestSuccessfulDeployment().getNetBoxHostId(), true))
        .thenReturn(null);
    Deployment deployment = deploymentService.findActiveDeploymentForIocDetails(ioc1.getId());
    assertEquals(job1.getId(), deployment.getAwxJobId());
    assertEquals(host1.getFqdn(), deployment.getHost().getFqdn());
  }

  @Test
  public void findAllDeployments() {
    Mockito.when(utils.pageLimitConverter(0, 2)).thenReturn(new PagingLimitDto(0, 2));
    Mockito.when(
            deploymentRepository.findAllAwxJobs(
                null,
                JobKind.DEPLOYMENT,
                JobStatusQuery.SUCCESSFUL.name(),
                "testuser",
                "demo",
                null,
                null,
                1L,
                true,
                JobOrder.TIME,
                true,
                0,
                2))
        .thenReturn(
            ImmutableList.of(
                iocDeploymentEntityActive1.getAwxJob(), iocDeploymentEntityActive2.getAwxJob()));
    Mockito.when(
            deploymentRepository.countJobsForPaging(
                null,
                JobKind.DEPLOYMENT,
                JobStatusQuery.SUCCESSFUL.name(),
                "testuser",
                "demo",
                null,
                null,
                1L,
                true))
        .thenReturn(2L);
    DeploymentService deploymentService = createDeploymentService();
    PagedJobResponse response =
        deploymentService.findAllJobs(
            null,
            JobKind.DEPLOYMENT,
            JobStatusQuery.SUCCESSFUL,
            "testuser",
            "demo",
            null,
            null,
            null,
            "MV90cnVl",
            JobOrder.TIME,
            true,
            0,
            2);
    assertNotNull(response);
    assertEquals(2, response.getTotalCount());
    assertEquals(0, response.getPageNumber());
    assertEquals(2, response.getLimit());
    assertEquals(2, response.getListSize());
    assertEquals(2, response.getJobs().size());
    assertEquals(1, response.getJobs().get(0).getId());
    assertEquals(3, response.getJobs().get(1).getId());
  }

  @Test
  public void executeNextQueuedDeploymentSuccess() {
    DeploymentService deploymentService = createDeploymentService();
    Mockito.when(deploymentRepository.findFirstQueuedAwxJob()).thenReturn(jobEntity2);
    Mockito.when(deploymentRepository.findAllPendingAwxJobs()).thenReturn(Collections.emptyList());
    Mockito.when(hostService.getHostByNetBoxId(host2.getExternalId(), host2.isVm()))
        .thenReturn(createHostWithId(host2));
    JobTemplate template = createJobTemplate();
    Mockito.when(awxService.getJobTemplateName()).thenReturn(template.getName());
    Mockito.when(awxService.getJobTemplate(template.getName())).thenReturn(template);
    String iocJson = new Gson().toJson(ImmutableList.of(ioc1.getNamingName()));
    Mockito.when(
            awxService.launchJob(
                "http://awx.job/1", host2.getFqdn(), iocJson, DeploymentType.DEPLOY))
        .thenReturn(job1);
    deploymentService.executeNextQueuedDeployment();

    Mockito.verify(deploymentRepository).findFirstQueuedAwxJob();
    Mockito.verify(deploymentRepository).findAllPendingAwxJobs();
    Mockito.verify(hostService).getHostByNetBoxId(host2.getExternalId(), host2.isVm());
    Mockito.verify(awxService).getJobTemplateName();
    Mockito.verify(awxService).getJobTemplate(template.getName());
    Mockito.verify(awxService)
        .launchJob("http://awx.job/1", host2.getFqdn(), iocJson, DeploymentType.DEPLOY);
  }

  private JobTemplate createJobTemplate() {
    JobTemplate template = new JobTemplate();
    template.setName("dummy-template");
    JobTemplate.Related related = new JobTemplate.Related();
    related.setLaunch("http://awx.job/1");
    template.setRelated(related);
    return template;
  }

  private UserDetails createUser() {
    UserDetails user = new UserDetails();
    user.setUserName("user");
    return user;
  }

  private HostInfoWithId createHostWithId(TargetHost host) {
    return new HostInfoWithId(
        host.getExternalId(),
        host.getFqdn(),
        host.getHostName(),
        null,
        null,
        host.getNetwork(),
        null,
        true,
        HostService.encodeHostIdentifier(
            new NetBoxHostIdentifier(host.getExternalId(), host.isVm())),
        null);
  }

  private DeploymentService createDeploymentService() {
    return createDeploymentService(20);
  }

  private DeploymentService createDeploymentService(Integer iocsOnHostLimit) {
    DeploymentService deploymentService =
        new DeploymentService(
            deploymentRepository,
            iocRepository,
            utils,
            hostService,
            awxService,
            authorizationService,
            gitLabService,
            deploymentAlertGenerator,
            iocsOnHostLimit,
            ZonedDateTime.parse(
                "2024-04-12T08:29:46.000+0200",
                DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSZ")),
            namingService);
    deploymentService.setServerTimeZone("Europe/Stockholm");
    return deploymentService;
  }
}
