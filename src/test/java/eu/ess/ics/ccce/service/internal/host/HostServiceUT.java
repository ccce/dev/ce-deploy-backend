/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.service.internal.host;

import static org.junit.jupiter.api.Assertions.*;

import eu.ess.ics.ccce.common.alert.HostAlertGenerator;
import eu.ess.ics.ccce.common.conversion.PagingUtil;
import eu.ess.ics.ccce.repository.api.IIocRepository;
import eu.ess.ics.ccce.rest.model.host.response.HostStatus;
import eu.ess.ics.ccce.service.external.netbox.NetBoxService;
import eu.ess.ics.ccce.service.internal.status.StatusService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 */
@ExtendWith(MockitoExtension.class)
public class HostServiceUT {
  @Mock private PagingUtil utils;
  @Mock private StatusService statusService;
  @Mock private NetBoxService netBoxService;
  @Mock private IIocRepository iocRepository;
  @Mock private HostAlertGenerator hostAlertGenerator;

  @Test
  public void getHostStatusEmptyHostname() {
    HostService hostService = createHostService();
    assertEquals(HostStatus.UNREACHABLE, hostService.getHostStatus(""));
  }

  private HostService createHostService() {
    return new HostService(utils, statusService, netBoxService, iocRepository, hostAlertGenerator);
  }
}
