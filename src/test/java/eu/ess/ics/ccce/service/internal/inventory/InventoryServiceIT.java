package eu.ess.ics.ccce.service.internal.inventory;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import eu.ess.ics.ccce.repository.entity.IocDeploymentEntity;
import eu.ess.ics.ccce.repository.entity.IocEntity;
import eu.ess.ics.ccce.repository.impl.IocRepository;
import eu.ess.ics.ccce.rest.model.host.response.HostInfoWithId;
import eu.ess.ics.ccce.rest.model.host.response.PagedNetBoxHostResponse;
import eu.ess.ics.ccce.rest.model.inventory.response.InventoryResponse;
import eu.ess.ics.ccce.service.internal.deployment.dto.DeploymentType;
import eu.ess.ics.ccce.service.internal.host.HostService;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 */
@ExtendWith(MockitoExtension.class)
@Tag("IntegrationTest")
public class InventoryServiceIT {

  @Mock private HostService hostService;
  @Mock private IocRepository iocRepository;

  private static final String HOST_1 = "test.host.name";
  private static final String HOST_2 = "another_test.host.name";
  private static final String HOST_3 = "test.network.test_name";
  private static final String HOST_4 = "something.host.com";
  private static final String HOST_5 = "unknown.host.org";
  private static final String HOST_6 = "dont_use.this_host.org";
  private static final String HOST_7 = "production.host.com";
  private static final String HOST_8 = "experimental.host.name";
  private static final String HOST_9 = "newly_created.host.name";
  private static final String HOST_10 = "never_tried.this_host.name";

  @BeforeEach
  public void init() {
    Mockito.when(
            hostService.findAllFromNetBoxPaged(
                ArgumentMatchers.any(),
                ArgumentMatchers.isNull(),
                ArgumentMatchers.anyInt(),
                ArgumentMatchers.isNull(),
                ArgumentMatchers.isNull()))
        .thenReturn(createNetboxResp());
  }

  @Test
  void checkInventory() {
    List<IocDeploymentEntity> iocs = createIOCs();
    Mockito.when(iocRepository.findIOCsForInventory()).thenReturn(iocs);
    InventoryService service = createService();
    InventoryResponse inventory = service.getInventory();

    assertFalse(inventory.meta().getHostVars().isEmpty());
    assertFalse(inventory.allHosts().hosts().isEmpty());
    assertEquals(10, inventory.allHosts().hosts().size());
    assertEquals(3, inventory.meta().getHostVars().size());
    assertEquals(2, inventory.meta().getHostVars().get(HOST_1).iocs().size());
    assertNull(inventory.meta().getHostVars().get(HOST_10));
  }

  @Test
  void noDeployments() {
    InventoryService service = createService();
    InventoryResponse inventory = service.getInventory();

    assertTrue(inventory.meta().getHostVars().isEmpty());
    assertFalse(inventory.allHosts().hosts().isEmpty());
    assertEquals(10, inventory.allHosts().hosts().size());
  }

  private InventoryService createService() {
    return new InventoryService(hostService, iocRepository);
  }

  private List<HostInfoWithId> createsHostList() {
    List<HostInfoWithId> result = new ArrayList<>();

    result.add(
        new HostInfoWithId(
            1L,
            HOST_1,
            "test_host_name",
            "test_scope",
            "test_description",
            "test_network",
            "unknown_type",
            true,
            "host_id_1",
            null));
    result.add(
        new HostInfoWithId(
            2L,
            HOST_2,
            "test_host_name_2",
            "test_scope",
            "test_description",
            "test_network",
            "unknown_type",
            true,
            "host_id_2",
            null));
    result.add(
        new HostInfoWithId(
            3L,
            HOST_3,
            "test_host_name_3",
            "test_scope",
            "test_description",
            "test_network",
            "unknown_type",
            false,
            "host_id_3",
            null));
    result.add(
        new HostInfoWithId(
            4L,
            HOST_4,
            "test_host_name_4",
            "test_scope",
            "test_description",
            "test_network",
            "unknown_type",
            true,
            "host_id_4",
            null));
    result.add(
        new HostInfoWithId(
            5L,
            HOST_5,
            "test_host_name_5",
            "test_scope",
            "test_description",
            "test_network",
            "unknown_type",
            true,
            "host_id_5",
            null));
    result.add(
        new HostInfoWithId(
            6L,
            HOST_6,
            "test_host_name_6",
            "test_scope",
            "test_description",
            "test_network",
            "unknown_type",
            false,
            "host_id_6",
            null));
    result.add(
        new HostInfoWithId(
            7L,
            HOST_7,
            "test_host_name_7",
            "test_scope",
            "test_description",
            "test_network",
            "unknown_type",
            true,
            "host_id_7",
            null));
    result.add(
        new HostInfoWithId(
            8L,
            HOST_8,
            "test_host_name_8",
            "test_scope",
            "test_description",
            "test_network",
            "unknown_type",
            true,
            "host_id_8",
            null));
    result.add(
        new HostInfoWithId(
            9L,
            HOST_9,
            "test_host_name_9",
            "test_scope",
            "test_description",
            "test_network",
            "unknown_type",
            false,
            "host_id_9",
            null));
    result.add(
        new HostInfoWithId(
            10L,
            HOST_10,
            "test_host_name_10",
            "test_scope",
            "test_description",
            "test_network",
            "unknown_type",
            true,
            "host_id_10",
            null));
    return result;
  }

  private PagedNetBoxHostResponse createNetboxResp() {
    List<HostInfoWithId> hostList = createsHostList();
    return new PagedNetBoxHostResponse(hostList.size(), hostList.size(), 0, 150, hostList);
  }

  private List<IocDeploymentEntity> createIOCs() {
    List<IocDeploymentEntity> result = new ArrayList<>();

    IocEntity ent = new IocEntity();
    IocDeploymentEntity depl = new IocDeploymentEntity();

    ent.setNamingName("IOC_1");
    depl.setNamingName("IOC_1");
    depl.setDeploymentType(DeploymentType.DEPLOY.name());
    depl.setGitProjectUrl("git.project1.org");
    depl.setSourceVersion("Version_1");
    depl.setHostFqdn(HOST_1);
    ent.setLatestSuccessfulDeployment(depl);

    result.add(depl);

    ent = new IocEntity();
    depl = new IocDeploymentEntity();
    ent.setNamingName("IOC_2");
    depl.setNamingName("IOC_2");
    depl.setDeploymentType(DeploymentType.DEPLOY.name());
    depl.setGitProjectUrl("git.project2.org");
    depl.setSourceVersion("Version_113");
    depl.setHostFqdn(HOST_1);
    ent.setLatestSuccessfulDeployment(depl);

    result.add(depl);

    ent = new IocEntity();
    depl = new IocDeploymentEntity();
    ent.setNamingName("IOC_3");
    depl.setNamingName("IOC_3");
    depl.setDeploymentType(DeploymentType.DEPLOY.name());
    depl.setGitProjectUrl("git.project3.org");
    depl.setSourceVersion("Version_45");
    depl.setHostFqdn(HOST_4);
    ent.setLatestSuccessfulDeployment(depl);

    result.add(depl);

    ent = new IocEntity();
    depl = new IocDeploymentEntity();
    ent.setNamingName("IOC_4");
    depl.setNamingName("IOC_4");
    depl.setDeploymentType(DeploymentType.DEPLOY.name());
    depl.setGitProjectUrl("git.project67.org");
    depl.setSourceVersion("Version_7");
    depl.setHostFqdn(HOST_5);
    ent.setLatestSuccessfulDeployment(depl);

    result.add(depl);

    ent = new IocEntity();
    depl = new IocDeploymentEntity();
    ent.setNamingName("IOC_5");
    depl.setNamingName("IOC_5");
    depl.setDeploymentType(DeploymentType.DEPLOY.name());
    depl.setGitProjectUrl("git.project2341.org");
    depl.setSourceVersion("Version_4567");
    depl.setHostFqdn(HOST_5);
    ent.setLatestSuccessfulDeployment(depl);

    result.add(depl);

    return result;
  }
}
