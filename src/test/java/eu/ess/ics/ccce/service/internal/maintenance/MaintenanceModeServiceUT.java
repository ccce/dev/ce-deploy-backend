/*
 * Copyright (C) 2024 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.service.internal.maintenance;

import eu.ess.ics.ccce.common.Utils;
import eu.ess.ics.ccce.exceptions.ConflictException;
import eu.ess.ics.ccce.exceptions.EntityNotFoundException;
import eu.ess.ics.ccce.repository.api.IMaintenanceModeRepository;
import eu.ess.ics.ccce.repository.entity.MaintenanceModeEntity;
import eu.ess.ics.ccce.rest.model.maintenance.request.SetMaintenance;
import eu.ess.ics.ccce.rest.model.maintenance.response.MaintenanceMode;
import java.time.ZonedDateTime;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.dao.EmptyResultDataAccessException;

@ExtendWith(MockitoExtension.class)
class MaintenanceModeServiceUT {

  @Mock private IMaintenanceModeRepository maintenanceModeRepository;

  @Test
  void getMaintenanceModeIfExistsExists() {
    MaintenanceModeEntity maintenanceModeEntity =
        createTestEntity(ZonedDateTime.now().minusDays(1), ZonedDateTime.now().minusHours(1));
    Mockito.when(maintenanceModeRepository.find()).thenReturn(maintenanceModeEntity);
    MaintenanceService maintenanceService = createMaintenanceService();
    MaintenanceMode maintenanceMode = maintenanceService.getMaintenanceModeIfExists();

    Assertions.assertEquals(
        Utils.toUtilDate(maintenanceMode.startAt()), maintenanceModeEntity.getStartAt());
  }

  private static @NotNull MaintenanceModeEntity createTestEntity(
      ZonedDateTime start, ZonedDateTime end, String message) {
    MaintenanceModeEntity maintenanceModeEntity = new MaintenanceModeEntity();
    maintenanceModeEntity.setStartAt(Utils.toUtilDate(start));
    maintenanceModeEntity.setEndAt(Utils.toUtilDate(end));
    maintenanceModeEntity.setMessage(message);
    return maintenanceModeEntity;
  }

  private static @NotNull MaintenanceModeEntity createTestEntity(
      ZonedDateTime start, ZonedDateTime end) {
    return createTestEntity(start, end, "message 1");
  }

  @Test
  void getMaintenanceModeIfExistsDoesNotExist() {
    Mockito.when(maintenanceModeRepository.find()).thenThrow(EmptyResultDataAccessException.class);
    MaintenanceService maintenanceService = createMaintenanceService();

    Assertions.assertThrows(
        EntityNotFoundException.class, maintenanceService::getMaintenanceModeIfExists);
  }

  @Test
  void getCurrentMaintenanceModeInPeriod() {
    MaintenanceModeEntity maintenanceModeEntity =
        createTestEntity(ZonedDateTime.now().minusDays(1), ZonedDateTime.now().plusDays(1));
    Mockito.when(maintenanceModeRepository.find()).thenReturn(maintenanceModeEntity);
    MaintenanceService maintenanceService = createMaintenanceService();
    MaintenanceMode maintenanceMode = maintenanceService.getCurrentMaintenanceMode();

    Assertions.assertEquals(
        Utils.toUtilDate(maintenanceMode.startAt()), maintenanceModeEntity.getStartAt());
  }

  @Test
  void getCurrentMaintenanceModeOutOfPeriod() {
    MaintenanceModeEntity maintenanceModeEntity =
        createTestEntity(ZonedDateTime.now().minusDays(1), ZonedDateTime.now().minusHours(12));
    Mockito.when(maintenanceModeRepository.find()).thenReturn(maintenanceModeEntity);
    MaintenanceService maintenanceService = createMaintenanceService();

    Assertions.assertThrows(
        EntityNotFoundException.class, maintenanceService::getCurrentMaintenanceMode);
  }

  @Test
  void setMaintenanceModeCreate() {
    ZonedDateTime start = ZonedDateTime.now().minusDays(1);
    ZonedDateTime end = ZonedDateTime.now().plusDays(1);

    MaintenanceModeEntity maintenanceModeEntity = createTestEntity(start, end);
    Mockito.when(maintenanceModeRepository.find())
        .thenThrow(EntityNotFoundException.class)
        .thenReturn(maintenanceModeEntity);
    MaintenanceService maintenanceService = createMaintenanceService();

    MaintenanceMode maintenanceMode =
        maintenanceService.setMaintenanceMode(new SetMaintenance(start, end, "message 1"));

    Assertions.assertEquals(
        Utils.toUtilDate(maintenanceMode.startAt()), maintenanceModeEntity.getStartAt());
  }

  @Test
  void setMaintenanceModeUpdate() {
    ZonedDateTime start = ZonedDateTime.now().minusDays(1);
    ZonedDateTime end = ZonedDateTime.now().plusDays(1);

    MaintenanceModeEntity maintenanceModeEntity = createTestEntity(start, end);
    String message2 = "message2";
    MaintenanceModeEntity maintenanceModeEntity2 = createTestEntity(start, end, message2);

    Mockito.when(maintenanceModeRepository.find())
        .thenReturn(maintenanceModeEntity)
        .thenReturn(maintenanceModeEntity2);
    MaintenanceService maintenanceService = createMaintenanceService();

    MaintenanceMode maintenanceMode =
        maintenanceService.setMaintenanceMode(new SetMaintenance(start, end, message2));

    Assertions.assertEquals(
        Utils.toUtilDate(maintenanceMode.startAt()), maintenanceModeEntity.getStartAt());
    Assertions.assertEquals(maintenanceMode.message(), message2);
  }

  @Test
  void setMaintenanceModeEndBeforeNow() {
    ZonedDateTime start = ZonedDateTime.now().minusDays(2);
    ZonedDateTime end = ZonedDateTime.now().minusDays(1);
    MaintenanceService maintenanceService = createMaintenanceService();

    Assertions.assertThrows(
        ConflictException.class,
        () -> maintenanceService.setMaintenanceMode(new SetMaintenance(start, end, "message")));
  }

  @Test
  void checkMaintenanceMode() {

    MaintenanceModeEntity maintenanceModeEntity =
        createTestEntity(ZonedDateTime.now().minusDays(1), ZonedDateTime.now().plusDays(1));
    maintenanceModeEntity.setMessage("message");
    Mockito.when(maintenanceModeRepository.find()).thenReturn(maintenanceModeEntity);
    MaintenanceService maintenanceService = createMaintenanceService();

    Assertions.assertEquals("message", maintenanceService.checkMaintenanceMode("/api/ioc"));
    Assertions.assertNull(maintenanceService.checkMaintenanceMode("/api/maintenance_mode"));
    Assertions.assertNull(maintenanceService.checkMaintenanceMode("/api/maintenance_mode/current"));
  }

  private MaintenanceService createMaintenanceService() {
    MaintenanceService maintenanceService = new MaintenanceService(maintenanceModeRepository);
    maintenanceService.setServerTimeZone("Europe/Stockholm");
    return maintenanceService;
  }
}
