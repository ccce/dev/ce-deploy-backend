package eu.ess.ics.ccce.service.internal.deployment;

import static org.junit.jupiter.api.Assertions.assertNull;

import eu.ess.ics.ccce.rest.model.job.response.Deployment;
import org.junit.jupiter.api.Test;

/**
 * @author <a href="mailto:domonkos.gulyas@ess.eu">Domonkos Gulyas</a>
 */
public class DeploymentUtilUT {
  @Test
  void emptyDeploymentConversion() {
    Deployment deploymentResult =
        DeploymentUtil.convertToDeploymentDetails(
            null, null, null, false, null, null, null, "Europe/Stockholm");
    assertNull(deploymentResult);
  }
}
