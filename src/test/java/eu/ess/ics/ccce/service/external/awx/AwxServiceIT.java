/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.service.external.awx;

import static org.junit.jupiter.api.Assertions.*;

import com.google.common.collect.ImmutableList;
import eu.ess.ics.ccce.exceptions.AwxServiceException;
import eu.ess.ics.ccce.exceptions.EntityNotFoundException;
import eu.ess.ics.ccce.service.external.awx.model.Credential;
import eu.ess.ics.ccce.service.external.awx.model.JobStatus;
import eu.ess.ics.ccce.service.external.awx.model.response.Command;
import eu.ess.ics.ccce.service.external.awx.model.response.CommandDetails;
import eu.ess.ics.ccce.service.external.awx.model.response.Inventory;
import eu.ess.ics.ccce.service.external.awx.model.response.Job;
import eu.ess.ics.ccce.service.external.awx.model.response.JobDetails;
import eu.ess.ics.ccce.service.external.awx.model.response.JobHostSummary;
import eu.ess.ics.ccce.service.external.awx.model.response.JobTemplate;
import eu.ess.ics.ccce.service.external.common.HttpClientService;
import eu.ess.ics.ccce.service.external.common.OkHttpConfiguration;
import eu.ess.ics.ccce.service.internal.deployment.dto.DeploymentType;
import eu.ess.ics.ccce.testutils.ExternalServiceMock;
import java.util.Collections;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockserver.model.Parameter;
import org.springframework.core.env.Environment;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 */
@ExtendWith(MockitoExtension.class)
@Tag("IntegrationTest")
public class AwxServiceIT extends ExternalServiceMock {
  private AwxService awxService;
  @Mock private Environment environment;
  private HttpClientService httpClientService;
  @InjectMocks private OkHttpConfiguration okHttpConfiguration;

  public AwxServiceIT() {
    super("/awx.response");
  }

  @BeforeEach
  public void setup() {
    Mockito.when(environment.getProperty(AwxService.AWX_URL))
        .thenReturn(ExternalServiceMock.TEST_URL);
    Mockito.when(environment.getProperty(AwxService.AWX_TOKEN)).thenReturn("TEST_TOKEN");
    Mockito.when(environment.getProperty(AwxService.AWX_JOB_EVENT_ROLE_FILTER))
        .thenReturn("ics-role");
    Mockito.when(environment.getProperty("allow.untrusted.certs")).thenReturn("true");
    Mockito.when(environment.getProperty("http.client.timeout")).thenReturn("20");
    httpClientService = new HttpClientService(okHttpConfiguration.okHttpClient());
    awxService = new AwxService(environment, httpClientService);
  }

  @Test
  void getJobTemplatesSuccess() throws AwxServiceException {
    createGetExpectationSuccess(
        "/api/v2/job_templates",
        ImmutableList.of(new Parameter("name", "ccce-dummy-playbook")),
        "templates.json");
    JobTemplate dummyPlaybook = awxService.getJobTemplate("ccce-dummy-playbook");
    assertNotNull(dummyPlaybook);
    assertEquals("ccce-dummy-playbook", dummyPlaybook.getName());
    assertEquals("/api/v2/job_templates/26/", dummyPlaybook.getUrl());
    assertNotNull(dummyPlaybook.getRelated());
    assertEquals("/api/v2/job_templates/26/launch/", dummyPlaybook.getRelated().getLaunch());
  }

  @Test
  void getJobTemplatesFail() {
    createExpectationFail(
        "GET",
        "/api/v2/job_templates",
        ImmutableList.of(new Parameter("name", "ccce-dummy-playbook")),
        401);
    assertThrows(AwxServiceException.class, () -> awxService.getJobTemplate("ccce-dummy-playbook"));
  }

  @Test
  void launchJobTemplatesSuccess() throws AwxServiceException {
    String hostname = "ccce-demo-1.esss.lu.se";
    createPostExpectationSuccess(
        "/api/v2/job_templates/26/launch", Collections.emptyList(), null, "launch_job.json", 201);

    Job job =
        awxService.launchJob(
            "/api/v2/job_templates/26/launch", hostname, "[]", DeploymentType.DEPLOY);
    assertNotNull(job);
    assertEquals(952, job.getId());
    assertEquals("job", job.getType());
    assertEquals(952, job.getJob());
    assertEquals("/api/v2/jobs/952/", job.getUrl());
    assertEquals("run", job.getJobType());
    assertEquals(JobStatus.PENDING, job.getStatus());
    assertNull(job.getStarted());
    assertNull(job.getFinished());
    assertNotNull(job.getRelated());
    assertEquals("/api/v2/jobs/952/cancel/", job.getRelated().getCancel());
    assertEquals("/api/v2/jobs/952/relaunch/", job.getRelated().getRelaunch());
  }

  @Test
  void launchJobTemplatesFail() {
    String hostname = "ccce-demo-1.esss.lu.se";
    createExpectationFail("POST", "/api/v2/job_templates/26/launch/", Collections.emptyList(), 401);
    assertThrows(
        AwxServiceException.class,
        () ->
            awxService.launchJob(
                "/api/v2/job_templates/26/launch/", hostname, "[]", DeploymentType.DEPLOY));
  }

  @Test
  void getJobDetailsSuccess() throws EntityNotFoundException, AwxServiceException {
    createGetExpectationSuccess("/api/v2/jobs/856", Collections.emptyList(), "job_details.json");
    createGetExpectationSuccess(
        "/api/v2/jobs/856/job_events/", Collections.emptyList(), "job_events.json");
    createGetExpectationSuccess(
        "/api/v2/jobs/856/job_host_summaries/", Collections.emptyList(), "job_host_summaries.json");
    createGetExpectationSuccess(
        "/api/v2/hosts", ImmutableList.of(new Parameter("id", "9")), "host.json");
    createGetExpectationSuccess(
        "/api/v2/jobs/856/stdout/",
        ImmutableList.of(new Parameter("format", "html")),
        "job_stdout.html");
    createGetExpectationSuccess(
        "/api/v2/hosts/9/ansible_facts/", Collections.emptyList(), "ansible_facts.json");

    JobDetails details = awxService.getJobDetails(856);
    assertNotNull(details.getJob());
    assertEquals(856, details.getJob().getId());
    assertEquals(JobStatus.SUCCESSFUL, details.getJob().getStatus());

    assertNotNull(details.getHostSummaries());
    assertEquals(1, details.getHostSummaries().size());
    for (JobHostSummary summary : details.getHostSummaries()) {
      assertNotEquals(0, summary.getId());
      assertEquals("job_host_summary", summary.getType());
      assertNotNull(summary.getCreated());
    }
    JobHostSummary hostSummary = details.getHostSummaries().get(0);
    assertEquals(267, hostSummary.getId());
    assertEquals("ccce-demo-04.cslab.esss.lu.se", hostSummary.getHostName());
    assertNull(hostSummary.getHost());
    assertEquals(
        new DateTime(2021, 3, 4, 9, 10, 25, 512, DateTimeZone.UTC).toDate(),
        hostSummary.getCreated());
    assertEquals(
        new DateTime(2021, 3, 4, 9, 10, 25, 512, DateTimeZone.UTC).toDate(),
        hostSummary.getModified());
    assertEquals(2, hostSummary.getOk());
    assertEquals(856, hostSummary.getJob());
    assertEquals(0, hostSummary.getChanged());
    assertEquals(0, hostSummary.getDark());
    assertEquals(0, hostSummary.getFailures());
    assertEquals(1, hostSummary.getProcessed());
    assertEquals(0, hostSummary.getIgnored());
    assertEquals(0, hostSummary.getRescued());
    assertFalse(hostSummary.isFailed());

    assertNotNull(hostSummary.getSummaryFields().getHost());
    assertEquals(9, hostSummary.getSummaryFields().getHost().getId());
    assertEquals(
        "ccce-demo-04.cslab.esss.lu.se", hostSummary.getSummaryFields().getHost().getName());
    assertEquals("imported", hostSummary.getSummaryFields().getHost().getDescription());

    assertNotNull(hostSummary.getSummaryFields().getJob());
    assertEquals(856, hostSummary.getSummaryFields().getJob().getId());
    assertEquals("ccce-dummy-playbook", hostSummary.getSummaryFields().getJob().getName());
    assertEquals("Dummy", hostSummary.getSummaryFields().getJob().getDescription());
    assertEquals("successful", hostSummary.getSummaryFields().getJob().getStatus());
    assertFalse(hostSummary.getSummaryFields().getJob().isFailed());
    assertEquals(3.565, hostSummary.getSummaryFields().getJob().getElapsed());
    assertEquals("job", hostSummary.getSummaryFields().getJob().getType());
    assertEquals(26, hostSummary.getSummaryFields().getJob().getJobTemplateId());
    assertEquals(
        "ccce-dummy-playbook", hostSummary.getSummaryFields().getJob().getJobTemplateName());

    assertEquals(JobStatus.SUCCESSFUL, details.getJob().getStatus());
  }

  @Test
  void getJobDetailsNoIocDeploymentResultSuccess()
      throws EntityNotFoundException, AwxServiceException {
    createGetExpectationSuccess("/api/v2/jobs/856", Collections.emptyList(), "job_details.json");
    createGetExpectationSuccess(
        "/api/v2/jobs/856/job_events/", Collections.emptyList(), "job_events.json");
    createGetExpectationSuccess(
        "/api/v2/jobs/856/job_host_summaries/", Collections.emptyList(), "job_host_summaries.json");
    createGetExpectationSuccess(
        "/api/v2/hosts", ImmutableList.of(new Parameter("id", "9")), "host.json");
    createGetExpectationSuccess(
        "/api/v2/jobs/856/stdout/",
        ImmutableList.of(new Parameter("format", "html")),
        "job_stdout.html");
    createGetExpectationSuccess(
        "/api/v2/hosts/9/ansible_facts/", Collections.emptyList(), "ansible_facts_no_result.json");

    JobDetails details = awxService.getJobDetails(856);
    assertNotNull(details.getJob());
    assertEquals(856, details.getJob().getId());
    assertEquals(JobStatus.SUCCESSFUL, details.getJob().getStatus());

    assertNotNull(details.getHostSummaries());
    assertEquals(1, details.getHostSummaries().size());
    for (JobHostSummary summary : details.getHostSummaries()) {
      assertNotEquals(0, summary.getId());
      assertEquals("job_host_summary", summary.getType());
      assertNotNull(summary.getCreated());
    }
    JobHostSummary hostSummary = details.getHostSummaries().get(0);
    assertEquals(267, hostSummary.getId());
    assertEquals("ccce-demo-04.cslab.esss.lu.se", hostSummary.getHostName());
    assertNull(hostSummary.getHost());
    assertEquals(
        new DateTime(2021, 3, 4, 9, 10, 25, 512, DateTimeZone.UTC).toDate(),
        hostSummary.getCreated());
    assertEquals(
        new DateTime(2021, 3, 4, 9, 10, 25, 512, DateTimeZone.UTC).toDate(),
        hostSummary.getModified());
    assertEquals(2, hostSummary.getOk());
    assertEquals(856, hostSummary.getJob());
    assertEquals(0, hostSummary.getChanged());
    assertEquals(0, hostSummary.getDark());
    assertEquals(0, hostSummary.getFailures());
    assertEquals(1, hostSummary.getProcessed());
    assertEquals(0, hostSummary.getIgnored());
    assertEquals(0, hostSummary.getRescued());
    assertFalse(hostSummary.isFailed());

    assertNotNull(hostSummary.getSummaryFields().getHost());
    assertEquals(9, hostSummary.getSummaryFields().getHost().getId());
    assertEquals(
        "ccce-demo-04.cslab.esss.lu.se", hostSummary.getSummaryFields().getHost().getName());
    assertEquals("imported", hostSummary.getSummaryFields().getHost().getDescription());

    assertNotNull(hostSummary.getSummaryFields().getJob());
    assertEquals(856, hostSummary.getSummaryFields().getJob().getId());
    assertEquals("ccce-dummy-playbook", hostSummary.getSummaryFields().getJob().getName());
    assertEquals("Dummy", hostSummary.getSummaryFields().getJob().getDescription());
    assertEquals("successful", hostSummary.getSummaryFields().getJob().getStatus());
    assertFalse(hostSummary.getSummaryFields().getJob().isFailed());
    assertEquals(3.565, hostSummary.getSummaryFields().getJob().getElapsed());
    assertEquals("job", hostSummary.getSummaryFields().getJob().getType());
    assertEquals(26, hostSummary.getSummaryFields().getJob().getJobTemplateId());
    assertEquals(
        "ccce-dummy-playbook", hostSummary.getSummaryFields().getJob().getJobTemplateName());
  }

  @Test
  void getJobDetailsFail() throws EntityNotFoundException {
    createExpectationFail("GET", "/api/v2/jobs/856", Collections.emptyList(), 404);
    assertThrows(EntityNotFoundException.class, () -> awxService.getJobDetails(856));
  }

  @Test
  void getInventorySuccess() throws AwxServiceException {
    createGetExpectationSuccess(
        "/api/v2/inventories",
        ImmutableList.of(new Parameter("name", "test-inventory")),
        "inventory.json");
    Inventory inventory = awxService.getInventory("test-inventory");
    assertNotNull(inventory);
    assertEquals(8, inventory.getId());
    assertEquals("test-inventory", inventory.getName());
  }

  @Test
  void getInventoryFail() {
    createGetExpectationSuccess(
        "/api/v2/inventories",
        ImmutableList.of(new Parameter("name", "test-inventory")),
        "awx_not_found_empty_array.json");
    assertThrows(EntityNotFoundException.class, () -> awxService.getInventory("test-inventory"));
  }

  @Test
  void getCredentialSuccess() throws AwxServiceException {
    createGetExpectationSuccess(
        "/api/v2/credentials", ImmutableList.of(new Parameter("name", "demo")), "credentail.json");
    Credential credential = awxService.getCredential("demo");
    assertNotNull(credential);
    assertEquals(1, credential.getId());
    assertEquals("demo", credential.getName());
  }

  @Test
  void getCredentialFail() {
    createGetExpectationSuccess(
        "/api/v2/credentials",
        ImmutableList.of(new Parameter("name", "demo")),
        "awx_not_found_empty_array.json");
    assertThrows(EntityNotFoundException.class, () -> awxService.getCredential("demo"));
  }

  @Test
  void launchAdHocCommandSuccess() throws AwxServiceException {
    String hostname = "ccce-demo-1.esss.lu.se";
    String moduleName = "module";
    String moduleArgs = "arg";
    Inventory inventory = new Inventory();
    inventory.setId(1);
    Credential credential = new Credential();
    credential.setId(1);

    String body =
        "{\"job_type\":\"run\",\"inventory\":"
            + inventory.getId()
            + ",\"limit\":\""
            + hostname
            + "\",\"credential\":"
            + credential.getId()
            + ",\"module_name\":\""
            + moduleName
            + "\",\"module_args\":\""
            + moduleArgs
            + "\",\"forks\":0,\"verbosity\":3,\"become_enabled\":true,\"diff_mode\":false}";
    createPostExpectationSuccess(
        "/api/v2/ad_hoc_commands/", Collections.emptyList(), body, "launch_command.json", 201);

    Command command =
        awxService.launchAdHocCommand(
            inventory.getId(), hostname, credential, moduleName, moduleArgs, null);
    assertNotNull(command);
    assertEquals(1225, command.getId());
    assertEquals("ad_hoc_command", command.getType());
    assertEquals("/api/v2/ad_hoc_commands/1225/", command.getUrl());
  }

  @Test
  void launchAdHocCommandFail() {
    String hostname = "ccce-demo-1.esss.lu.se";
    String moduleName = "module";
    String moduleArgs = "arg";
    Inventory inventory = new Inventory();
    inventory.setId(1);
    Credential credential = new Credential();
    credential.setId(1);
    createExpectationFail("POST", "/api/v2/ad_hoc_commands/", Collections.emptyList(), 401);
    assertThrows(
        AwxServiceException.class,
        () ->
            awxService.launchAdHocCommand(
                inventory.getId(), hostname, credential, moduleName, moduleArgs, null));
  }

  @Test
  void getCommandDetailsSuccess() throws AwxServiceException {
    createGetExpectationSuccess(
        "/api/v2/ad_hoc_commands/1225", Collections.emptyList(), "command_details.json");
    createGetExpectationSuccess(
        "/api/v2/ad_hoc_commands/1225/events/", Collections.emptyList(), "command_events.json");
    createGetExpectationSuccess(
        "/api/v2/ad_hoc_commands/1225/stdout/",
        ImmutableList.of(new Parameter("format", "html")),
        "command_stdout.html");

    CommandDetails details = awxService.getCommandDetails(1225);
    assertNotNull(details.getCommand());
    assertEquals(1225, details.getCommand().getId());
    assertEquals(JobStatus.SUCCESSFUL, details.getCommand().getStatus());
  }

  @Test
  void getCommandDetailsFail() throws EntityNotFoundException {
    createExpectationFail("GET", "/api/v2/ad_hoc_commands/1225", Collections.emptyList(), 404);
    assertThrows(EntityNotFoundException.class, () -> awxService.getCommandDetails(1225));
  }
}
