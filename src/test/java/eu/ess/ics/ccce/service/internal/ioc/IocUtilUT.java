package eu.ess.ics.ccce.service.internal.ioc;

import static org.junit.jupiter.api.Assertions.assertNull;

import eu.ess.ics.ccce.rest.model.ioc.response.Ioc;
import org.junit.jupiter.api.Test;

/**
 * @author <a href="mailto:domonkos.gulyas@ess.eu">Domonkos Gulyas</a>
 */
public class IocUtilUT {

  @Test
  void emptyIocConversion() {
    Ioc iocResult = IocUtil.convertToIocDetails(null, null, null, null);
    assertNull(iocResult);
  }
}
