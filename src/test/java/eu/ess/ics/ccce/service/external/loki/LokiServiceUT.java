/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.service.external.loki;

import static org.junit.jupiter.api.Assertions.*;

import com.google.common.collect.ImmutableList;
import com.google.gson.Gson;
import eu.ess.ics.ccce.exceptions.LokiServiceException;
import eu.ess.ics.ccce.exceptions.RemoteServiceException;
import eu.ess.ics.ccce.rest.model.loki.response.LokiResponse;
import eu.ess.ics.ccce.service.external.common.HttpClientService;
import eu.ess.ics.ccce.service.external.loki.model.Data;
import eu.ess.ics.ccce.service.external.loki.model.Result;
import eu.ess.ics.ccce.service.external.loki.model.Root;
import java.net.SocketTimeoutException;
import okhttp3.Headers;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.util.ReflectionTestUtils;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 */
@SpringBootTest
public class LokiServiceUT {
  @Mock private HttpClientService httpClientService;

  @Value("${server.time-zone}")
  private String timeZone;

  @Value("${loki.server.address}")
  private String baseUrl;

  @Test
  public void getLogLinesSucces() throws RemoteServiceException, SocketTimeoutException {
    LokiService service = createService();

    HttpClientService.ServiceResponse<Root> httpResponse = logLines();
    Mockito.when(
            httpClientService.executeGetRequest(
                ArgumentMatchers.any(Headers.class),
                ArgumentMatchers.anyString(),
                ArgumentMatchers.eq(Root.class)))
        .thenReturn(httpResponse);

    LokiResponse response = service.getLogLines("test-host.com", "TEST-IOC", 1000);
    assertNotNull(response);
    assertEquals(4, response.getLines().size());
    assertEquals(
        1629971764107L, response.getLines().get(0).getLogDate().toInstant().toEpochMilli());
    assertTrue(response.getLines().get(0).getLogMessage().contains("procServ  -- MARK --"));
  }

  @Test
  public void getLogLinesFail() throws RemoteServiceException, SocketTimeoutException {
    LokiService service = createService();

    Mockito.when(
            httpClientService.executeGetRequest(
                ArgumentMatchers.any(Headers.class),
                ArgumentMatchers.anyString(),
                ArgumentMatchers.eq(Root.class)))
        .thenReturn(logUnavailable());

    assertThrows(
        LokiServiceException.class, () -> service.getLogLines("test-host.com", "TEST-IOC", 1000));
  }

  private HttpClientService.ServiceResponse<Root> logLines() {
    Gson g = new Gson();
    String jsonString =
        """
        {
            "status": "success",
            "data": {
                "resultType": "streams",
                "result": [
                    {
                        "stream": {
                            "host": "icsv-pwrea-01",
                            "job": "syslog"
                        },
                        "values": [
                            [
                                "1629974571108302106",
                                "procServ  -- MARK --\\n"
                            ],
                            [
                                "1629973371107679275",
                                "procServ  -- MARK --\\n"
                            ],
                            [
                                "1629972171322646834",
                                "procServ /var/log/procServ/out-Pwr-L1U2_Ctrl-IOC-01.log 2021/08/26 12:02:51.215 drvModbusAsyn::doModbusIO, port Pwr-L1U3:CnPw-EA-ACM230-IME_DATA1 expected 34 words, actually received 8\\n"
                            ],
                            [
                                "1629971764107431735",
                                "procServ  -- MARK --\\n"
                            ]
                        ]
                    }
                ],
                "stats": {
                    "summary": {
                        "bytesProcessedPerSecond": 8856,
                        "linesProcessedPerSecond": 108,
                        "totalBytesProcessed": 328,
                        "totalLinesProcessed": 4,
                        "execTime": 0.037035602
                    },
                    "store": {
                        "totalChunksRef": 0,
                        "totalChunksDownloaded": 0,
                        "chunksDownloadTime": 0,
                        "headChunkBytes": 0,
                        "headChunkLines": 0,
                        "decompressedBytes": 0,
                        "decompressedLines": 0,
                        "compressedBytes": 0,
                        "totalDuplicates": 0
                    },
                    "ingester": {
                        "totalReached": 1,
                        "totalChunksMatched": 1,
                        "totalBatches": 1,
                        "totalLinesSent": 4,
                        "headChunkBytes": 0,
                        "headChunkLines": 0,
                        "decompressedBytes": 328,
                        "decompressedLines": 4,
                        "compressedBytes": 369,
                        "totalDuplicates": 0
                    }
                }
            }
        }\
        """;
    Root p = g.fromJson(jsonString, Root.class);

    return new HttpClientService.ServiceResponse<>(p, 200, null);
  }

  private HttpClientService.ServiceResponse<Root> logUnavailable() {
    return new HttpClientService.ServiceResponse<>(null, 500, null);
  }

  private LokiService createService() {
    LokiService service = new LokiService(httpClientService, baseUrl, timeZone);
    ReflectionTestUtils.setField(service, "baseUrl", "http://loki-01.cslab.esss.lu.se:3100");
    return service;
  }

  @Test
  void lokiResponseConversion() {
    Root root =
        new Root(
            new Data(
                ImmutableList.of(
                    new Result(
                        ImmutableList.of(ImmutableList.of("1629974571108302106", "item"))))));
    LokiResponse response = LokiService.convertToLokiResponse(root, "UTC");
    assertNotNull(response);
    assertEquals(1, response.getLines().size());
    assertEquals(
        1629974571108L, response.getLines().get(0).getLogDate().toInstant().toEpochMilli());
    assertEquals("item", response.getLines().get(0).getLogMessage());
  }
}
