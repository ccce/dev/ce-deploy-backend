package eu.ess.ics.ccce.service.external.gitlab;

import static org.junit.jupiter.api.Assertions.assertEquals;

import eu.ess.ics.ccce.exceptions.InputValidationException;
import eu.ess.ics.ccce.service.external.common.HttpClientService;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.util.ReflectionTestUtils;

@SpringBootTest
class GitLabServiceUT {

  @Mock private HttpClientService httpClientService;

  @Value("${gitlab.server.address}")
  private String gitServerAdr;

  @Test
  void getProjectByUrlFailDomainNameMismatch() {
    GitLabService gitLabService = createService();
    try {
      gitLabService.getProjectByUrl("https://gitlab.esssfake.lu.se/group/projectname.git");
    } catch (InputValidationException e) {
      assertEquals("Invalid request: Invalid organisation domain name", e.getMessage());
    }
  }

  private GitLabService createService() {
    GitLabService service = new GitLabService(httpClientService);
    ReflectionTestUtils.setField(service, "gitServerAddress", gitServerAdr);
    return service;
  }
}
