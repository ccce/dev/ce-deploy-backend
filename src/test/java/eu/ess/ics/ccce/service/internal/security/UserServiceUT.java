/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.service.internal.security;

import static org.junit.jupiter.api.Assertions.*;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import eu.ess.ics.ccce.exceptions.AuthenticationException;
import eu.ess.ics.ccce.service.external.rbac.RBACService;
import eu.ess.ics.ccce.service.external.rbac.model.RBACToken;
import eu.ess.ics.ccce.service.internal.security.dto.LoginTokenDto;
import eu.ess.ics.ccce.service.internal.security.dto.UserDetails;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 */
@SpringBootTest
public class UserServiceUT {

  @Mock private RBACService rbacService;

  @Test
  void loginFail() {
    UserService userService = createService();
    Mockito.when(rbacService.loginUser("test", "test")).thenThrow(AuthenticationException.class);
    assertThrows(
        AuthenticationException.class,
        () -> {
          userService.loginUser("test", "test");
        });
  }

  @Test
  void loginSuccess() throws JsonProcessingException {
    UserService userService = createService();
    RBACToken rbacLoginResponse = createLogin();
    Mockito.when(rbacService.loginUser("test", "test")).thenReturn(rbacLoginResponse);

    LoginTokenDto tokenDto = userService.loginUser("test", "test");

    assertNotNull(tokenDto);
    assertNotNull(tokenDto.getToken());
    assertEquals(rbacLoginResponse.getId(), tokenDto.getToken());
  }

  @Test
  void userInfoFromToken() throws JsonProcessingException {
    UserService userService = createService();
    RBACToken rbacInfoResponse = createLogin();
    Mockito.when(rbacService.userInfoFromToken(rbacInfoResponse.getId()))
        .thenReturn(rbacInfoResponse);
    UserDetails details = userService.getUserInfoFromToken(rbacInfoResponse.getId());
    assertNotNull(details);
    assertEquals(rbacInfoResponse.getId(), details.getToken());
  }

  @Test
  void renewToken() throws JsonProcessingException {
    UserService userService = createService();
    RBACToken rbacToken = createLogin();
    RBACToken rbacTokenRenew = createRenew();
    Mockito.when(rbacService.renewToken(rbacToken.getId())).thenReturn(rbacTokenRenew);
    LoginTokenDto token = userService.renewToken(rbacToken.getId());
    assertNotNull(token);
    assertEquals(rbacToken.getId(), token.getToken());
    assertTrue(token.getExpirationDuration() > 0);
  }

  private RBACToken createLogin() throws JsonProcessingException {
    String xml =
        """
        <?xml version="1.0" encoding="UTF-8" standalone="yes"?>
        <token>
            <id>AAAA-BBBBB-CCCC-DDDD-EEFFGGHH</id>
            <username>johndoe</username>
            <firstName>John</firstName>
            <lastName>Doe</lastName>
            <creationTime>1620823391053</creationTime>
            <expirationTime>1620852191053</expirationTime>
            <ip>10.0.42.5</ip>
            <roles>
                <role>CCDBAdministrator</role>
                <role>CableAdministrator</role>
                <role>IOCFactoryAdministrator</role>
                <role>NamingAdministrator</role>
            </roles>
            <signature>764AAF5B29498D3424226C6FF54E5BD7FC0751C35E8986CEBFA25E8867FBEBC2F9482C6189CDEFC8312BEB244D3E122B2E1876344BD8CC7C9B08420A3A536169</signature>
        </token>""";

    XmlMapper xmlMapper = new XmlMapper();

    return xmlMapper.readValue(xml, RBACToken.class);
  }

  private RBACToken createRenew() throws JsonProcessingException {
    String xml =
        """
        <?xml version="1.0" encoding="UTF-8" standalone="yes"?>
        <token>
            <id>AAAA-BBBBB-CCCC-DDDD-EEFFGGHH</id>
            <username>johndoe</username>
            <firstName>John</firstName>
            <lastName>Doe</lastName>
            <creationTime>1620823391053</creationTime>
            <expirationTime>1620852291053</expirationTime>
            <ip>10.0.42.5</ip>
            <roles>
                <role>CCDBAdministrator</role>
                <role>CableAdministrator</role>
                <role>IOCFactoryAdministrator</role>
                <role>NamingAdministrator</role>
            </roles>
            <signature>764AAF5B29498D3424226C6FF54E5BD7FC0751C35E8986CEBFA25E8867FBEBC2F9482C6189CDEFC8312BEB244D3E122B2E1876344BD8CC7C9B08420A3A536169</signature>
        </token>""";

    XmlMapper xmlMapper = new XmlMapper();

    return xmlMapper.readValue(xml, RBACToken.class);
  }

  @Test
  void emptyRbacTokenConversion() {
    UserDetails userDetailsResult = UserService.convertToUserDetails(null);
    assertNull(userDetailsResult);
  }

  @Test
  void rbacTokenConversion() {
    RBACToken token = new RBACToken();
    token.setId("Test Id");
    token.setUserName("Test user");

    UserDetails userDetails = UserService.convertToUserDetails(token);
    assertEquals(userDetails.getUserName(), token.getUserName());
    assertEquals(userDetails.getToken(), token.getId());
  }

  private UserService createService() {
    return new UserService(rbacService);
  }
}
