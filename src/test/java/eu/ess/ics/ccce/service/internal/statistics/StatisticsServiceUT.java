package eu.ess.ics.ccce.service.internal.statistics;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.google.common.collect.ImmutableList;
import eu.ess.ics.ccce.repository.api.IDeploymentStatRepository;
import eu.ess.ics.ccce.repository.entity.DeploymentStatisticsEntity;
import eu.ess.ics.ccce.repository.impl.IocDeploymentRepository;
import eu.ess.ics.ccce.repository.impl.IocRepository;
import eu.ess.ics.ccce.rest.model.statistics.response.*;
import eu.ess.ics.ccce.service.internal.host.HostService;
import eu.ess.ics.ccce.service.internal.status.StatusService;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class StatisticsServiceUT {

  @Mock private StatusService statusService;
  @Mock private IocRepository iocRepository;
  @Mock private IocDeploymentRepository deploymentRepository;
  @Mock private IDeploymentStatRepository deploymentStatRepository;
  @Mock private HostService hostService;

  @Test
  public void calculateDeployments() {
    LocalDate today = LocalDate.now();

    LocalDate todayMinus1Day = today.minusDays(1);
    Date todayStart = Date.from(today.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
    Date todayMinus1DayStart =
        Date.from(todayMinus1Day.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
    Mockito.when(deploymentRepository.countAllDeployments(today)).thenReturn(3L);
    Mockito.when(deploymentRepository.countSuccessfulDeployments(today)).thenReturn(2L);
    Mockito.when(deploymentRepository.countAllDeployments(todayMinus1Day)).thenReturn(2L);
    Mockito.when(deploymentRepository.countSuccessfulDeployments(todayMinus1Day)).thenReturn(1L);
    List<DeploymentStatisticsResponse> expected =
        ImmutableList.of(
            new DeploymentStatisticsResponse(todayMinus1DayStart, 2L, 1L),
            new DeploymentStatisticsResponse(todayStart, 3L, 2L));
    List<DeploymentStatisticsResponse> actual = createService().calculateDeployments(2);

    assertEquals(expected, actual);

    Mockito.verify(deploymentRepository).countAllDeployments(today);
    Mockito.verify(deploymentRepository).countAllDeployments(todayMinus1Day);
    Mockito.verify(deploymentRepository).countSuccessfulDeployments(today);
    Mockito.verify(deploymentRepository).countSuccessfulDeployments(todayMinus1Day);
  }

  @Test
  public void calculateIocStatistics() {

    Date createdAt =
        Date.from(
            LocalDate.of(2023, 1, 1).atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
    LocalDate yesterday = LocalDate.now().minusDays(1);
    Date yesterdayStart =
        Date.from(yesterday.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
    DeploymentStatisticsEntity deploymentStatistics = new DeploymentStatisticsEntity();
    deploymentStatistics.setId(1L);
    deploymentStatistics.setIocCount(3L);
    deploymentStatistics.setNetwork("test network");
    deploymentStatistics.setCreatedAt(createdAt);

    Mockito.when(deploymentStatRepository.collectDeploymentStat(yesterdayStart))
        .thenReturn(ImmutableList.of(deploymentStatistics));
    List<DeploymentOnHost> actual = createService().calculateIocStatistics(1);
    List<DeploymentOnHost> expected =
        ImmutableList.of(
            new DeploymentOnHost(
                "test network", ImmutableList.of(new DeploymentCount(3L, createdAt))));
    assertEquals(expected, actual);
    Mockito.verify(deploymentStatRepository).collectDeploymentStat(yesterdayStart);
  }

  @Test
  public void calculateActiveIocHistory() {
    LocalDate today = LocalDate.now();

    LocalDate todayMinus1Day = today.minusDays(1);

    Date todayMinus1DayStart =
        Date.from(todayMinus1Day.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());

    LocalDate todayMinus2Day = today.minusDays(2);

    Date todayMinus2DayStart =
        Date.from(todayMinus2Day.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());

    Mockito.when(deploymentStatRepository.countActiveIocs(todayMinus1DayStart)).thenReturn(2L);
    Mockito.when(deploymentStatRepository.countActiveIocs(todayMinus2DayStart)).thenReturn(3L);
    List<ActiveIOCSForHistoryResponse> actual = createService().calculateActiveIocHistory(2);
    List<ActiveIOCSForHistoryResponse> expected =
        ImmutableList.of(
            new ActiveIOCSForHistoryResponse(3L, todayMinus2DayStart),
            new ActiveIOCSForHistoryResponse(2L, todayMinus1DayStart));
    assertEquals(expected, actual);

    Mockito.verify(deploymentStatRepository).countActiveIocs(todayMinus1DayStart);
    Mockito.verify(deploymentStatRepository).countActiveIocs(todayMinus2DayStart);
  }

  @Test
  public void calculateIocDeploymentHistory() {
    LocalDate today = LocalDate.now();

    LocalDate todayMinus1Day = today.minusDays(1);

    Date todayMinus1DayStart =
        Date.from(todayMinus1Day.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());

    LocalDate todayMinus2Day = today.minusDays(2);

    Date todayMinus2DayStart =
        Date.from(todayMinus2Day.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
    Mockito.when(deploymentStatRepository.countSuccessfulIocDeployments(todayMinus1Day))
        .thenReturn(2L);
    Mockito.when(deploymentStatRepository.countSuccessfulIocDeployments(todayMinus2Day))
        .thenReturn(3L);

    List<ActiveIOCSForHistoryResponse> actual = createService().calculateIocDeploymentHistory(2);

    List<ActiveIOCSForHistoryResponse> expected =
        ImmutableList.of(
            new ActiveIOCSForHistoryResponse(3L, todayMinus2DayStart),
            new ActiveIOCSForHistoryResponse(2L, todayMinus1DayStart));
    assertEquals(expected, actual);

    Mockito.verify(deploymentStatRepository).countSuccessfulIocDeployments(todayMinus1Day);
    Mockito.verify(deploymentStatRepository).countSuccessfulIocDeployments(todayMinus2Day);
  }

  private StatisticsService createService() {
    return new StatisticsService(
        statusService, iocRepository, deploymentRepository, deploymentStatRepository, hostService);
  }
}
