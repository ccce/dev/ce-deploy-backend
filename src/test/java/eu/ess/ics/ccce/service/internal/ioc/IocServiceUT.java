/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.service.internal.ioc;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.common.collect.ImmutableList;
import eu.ess.ics.ccce.common.alert.IOCAlertGenerator;
import eu.ess.ics.ccce.common.conversion.PagingUtil;
import eu.ess.ics.ccce.exceptions.ConflictException;
import eu.ess.ics.ccce.exceptions.InputValidationException;
import eu.ess.ics.ccce.exceptions.LimitExceededException;
import eu.ess.ics.ccce.repository.api.IIocDeploymentRepository;
import eu.ess.ics.ccce.repository.api.IIocRepository;
import eu.ess.ics.ccce.repository.entity.AwxJobEntity;
import eu.ess.ics.ccce.repository.entity.IocDeploymentEntity;
import eu.ess.ics.ccce.repository.entity.IocEntity;
import eu.ess.ics.ccce.rest.model.BatchType;
import eu.ess.ics.ccce.rest.model.git.response.GitReference;
import eu.ess.ics.ccce.rest.model.host.response.HostInfoWithId;
import eu.ess.ics.ccce.rest.model.ioc.request.CreateIoc;
import eu.ess.ics.ccce.rest.model.ioc.request.IocOrder;
import eu.ess.ics.ccce.rest.model.ioc.response.Ioc;
import eu.ess.ics.ccce.rest.model.ioc.response.PagedIocResponse;
import eu.ess.ics.ccce.rest.model.job.JobAction;
import eu.ess.ics.ccce.rest.model.job.request.DeploymentRequest;
import eu.ess.ics.ccce.rest.model.job.request.UpdateAndDeployIoc;
import eu.ess.ics.ccce.rest.model.job.response.*;
import eu.ess.ics.ccce.service.external.gitlab.GitLabService;
import eu.ess.ics.ccce.service.external.naming.NamingService;
import eu.ess.ics.ccce.service.external.naming.model.NamingIocResponse;
import eu.ess.ics.ccce.service.internal.PagingLimitDto;
import eu.ess.ics.ccce.service.internal.deployment.DeploymentService;
import eu.ess.ics.ccce.service.internal.deployment.dto.AwxJobType;
import eu.ess.ics.ccce.service.internal.deployment.dto.DeploymentType;
import eu.ess.ics.ccce.service.internal.host.HostService;
import eu.ess.ics.ccce.service.internal.security.AuthorizationService;
import eu.ess.ics.ccce.service.internal.security.dto.UserDetails;
import eu.ess.ics.ccce.service.internal.status.StatusService;
import java.time.ZonedDateTime;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import org.gitlab.api.models.GitlabProject;
import org.gitlab4j.api.GitLabApiException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 */
@ExtendWith(MockitoExtension.class)
public class IocServiceUT {
  private static final Integer IOCS_ON_HOST_LIMIT = 4;
  private static final String TEST_HOST_FQDN = "test1.esss.lu.se";
  private static final String TEST_HOST_NAME = "test 1";
  private static final String TEST_HOST_NW_SCOPE = "nw scope";
  private static final String TEST_HOST_DESCRIPTION = "test 1 description";
  private static final String TEST_HOSTS_NETWORK = "test network";
  public static final String GIT_REFERENCE = "1.0.0";
  private final String GIT_PROJECT_1111_URL = "http://gitlab.test.com/test-project.git";
  private final Date TEST_DATE = new Date();
  private final ZonedDateTime TEST_ZONED_DATE_TIME = ZonedDateTime.now();
  private static final String HOST_ID = "MV90cnVl";

  @Mock private PagingUtil utils;
  @Mock private IIocRepository iocRepository;
  @Mock private IIocDeploymentRepository deploymentRepository;
  @Mock private DeploymentService deploymentService;
  @Mock private StatusService statusService;
  @Mock private AuthorizationService authorizationService;
  @Mock private MetadataService metadataService;
  @Mock private NamingService namingService;
  @Mock private GitLabService gitLabService;
  @Mock private IOCAlertGenerator IOCAlertGenerator;
  @Mock private HostService hostService;

  private final IocEntity ioc1;
  private final IocDeploymentEntity iocDeploymentEntity;
  private final IocDeploymentEntity batchDeploymentEntity;
  private final AwxJobEntity batchJob;
  private final Deployment deployment;
  private final Host host;
  private final NamingIocResponse nameDetail1;
  private final UserDetails userDetails;

  IocServiceUT() {
    userDetails = new UserDetails();
    userDetails.setUserName("testuser");
    userDetails.setToken("TOKEN");

    ioc1 = new IocEntity();
    ioc1.setId(1L);
    ioc1.setNamingUuid("");
    ioc1.setDeployedWithOldPlaybook(true);
    ioc1.setNamingName("TEST-IOC:1");
    ioc1.setGitProjectId(1234L);

    nameDetail1 =
        new NamingIocResponse(
            ioc1.getNamingUuid(), ioc1.getNamingName(), "IOC", "SC", "Test IOC", "ACTIVE");

    iocDeploymentEntity = new IocDeploymentEntity();
    iocDeploymentEntity.setIoc(ioc1);
    iocDeploymentEntity.setCreatedAt(TEST_DATE);
    iocDeploymentEntity.setDeploymentType(DeploymentType.DEPLOY.name());
    iocDeploymentEntity.setCreatedBy("test user");
    iocDeploymentEntity.setPending(false);
    iocDeploymentEntity.setNetBoxHostId(10L);
    iocDeploymentEntity.setVm(true);

    host = new Host("hostId", true, TEST_HOST_FQDN, TEST_HOST_NAME, "test  network");
    deployment =
        new Deployment(
            1L,
            "test user",
            TEST_ZONED_DATE_TIME,
            TEST_ZONED_DATE_TIME,
            TEST_ZONED_DATE_TIME,
            "name_id",
            "TEST-IOC:1",
            1234L,
            GIT_PROJECT_1111_URL,
            GIT_REFERENCE,
            "V1",
            host,
            JobStatus.RUNNING,
            1L,
            false,
            1L);
    ioc1.setLatestSuccessfulDeployment(iocDeploymentEntity);

    batchDeploymentEntity = new IocDeploymentEntity();
    batchDeploymentEntity.setIoc(ioc1);
    batchDeploymentEntity.setCreatedAt(TEST_DATE);
    batchDeploymentEntity.setDeploymentType(DeploymentType.DEPLOY.name());
    batchDeploymentEntity.setCreatedBy(userDetails.getUserName());
    batchDeploymentEntity.setPending(false);
    batchDeploymentEntity.setNetBoxHostId(1L);
    batchDeploymentEntity.setVm(true);
    batchDeploymentEntity.setSourceVersion(GIT_REFERENCE);
    batchDeploymentEntity.setGitProjectId(ioc1.getGitProjectId());
    batchDeploymentEntity.setHostFqdn(TEST_HOST_FQDN);
    batchDeploymentEntity.setHostNetwork(TEST_HOSTS_NETWORK);
    batchDeploymentEntity.setHostName(TEST_HOST_NAME);

    batchJob = new AwxJobEntity();
    batchJob.setJobType(AwxJobType.BATCH_DEPLOY.name());
    batchJob.setStatus(eu.ess.ics.ccce.service.external.awx.model.JobStatus.CREATED.name());
    batchJob.setDeployment(batchDeploymentEntity);
    batchJob.setId(1L);
  }

  @Test
  public void searchIocById() {
    Mockito.when(iocRepository.findById(1L)).thenReturn(ioc1);
    Mockito.when(deploymentService.findActiveDeploymentForIocDetails(iocDeploymentEntity, null))
        .thenReturn(deployment);
    Mockito.when(gitLabService.getNullableGitlabProject(ioc1.getGitProjectId())).thenReturn(null);
    Mockito.when(namingService.getIocByUuid(ioc1.getNamingUuid())).thenReturn(nameDetail1);

    Ioc iocById = createService().searchIocById(1L);

    assertEquals(1L, iocById.getId());
    assertEquals("", iocById.getNamingUuid());
    assertEquals("TEST-IOC:1", iocById.getNamingName());
    assertEquals("Test IOC", iocById.getDescription());
    assertNull(iocById.getGitProjectId());

    Mockito.verify(iocRepository).findById(1L);
    Mockito.verify(deploymentService).findActiveDeploymentForIocDetails(iocDeploymentEntity, null);
    Mockito.verify(namingService).getIocByUuid(ioc1.getNamingUuid());
    Mockito.verify(gitLabService).getNullableGitlabProject(ioc1.getGitProjectId());
  }

  @Test
  public void createNewIoc() throws GitLabApiException {
    Mockito.when(namingService.getIocByUuid(ioc1.getNamingUuid())).thenReturn(nameDetail1);
    CreateIoc iocToCreate = new CreateIoc(ioc1.getNamingUuid(), ioc1.getGitProjectId(), null);

    IocService iocService = Mockito.spy(createService());
    Mockito.doReturn(ioc1)
        .when(iocService)
        .createNewIOCEntity(Mockito.anyString(), any(), Mockito.anyLong());

    Mockito.when(gitLabService.getNullableGitlabProject(ioc1.getGitProjectId()))
        .thenReturn(
            new GitlabProject() {
              {
                setId(Math.toIntExact(ioc1.getGitProjectId()));
              }
            });

    Ioc ioc = iocService.createNewIoc(iocToCreate, userDetails);
    assertNotNull(ioc);
  }

  @Test
  public void doNotCreateIocRepoIfNameInUse() throws GitLabApiException {
    Mockito.when(iocRepository.countIocByNamingName(ioc1.getNamingName())).thenReturn(1L);
    Mockito.when(namingService.getIocByUuid(ioc1.getNamingUuid())).thenReturn(nameDetail1);
    CreateIoc iocToCreate = new CreateIoc(ioc1.getNamingUuid(), null, "e3-ioc-bar");

    IocService iocService = createService();
    assertThrows(ConflictException.class, () -> iocService.createNewIoc(iocToCreate, userDetails));
  }

  @Test
  public void findAll() {
    Mockito.when(utils.pageLimitConverter(0, 2, false)).thenReturn(new PagingLimitDto(0, 2, false));
    Mockito.when(
            iocRepository.findAll(null, "testuser", null, "test", IocOrder.ID, true, 0, 2, false))
        .thenReturn(ImmutableList.of(ioc1));
    Mockito.when(iocRepository.countForPaging(null, "testuser", null, "test")).thenReturn(1L);
    IocService iocService = createService();
    PagedIocResponse response =
        iocService.findAll(
            null, "testuser", null, null, null, null, "test", IocOrder.ID, true, 0, 2, false);
    assertNotNull(response);
    assertEquals(1, response.getTotalCount());
    assertEquals(0, response.getPageNumber());
    assertEquals(2, response.getLimit());
    assertEquals(1, response.getListSize());
    assertEquals(1, response.getIocList().size());
    assertEquals(1L, response.getIocList().get(0).getId());
  }

  @Test
  public void updateIoc() throws JsonProcessingException {
    UpdateAndDeployIoc iocToUpdate = new UpdateAndDeployIoc("2.0.0", "hostId");
    Mockito.when(iocRepository.findById(ioc1.getId())).thenReturn(ioc1);
    metadataService.parseMetadata(
        ArgumentMatchers.anyLong(), ArgumentMatchers.eq(iocToUpdate.getSourceVersion()));
    IocService iocService = createService();

    IocEntity ioc = iocService.searchAndCheckById(ioc1.getId());
    iocService.checkMetadata(ioc, iocToUpdate.getSourceVersion(), userDetails);
    assertNotNull(ioc);
  }

  @Test
  public void createBatchDeploymentFailOnDuplicatedIOcs() throws Exception {
    IocService iocService = createService();

    List<DeploymentRequest> deployments =
        List.of(
            new DeploymentRequest(1L, GIT_REFERENCE, HOST_ID),
            new DeploymentRequest(1L, GIT_REFERENCE, HOST_ID));

    try {
      iocService.createBatchDeployment(userDetails, deployments, BatchType.DEPLOY);
    } catch (InputValidationException e) {
      assertEquals("Invalid request: The request contains duplicated IOCs.", e.getMessage());
    }
  }

  @Test
  public void createBatchDeploymentFailOnHostCapacityIsFull() throws Exception {
    IocService iocService = createService();

    List<DeploymentRequest> deployments =
        List.of(
            new DeploymentRequest(1L, GIT_REFERENCE, HOST_ID),
            new DeploymentRequest(2L, GIT_REFERENCE, HOST_ID),
            new DeploymentRequest(3L, GIT_REFERENCE, HOST_ID),
            new DeploymentRequest(4L, GIT_REFERENCE, HOST_ID),
            new DeploymentRequest(5L, GIT_REFERENCE, HOST_ID));

    try {
      iocService.createBatchDeployment(userDetails, deployments, BatchType.DEPLOY);
    } catch (LimitExceededException e) {
      assertEquals(
          "Limit exceeded: Max number of IOCs per host is %d.".formatted(IOCS_ON_HOST_LIMIT),
          e.getMessage());
    }
  }

  @Test
  public void createBatchDeployment() throws Exception {
    IocService iocService = createService();
    List<DeploymentRequest> deployments =
        List.of(new DeploymentRequest(1L, GIT_REFERENCE, HOST_ID));
    HostInfoWithId hostInfoWithId =
        new HostInfoWithId(
            1L,
            TEST_HOST_FQDN,
            TEST_HOST_NAME,
            TEST_HOST_NW_SCOPE,
            TEST_HOST_DESCRIPTION,
            TEST_HOSTS_NETWORK,
            "VirtualMachine",
            true,
            HOST_ID,
            Collections.emptyList());

    List<GitReference> gitReferences = List.of(new GitReference(GIT_REFERENCE));
    Mockito.when(iocRepository.findById(1L)).thenReturn(ioc1);
    Mockito.when(gitLabService.tagsAndCommitIds(ioc1.getGitProjectId())).thenReturn(gitReferences);
    Mockito.when(deploymentService.getConcurrentHostIds(ioc1, hostInfoWithId.getHostIdentifier()))
        .thenReturn(Collections.emptySet());

    Mockito.when(
            deploymentService.prepareDeploymentForBatch(
                ioc1, hostInfoWithId, GIT_REFERENCE, userDetails.getUserName(), BatchType.DEPLOY))
        .thenReturn(batchDeploymentEntity);
    Mockito.when(
            deploymentService.storeBatchDeployment(
                List.of(batchDeploymentEntity), userDetails.getUserName(), BatchType.DEPLOY, false))
        .thenReturn(batchJob);
    DeploymentHostInfo deploymentHostInfo =
        new DeploymentHostInfo(HOST_ID, TEST_HOST_NAME, TEST_HOSTS_NETWORK, TEST_HOST_FQDN);
    JobEntry jobEntry =
        new JobEntry(
            ioc1.getNamingName(),
            ioc1.getId(),
            batchDeploymentEntity.getId(),
            ioc1.getGitProjectId(),
            GIT_REFERENCE,
            deploymentHostInfo);
    Mockito.when(hostService.getHostByNetBoxId(HOST_ID)).thenReturn(hostInfoWithId);
    Job expected =
        Job.createBatchJob(
            1L,
            null,
            null,
            null,
            null,
            JobAction.BATCH_DEPLOY,
            JobStatus.QUEUED,
            List.of(jobEntry));
    Job actual = iocService.createBatchDeployment(userDetails, deployments, BatchType.DEPLOY);
    assertEquals(expected, actual);
  }

  private IocService createService() {
    return new IocService(
        iocRepository,
        utils,
        deploymentService,
        statusService,
        authorizationService,
        metadataService,
        namingService,
        gitLabService,
        IOCAlertGenerator,
        hostService,
        deploymentRepository,
        IOCS_ON_HOST_LIMIT);
  }
}
