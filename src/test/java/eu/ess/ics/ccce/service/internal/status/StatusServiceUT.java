/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.service.internal.status;

import static org.junit.jupiter.api.Assertions.*;

import com.google.gson.Gson;
import eu.ess.ics.ccce.exceptions.RemoteServiceException;
import eu.ess.ics.ccce.service.external.common.HttpClientService;
import eu.ess.ics.ccce.service.external.common.HttpClientService.*;
import eu.ess.ics.ccce.service.external.prometheus.PrometheusService;
import eu.ess.ics.ccce.service.external.prometheus.model.PrometheusResponse;
import java.net.SocketTimeoutException;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import okhttp3.Headers;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.env.Environment;

/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 */
@SpringBootTest
public class StatusServiceUT {
  @Mock private HttpClientService httpClientService;
  @Mock private Environment env;

  @Test
  public void checkNonDeployedIocIsActive() throws SocketTimeoutException, RemoteServiceException {
    Mockito.when(env.getProperty(ArgumentMatchers.anyString())).thenReturn("http://test.host/");

    ServiceResponse<PrometheusResponse> httpResponse = iocUpOnHost();

    Mockito.when(
            httpClientService.executeGetRequest(
                ArgumentMatchers.any(Headers.class),
                ArgumentMatchers.anyString(),
                ArgumentMatchers.eq(PrometheusResponse.class)))
        .thenReturn(httpResponse);
    StatusService service = createService();
    Boolean iocStatus = service.checkNonDeployedIocIsActive("FAKE:IOC");
    assertTrue(iocStatus);
  }

  @Test
  public void fetchIOCIsRunningWithOldPlaybook()
      throws SocketTimeoutException, RemoteServiceException {
    Mockito.when(env.getProperty(ArgumentMatchers.anyString())).thenReturn("http://test.host/");

    ServiceResponse<PrometheusResponse> httpResponse = iocUpOnHostDeployedWithOldPlaybook();
    Mockito.when(
            httpClientService.executeGetRequest(
                ArgumentMatchers.any(Headers.class),
                ArgumentMatchers.anyString(),
                ArgumentMatchers.eq(PrometheusResponse.class)))
        .thenReturn(httpResponse);
    StatusService service = createService();
    List<String> actual = service.fetchWhereIOCIsRunning("FAKE:IOC");
    assertEquals(List.of("some.fake.host"), actual);
  }

  @Test
  public void fetchIOCIsRunningWithNewPlaybook()
      throws SocketTimeoutException, RemoteServiceException {
    Mockito.when(env.getProperty(ArgumentMatchers.anyString())).thenReturn("http://test.host/");

    ServiceResponse<PrometheusResponse> httpResponse = iocUpOnHost();
    Mockito.when(
            httpClientService.executeGetRequest(
                ArgumentMatchers.any(Headers.class),
                ArgumentMatchers.anyString(),
                ArgumentMatchers.eq(PrometheusResponse.class)))
        .thenReturn(httpResponse);
    StatusService service = createService();
    List<String> actual = service.fetchWhereIOCIsRunning("FAKE:IOC");
    assertEquals(List.of("some.fake.host"), actual);
  }

  @Test
  void testHostIsActive() throws RemoteServiceException, SocketTimeoutException {

    Mockito.when(env.getProperty(ArgumentMatchers.anyString())).thenReturn("http://test.host/");

    ServiceResponse<PrometheusResponse> httpResponse = hostUp();

    Mockito.when(
            httpClientService.executeGetRequest(
                ArgumentMatchers.any(Headers.class),
                ArgumentMatchers.anyString(),
                ArgumentMatchers.eq(PrometheusResponse.class)))
        .thenReturn(httpResponse);

    StatusService service = createService();
    Boolean hostStatus = service.checkHostIsActive("other.fake.host");
    assertTrue(hostStatus);
  }

  @Test
  void testHostStateUnknown() throws RemoteServiceException, SocketTimeoutException {

    Mockito.when(env.getProperty(ArgumentMatchers.anyString())).thenReturn("http://test.host/");

    ServiceResponse<PrometheusResponse> httpResponse = hostStateUnknown();

    Mockito.when(
            httpClientService.executeGetRequest(
                ArgumentMatchers.any(Headers.class),
                ArgumentMatchers.anyString(),
                ArgumentMatchers.eq(PrometheusResponse.class)))
        .thenReturn(httpResponse);

    StatusService service = createService();
    Boolean hostStatus = service.checkHostIsActive("testHost");
    assertNull(hostStatus);
  }

  @Test
  void testHostDown() throws RemoteServiceException, SocketTimeoutException {

    Mockito.when(env.getProperty(ArgumentMatchers.anyString())).thenReturn("http://test.host/");

    ServiceResponse<PrometheusResponse> httpResponse = hostDown();

    Mockito.when(
            httpClientService.executeGetRequest(
                ArgumentMatchers.any(Headers.class),
                ArgumentMatchers.anyString(),
                ArgumentMatchers.eq(PrometheusResponse.class)))
        .thenReturn(httpResponse);

    StatusService service = createService();
    Boolean hostStatus = service.checkHostIsActive("other.fake.host");
    assertFalse(hostStatus);
  }

  @Test
  void testIocUpOnHost() throws RemoteServiceException, SocketTimeoutException {

    Mockito.when(env.getProperty(ArgumentMatchers.anyString())).thenReturn("http://test.host/");

    ServiceResponse<PrometheusResponse> httpResponse = iocUpOnHost();

    Mockito.when(
            httpClientService.executeGetRequest(
                ArgumentMatchers.any(Headers.class),
                ArgumentMatchers.anyString(),
                ArgumentMatchers.eq(PrometheusResponse.class)))
        .thenReturn(httpResponse);

    StatusService service = createService();
    Boolean hostStatus =
        service.checkDeployedIocIsActive("FAKE:IOC", ZonedDateTime.now(), "some.fake.host");
    assertTrue(hostStatus);
  }

  @Test
  void testIocDownOnHost() throws RemoteServiceException, SocketTimeoutException {

    Mockito.when(env.getProperty(ArgumentMatchers.anyString())).thenReturn("http://test.host/");

    ServiceResponse<PrometheusResponse> httpResponse = iocDownOnHost();

    Mockito.when(
            httpClientService.executeGetRequest(
                ArgumentMatchers.any(Headers.class),
                ArgumentMatchers.anyString(),
                ArgumentMatchers.eq(PrometheusResponse.class)))
        .thenReturn(httpResponse);

    StatusService service = createService();
    Boolean hostStatus =
        service.checkDeployedIocIsActive("FAKE:IOC", ZonedDateTime.now(), "some.fake.host");
    assertFalse(hostStatus);
  }

  @Test
  void testIocUp() throws RemoteServiceException, SocketTimeoutException {

    Mockito.when(env.getProperty(ArgumentMatchers.anyString())).thenReturn("http://test.host/");

    ServiceResponse<PrometheusResponse> httpResponse = iocUpOnHost();

    Mockito.when(
            httpClientService.executeGetRequest(
                ArgumentMatchers.any(Headers.class),
                ArgumentMatchers.anyString(),
                ArgumentMatchers.eq(PrometheusResponse.class)))
        .thenReturn(httpResponse);

    StatusService service = createService();
    Boolean hostStatus =
        service.checkDeployedIocIsActive("FAKE:IOC", ZonedDateTime.now(), "some.fake.host");
    assertTrue(hostStatus);
  }

  @Test
  void testIocUpWithOldPlaybook() throws RemoteServiceException, SocketTimeoutException {

    Mockito.when(env.getProperty(ArgumentMatchers.anyString())).thenReturn("http://test.host/");

    ServiceResponse<PrometheusResponse> httpResponse = iocUpOnHostDeployedWithOldPlaybook();

    Mockito.when(
            httpClientService.executeGetRequest(
                ArgumentMatchers.any(Headers.class),
                ArgumentMatchers.anyString(),
                ArgumentMatchers.eq(PrometheusResponse.class)))
        .thenReturn(httpResponse);

    StatusService service = createService();
    ZonedDateTime veryOldDeploymentTime =
        ZonedDateTime.of(2000, 1, 1, 0, 0, 0, 0, ZoneId.systemDefault());
    Boolean hostStatus =
        service.checkDeployedIocIsActive("FAKE:IOC", veryOldDeploymentTime, "some.fake.host");
    assertTrue(hostStatus);
  }

  private ServiceResponse<PrometheusResponse> iocUpOnHostDeployedWithOldPlaybook() {
    Gson g = new Gson();
    String jsonString =
        """
        {
          "status": "success",
          "data": {
            "resultType": "vector",
            "result": [
              {
                "metric": {
                  "__name__": "node_systemd_unit_state",
                  "csentry_device_type": "VirtualMachine",
                  "csentry_domain": "cslab.esss.lu.se",
                  "csentry_group_e3_exporter": "1",
                  "csentry_group_iocs": "1",
                  "csentry_group_labnetworks": "1",
                  "csentry_group_servers": "1",
                  "csentry_group_utgard": "1",
                  "csentry_group_utgard_dev": "1",
                  "csentry_group_virtualmachine": "1",
                  "csentry_is_ioc": "1",
                  "csentry_vlan": "Utgard_Dev",
                  "csentry_vlan_id": "3974",
                  "instance": "some.fake.host",
                  "job": "node",
                  "name": "ioc@FAKE_IOC.service",
                  "state": "active",
                  "type": "forking"
                },
                "value": [
                  1615970157.5,
                  "1"
                ]
              }
            ]
          }
        }\
        """;
    PrometheusResponse p = g.fromJson(jsonString, PrometheusResponse.class);

    return new ServiceResponse<>(p, 200, null);
  }

  @Test
  void testIocDown() throws RemoteServiceException, SocketTimeoutException {

    Mockito.when(env.getProperty(ArgumentMatchers.anyString())).thenReturn("http://test.host/");

    ServiceResponse<PrometheusResponse> httpResponse = iocDownOnHost();

    Mockito.when(
            httpClientService.executeGetRequest(
                ArgumentMatchers.any(Headers.class),
                ArgumentMatchers.anyString(),
                ArgumentMatchers.eq(PrometheusResponse.class)))
        .thenReturn(httpResponse);

    StatusService service = createService();
    Boolean hostStatus =
        service.checkDeployedIocIsActive("FAKE:IOC", ZonedDateTime.now(), "some.fake.host");
    assertFalse(hostStatus);
  }

  @Test
  void testHostList() throws RemoteServiceException, SocketTimeoutException {

    Mockito.when(env.getProperty(ArgumentMatchers.anyString())).thenReturn("http://test.host/");

    ServiceResponse<PrometheusResponse> httpResponse = hostList();

    Mockito.when(
            httpClientService.executeGetRequest(
                ArgumentMatchers.any(Headers.class),
                ArgumentMatchers.anyString(),
                ArgumentMatchers.eq(PrometheusResponse.class)))
        .thenReturn(httpResponse);

    StatusService service = createService();
    List<String> hostList = service.listIocHosts();
    assertFalse(hostList.isEmpty());
    assertEquals(4, hostList.size());
  }

  @Test
  void testEmptyHostList() throws RemoteServiceException, SocketTimeoutException {

    Mockito.when(env.getProperty(ArgumentMatchers.anyString())).thenReturn("http://test.host/");

    ServiceResponse<PrometheusResponse> httpResponse = emptyHostList();

    Mockito.when(
            httpClientService.executeGetRequest(
                ArgumentMatchers.any(Headers.class),
                ArgumentMatchers.anyString(),
                ArgumentMatchers.eq(PrometheusResponse.class)))
        .thenReturn(httpResponse);

    StatusService service = createService();
    List<String> hostList = service.listIocHosts();
    assertTrue(hostList.isEmpty());
  }

  private ServiceResponse<PrometheusResponse> hostUp() {
    Gson g = new Gson();
    String jsonString =
        """
        {
          "status": "success",
          "data": {
            "resultType": "vector",
            "result": [
              {
                "metric": {
                  "__name__": "up",
                  "csentry_device_type": "VirtualMachine",
                  "csentry_domain": "cslab.esss.lu.se",
                  "csentry_group_cslab_generallab": "1",
                  "csentry_group_e3_exporter": "1",
                  "csentry_group_iocs": "1",
                  "csentry_group_labnetworks": "1",
                  "csentry_group_servers": "1",
                  "csentry_group_virtualmachine": "1",
                  "csentry_is_ioc": "1",
                  "csentry_vlan": "CSLab_GeneralLab",
                  "csentry_vlan_id": "3805",
                  "instance": "other.fake.host",
                  "job": "node"
                },
                "value": [
                  1614615642.956,
                  "1"
                ]
              }
            ]
          }
        }\
        """;
    PrometheusResponse p = g.fromJson(jsonString, PrometheusResponse.class);

    return new ServiceResponse<>(p, 200, null);
  }

  private ServiceResponse<PrometheusResponse> hostDown() {
    Gson g = new Gson();
    String jsonString =
        """
        {
          "status": "success",
          "data": {
            "resultType": "vector",
            "result": [
              {
                "metric": {
                  "__name__": "up",
                  "csentry_device_type": "VirtualMachine",
                  "csentry_domain": "cslab.esss.lu.se",
                  "csentry_group_cslab_generallab": "1",
                  "csentry_group_e3_exporter": "1",
                  "csentry_group_iocs": "1",
                  "csentry_group_labnetworks": "1",
                  "csentry_group_servers": "1",
                  "csentry_group_virtualmachine": "1",
                  "csentry_is_ioc": "1",
                  "csentry_vlan": "CSLab_GeneralLab",
                  "csentry_vlan_id": "3805",
                  "instance": "other.fake.host",
                  "job": "node"
                },
                "value": [
                  1614615642.956,
                  "0"
                ]
              }
            ]
          }
        }\
        """;
    PrometheusResponse p = g.fromJson(jsonString, PrometheusResponse.class);

    return new ServiceResponse<>(p, 200, null);
  }

  private ServiceResponse<PrometheusResponse> hostStateUnknown() {
    Gson g = new Gson();
    String jsonString =
        """
        {
          "status": "success",
          "data": {
            "resultType": "vector",
            "result": []
          }
        }\
        """;
    PrometheusResponse p = g.fromJson(jsonString, PrometheusResponse.class);

    return new ServiceResponse<>(p, 200, null);
  }

  private ServiceResponse<PrometheusResponse> iocUpOnHost() {
    Gson g = new Gson();
    String jsonString =
        """
        {
          "status": "success",
          "data": {
            "resultType": "vector",
            "result": [
              {
                "metric": {
                  "__name__": "node_systemd_unit_state",
                  "csentry_device_type": "VirtualMachine",
                  "csentry_domain": "cslab.esss.lu.se",
                  "csentry_group_e3_exporter": "1",
                  "csentry_group_iocs": "1",
                  "csentry_group_labnetworks": "1",
                  "csentry_group_servers": "1",
                  "csentry_group_utgard": "1",
                  "csentry_group_utgard_dev": "1",
                  "csentry_group_virtualmachine": "1",
                  "csentry_is_ioc": "1",
                  "csentry_vlan": "Utgard_Dev",
                  "csentry_vlan_id": "3974",
                  "instance": "some.fake.host",
                  "job": "node",
                  "name": "ioc-FAKE_IOC.service",
                  "state": "active",
                  "type": "forking"
                },
                "value": [
                  1615970157.5,
                  "1"
                ]
              }
            ]
          }
        }\
        """;
    PrometheusResponse p = g.fromJson(jsonString, PrometheusResponse.class);

    return new ServiceResponse<>(p, 200, null);
  }

  private ServiceResponse<PrometheusResponse> iocDownOnHost() {
    Gson g = new Gson();
    String jsonString =
        """
        {
          "status": "success",
          "data": {
            "resultType": "vector",
            "result": [
              {
                "metric": {
                  "__name__": "node_systemd_unit_state",
                  "csentry_device_type": "VirtualMachine",
                  "csentry_domain": "cslab.esss.lu.se",
                  "csentry_group_e3_exporter": "1",
                  "csentry_group_iocs": "1",
                  "csentry_group_labnetworks": "1",
                  "csentry_group_servers": "1",
                  "csentry_group_utgard": "1",
                  "csentry_group_utgard_dev": "1",
                  "csentry_group_virtualmachine": "1",
                  "csentry_is_ioc": "1",
                  "csentry_vlan": "Utgard_Dev",
                  "csentry_vlan_id": "3974",
                  "instance": "some.fake.host",
                  "job": "node",
                  "name": "ioc-FAKE_IOC.service",
                  "state": "active",
                  "type": "forking"
                },
                "value": [
                  1615970157.5,
                  "0"
                ]
              }
            ]
          }
        }\
        """;
    PrometheusResponse p = g.fromJson(jsonString, PrometheusResponse.class);

    return new ServiceResponse<>(p, 200, null);
  }

  private ServiceResponse<PrometheusResponse> hostList() {
    Gson g = new Gson();
    String jsonString =
        """
        {
          "status": "success",
          "data": {
            "resultType": "vector",
            "result": [
              {
                "metric": {
                  "__name__": "up",
                  "csentry_device_type": "MTCA-AMC",
                  "csentry_domain": "cslab.esss.lu.se",
                  "csentry_group_beam_diagnostics_machines": "1",
                  "csentry_group_iocs": "1",
                  "csentry_group_labnetworks": "1",
                  "csentry_group_mtca_amc": "1",
                  "csentry_is_ioc": "1",
                  "csentry_vlan": "CSLab_BeamDiag",
                  "csentry_vlan_id": "3880",
                  "instance": "bd-cpu19.cslab.esss.lu.se",
                  "job": "node"
                },
                "value": [
                  1620819560.746,
                  "0"
                ]
              },
              {
                "metric": {
                  "__name__": "up",
                  "csentry_device_type": "MTCA-AMC",
                  "csentry_domain": "cslab.esss.lu.se",
                  "csentry_group_cslab_generallab": "1",
                  "csentry_group_deploy_cct": "1",
                  "csentry_group_iocs": "1",
                  "csentry_group_labnetworks": "1",
                  "csentry_group_mtca_amc": "1",
                  "csentry_is_ioc": "1",
                  "csentry_vlan": "CSLab_GeneralLab",
                  "csentry_vlan_id": "3805",
                  "instance": "ccpu-35564-007.cslab.esss.lu.se",
                  "job": "node"
                },
                "value": [
                  1620819560.746,
                  "0"
                ]
              },
              {
                "metric": {
                  "__name__": "up",
                  "csentry_device_type": "MTCA-AMC",
                  "csentry_domain": "cslab.esss.lu.se",
                  "csentry_group_cslab_generallab": "1",
                  "csentry_group_iocs": "1",
                  "csentry_group_labnetworks": "1",
                  "csentry_group_mtca_amc": "1",
                  "csentry_is_ioc": "1",
                  "csentry_vlan": "CSLab_GeneralLab",
                  "csentry_vlan_id": "3805",
                  "instance": "dtl-llrf5-cpu-ioc.cslab.esss.lu.se",
                  "job": "node"
                },
                "value": [
                  1620819560.746,
                  "0"
                ]
              },
              {
                "metric": {
                  "__name__": "up",
                  "csentry_device_type": "MTCA-AMC",
                  "csentry_domain": "cslab.esss.lu.se",
                  "csentry_group_iocs": "1",
                  "csentry_group_labnetworks": "1",
                  "csentry_group_mtca_amc": "1",
                  "csentry_is_ioc": "1",
                  "csentry_vlan": "CSLab_BeamDiag",
                  "csentry_vlan_id": "3880",
                  "instance": "bcm01-cpu-201.cslab.esss.lu.se",
                  "job": "node"
                },
                "value": [
                  1620819560.746,
                  "1"
                ]
              }\
            ]
          }
        }\
        """;
    PrometheusResponse p = g.fromJson(jsonString, PrometheusResponse.class);

    return new ServiceResponse<>(p, 200, null);
  }

  private ServiceResponse<PrometheusResponse> emptyHostList() {
    Gson g = new Gson();
    String jsonString =
        """
        {
          "status": "success",
          "data": {
            "resultType": "vector",
            "result": []
          }
        }\
        """;
    PrometheusResponse p = g.fromJson(jsonString, PrometheusResponse.class);

    return new ServiceResponse<>(p, 200, null);
  }

  private StatusService createService() {
    PrometheusService prometheusService = new PrometheusService(env, httpClientService);
    ZonedDateTime playbookTransitionTime =
        ZonedDateTime.parse(
            "2024-04-12T08:29:46.000+0200",
            DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSZ"));

    return new StatusService(prometheusService, playbookTransitionTime);
  }
}
