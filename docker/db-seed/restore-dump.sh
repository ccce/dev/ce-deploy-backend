#!/bin/bash
set -e

until pg_isready; do
  sleep 1
done

psql -v ON_ERROR_STOP=1 -d "$POSTGRES_DB" < /docker-entrypoint-initdb.d/init.dump
